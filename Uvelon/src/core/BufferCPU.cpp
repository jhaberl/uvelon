#include "core/BufferCPU.hpp"

#include "rendering/Primitives.hpp"
#include "rendering/RenderData.hpp"
#include "rendering/Renderer.hpp"

namespace Uvelon {

BufferH BufferCPU::toGPU(VkBufferUsageFlags usage) {
  return Renderer::getInstance().getRenderData().allocateBuffer(
      raw_buffer.data(), raw_buffer.size(), usage);
}

BufferCPU::BufferCPU(const BufferCPU& o) : component_type(o.component_type) {
  raw_buffer.resize(o.raw_buffer.size());
  memcpy(raw_buffer.data(), o.raw_buffer.data(), o.raw_buffer.size());
}

}  // namespace Uvelon