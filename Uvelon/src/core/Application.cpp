#include "core/Application.hpp"

#include <chrono>
#include <iostream>

#include "backends/imgui_impl_sdl3.h"
#include "backends/imgui_impl_vulkan.h"
#include "core/Input.hpp"
#include "scene/Node.hpp"
#include "scene/Scene.hpp"
#include "scene/SceneEnvironment.hpp"
#include "utils/Instrumentation.hpp"
#include "utils/Logger.hpp"

namespace Uvelon {

Application::Application(Config& config)
    : config_(config),
      window_(config.title, config.width, config.height, config.maximized,
              config.fullscreen) {
  setlocale(LC_NUMERIC, "");
  UVELON_LOG_INFO("Using Uvelon " + std::to_string(UVELON_VERSION_MAJOR) + "." +
                  std::to_string(UVELON_VERSION_MINOR) + "." +
                  std::to_string(UVELON_VERSION_REVISION));

  scene_ = std::make_shared<Scene>();
}

Application::~Application() { UVELON_LOG_INFO("Exited gracefully"); }

void Application::run() {
  window_.open();

  // Init renderer
  config_.renderer_config.window = &window_;
  auto& renderer = Renderer::getInstance();
  renderer.init(config_.renderer_config);

  // Trigger application init
  init();

  SDL_Event event;
  bool quit{false};
  auto& imgui_io = ImGui::GetIO();

  std::unique_ptr<DrawData> draw_data = std::make_unique<DrawData>();

  auto time_last = std::chrono::steady_clock::now();
  while (!quit) {
    auto time_start = std::chrono::steady_clock::now();
    float delta = std::chrono::duration_cast<std::chrono::microseconds>(
                      time_start - time_last)
                      .count() /
                  1000.0f;
    time_last = time_start;

    // Handle SDL events
    Input::startFrame();
    while (SDL_PollEvent(&event) != 0) {
      if (event.type == SDL_EVENT_QUIT) quit = true;

      window_.handleEvent(event);
      ImGui_ImplSDL3_ProcessEvent(&event);
      Input::handleEvent(event, imgui_io.WantCaptureMouse,
                         imgui_io.WantCaptureKeyboard);
    }

    scene_->preUpdate(delta);

    // Trigger application update
    update(delta);

    // ImGui UI
    ImGui_ImplVulkan_NewFrame();
    ImGui_ImplSDL3_NewFrame();
    ImGui::NewFrame();
    drawUI(delta);
    ImGui::Render();

    scene_->update(delta);

    draw_data->render_objects.clear();
    draw_data->light_primitives.clear();
    if (scene_->getEnvironment())
      draw_data->environment_ibl = scene_->getEnvironment()->getEnvironment();
    else
      draw_data->environment_ibl = EnvironmentH();
    scene_->addRenderObjects(draw_data->render_objects);
    scene_->addLightPrimitives(draw_data->light_primitives);
    std::shared_ptr<Node> camera_node;
    if (scene_->getActiveCamera(camera_node)) {
      draw_data->view_matrix = camera_node->getCamera()->computeViewMatrix();
      draw_data->projection_matrix =
          camera_node->getCamera()->getProjectionMatrix();
    }
    renderer.draw(*draw_data);
  }

  // Ensure all the renderobjects with its handles are destroyed to avoid them
  // trying to access the containers we delete once we shut down the renderer
  draw_data.reset();
  scene_.reset();

  renderer.shutdown();
}

}  // namespace Uvelon