#include "core/Input.hpp"

#include "utils/Logger.hpp"

namespace Uvelon {

std::vector<SDL_Scancode> Input::down_keys_;
float Input::mouse_x_{0.0f};
float Input::mouse_y_{0.0f};
std::vector<uint8_t> Input::down_mouse_buttons_;
float Input::scroll_x_{0.0f};
float Input::scroll_y_{0.0f};

void Input::startFrame() {
  scroll_x_ = 0.0f;
  scroll_y_ = 0.0f;
}

void Input::handleEvent(SDL_Event& event, bool ignore_mouse,
                        bool ignore_keyboard) {
  switch (event.type) {
    case SDL_EVENT_KEY_DOWN: {
      if (ignore_keyboard) return;

      if (!event.key.repeat) down_keys_.push_back(event.key.scancode);
      break;
    }
    case SDL_EVENT_KEY_UP: {
      if (ignore_keyboard) return;

      auto it = down_keys_.begin();
      while (it != down_keys_.end()) {
        if (*it == event.key.scancode) {
          down_keys_.erase(it);
          break;
        }
        it++;
      }
      break;
    }
    case SDL_EVENT_MOUSE_MOTION: {
      if (ignore_mouse) return;

      mouse_x_ = event.motion.x;
      mouse_y_ = event.motion.y;
      break;
    }
    case SDL_EVENT_MOUSE_BUTTON_DOWN: {
      if (ignore_mouse) return;

      down_mouse_buttons_.push_back(event.button.button);
      break;
    }
    case SDL_EVENT_MOUSE_BUTTON_UP: {
      if (ignore_mouse) return;

      auto it = down_mouse_buttons_.begin();
      while (it != down_mouse_buttons_.end()) {
        if (*it == event.button.button) {
          down_mouse_buttons_.erase(it);
          break;
        }
        it++;
      }
      break;
    }
    case SDL_EVENT_MOUSE_WHEEL: {
      if (ignore_mouse) return;

      scroll_x_ = event.wheel.x;
      scroll_y_ = event.wheel.y;
      break;
    }
  }
}

bool Input::isKeyDown(SDL_Scancode code) {
  auto it = down_keys_.begin();
  while (it != down_keys_.end()) {
    if (*it == code) return true;
    it++;
  }
  return false;
}

void Input::getMousePosition(float& x, float& y) {
  x = mouse_x_;
  y = mouse_y_;
}

bool Input::isMouseButtonDown(uint8_t button) {
  auto it = down_mouse_buttons_.begin();
  while (it != down_mouse_buttons_.end()) {
    if (*it == button) return true;
    it++;
  }
  return false;
}

void Input::getScroll(float& x, float& y) {
  x = scroll_x_;
  y = scroll_y_;
}

}  // namespace Uvelon