#include "core/CVarManager.hpp"

#include <imgui.h>

#include <unordered_map>
#include <variant>
#include <vector>

#include "utils/Logger.hpp"

namespace Uvelon {

typedef std::variant<int32_t, bool, double, std::string> CVarValue;
struct CVarDrawVisitor {
  std::string name_;
  std::string description_;
  CVarDrawVisitor(std::string name, std::string description)
      : name_(name), description_(description) {}
  void operator()(int32_t& i) const { ImGui::InputInt(name_.c_str(), &i); }
  void operator()(bool& b) const { ImGui::Checkbox(name_.c_str(), &b); }
  void operator()(double& d) const { ImGui::InputDouble(name_.c_str(), &d); }
  void operator()(std::string& s) const { ImGui::Text("%s", s.c_str()); }
};

struct CVarInternal {
  CVarInternal() = default;
  CVarInternal(std::string name, bool allow_draw, std::string description,
               CVarValue value);
  std::string getName() const { return name_; }
  std::string getDescription() const { return description_; }
  template <typename T>
  T& getValue() {
    return std::get<T>(value_);
  }
  void draw();

 private:
  std::string name_;
  std::string description_;
  bool allow_draw_;
  CVarValue value_;
};

CVarInternal::CVarInternal(std::string name, bool allow_draw,
                           std::string description, CVarValue value)
    : name_(name),
      allow_draw_(allow_draw),
      description_(description),
      value_(value) {}

void CVarInternal::draw() {
  if (allow_draw_) std::visit(CVarDrawVisitor(name_, description_), value_);
}

bool CVar::isValid() { return var_; }
std::string CVar::getName() { return var_->getName(); }
std::string CVar::getDescription() { return var_->getDescription(); }
template <typename T>
T& CVar::getValue() {
  assert(var_);
  return var_->getValue<T>();
}

template int32_t& CVar::getValue<int32_t>();
template bool& CVar::getValue<bool>();
template double& CVar::getValue<double>();
template std::string& CVar::getValue<std::string>();
// ----
class CVarManagerImpl : public CVarManager {
 protected:
  virtual void setVarInternal(CVarInternal&& var);
  virtual CVar getVarInternal(std::string name);
  virtual void drawInternal();

 private:
  std::unordered_map<std::string, CVarInternal> variables_;
};

void CVarManagerImpl::setVarInternal(CVarInternal&& var) {
  // TODO: If the new description is null, keep the previous description
  variables_.insert_or_assign(var.getName(), var);
}

CVar CVarManagerImpl::getVarInternal(std::string name) {
  if (auto result = variables_.find(name); result != variables_.end()) {
    return {&std::get<1>(*result)};
  }
  return {nullptr};
}

void CVarManagerImpl::drawInternal() {
  if (ImGui::Begin("Variables", nullptr, ImGuiWindowFlags_AlwaysAutoResize)) {
    auto it = variables_.begin();
    while (it != variables_.end()) {
      std::get<1>(*it).draw();
      it++;
    }
  }
  ImGui::End();
}

// ---------

CVarManager& CVarManager::getInstance() {
  static CVarManagerImpl impl_instance;
  return impl_instance;
};

template <typename T>
void CVarManager::setVar(std::string name, T value, bool allow_draw,
                         std::string description) {
  getInstance().setVarInternal({name, allow_draw, description, {value}});
}

CVar CVarManager::getVar(std::string name) {
  return getInstance().getVarInternal(name);
}

template <typename T>
CVar CVarManager::getVar(std::string name, T default_value) {
  auto result = getInstance().getVarInternal(name);
  if (!result.isValid()) {
    setVar(name, default_value);
    result = getInstance().getVarInternal(name);
  }
  return result;
}

void CVarManager::draw() { return getInstance().drawInternal(); }

template void CVarManager::setVar(std::string, int32_t, bool, std::string);
template void CVarManager::setVar(std::string, bool, bool, std::string);
template void CVarManager::setVar(std::string, double, bool, std::string);
template void CVarManager::setVar(std::string, std::string, bool, std::string);
template <>
void CVarManager::setVar(std::string name, const char* value, bool allow_draw,
                         std::string description) {
  setVar(name, std::string(value), allow_draw, description);
}

template CVar CVarManager::getVar(std::string, int32_t);
template CVar CVarManager::getVar(std::string, bool);
template CVar CVarManager::getVar(std::string, double);
template CVar CVarManager::getVar(std::string, std::string);

}  // namespace Uvelon
