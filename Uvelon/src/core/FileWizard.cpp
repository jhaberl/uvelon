#include "core/FileWizard.hpp"

#include <cassert>

#include "utils/Logger.hpp"

#ifdef __APPLE__
#include <CoreFoundation/CoreFoundation.h>
#endif

#ifdef HAVE_NFD
#include "nfd.hpp"
#endif

namespace Uvelon {

FileWizard::FileWizard() {
#ifdef HAVE_NFD
  nfdresult_t nfd_init = NFD_Init();
  if (nfd_init == NFD_OKAY) {
    UVELON_LOG_INFO("NFD initialized");
  } else {
    UVELON_LOG_WARNING("NFD initialization failed: " +
                       std::string(NFD_GetError()));
  }
#endif

#ifdef __APPLE__
  // Resources
  // Can cache this, moving bundles during execution is undefined
  // TODO: Better error handling?
  CFBundleRef bundle_ref = CFBundleGetMainBundle();
  CFURLRef bundle_url = CFBundleCopyBundleURL(bundle_ref);
  CFStringRef bundle_path_ref = CFURLCopyPath(bundle_url);
  const char* bundle_path_cstr =
      CFStringGetCStringPtr(bundle_path_ref, kCFStringEncodingUTF8);
  std::filesystem::path bundle_path = bundle_path_cstr;

  CFURLRef res_url = CFBundleCopyResourcesDirectoryURL(bundle_ref);
  CFStringRef out_path_ref = CFURLCopyPath(res_url);
  const char* out_path_cstr =
      CFStringGetCStringPtr(out_path_ref, kCFStringEncodingUTF8);
  std::filesystem::path resources_path = out_path_cstr;
  CFRelease(out_path_ref);
  CFRelease(res_url);
  CFRelease(bundle_path_ref);
  CFRelease(bundle_url);
  apple_resources_path_ = bundle_path.append(resources_path.string());

#if TARGET_OS_IPHONE
  // Workspace = sandbox root
  CFURLRef home_url = CFCopyHomeDirectoryURL();
  CFStringRef home_path_ref = CFURLCopyPath(home_url);
  const char* home_path_cstr =
      CFStringGetCStringPtr(home_path_ref, kCFStringEncodingUTF8);
  apple_workspace_path_ = home_path_cstr;
  CFRelease(home_path_ref);
  CFRelease(home_url);

  // Create directory symlink to read resources via workspace
  std::filesystem::path symlink_path =
      std::filesystem::path(apple_workspace_path_).append("tmp/Bundle");
  if (std::filesystem::is_symlink(symlink_path))
    std::filesystem::remove(symlink_path);
  std::filesystem::create_directory_symlink(apple_resources_path_,
                                            symlink_path);
#else
  apple_workspace_path_ = std::filesystem::absolute(apple_resources_path_)
                              .parent_path()
                              .parent_path()
                              .parent_path()
                              .parent_path()
                              .parent_path()
                              .append("workspace/");
#endif
#endif
}

FileWizard::~FileWizard() {
#ifdef HAVE_NFD
  NFD_Quit();
#endif
}

std::filesystem::path FileWizard::getPath(StorageType storage_type,
                                          const std::string& relative_path) {
  switch (storage_type) {
    case RESOURCES: {
#ifdef WIN32
      return std::filesystem::path("../resources/").append(relative_path);
#elif defined(__APPLE__)
      return std::filesystem::path(apple_resources_path_).append(relative_path);
#endif
    }
    case WORKSPACE: {
#ifdef WIN32
      return std::filesystem::path("../workspace/").append(relative_path);
#elif defined(__APPLE__)
      return std::filesystem::path(apple_workspace_path_).append(relative_path);
#endif
    }
    default:
      return relative_path;
  }
  return "";
}

bool FileWizard::pickFolder(std::filesystem::path& result,
                            const std::string& default_path) {
#ifdef HAVE_NFD
  bool use_default_path =
      !default_path.empty() && std::filesystem::exists(default_path);
  nfdchar_t* out_path;
  nfdresult_t nfd_result = NFD_PickFolderU8(
      &out_path, use_default_path
                     ? std::filesystem::absolute(default_path).string().c_str()
                     : nullptr);
  if (nfd_result == NFD_OKAY) {
    result = std::filesystem::path(out_path);
    NFD_FreePathU8(out_path);
    return true;
  } else if (nfd_result == NFD_ERROR) {
    UVELON_LOG_WARNING("NFD failed: " + std::string(NFD_GetError()));
  }
#else
  UVELON_LOG_ERROR("Native file dialog not available, use FileDialog!");
#endif
  return false;
}

bool FileWizard::openDialog(std::filesystem::path& result,
                            const FilterItem* filter, int filter_count,
                            const std::string& default_path) {
  bool use_default_path =
      !default_path.empty() && std::filesystem::exists(default_path);
#ifdef HAVE_NFD
  nfdchar_t* out_path;
  nfdresult_t nfd_result = NFD_OpenDialogU8(
      &out_path, reinterpret_cast<const nfdfilteritem_t*>(filter), filter_count,
      use_default_path
          ? std::filesystem::absolute(default_path).string().c_str()
          : nullptr);

  if (nfd_result == NFD_OKAY) {
    result = out_path;
    NFD_FreePathU8(out_path);
    return true;
  } else if (nfd_result == NFD_ERROR) {
    UVELON_LOG_WARNING("NFD failed: " + std::string(NFD_GetError()));
  }
#else
  UVELON_LOG_ERROR("Native file dialog not available, use FileDialog!");
#endif
  return false;
}

bool FileWizard::saveDialog(std::filesystem::path& result,
                            const FilterItem* filter, int filter_count,
                            const std::string& name,
                            const std::string& default_path) {
  bool use_default_path =
      !default_path.empty() && std::filesystem::exists(default_path);
#ifdef HAVE_NFD
  nfdchar_t* out_path;
  nfdresult_t nfd_result = NFD_SaveDialogU8(
      &out_path, reinterpret_cast<const nfdfilteritem_t*>(filter), filter_count,
      use_default_path
          ? std::filesystem::absolute(default_path).string().c_str()
          : nullptr,
      name.c_str());

  if (nfd_result == NFD_OKAY) {
    result = std::filesystem::path(out_path);
    NFD_FreePathU8(out_path);
    return true;
  } else if (nfd_result == NFD_ERROR) {
    UVELON_LOG_WARNING("NFD failed: " + std::string(NFD_GetError()));
  }
#else
  UVELON_LOG_ERROR("Native file dialog not available, use FileDialog!");
#endif
  return false;
}

}  // namespace Uvelon