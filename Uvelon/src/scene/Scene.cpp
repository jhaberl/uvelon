#include "scene/Scene.hpp"

#include "rendering/Primitives.hpp"
#include "scene/Animation.hpp"
#include "scene/Node.hpp"
#include "scene/SceneEnvironment.hpp"

namespace Uvelon {

Scene::Scene() : Scene(std::make_shared<Node>()) {}

Scene::Scene(const std::shared_ptr<Node> root) : Scene(root, {}) {}

Scene::Scene(const std::shared_ptr<Node> root,
             std::vector<std::shared_ptr<Animation>> animations)
    : Scene(root, animations, nullptr) {}

Scene::Scene(const std::shared_ptr<Node> root,
             std::vector<std::shared_ptr<Animation>> animations,
             std::shared_ptr<SceneEnvironment> environment)
    : root_(root), animations_(animations), environment_(environment) {
  // Set any camera to active
  std::vector<std::shared_ptr<Node>> camera_nodes;
  addCameraNodes(camera_nodes);
  if (!camera_nodes.empty()) setActiveCamera(camera_nodes[0]);
}

void Scene::preUpdate(float delta) {
  for (auto& animation : animations_) animation->update(delta);
}

void Scene::update(float delta) { root_->update(); }

bool Scene::getActiveCamera(std::shared_ptr<Node>& camera_node) const {
  return (camera_node = active_camera_.lock()) != nullptr;
}

void Scene::addRenderObjects(std::vector<RenderObject>& render_objects,
                             std::shared_ptr<Node> node) const {
  if (!node) {
    node = root_;
    if (environment_)
      render_objects.emplace_back(environment_->getEnvironment());
  }

  if (node->mesh_) {
    auto& primitives = node->mesh_->getPrimitives();
    for (auto primitive : primitives) {
      render_objects.emplace_back(primitive, node->transform_.getModelMatrix());
    }
  }
  if (node->splats_) {
    render_objects.emplace_back(node->splats_->getHandle(),
                                node->transform_.getModelMatrix());
  }

  for (auto child : node->children_) {
    addRenderObjects(render_objects, child);
  }
}

void Scene::addLightPrimitives(std::vector<LightPrimitive>& light_primitives,
                               std::shared_ptr<Node> node) const {
  if (!node) node = root_;

  if (node->light_)
    light_primitives.emplace_back(node->light_->constructPrimitive());

  for (auto child : node->children_) {
    addLightPrimitives(light_primitives, child);
  }
}

void Scene::addLightNodes(std::vector<std::shared_ptr<Node>>& light_nodes,
                          std::shared_ptr<Node> node) const {
  if (!node) node = root_;

  if (node->light_) light_nodes.push_back(node);

  for (auto child : node->children_) {
    addLightNodes(light_nodes, child);
  }
}

void Scene::addCameraNodes(std::vector<std::shared_ptr<Node>>& camera_nodes,
                           std::shared_ptr<Node> node) const {
  if (!node) node = root_;

  if (node->camera_) camera_nodes.push_back(node);

  for (auto child : node->children_) {
    addCameraNodes(camera_nodes, child);
  }
}

}  // namespace Uvelon