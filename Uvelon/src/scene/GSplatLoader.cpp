#include "scene/GSplatLoader.hpp"

#include "core/CVarManager.hpp"
#include "core/FileWizard.hpp"
#include "rendering/Descriptor.hpp"
#include "rendering/Pipeline.hpp"
#include "rendering/RenderData.hpp"
#include "rendering/Renderer.hpp"
#include "rendering/Utils.hpp"
#include "scene/Loader.hpp"
#include "scene/LoaderPrimitives.hpp"
#include "scene/Node.hpp"
#include "scene/Scene.hpp"
#include "utils/Logger.hpp"
#include "utils/MappedFile.hpp"

namespace Uvelon {

GSplatLoader::GSplatLoader(const std::string& path)
    : file_(std::make_shared<MappedFile>(path)) {
  fs_path_ = std::filesystem::path(path);

  push_constant_ = {};
  push_constant_.size = sizeof(GaussianSplatsPC);
  push_constant_.stageFlags =
      VK_SHADER_STAGE_ALL_GRAPHICS | VK_SHADER_STAGE_COMPUTE_BIT;
}

std::unique_ptr<Scene> GSplatLoader::load() {
  if (!file_->data) return std::make_unique<Scene>();

  RenderData& rd = Renderer::getInstance().getRenderData();

  if (!readHeader()) return std::make_unique<Scene>();

  if (gs_.proxy_mesh) {
    // Use virtual memory
    gs_.source_file = std::shared_ptr<MappedFile>(std::move(file_));

    gs_.num_inmemory_max =
        static_cast<uint32_t>(GAUSSIAN_MAX_INMEMORY_PAGES * gs_.page_size);
    gs_.gaussian_buffer = rd.allocateBuffer(
        gs_.num_inmemory_max * GAUSSIAN_COMPONENTS * sizeof(float),
        VK_BUFFER_USAGE_STORAGE_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT);
    gs_.page_table.resize(GAUSSIAN_MAX_INMEMORY_PAGES);

    createVmemResources();
  } else {
    // No virtual memory
    loadFullDataClassic();
  }

  createGaussianMaterial();

  uint32_t* indices = new uint32_t[gs_.num_inmemory_max];
  for (uint64_t i = 0; i < gs_.num_inmemory_max; i++)
    indices[i] = static_cast<uint32_t>(i);
  gs_.indices = rd.allocateBuffer(
      indices, gs_.num_inmemory_max * sizeof(uint32_t),
      VK_BUFFER_USAGE_INDEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT);
  delete[] indices;

  createDepthSortResources();

  auto root = std::make_shared<Node>(fs_path_.string());
  root->setGaussianSplats(std::make_shared<GaussianSplatsNode>(
      rd.addGaussianSplats(std::move(gs_))));

  return std::make_unique<Scene>(root);
}

bool GSplatLoader::readHeader() {
  char* raw = static_cast<char*>(file_->data);
  size_t offset = 0;

  std::string line, keyword, spec1, spec2;
  size_t first_space, second_space;
  while (true) {
    line.clear();
    while (offset < file_->size && raw[offset] != '\n') {
      line.push_back(raw[offset]);
      offset++;
    }
    offset++;

    first_space = line.find_first_of(" ");
    second_space = line.find_first_of(" ", first_space + 1);
    keyword = line.substr(0, first_space);
    if (second_space != std::string::npos) {
      spec1 = line.substr(first_space + 1, second_space - first_space - 1);
      spec2 = line.substr(second_space + 1);
    }

    if (keyword == "format") {
      if (spec1 != "binary_little_endian" || spec2 != "1.0") {
        UVELON_LOG_ERROR("Unsupported format: " + spec1 + " " + spec2);
        return false;
      }
    } else if (keyword == "element") {
      if (spec1 == "vertex")
        gs_.num_total = std::strtoull(spec2.c_str(), nullptr, 10);
    } else if (keyword == "property") {
      // Ignore for now
    } else if (keyword == "end_header") {
      break;
    } else if (keyword == "comment") {
      auto content =
          line.substr(first_space + 1, line.size() - first_space - 1);
      if (content == "VMEM_READY") {
        // This ply has been preprocessed for use with our virtual memory
        UVELON_LOG_INFO("Using virtual memory");
        if (!loadProxyMesh()) {
          return false;
        }
      }
    } else if (keyword != "ply") {
      UVELON_LOG_WARNING("Ignoring header property: " + keyword);
    }
  }
  gs_.header_size = static_cast<uint32_t>(offset);
  return true;
}

bool GSplatLoader::loadProxyMesh() {
  // Load proxy mesh from gltf
  auto proxy_path = std::filesystem::path(fs_path_).replace_extension("glb");
  if (!std::filesystem::exists(proxy_path)) {
    proxy_path.replace_extension("gltf");
    if (!std::filesystem::exists(proxy_path)) {
      UVELON_LOG_ERROR(
          "Mesh is preprocessed for virtual memory but no proxy mesh "
          "found!");
      return false;
    }
  }

  Loader loader(proxy_path.string());
  bool extras_success = false;
  loader.ef_mesh_primitive =
      [&](nlohmann::json gltf_primitive, MeshPrimitive& primitive,
          std::vector<std::shared_ptr<Accessor>> accessors,
          std::vector<std::shared_ptr<BufferView>> buffer_views,
          std::vector<std::shared_ptr<Buffer>> buffers,
          std::vector<MaterialH> materials) {
        if (gltf_primitive.contains("extras")) {
          auto extras = gltf_primitive["extras"];

          gs_.page_size = extras.value("page_size", 0);
          gs_.num_pages = extras.value(
              "num_pages",
              static_cast<uint32_t>(gs_.num_total / gs_.page_size));
          gs_.lod_levels = extras.value("lod_levels", 0);
          if (gs_.lod_levels > 3) UVELON_LOG_WARNING("Unsupported LOD levels");
          int triangle_pageid_accessor =
              extras.value("triangle_pageid_accessor", -1);
          int page_link_accessor = extras.value("page_link_accessor", -1);

          if (gs_.page_size && triangle_pageid_accessor >= 0 &&
              page_link_accessor >= 0) {
            gs_.face_pageid_mapping =
                accessors[triangle_pageid_accessor]->loadGPU(
                    VK_BUFFER_USAGE_STORAGE_BUFFER_BIT);
            gs_.page_links = accessors[page_link_accessor]->loadGPU(
                VK_BUFFER_USAGE_STORAGE_BUFFER_BIT);
            extras_success = true;
          }
        }
      };

  auto proxy_scene = loader.load();
  if (extras_success) {
    auto proxy_node = proxy_scene->getRoot();

    if (!proxy_node->getChildren().empty()) {
      auto gltf_root = proxy_node->getChildren().front();
      if (!gltf_root->getChildren().empty()) {
        auto proxy_mesh = gltf_root->getChildren().front()->getMesh();
        if (!proxy_mesh->getPrimitives().empty()) {
          gs_.proxy_mesh = proxy_mesh->getPrimitives().front();
          return true;
        }
      }
    }
  }

  UVELON_LOG_ERROR("Error loading proxy mesh for virtual memory");
  return false;
}

void GSplatLoader::createVmemResources() {
  // TODO: Create single image in GaussianSystem!
  // Image to render page ids to
  VkExtent3D extent = GAUSSIAN_PROXY_RENDER_DIMS;
  gs_.proxy_image = Renderer::getInstance().getRenderData().addImage(
      Image(extent, VK_FORMAT_R32_UINT,
            VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT));
  gs_.proxy_sampler = Renderer::getInstance().getRenderData().createSampler(
      VK_FILTER_NEAREST, VK_FILTER_NEAREST, VK_SAMPLER_MIPMAP_MODE_NEAREST,
      VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER,
      VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER, true);
  gs_.proxy_image_depth = Renderer::getInstance().getRenderData().addImage(
      Image(extent, VK_FORMAT_D32_SFLOAT,
            VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT |
                VK_IMAGE_USAGE_SAMPLED_BIT));

  // Replace proxy mesh's material
  Material material;
  material.descriptor_set =
      DescriptorBuilder::begin()
          .addBuffer(VK_SHADER_STAGE_ALL, VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
                     gs_.face_pageid_mapping, 0, gs_.face_pageid_mapping->size)
          .build();

  VkFormat color_format = gs_.proxy_image->getFormat();
  PipelineH pipeline =
      PipelineBuilder::beginGraphics()
          .addShader(UVELON_FILE_RESOURCES(
                         "shaders/gaussian_splatting/gsplat_proxy_render.vert")
                         .string(),
                     VK_SHADER_STAGE_VERTEX_BIT)
          .addShader(UVELON_FILE_RESOURCES(
                         "shaders/gaussian_splatting/gsplat_proxy_render.frag")
                         .string(),
                     VK_SHADER_STAGE_FRAGMENT_BIT)
          .addPushConstant(push_constant_)
          .addDescriptorSet(
              Renderer::getInstance().getGlobalDescriptorSet()->layout)
          .addDescriptorSet(material.descriptor_set->layout)
          .setVertexInputInfo(
              gs_.proxy_mesh->binding_description.data(),
              static_cast<uint32_t>(gs_.proxy_mesh->binding_description.size()),
              gs_.proxy_mesh->attribute_description.data(), 1)
          .setAttachmentFormats(&color_format, 1,
                                gs_.proxy_image_depth->getFormat())
          .build();
  material.pipeline = pipeline;
  gs_.proxy_mesh->material =
      Renderer::getInstance().getRenderData().addMaterial(std::move(material));

  // Page ID reduction
  gs_.reduction_result = Renderer::getInstance().getRenderData().allocateBuffer(
      GAUSSIAN_MAX_ALLOWED_PAGES * sizeof(float),
      VK_BUFFER_USAGE_STORAGE_BUFFER_BIT, true);
  gs_.reduction_ds =
      DescriptorBuilder::begin()
          .addCombinedImageSampler(VK_SHADER_STAGE_COMPUTE_BIT, gs_.proxy_image,
                                   VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
                                   gs_.proxy_sampler)
          .addCombinedImageSampler(
              VK_SHADER_STAGE_COMPUTE_BIT, gs_.proxy_image_depth,
              VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, gs_.proxy_sampler)
          .addBuffer(VK_SHADER_STAGE_ALL, VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
                     gs_.page_links, 0, gs_.page_links->size)
          .addBuffer(VK_SHADER_STAGE_COMPUTE_BIT,
                     VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, gs_.reduction_result, 0,
                     gs_.reduction_result->size)
          .build();

  gs_.reduction_pipe =
      PipelineBuilder::beginCompute()
          .addDescriptorSet(
              Renderer::getInstance().getGlobalDescriptorSet()->layout)
          .addDescriptorSet(gs_.reduction_ds->layout)
          .addShader(UVELON_FILE_RESOURCES(
                         "shaders/gaussian_splatting/gsplat_pageid_reduce.comp")
                         .string(),
                     VK_SHADER_STAGE_COMPUTE_BIT)
          .build();

  // Staging buffer for gaussians
  // TODO: Find a better solution for this
  gs_.vmem_staging_buffer =
      Renderer::getInstance().getRenderData().allocateBuffer(
          GAUSSIAN_MAX_PAGES_PER_FRAME * gs_.page_size *
              GAUSSIAN_PER_GAUSSIAN_BYTES,
          VK_BUFFER_USAGE_TRANSFER_SRC_BIT, true, true);

  gs_.index_staging_buffer =
      Renderer::getInstance().getRenderData().allocateBuffer(
          gs_.num_inmemory_max * 2 * sizeof(uint32_t),
          VK_BUFFER_USAGE_TRANSFER_SRC_BIT, true, true);
}

void GSplatLoader::loadFullDataClassic() {
  float* gaussian_buffer;
  if (Renderer::getInstance().isIntegratedDevice()) {
    gs_.gaussian_buffer =
        Renderer::getInstance().getRenderData().allocateBuffer(
            gs_.num_total * GAUSSIAN_COMPONENTS * sizeof(float),
            VK_BUFFER_USAGE_STORAGE_BUFFER_BIT, true,
            true);  // TODO: Test last param
    gaussian_buffer =
        static_cast<float*>(gs_.gaussian_buffer->allocation_info.pMappedData);
  } else {
    gaussian_buffer = new float[gs_.num_total * GAUSSIAN_COMPONENTS];
  }

  float* file_data = reinterpret_cast<float*>(
      reinterpret_cast<char*>(file_->data) + gs_.header_size);
  for (size_t index = 0; index < gs_.num_total; index++) {
    memcpy(&gaussian_buffer[index * GAUSSIAN_COMPONENTS], file_data,
           3 * sizeof(float));
    // nx, ny, nz are unused
    file_data += 6;
    //  Spherical harmonics: DC
    memcpy(&gaussian_buffer[index * GAUSSIAN_COMPONENTS + 3], file_data,
           3 * sizeof(float));
    file_data += 3;
    // Other 15 spherical harmonics
    for (int i = 0; i < 45; i++) {
      gaussian_buffer[index * GAUSSIAN_COMPONENTS + 6 + (i % 15) * 3 + i / 15] =
          *file_data;
      file_data++;
    }
    // Opacity (1), Scale (3), Rotation (4)
    memcpy(&gaussian_buffer[index * GAUSSIAN_COMPONENTS + 51], file_data,
           8 * sizeof(float));
    file_data += 8;
  }

  file_.reset();
  UVELON_LOG_DEBUG("Done reading data");

  // Process opacity, scale, rotation
  float x_min{0.0f}, x_max{0.0f}, y_min{0.0f}, y_max{0.0f}, z_min{0.0f},
      z_max{0.0f};
  for (uint64_t i = 0; i < gs_.num_total; i++) {
    float* pos = &gaussian_buffer[i * GAUSSIAN_COMPONENTS];
    x_min = std::min(x_min, pos[0]);
    x_max = std::max(x_max, pos[0]);
    y_min = std::min(y_min, pos[1]);
    y_max = std::max(y_max, pos[1]);
    z_min = std::min(z_min, pos[2]);
    z_max = std::max(z_max, pos[2]);
    // Opacity -> sigmoid
    gaussian_buffer[i * GAUSSIAN_COMPONENTS + 51] =
        1.0f / (1.0f + exp(-gaussian_buffer[i * GAUSSIAN_COMPONENTS + 51]));
    // Scale -> e^scale
    gaussian_buffer[i * GAUSSIAN_COMPONENTS + 52 + 0] =
        exp(gaussian_buffer[i * GAUSSIAN_COMPONENTS + 52 + 0]);
    gaussian_buffer[i * GAUSSIAN_COMPONENTS + 52 + 1] =
        exp(gaussian_buffer[i * GAUSSIAN_COMPONENTS + 52 + 1]);
    gaussian_buffer[i * GAUSSIAN_COMPONENTS + 52 + 2] =
        exp(gaussian_buffer[i * GAUSSIAN_COMPONENTS + 52 + 2]);
    // Rotation -> normalize, rotate to yzwx
    float len = static_cast<float>(std::sqrt(
        std::pow(gaussian_buffer[i * GAUSSIAN_COMPONENTS + 55 + 0], 2) +
        std::pow(gaussian_buffer[i * GAUSSIAN_COMPONENTS + 55 + 1], 2) +
        std::pow(gaussian_buffer[i * GAUSSIAN_COMPONENTS + 55 + 2], 2) +
        std::pow(gaussian_buffer[i * GAUSSIAN_COMPONENTS + 55 + 3], 2)));
    float temp_x = gaussian_buffer[i * GAUSSIAN_COMPONENTS + 55 + 0];
    gaussian_buffer[i * GAUSSIAN_COMPONENTS + 55 + 0] =
        gaussian_buffer[i * GAUSSIAN_COMPONENTS + 55 + 1] / len;
    gaussian_buffer[i * GAUSSIAN_COMPONENTS + 55 + 1] =
        gaussian_buffer[i * GAUSSIAN_COMPONENTS + 55 + 2] / len;
    gaussian_buffer[i * GAUSSIAN_COMPONENTS + 55 + 2] =
        gaussian_buffer[i * GAUSSIAN_COMPONENTS + 55 + 3] / len;
    gaussian_buffer[i * GAUSSIAN_COMPONENTS + 55 + 3] = temp_x / len;
  }

  UVELON_LOG_INFO("Scene (" + std::to_string(gs_.num_total) + ") bounds: x=(" +
                  std::to_string(x_min) + ", " + std::to_string(x_max) +
                  "), y=(" + std::to_string(y_min) + ", " +
                  std::to_string(y_max) + "), z=(" + std::to_string(z_min) +
                  ", " + std::to_string(z_max) + ")");
  UVELON_LOG_DEBUG("Done processing data");

  gs_.num_inmemory_max = gs_.num_inmemory_current = gs_.num_total;

  if (Renderer::getInstance().isIntegratedDevice()) {
    VK_CHECK(vmaFlushAllocation(Renderer::getInstance().getAllocator(),
                                gs_.gaussian_buffer->allocation, 0,
                                VK_WHOLE_SIZE));
    VK_CHECK(vmaInvalidateAllocation(Renderer::getInstance().getAllocator(),
                                     gs_.gaussian_buffer->allocation, 0,
                                     VK_WHOLE_SIZE));
    vmaUnmapMemory(Renderer::getInstance().getAllocator(),
                   gs_.gaussian_buffer->allocation);
  } else {
    gs_.gaussian_buffer =
        Renderer::getInstance().getRenderData().allocateBuffer(
            gaussian_buffer,
            gs_.num_inmemory_max * GAUSSIAN_COMPONENTS * sizeof(float),
            VK_BUFFER_USAGE_STORAGE_BUFFER_BIT);

    delete[] gaussian_buffer;
  }

  // TODO: Remove
  /*CVarManager::getVar("gaussian.slice.export").getValue<bool>() = true;
  CVarManager::getVar("gaussian.slice.num_slices.x").getValue<int32_t>() =
  1000; CVarManager::getVar("gaussian.slice.num_slices.y").getValue<int32_t>()
  = 1000;
  CVarManager::getVar("gaussian.slice.num_slices.z").getValue<int32_t>() =
  1000; CVarManager::getVar("gaussian.slice.bounds.x").getValue<double>() =
  1000.0; CVarManager::getVar("gaussian.slice.bounds.y").getValue<double>() =
  1000.0; CVarManager::getVar("gaussian.slice.bounds.z").getValue<double>() =
  1000.0; CVarManager::getVar("gaussian.slice.opacity").getValue<double>() =
  0.8; CVarManager::getVar("gaussian.slice.path").getValue<std::string>() =
      "./vast_campus";*/
}

void GSplatLoader::createGaussianMaterial() {
  Material material = {};
  material.descriptor_set =
      DescriptorBuilder::begin()
          .addBuffer(VK_SHADER_STAGE_VERTEX_BIT,
                     VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, gs_.gaussian_buffer, 0,
                     gs_.gaussian_buffer->size)
          .build();
  VkFormat swapchain_format =
      Renderer::getInstance().getSwapchain().getImageFormat();
#if defined(__APPLE__) || !defined(GAUSSIAN_ENABLE_GEOMETRY)
  UVELON_LOG_INFO("Geometry shader disabled");
  bool geometry_shader = false;
  std::vector<std::pair<std::string, std::string>> macros = {};
#else
  bool geometry_shader = true;
  std::vector<std::pair<std::string, std::string>> macros = {
      {"UVELON_GSPLAT_GEOMETRY", "1"}};
#endif
  PipelineBuilder builder =
      PipelineBuilder::beginGraphics()
          .addPushConstant(push_constant_)
          .addDescriptorSet(
              Renderer::getInstance().getGlobalDescriptorSet()->layout)
          .addDescriptorSet(material.descriptor_set->layout)
          .addShader(
              UVELON_FILE_RESOURCES("shaders/gaussian_splatting/gsplat.vert")
                  .string(),
              VK_SHADER_STAGE_VERTEX_BIT, macros)
          .addShader(
              UVELON_FILE_RESOURCES("shaders/gaussian_splatting/gsplat.frag")
                  .string(),
              VK_SHADER_STAGE_FRAGMENT_BIT, macros)
          .setVertexInputInfo(nullptr, 0, nullptr, 0)
          .setTopology(VK_PRIMITIVE_TOPOLOGY_POINT_LIST)
          .setAttachmentFormats(&swapchain_format, 1, VK_FORMAT_UNDEFINED);
  if (geometry_shader) {
    builder.addShader(
        UVELON_FILE_RESOURCES("shaders/gaussian_splatting/gsplat.geom")
            .string(),
        VK_SHADER_STAGE_GEOMETRY_BIT, macros);
  }
  material.pipeline = builder.build();
  gs_.material =
      Renderer::getInstance().getRenderData().addMaterial(std::move(material));
}

void GSplatLoader::createDepthSortResources() {
  RenderData& rd = Renderer::getInstance().getRenderData();

  gs_.sort_elements_per_wg = 8192;
  gs_.sort_num_workgroups = static_cast<uint32_t>(
      gs_.num_inmemory_max / gs_.sort_elements_per_wg +
      (gs_.num_inmemory_max % gs_.sort_elements_per_wg ? 1 : 0));
  gs_.sort_buf_hist =
      rd.allocateBuffer(256 * sizeof(uint32_t) * gs_.sort_num_workgroups,
                        VK_BUFFER_USAGE_STORAGE_BUFFER_BIT);

  gs_.sort_buf_packed[0] = rd.allocateBuffer(
      gs_.num_inmemory_max * 2 * sizeof(uint32_t),
      VK_BUFFER_USAGE_STORAGE_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_SRC_BIT |
          VK_BUFFER_USAGE_TRANSFER_DST_BIT);
  gs_.sort_buf_packed[1] = rd.allocateBuffer(
      gs_.num_inmemory_max * 2 * sizeof(uint32_t),
      VK_BUFFER_USAGE_STORAGE_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT);

  gs_.sort_buf_data = Renderer::getInstance().getRenderData().allocateBuffer(
      sizeof(GaussianSplatsSortData), VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, true);
  gs_.sort_ds =
      DescriptorBuilder::begin()
          .addBuffer(VK_SHADER_STAGE_COMPUTE_BIT,
                     VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, gs_.sort_buf_data, 0,
                     gs_.sort_buf_data->size)
          .addBuffer(VK_SHADER_STAGE_COMPUTE_BIT,
                     VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, gs_.gaussian_buffer, 0,
                     gs_.gaussian_buffer->size)
          .addBuffer(VK_SHADER_STAGE_COMPUTE_BIT,
                     VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, gs_.sort_buf_packed[0],
                     0, gs_.sort_buf_packed[0]->size)
          .addBuffer(VK_SHADER_STAGE_COMPUTE_BIT,
                     VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, gs_.sort_buf_packed[1],
                     0, gs_.sort_buf_packed[1]->size)
          .addBuffer(VK_SHADER_STAGE_COMPUTE_BIT,
                     VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, gs_.sort_buf_hist, 0,
                     gs_.sort_buf_hist->size)
          .build();
  gs_.sort_pipe_depth =
      PipelineBuilder::beginCompute()
          .addShader(UVELON_FILE_RESOURCES(
                         "shaders/gaussian_splatting/gsplat_sort_depth.comp")
                         .string(),
                     VK_SHADER_STAGE_COMPUTE_BIT)
          .addDescriptorSet(
              Renderer::getInstance().getGlobalDescriptorSet()->layout)
          .addDescriptorSet(gs_.sort_ds->layout)
          .addPushConstant(push_constant_)
          .build();
  gs_.sort_pipe_hist =
      PipelineBuilder::beginCompute()
          .addShader(UVELON_FILE_RESOURCES(
                         "shaders/gaussian_splatting/gsplat_sort_hist.comp")
                         .string(),
                     VK_SHADER_STAGE_COMPUTE_BIT)
          .addDescriptorSet(
              Renderer::getInstance().getGlobalDescriptorSet()->layout)
          .addDescriptorSet(gs_.sort_ds->layout)
          .addPushConstant(push_constant_)
          .build();
  gs_.sort_pipe_sort =
      PipelineBuilder::beginCompute()
          .addShader(UVELON_FILE_RESOURCES(
                         "shaders/gaussian_splatting/gsplat_sort_sort.comp")
                         .string(),
                     VK_SHADER_STAGE_COMPUTE_BIT)
          .addDescriptorSet(
              Renderer::getInstance().getGlobalDescriptorSet()->layout)
          .addDescriptorSet(gs_.sort_ds->layout)
          .addPushConstant(push_constant_)
          .build();
}

}  // namespace Uvelon