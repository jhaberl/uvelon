#include "scene/Camera.hpp"

#include <glm/gtc/matrix_transform.hpp>

#include "scene/Node.hpp"
#include "utils/Logger.hpp"

namespace Uvelon {

Camera::Camera(CameraType type) : type_(type) {}

PerspectiveCamera::PerspectiveCamera(float aspect_ratio, float yfov,
                                     float znear)
    : aspect_ratio_(aspect_ratio),
      yfov_(yfov),
      znear_(znear),
      Camera(Camera::CameraType::PERSPECTIVE) {
  projection_matrix_ = glm::infinitePerspective(
      yfov, aspect_ratio, -znear);  // -near for reverse z buffer
}

PerspectiveCamera::PerspectiveCamera(float aspect_ratio, float yfov,
                                     float znear, float zfar)
    : aspect_ratio_(aspect_ratio),
      yfov_(yfov),
      znear_(znear),
      zfar_(zfar),
      Camera(Camera::CameraType::PERSPECTIVE) {
  computeProjection();
}

OrthographicCamera::OrthographicCamera(float xmag, float ymag, float znear,
                                       float zfar)
    : xmag_(xmag),
      ymag_(ymag),
      znear_(znear),
      zfar_(zfar),
      Camera(Camera::CameraType::ORTHOGRAPHIC) {
  computeProjection();
}

const glm::mat4 Camera::computeViewMatrix() const {
  glm::mat4 result;
  if (auto parent = parent_.lock()) {
    // View matrix is inverse of camera transform
    auto& translation = parent->getTransform().getTranslation();
    glm::mat4 translation_mat_inv =
        glm::translate(glm::mat4(1.0f), -translation);
    auto& rotation = parent->getTransform().getRotation();
    // Rotation matrix is orthogonal, so inverse is transpose.
    glm::mat4 rotation_mat_inv = glm::transpose(glm::mat4_cast(rotation));
    result = rotation_mat_inv * translation_mat_inv;
  } else {
    result = glm::mat4(1.0f);
  }
  return result;
}

void Camera::lookAt(const glm::vec3& eye, const glm::vec3& center) {
  if (auto parent = parent_.lock()) {
    glm::vec3 direction = glm::normalize(center - eye);
    if (direction != UVELON_UP) {
      parent->getTransform().setTranslation(eye);
      parent->getTransform().setRotation(glm::quatLookAt(direction, UVELON_UP));
    }
  }
}

void PerspectiveCamera::computeProjection() {
  // Switch near/far for reverse z buffer
  projection_matrix_ = glm::perspective(yfov_, aspect_ratio_, zfar_, znear_);
}

void OrthographicCamera::computeProjection() {
  // Switch near/far for reverse z buffer
  projection_matrix_ = glm::ortho(-xmag_, xmag_, -ymag_, ymag_, zfar_, znear_);
}

}  // namespace Uvelon