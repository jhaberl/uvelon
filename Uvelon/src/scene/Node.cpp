#include "scene/Node.hpp"

#include "rendering/Primitives.hpp"

namespace Uvelon {

void Transform::setTranslation(const glm::vec3& translation) {
  translation_ = translation;
  dirty_ = true;
}

void Transform::setRotation(const glm::quat& rotation) {
  rotation_ = rotation;
  dirty_ = true;
}

void Transform::setScale(const glm::vec3& scale) {
  scale_ = scale;
  dirty_ = true;
}

glm::mat4 Transform::calculateLocalMatrix() {
  glm::mat4 translation_matrix = glm::translate(glm::mat4(1.0f), translation_);
  glm::mat4 rotation_matrix = glm::toMat4(rotation_);
  glm::mat4 scale_matrix = glm::scale(glm::mat4(1.0f), scale_);
  return translation_matrix * rotation_matrix * scale_matrix;
}

void Transform::updateModelMatrix() {
  model_matrix_ = calculateLocalMatrix();
  dirty_ = false;
}

void Transform::updateModelMatrix(const glm::mat4& parent_model_matrix) {
  model_matrix_ = parent_model_matrix * calculateLocalMatrix();
  dirty_ = false;
}

Node::Node() : name_("") {}

Node::Node(const std::string& name) : name_(name) {}

const glm::mat4& Transform::getModelMatrix() const {
  assert(!dirty_ && "Retrieved dirty model matrix!");
  return model_matrix_;
}

void Node::attach(std::shared_ptr<Node> parent, std::shared_ptr<Node> child) {
  parent->children_.push_back(child);
  child->parent_ = parent;

  child->getTransform().setDirty();
}

void Node::detachChild(std::shared_ptr<Node> child) {
  auto it = children_.begin();
  while (it != children_.end()) {
    if (*it == child) {
      child->parent_.reset();
      children_.erase(it);
      break;
    }
    it++;
  }
}

void Node::update(bool force) {
  force |= transform_.isDirty();

  if (force) {
    if (auto parent = parent_.lock())
      transform_.updateModelMatrix(parent->getTransform().getModelMatrix());
    else
      transform_.updateModelMatrix();
  }

  for (auto child : children_) {
    child->update(force);
  }
}

}  // namespace Uvelon