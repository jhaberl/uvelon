#include "scene/Loader.hpp"

#include <filesystem>
#include <memory>
#include <set>

#include "core/BufferCPU.hpp"
#include "core/FileWizard.hpp"
#include "rendering/Descriptor.hpp"
#include "rendering/RenderData.hpp"
#include "rendering/Renderer.hpp"
#include "rendering/systems/ShadowMapSystem.hpp"
#include "scene/Animation.hpp"
#include "scene/Light.hpp"
#include "scene/Node.hpp"
#include "scene/Scene.hpp"
#include "utils/Instrumentation.hpp"
#include "utils/Logger.hpp"
#include "utils/TangentGenerator.hpp"
#include "utils/ThreadPool.hpp"

#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#define GLM_ENABLE_EXPERIMENTAL
#define GLM_FORCE_LEFT_HANDED
#include <glm/glm.hpp>
#include <glm/gtx/matrix_decompose.hpp>

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

namespace Uvelon {

Loader::Loader(const std::string& path)
    : file_(std::make_shared<MappedFile>(path)) {
  glb_ = path.find(".glb") != std::string::npos;

  fs_path_ = std::filesystem::path(path);
  if (!file_->data) {
    UVELON_LOG_ERROR("File not found: " + path);
  } else {
    if (glb_) {
      // https://registry.khronos.org/glTF/specs/2.0/glTF-2.0.html#glb-file-format-specification
      // TODO: File is little endian, ensure correct endianness after reading
      // Header
      uint32_t magic, version, length;
      file_->stream->read(reinterpret_cast<char*>(&magic), sizeof(uint32_t));
      file_->stream->read(reinterpret_cast<char*>(&version), sizeof(uint32_t));
      file_->stream->read(reinterpret_cast<char*>(&length), sizeof(uint32_t));

      // JSON chunk
      uint32_t chunk_type;
      file_->stream->read(reinterpret_cast<char*>(&glb_json_length_),
                          sizeof(uint32_t));
      file_->stream->read(reinterpret_cast<char*>(&chunk_type),
                          sizeof(uint32_t));
      glb_json_offset_ = static_cast<uint32_t>(file_->stream->tellg());

      file_->stream->seekg(glb_json_length_, std::ios_base::cur);

      file_->stream->read(reinterpret_cast<char*>(&glb_buffer_length_),
                          sizeof(uint32_t));
      file_->stream->read(reinterpret_cast<char*>(&chunk_type),
                          sizeof(uint32_t));
      glb_buffer_offset_ = static_cast<uint32_t>(file_->stream->tellg());
    }
  }
}

nlohmann::json Loader::getJson() {
  if (glb_) {
    std::string json_content(glb_json_length_, 0);
    memcpy(json_content.data(),
           static_cast<char*>(file_->data) + glb_json_offset_,
           glb_json_length_);
    return nlohmann::json::parse(json_content);
  } else {
    std::string json_content(file_->size, 0);
    memcpy(json_content.data(), static_cast<char*>(file_->data), file_->size);
    return nlohmann::json::parse(json_content);
  }
}

std::vector<std::string> Loader::queryScenes(int& default_scene) {
  std::vector<std::string> result;

  auto gltf = getJson();
  if (gltf.contains("scenes")) {
    int index = 0;
    for (auto scene : gltf["scenes"]) {
      if (scene.contains("name"))
        result.push_back(scene["name"]);
      else
        result.push_back(std::to_string(index));
      index++;
    }
  }
  default_scene = 0;
  if (gltf.contains("scene")) default_scene = gltf["scene"];
  return result;
}

std::shared_ptr<Scene> Loader::load(int scene_id) {
  auto gltf = getJson();

  // Buffer
  std::vector<std::shared_ptr<Buffer>> buffers;
  if (gltf.contains("buffers")) buffers = loadBuffers(gltf["buffers"]);

  // Bufferview
  std::vector<std::shared_ptr<BufferView>> buffer_views;
  if (gltf.contains("bufferViews"))
    buffer_views = loadBufferViews(gltf["bufferViews"], buffers);

  // Accessors
  std::vector<std::shared_ptr<Accessor>> accessors;
  if (gltf.contains("accessors"))
    accessors = loadAccessors(gltf["accessors"], buffer_views);

  // Samplers
  std::vector<SamplerH> samplers;
  if (gltf.contains("samplers")) samplers = loadSamplers(gltf["samplers"]);
  addPlaceholderSampler(samplers);

  // Images
  std::vector<std::shared_ptr<ImageData>> images;
  if (gltf.contains("images"))
    images = loadImages(gltf["images"], buffer_views);

  // Textures
  std::vector<Texture> textures;
  if (gltf.contains("textures"))
    textures = loadTextures(gltf["textures"], samplers, images);
  addPlaceholderTextures(textures);

  // Materials
  std::vector<MaterialH> materials;
  if (gltf.contains("materials"))
    materials = loadMaterials(gltf["materials"], textures);

  // Create meshes
  std::vector<std::shared_ptr<Mesh>> meshes;
  if (gltf.contains("meshes"))
    meshes =
        loadMeshes(gltf["meshes"], accessors, buffer_views, buffers, materials);

  // Cameras
  std::vector<std::shared_ptr<Camera>> cameras;
  if (gltf.contains("cameras")) cameras = loadCameras(gltf["cameras"]);

  // Lights
  std::vector<std::shared_ptr<Light>> lights = loadLights(gltf);

  // Nodes
  std::vector<std::shared_ptr<Node>> nodes;
  if (gltf.contains("nodes"))
    nodes = loadNodes(gltf["nodes"], meshes, cameras, lights);
  auto root = loadSceneRoot(gltf, fs_path_.string(), nodes, scene_id);

  fixWindingOrder(root);

  // Animations
  std::vector<std::shared_ptr<Animation>> animations;
  if (gltf.contains("animations"))
    animations = loadAnimations(gltf["animations"], accessors, nodes);

  // Create scene
  auto scene = std::make_shared<Scene>(root, animations);

  UVELON_LOG_DEBUG("Loaded glTF: " + fs_path_.string() + " (Scene " +
                   std::to_string(scene_id) + ")");
  return scene;
}

std::vector<std::shared_ptr<Buffer>> Loader::loadBuffers(
    const nlohmann::json& gltf_buffers) {
  std::vector<std::shared_ptr<Buffer>> result;
  for (auto& buffer : gltf_buffers) {
    if (buffer.contains("byteLength")) {
      int length = buffer["byteLength"];

      if (glb_ && !buffer.contains("uri")) {
        // This is the binary buffer contained in the glb file
        if (length != glb_buffer_length_)
          UVELON_LOG_ERROR("Invalid buffer length in glb file");

        result.push_back(
            std::make_shared<Buffer>(file_, glb_buffer_offset_, length));
      } else {
        // Check for data uri
        std::string uri = buffer["uri"].get<std::string>();
        if (uri.find("data:") != std::string::npos) {
          UVELON_LOG_WARNING("Data URIs not supported!");
          continue;
        }

        // Read file into buffer
        std::filesystem::path path(fs_path_);
        path.replace_filename({buffer["uri"].get<std::string>()});
        auto file = std::make_shared<MappedFile>(path.string());
        if (file->data) {
          result.push_back(std::make_shared<Buffer>(file, 0, length));
        } else {
          result.push_back(std::make_shared<Buffer>(nullptr, 0, 0));
          UVELON_LOG_WARNING(std::string("File not found: ") +
                             buffer["uri"].get<std::string>());
        }
      }
    } else {
      UVELON_LOG_WARNING(std::string("No URI present"));
      result.push_back(std::make_shared<Buffer>(nullptr, 0, 0));
    }
  }
  return result;
}

std::vector<std::shared_ptr<BufferView>> Loader::loadBufferViews(
    const nlohmann::json& gltf_views,
    std::vector<std::shared_ptr<Buffer>>& buffers) {
  std::vector<std::shared_ptr<BufferView>> result;
  for (auto& view : gltf_views) {
    int buffer = view["buffer"];
    size_t offset = view.value<size_t>("byteOffset", 0);
    size_t length = view["byteLength"].get<size_t>();
    size_t stride = view.value<size_t>("byteStride", 0);
    int target = view.value<int>("target", 0);
    result.push_back(std::make_shared<BufferView>(buffers[buffer], offset,
                                                  length, stride, target));
  }
  return result;
}

std::vector<std::shared_ptr<Accessor>> Loader::loadAccessors(
    const nlohmann::json& gltf_accessors,
    std::vector<std::shared_ptr<BufferView>>& views) {
  std::vector<std::shared_ptr<Accessor>> result;
  for (auto& accessor : gltf_accessors) {
    int buffer_view = accessor.value<int>("bufferView", -1);
    if (buffer_view == -1) {
      UVELON_LOG_WARNING("Accessor does not provide a bufferview!");
      continue;
    }

    std::string type_str = accessor["type"];
    int type = 1;
    if (type_str == "VEC2")
      type *= 2;
    else if (type_str == "VEC3")
      type *= 3;
    else if (type_str == "VEC4")
      type *= 4;
    else if (type_str == "MAT2")
      type *= 4;
    else if (type_str == "MAT3")
      type *= 9;
    else if (type_str == "MAT4")
      type *= 16;

    size_t offset = accessor.value<size_t>("byteOffset", 0);
    size_t count = accessor["count"].get<size_t>();
    int component_type = accessor["componentType"];
    result.push_back(std::make_shared<Accessor>(views[buffer_view], offset,
                                                count, component_type, type));

    uint64_t per_element_bytes =
        type * (component_type >= 5125 ? 4 : (component_type >= 5122 ? 2 : 1));

    result.back()->size = per_element_bytes * count;
  }
  return result;
}

std::vector<SamplerH> Loader::loadSamplers(
    const nlohmann::json& gltf_samplers) {
  std::vector<SamplerH> result;

  for (auto& gltf_sampler : gltf_samplers) {
    VkFilter mag_filter, min_filter;
    VkSamplerMipmapMode mipmap_mode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
    VkSamplerAddressMode address_mode_u, address_mode_v;

    int raw_mag_filter = gltf_sampler.value("magFilter", 9729);
    int raw_min_filter = gltf_sampler.value("minFilter", 9986);
    int raw_wrap_s = gltf_sampler.value("wrapS", 10497);
    int raw_wrap_t = gltf_sampler.value("wrapT", 10497);

    switch (raw_mag_filter) {
      case 9728:
        mag_filter = VK_FILTER_NEAREST;
        break;
      case 9729:
        mag_filter = VK_FILTER_LINEAR;
        break;
    }
    switch (raw_min_filter) {
      case 9728:
      case 9984:
      case 9986:
        min_filter = VK_FILTER_NEAREST;
        break;
      case 9729:
      case 9985:
      case 9987:
        min_filter = VK_FILTER_LINEAR;
        break;
    }
    switch (raw_min_filter) {
      case 9984:
      case 9985:
        mipmap_mode = VK_SAMPLER_MIPMAP_MODE_NEAREST;
        break;
      case 9986:
      case 9987:
        mipmap_mode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
        break;
    }
    switch (raw_wrap_s) {
      case 33071:
        address_mode_u = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
        break;
      case 33648:
        address_mode_u = VK_SAMPLER_ADDRESS_MODE_MIRRORED_REPEAT;
        break;
      case 10497:
        address_mode_u = VK_SAMPLER_ADDRESS_MODE_REPEAT;
        break;
    }
    switch (raw_wrap_t) {
      case 33071:
        address_mode_v = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
        break;
      case 33648:
        address_mode_v = VK_SAMPLER_ADDRESS_MODE_MIRRORED_REPEAT;
        break;
      case 10497:
        address_mode_v = VK_SAMPLER_ADDRESS_MODE_REPEAT;
        break;
    }

    result.push_back(Renderer::getInstance().getRenderData().createSampler(
        mag_filter, min_filter, mipmap_mode, address_mode_u, address_mode_v));
  }
  return result;
}

void Loader::addPlaceholderSampler(std::vector<SamplerH>& samplers) {
  samplers.push_back(Renderer::getInstance().getRenderData().createSampler(
      VK_FILTER_LINEAR, VK_FILTER_NEAREST, VK_SAMPLER_MIPMAP_MODE_LINEAR,
      VK_SAMPLER_ADDRESS_MODE_REPEAT, VK_SAMPLER_ADDRESS_MODE_REPEAT));
}

std::vector<std::shared_ptr<ImageData>> Loader::loadImages(
    const nlohmann::json& gltf_images,
    std::vector<std::shared_ptr<BufferView>>& views) {
  std::vector<std::shared_ptr<ImageData>> result(gltf_images.size());
  ThreadPool load_pool;
  load_pool.init();

  int index = 0;
  for (auto& image : gltf_images) {
    if (image.contains("uri")) {
      std::filesystem::path path(fs_path_);
      path.replace_filename({image["uri"].get<std::string>()});

      load_pool.enqueue([index, path, &result]() {
        int x, y;
        stbi_uc* loaded_img =
            stbi_load(path.string().c_str(), &x, &y, nullptr, 4);

        if (loaded_img) {
          VkExtent3D extent{static_cast<uint32_t>(x), static_cast<uint32_t>(y),
                            1};
          uint64_t size = extent.width * extent.height * extent.depth * 4;
          result[index] = std::make_shared<ImageData>(
              loaded_img, extent, size, VK_IMAGE_USAGE_SAMPLED_BIT);
        } else {
          UVELON_LOG_ERROR("Unable to load " + path.string());
        }
      });
    } else if (image.contains("bufferView")) {
      int view_index = image["bufferView"];
      auto view = views[view_index];
      assert(!view->stride);

      load_pool.enqueue([index, view, &result, view_index]() {
        int x, y;
        stbi_uc* loaded_img = stbi_load_from_memory(
            reinterpret_cast<unsigned char*>(view->buffer->data + view->offset),
            static_cast<int>(view->length), &x, &y, nullptr, 4);

        if (loaded_img) {
          VkExtent3D extent{static_cast<uint32_t>(x), static_cast<uint32_t>(y),
                            1};
          uint64_t size = extent.width * extent.height * extent.depth * 4;
          result[index] = std::make_shared<ImageData>(
              loaded_img, extent, size, VK_IMAGE_USAGE_SAMPLED_BIT);
        } else {
          UVELON_LOG_ERROR("Unable to load image from bufferView " +
                           std::to_string(view_index));
        }
      });
    }
    index++;
  }
  load_pool.waitIdle();
  return result;
}

std::vector<Texture> Loader::loadTextures(
    const nlohmann::json& gltf_textures, std::vector<SamplerH>& samplers,
    std::vector<std::shared_ptr<ImageData>>& images) {
  std::vector<Texture> result;
  for (auto gltf_texture : gltf_textures) {
    int sampler_index =
        gltf_texture.value("sampler", static_cast<int>(samplers.size() - 1));
    result.emplace_back(images[gltf_texture["source"]],
                        samplers[sampler_index]);
  }

  return result;
}

void Loader::addPlaceholderTextures(std::vector<Texture>& textures) {
  // Add textures to end in reverse order as in enum, ignore NONE
  SamplerH own_sampler = Renderer::getInstance().getRenderData().createSampler(
      VK_FILTER_NEAREST, VK_FILTER_NEAREST, VK_SAMPLER_MIPMAP_MODE_NEAREST,
      VK_SAMPLER_ADDRESS_MODE_REPEAT, VK_SAMPLER_ADDRESS_MODE_REPEAT);
  unsigned char data[4] = {0, 0, 0, 0};
  ImageH img_transparent = Renderer::getInstance().getRenderData().addImage(
      Image(&data, 4, {1, 1, 1}, VK_FORMAT_R8G8B8A8_UNORM,
            VK_IMAGE_USAGE_SAMPLED_BIT));
  textures.emplace_back(
      Texture{std::make_shared<ImageData>(img_transparent), own_sampler});
  data[3] = 255;
  ImageH img_black = Renderer::getInstance().getRenderData().addImage(
      Image(&data, 4, {1, 1, 1}, VK_FORMAT_R8G8B8A8_UNORM,
            VK_IMAGE_USAGE_SAMPLED_BIT));
  textures.emplace_back(
      Texture{std::make_shared<ImageData>(img_black), own_sampler});
  data[0] = data[1] = 128;
  data[2] = 255;
  ImageH img_normal_flat = Renderer::getInstance().getRenderData().addImage(
      Image(&data, 4, {1, 1, 1}, VK_FORMAT_R8G8B8A8_UNORM,
            VK_IMAGE_USAGE_SAMPLED_BIT));
  textures.emplace_back(
      Texture{std::make_shared<ImageData>(img_normal_flat), own_sampler});
  data[0] = data[1] = 255;
  ImageH img_white = Renderer::getInstance().getRenderData().addImage(
      Image(&data, 4, {1, 1, 1}, VK_FORMAT_R8G8B8A8_UNORM,
            VK_IMAGE_USAGE_SAMPLED_BIT));
  textures.emplace_back(
      Texture{std::make_shared<ImageData>(img_white), own_sampler});
}

std::vector<MaterialH> Loader::loadMaterials(
    const nlohmann::json& gltf_materials, std::vector<Texture>& textures) {
  std::vector<MaterialH> result;
  for (auto gltf_material : gltf_materials) {
    Material material;
    Material::Factors factors;
    bool requires_uv = false;

    int tex_indices[4];  // base color, metal rough, normal, emissive
    if (gltf_material.contains("pbrMetallicRoughness")) {
      auto pbr = gltf_material["pbrMetallicRoughness"];

      if (pbr.contains("baseColorTexture")) {
        tex_indices[0] = pbr["baseColorTexture"]["index"];
        textures[tex_indices[0]].image_data->bakeFormat(
            VK_FORMAT_R8G8B8A8_SRGB);
        requires_uv = true;
      } else {
        tex_indices[0] =
            static_cast<int>(textures.size() - PlaceholderTexture::WHITE);
      }
      if (pbr.contains("baseColorFactor")) {
        auto factor = pbr["baseColorFactor"].get<std::vector<float>>();
        factors.base_color_factor =
            glm::vec4(factor[0], factor[1], factor[2], factor[3]);
      }

      if (pbr.contains("metallicRoughnessTexture")) {
        tex_indices[1] = pbr["metallicRoughnessTexture"]["index"];
        textures[tex_indices[1]].image_data->bakeFormat(
            VK_FORMAT_R8G8B8A8_UNORM);
        requires_uv = true;
      } else {
        tex_indices[1] =
            static_cast<int>(textures.size() - PlaceholderTexture::WHITE);
      }
      if (pbr.contains("metallicFactor"))
        factors.metallic_factor = pbr["metallicFactor"];
      if (pbr.contains("roughnessFactor"))
        factors.roughness_factor = pbr["roughnessFactor"];
    }

    if (gltf_material.contains("normalTexture")) {
      tex_indices[2] = gltf_material["normalTexture"]["index"];
      textures[tex_indices[2]].image_data->bakeFormat(VK_FORMAT_R8G8B8A8_UNORM);
      requires_uv = true;
    } else {
      tex_indices[2] =
          static_cast<int>(textures.size() - PlaceholderTexture::NORMAL_FLAT);
    }

    if (gltf_material.contains("emissiveTexture")) {
      tex_indices[3] = gltf_material["emissiveTexture"]["index"];
      textures[tex_indices[3]].image_data->bakeFormat(VK_FORMAT_R8G8B8A8_SRGB);
      requires_uv = true;
    } else {
      tex_indices[3] =
          static_cast<int>(textures.size() - PlaceholderTexture::TRANSPARENT);
    }
    if (gltf_material.contains("emissiveFactor")) {
      auto factor = gltf_material["emissiveFactor"].get<std::vector<float>>();
      factors.emissive_factor = glm::vec3(factor[0], factor[1], factor[2]);
    }

    // TODO: Support texture scale, texCoord

    // Create material
    auto descriptor_builder = DescriptorBuilder::begin();
    for (int i = 0; i < 4; i++) {
      Texture& texture = textures[tex_indices[i]];
      descriptor_builder.addCombinedImageSampler(
          VK_SHADER_STAGE_FRAGMENT_BIT,
          texture.image_data->bakeFormat(VK_FORMAT_UNDEFINED),
          VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, texture.sampler);
    }
    BufferH factor_buffer =
        Renderer::getInstance().getRenderData().allocateBuffer(
            &factors, sizeof(Material::Factors),
            VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT);
    descriptor_builder.addBuffer(VK_SHADER_STAGE_ALL_GRAPHICS,
                                 VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
                                 factor_buffer, 0, factor_buffer->size);
    material.descriptor_set = descriptor_builder.build();

    std::vector<std::pair<std::string, std::string>> mtv_macros = {};
    if (!requires_uv) mtv_macros.push_back({"UVELON_NO_UV", "1"});

    MeshPrimitive temp;

    VkPushConstantRange push_constant_range = {};
    push_constant_range.offset = 0;
    push_constant_range.size = sizeof(glm::mat4);
    push_constant_range.stageFlags = VK_SHADER_STAGE_ALL;
    PipelineH pipeline =
        PipelineBuilder::beginGraphics()
            .addShader(UVELON_FILE_RESOURCES("shaders/mtv.vert").string(),
                       VK_SHADER_STAGE_VERTEX_BIT, mtv_macros)
            .addShader(UVELON_FILE_RESOURCES("shaders/mtv.frag").string(),
                       VK_SHADER_STAGE_FRAGMENT_BIT, mtv_macros)
            .addPushConstant(push_constant_range)
            .addDescriptorSet(
                Renderer::getInstance().getGlobalDescriptorSet()->layout)
            .addDescriptorSet(material.descriptor_set->layout)
            .build();
    material.pipeline = pipeline;
    result.push_back(Renderer::getInstance().getRenderData().addMaterial(
        std::move(material)));
  }
  return result;
}

std::vector<std::shared_ptr<Mesh>> Loader::loadMeshes(
    const nlohmann::json& gltf_meshes,
    std::vector<std::shared_ptr<Accessor>>& accessors,
    std::vector<std::shared_ptr<BufferView>>& buffer_views,
    std::vector<std::shared_ptr<Buffer>>& buffers,
    std::vector<MaterialH>& materials) {
  std::vector<std::shared_ptr<Mesh>> result;

  // Material variatons with different accessor/bufferview layouts
  std::vector<std::vector<std::pair<VertexData, MaterialH>>>
      material_variations(materials.size());

  for (auto& gltf_mesh : gltf_meshes) {
    std::vector<MeshPrimitiveH> render_primitives;

    for (auto& gltf_primitive : gltf_mesh["primitives"]) {
      // TODO: If indices does not exist, still load it but generate indices
      // from accessor count
      if (gltf_primitive.contains("indices")) {
        MeshPrimitive mesh_primitive = {};
        mesh_primitive.indices =
            accessors.at(gltf_primitive["indices"])
                ->loadGPU(VK_BUFFER_USAGE_INDEX_BUFFER_BIT);
        mesh_primitive.index_count = static_cast<uint32_t>(
            accessors.at(gltf_primitive["indices"])->count);
        int index_component_type =
            accessors.at(gltf_primitive["indices"])->component_type;
        switch (index_component_type) {
          case 5121:
            mesh_primitive.index_type = VK_INDEX_TYPE_UINT8_EXT;
            break;
          case 5123:
            mesh_primitive.index_type = VK_INDEX_TYPE_UINT16;
            break;
          case 5125:
            mesh_primitive.index_type = VK_INDEX_TYPE_UINT32;
            break;
        }

        bool generate_tangents = true;
        VertexData vertex_data;

        auto gltf_attributes = gltf_primitive["attributes"];
        if (gltf_attributes.contains("POSITION"))
          vertex_data.setAttribute(VertexData::POSITION,
                                   accessors.at(gltf_attributes["POSITION"]));
        if (gltf_attributes.contains("NORMAL")) {
          vertex_data.setAttribute(VertexData::NORMAL,
                                   accessors.at(gltf_attributes["NORMAL"]));
        } else {
          generate_tangents = true;
        }
        if (gltf_attributes.contains("TANGENT")) {
          vertex_data.setAttribute(VertexData::TANGENT,
                                   accessors.at(gltf_attributes["TANGENT"]));
          generate_tangents = false;
        }
        if (gltf_attributes.contains("TEXCOORD_0"))
          vertex_data.setAttribute(VertexData::UV,
                                   accessors.at(gltf_attributes["TEXCOORD_0"]));

        // Check for another material with the same vertex data layout and reuse
        MaterialH mat_variation;
        int material_index;
        if (gltf_primitive.contains("material")) {
          material_index = gltf_primitive["material"];
          for (auto& variation : material_variations[material_index]) {
            if (variation.first == vertex_data) {
              mat_variation = variation.second;
              break;
            }
          }
        }
        // Store vertex data. If this is a new material variation, store it with
        // the data before tangent generation. On reuse, the tangents will also
        // be generated.
        VertexData vertex_data_variation_copy(
            vertex_data.buffers, vertex_data.binding_descriptions,
            vertex_data.attribute_descriptions);

        if (generate_tangents) {
          // Generate tangents
          // TODO: Make tangents optional, only generate if normal map exists
          TangentGenerator tangent_generator(
              accessors.at(gltf_primitive["indices"])->loadCPU(),
              std::move(vertex_data));
          TangentGenerator::Result result = tangent_generator.run();
          mesh_primitive.indices =
              Renderer::getInstance().getRenderData().allocateBuffer(
                  result.indices.raw_buffer.data(),
                  result.indices.raw_buffer.size(),
                  VK_BUFFER_USAGE_INDEX_BUFFER_BIT);
          mesh_primitive.index_type = VK_INDEX_TYPE_UINT32;
          mesh_primitive.index_count =
              static_cast<uint32_t>(result.indices.size<int>());
          vertex_data = std::move(result.vertex_data);
        } else {
          vertex_data.loadGPU();
        }

        mesh_primitive.vertex_buffers = vertex_data.buffers;
        mesh_primitive.binding_description = vertex_data.binding_descriptions;
        mesh_primitive.attribute_description =
            vertex_data.attribute_descriptions;

        if (gltf_primitive.contains("material")) {
          if (!mat_variation) {
            // No existing version of material matches the vertex
            // attribute/binding description
            MaterialH mat_template = materials[gltf_primitive["material"]];
            PipelineH pipeline =
                PipelineBuilder::fromExisting(*mat_template->pipeline)
                    .setVertexInputInfo(
                        vertex_data.binding_descriptions.data(),
                        static_cast<uint32_t>(
                            vertex_data.binding_descriptions.size()),
                        vertex_data.attribute_descriptions.data(),
                        static_cast<uint32_t>(
                            vertex_data.attribute_descriptions.size()))
                    .build();
            mat_variation = Renderer::getInstance().getRenderData().addMaterial(
                {mat_template->descriptor_set, pipeline});
            material_variations[material_index].emplace_back(
                std::move(vertex_data_variation_copy), mat_variation);
          }

          mesh_primitive.material = mat_variation;

#ifdef __APPLE__
          VkVertexInputAttributeDescription attribute_desc =
              mesh_primitive.material->pipeline->pipeline_data
                  .vertex_input_attributes[0];
          Material temp_mat;
          temp_mat.pipeline =
              static_cast<ShadowMapSystem&>(
                  Renderer::getInstance().getShadowMapSystem())
                  .createPipelineBuilder(Light::Type::DIR)
                  .setVertexInputInfo(
                      mesh_primitive.material->pipeline->pipeline_data
                          .vertex_input_bindings.data(),
                      static_cast<uint32_t>(
                          mesh_primitive.material->pipeline->pipeline_data
                              .vertex_input_bindings.size()),
                      &attribute_desc, 1)
                  .build();
          mesh_primitive.mat_sm_directional =
              Renderer::getInstance().getRenderData().addMaterial(
                  std::move(temp_mat));
          temp_mat = Material();
          temp_mat.pipeline =
              static_cast<ShadowMapSystem&>(
                  Renderer::getInstance().getShadowMapSystem())
                  .createPipelineBuilder(Light::Type::POINT)
                  .setVertexInputInfo(
                      mesh_primitive.material->pipeline->pipeline_data
                          .vertex_input_bindings.data(),
                      static_cast<uint32_t>(
                          mesh_primitive.material->pipeline->pipeline_data
                              .vertex_input_bindings.size()),
                      &attribute_desc, 1)
                  .build();
          mesh_primitive.mat_sm_cube =
              Renderer::getInstance().getRenderData().addMaterial(
                  std::move(temp_mat));
#endif
        }

        // Extras function
        if (ef_mesh_primitive && gltf_primitive.contains("extras"))
          ef_mesh_primitive(gltf_primitive, mesh_primitive, accessors,
                            buffer_views, buffers, materials);

        render_primitives.push_back(
            Renderer::getInstance().getRenderData().addMeshPrimitive(
                std::move(mesh_primitive)));
      } else {
        UVELON_LOG_ERROR("Mesh primitives without indices unsupported!");
      }
    }

    result.push_back(
        std::make_shared<Mesh>(gltf_mesh.value("name", ""), render_primitives));
  }

  return result;
}

// TODO: Test this method
std::vector<std::shared_ptr<Camera>> Loader::loadCameras(
    const nlohmann::json& gltf_cameras) {
  std::vector<std::shared_ptr<Camera>> cameras;
  for (auto& camera : gltf_cameras) {
    std::string type = camera.value("type", "");
    if (type == "perspective") {
      auto perspective = camera["perspective"];
      // TODO: Should use viewport aspect ratio by default
      float aspect = perspective.value<float>("aspectRatio", 1.0f);
      if (perspective.contains("zfar")) {
        cameras.push_back(std::make_shared<PerspectiveCamera>(
            aspect, perspective["yfov"], perspective["znear"],
            perspective["zfar"]));
      } else {
        cameras.push_back(std::make_shared<PerspectiveCamera>(
            aspect, perspective["yfov"], perspective["znear"]));
      }
    } else if (type == "orthographic") {
      auto orthographic = camera["orthographic"];

      // This part of the glTF spec is nonsense...
      // xmag/ymag describe half width/height, which is just great with
      // different resolutions and aspect ratios
      cameras.push_back(std::make_shared<OrthographicCamera>(
          orthographic["xmag"], orthographic["ymag"], orthographic["znear"],
          orthographic["zfar"]));
    } else {
      UVELON_LOG_WARNING("Unknown camera type: " + type);
    }
  }

  return cameras;
}

std::vector<std::shared_ptr<Light>> Loader::loadLights(
    const nlohmann::json& gltf) {
  std::vector<std::shared_ptr<Light>> result;
  if (gltf.contains("extensions")) {
    if (gltf["extensions"].contains("KHR_lights_punctual")) {
      if (gltf["extensions"]["KHR_lights_punctual"].contains("lights")) {
        // Let there be light
        auto gltf_lights = gltf["extensions"]["KHR_lights_punctual"]["lights"];
        for (auto& gltf_light : gltf_lights) {
          result.push_back(std::make_shared<Light>());
          std::string type = gltf_light["type"];
          result.back()->type =
              type == "directional"
                  ? Light::Type::DIR
                  : (type == "spot" ? Light::Type::SPOT : Light::Type::POINT);
          std::vector<float> color =
              gltf_light.value("color", std::vector<float>{1.0f, 1.0f, 1.0f});
          result.back()->color = glm::vec3(color[0], color[1], color[2]);
          result.back()->intensity = gltf_light.value("intensity", 1.0f);
          if (gltf_light.contains("spot")) {
            float inner_cone_angle =
                gltf_light["spot"].value("innerConeAngle", 0.0f);
            float outer_cone_angle = gltf_light["spot"].value(
                "outerConeAngle", glm::pi<float>() / 4.0f);

            // Reference code:
            // https://github.com/KhronosGroup/glTF/blob/main/extensions/2.0/Khronos/KHR_lights_punctual/README.md
            result.back()->setSpotConeAngles(inner_cone_angle,
                                             outer_cone_angle);
          }
        }
      }
    }
  }
  return result;
}

std::vector<std::shared_ptr<Animation>> Loader::loadAnimations(
    const nlohmann::json& gltf_animations,
    std::vector<std::shared_ptr<Accessor>>& accessors,
    std::vector<std::shared_ptr<Node>>& nodes) {
  std::vector<std::shared_ptr<Animation>> result;
  for (auto gltf_anim : gltf_animations) {
    auto animation = std::make_shared<Animation>();
    animation->name_ = gltf_anim.value("name", "");
    if (gltf_anim.contains("samplers")) {
      for (auto gltf_sampler : gltf_anim["samplers"]) {
        std::string interpolation_str =
            gltf_sampler.value("interpolation", "LINEAR");
        Animation::Sampler::Interpolation interpolation =
            Animation::Sampler::Interpolation::LINEAR;
        if (interpolation_str == "STEP")
          interpolation = Animation::Sampler::Interpolation::STEP;
        else if (interpolation_str == "CUBICSPLINE")
          interpolation = Animation::Sampler::Interpolation::CUBICSPLINE;

        auto input = accessors[gltf_sampler["input"]];
        auto output = accessors[gltf_sampler["output"]];
        animation->samplers_.emplace_back(input->loadCPU(), interpolation,
                                          output->loadCPU(), output->type,
                                          output->component_type);
      }
    }
    if (gltf_anim.contains("channels")) {
      for (auto gltf_channel : gltf_anim["channels"]) {
        int sampler = gltf_channel["sampler"];
        auto gltf_target = gltf_channel["target"];
        if (!gltf_target.contains("node")) continue;

        std::string path_str = gltf_target["path"];
        Animation::Channel::Path path;
        if (path_str == "translation")
          path = Animation::Channel::Path::TRANSLATION;
        else if (path_str == "rotation")
          path = Animation::Channel::Path::ROTATION;
        else if (path_str == "scale")
          path = Animation::Channel::Path::SCALE;
        else if (path_str == "weights")
          path = Animation::Channel::Path::WEIGHTS;

        animation->channels_.emplace_back(
            sampler, std::weak_ptr(nodes[gltf_target["node"]]), path);
      }
    }
    animation->loop_ = true;
    result.push_back(animation);
  }
  return result;
}

std::vector<std::shared_ptr<Node>> Loader::loadNodes(
    const nlohmann::json& gltf_nodes,
    const std::vector<std::shared_ptr<Mesh>>& meshes,
    const std::vector<std::shared_ptr<Camera>>& cameras,
    const std::vector<std::shared_ptr<Light>>& lights) {
  std::vector<std::shared_ptr<Node>> nodes;

  std::vector<std::pair<int, int>> node_links;
  int index = 0;
  for (auto& gltf_node : gltf_nodes) {
    std::string name = gltf_node.value("name", "");

    nodes.push_back(std::make_shared<Node>(name));

    if (gltf_node.contains("mesh"))
      nodes.back()->setMesh(meshes[gltf_node["mesh"]]);
    else if (gltf_node.contains("camera"))
      nodes.back()->setCamera(cameras[gltf_node["camera"]]);
    else if (gltf_node.contains(nlohmann::json::json_pointer(
                 "/extensions/KHR_lights_punctual/light")))
      nodes.back()->setLight(lights[gltf_node[nlohmann::json::json_pointer(
          "/extensions/KHR_lights_punctual/light")]]);

    glm::vec3 translation(0.0f, 0.0f, 0.0f);
    glm::quat rotation(1.0f, 0.0f, 0.0f, 0.0f);
    glm::vec3 scale(1.0f, 1.0f, 1.0f);

    // Matrix or TRS
    if (gltf_node.contains("matrix")) {
      // Matrix
      auto matrix = gltf_node.at("matrix");
      glm::mat4 glm_matrix;
      for (int y = 0; y < 4; y++) {
        for (int x = 0; x < 4; x++) {
          glm_matrix[x][y] = matrix[y * 4 + x];
        }
      }
      // Decompose to TRS
      glm::vec3 skew;
      glm::vec4 perspective;
      glm::decompose(glm_matrix, scale, rotation, translation, skew,
                     perspective);
      rotation.w *= -1.0f;
    } else {
      // TRS
      if (gltf_node.contains("translation")) {
        auto gltf_translation = gltf_node["translation"];
        translation = glm::vec3(gltf_translation[0], gltf_translation[1],
                                gltf_translation[2]);
      }
      if (gltf_node.contains("rotation")) {
        auto gltf_rotation = gltf_node["rotation"];
        // glTF order: xyzw, glm order: wxyz
        rotation = glm::quat(gltf_rotation[3], gltf_rotation[0],
                             gltf_rotation[1], gltf_rotation[2]);
      }
      if (gltf_node.contains("scale")) {
        auto gltf_scale = gltf_node["scale"];
        scale = glm::vec3(gltf_scale[0], gltf_scale[1], gltf_scale[2]);
      }
    }

    auto& transform = nodes.back()->getTransform();
    transform.setTranslation(translation);
    transform.setRotation(rotation);
    transform.setScale(scale);

    if (gltf_node.contains("children")) {
      // Note hierarchy to create once all nodes are created
      for (auto gltf_child : gltf_node["children"]) {
        node_links.emplace_back(index, gltf_child);
      }
    }

    index++;
  }

  // Create hierarchy
  for (auto& link : node_links) {
    Node::attach(nodes[link.first], nodes[link.second]);
  }

  return nodes;
}

std::shared_ptr<Node> Loader::loadSceneRoot(
    const nlohmann::json& gltf, const std::string& path,
    const std::vector<std::shared_ptr<Node>>& nodes, int& scene) {
  auto scene_node = std::make_shared<Node>(path);
  if (scene < 0) {
    if (gltf.contains("scene")) {
      scene = gltf["scene"];
    } else {
      UVELON_LOG_WARNING(
          "No default scene in glTF file and no scene to load selected");
    }
  }
  if (scene >= 0 && gltf.contains("scenes")) {
    auto gltf_root = std::make_shared<Node>("glTF");
    // Convert coordinate system
    gltf_root->getTransform().setRotation(glm::quat(0.0f, 1.0f, 0.0f, 0.0f));
    Node::attach(scene_node, gltf_root);

    auto& scene_nodes = gltf["scenes"][scene]["nodes"];
    for (auto& node : scene_nodes) {
      Node::attach(gltf_root, nodes[node]);
    }
  }
  return scene_node;
}

void Loader::fixWindingOrder(std::shared_ptr<Node> node, bool update) {
  /* When a mesh primitive uses any triangle-based topology (i.e., triangles,
     triangle strip, or triangle fan), the determinant of the node’s global
     transform defines the winding order of that primitive. If the determinant
     is a positive value, the winding order triangle faces is
     counterclockwise; in the opposite case, the winding order is clockwise.
     https://registry.khronos.org/glTF/specs/2.0/glTF-2.0.html*/
  if (update) node->update();
  auto mesh = node->getMesh();
  if (mesh) {
    if (glm::determinant(node->getTransform().getModelMatrix()) < 0) {
      for (auto primitive : mesh->getPrimitives()) {
        // Recreate pipeline with clockwise winding order
        PipelineH old_pipeline = primitive->material->pipeline;
        primitive->material->pipeline =
            PipelineBuilder::fromExisting(*old_pipeline)
                .setFrontFace(VK_FRONT_FACE_CLOCKWISE)
                .build();
        old_pipeline->layout = VK_NULL_HANDLE;
      }
    }
  }
  for (auto child : node->getChildren()) {
    fixWindingOrder(child, false);
  }
}

}  // namespace Uvelon