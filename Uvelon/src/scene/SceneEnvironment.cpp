#include "scene/SceneEnvironment.hpp"

#include <filesystem>

#include "core/FileWizard.hpp"
#include "rendering/Descriptor.hpp"
#include "rendering/GBuffer.hpp"
#include "rendering/Pipeline.hpp"
#include "rendering/RenderData.hpp"
#include "rendering/Renderer.hpp"
#include "scene/DDS.hpp"
#include "utils/Logger.hpp"
#include "utils/MappedFile.hpp"

namespace Uvelon {

SceneEnvironment::SceneEnvironment(const std::string& folder_path) {
  Environment environment;

  for (auto& it : std::filesystem::directory_iterator(folder_path)) {
    Uvelon::MappedFile file(it.path().string());
    bool is_dds =
        Uvelon::DDS::is_dds_file(static_cast<char*>(file.data), file.size);
    if (is_dds) {  // TODO: Decide whether we need this file
      bool is_skybox =
          it.path().string().find("EnvHDR.dds") != std::string::npos;
      bool is_diffuse =
          it.path().string().find("DiffuseHDR.dds") != std::string::npos;
      bool is_specular =
          it.path().string().find("SpecularHDR.dds") != std::string::npos;
      bool is_brdf = it.path().string().find("Brdf.dds") != std::string::npos;

      if (is_skybox || is_diffuse || is_specular || is_brdf) {
        Uvelon::DDS::read_dds_data dds_data = {};
        bool success = Uvelon::DDS::load_dds_from_file(*file.stream, dds_data);

        if (success) {
          // TODO: Other formats?
          if (dds_data.format.type != DDS::ResourceFormatType::Regular ||
              dds_data.format.compType != DDS::CompType::Float ||
              dds_data.format.compCount != 4 ||
              dds_data.format.compByteWidth != 4) {
            UVELON_LOG_ERROR(
                "Unsupported file format for environment: type=" +
                std::to_string(static_cast<uint8_t>(dds_data.format.type)) +
                ", compType=" +
                std::to_string(static_cast<uint8_t>(dds_data.format.compType)) +
                ", compCount=" + std::to_string(dds_data.format.compCount) +
                ", compByteWidth=" +
                std::to_string(dds_data.format.compByteWidth));
            return;
          }

          // https://learn.microsoft.com/en-us/windows/win32/direct3ddds/dds-file-layout-for-cubic-environment-maps
          std::vector<VkBufferImageCopy> regions;
          uint32_t subresource_index = 0;
          for (uint32_t layer = 0; layer < dds_data.slices; layer++) {
            for (uint32_t mip = 0; mip < dds_data.mips; mip++) {
              auto& offset_size = dds_data.subresources[subresource_index];

              VkImageSubresourceLayers subresource = {};
              subresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
              subresource.mipLevel = mip;
              subresource.layerCount = 1;
              subresource.baseArrayLayer = layer;

              VkBufferImageCopy region = {};
              region.bufferOffset = offset_size.first;
              region.bufferRowLength = 0;
              region.bufferImageHeight = 0;
              region.imageSubresource = subresource;
              region.imageOffset = {0, 0, 0};
              region.imageExtent = {dds_data.width >> mip,
                                    dds_data.height >> mip, dds_data.depth};

              regions.push_back(region);
              subresource_index++;
            }
          }
          ImageH img = Renderer::getInstance().getRenderData().addImage(
              Image(dds_data.buffer.data(),
                    static_cast<uint64_t>(dds_data.buffer.size()),
                    {dds_data.width, dds_data.height, dds_data.depth},
                    VK_FORMAT_R32G32B32A32_SFLOAT, VK_IMAGE_USAGE_SAMPLED_BIT,
                    dds_data.slices, dds_data.mips, regions, dds_data.cubemap));
          if (is_skybox)
            environment.skybox = img;
          else if (is_diffuse)
            environment.diffuse = img;
          else if (is_specular)
            environment.specular = img;
          else if (is_brdf)
            environment.brdf = img;
        }
      }
    }
  }

  if (environment.skybox) {
    Material material;
    SamplerH sampler = Renderer::getInstance().getRenderData().createSampler(
        VK_FILTER_LINEAR, VK_FILTER_LINEAR, VK_SAMPLER_MIPMAP_MODE_LINEAR,
        VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER,
        VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER);
    material.descriptor_set =
        DescriptorBuilder::begin()
            .addCombinedImageSampler(
                VK_SHADER_STAGE_ALL_GRAPHICS, environment.skybox,
                VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, sampler)
            .build();

    std::array<VkFormat, 2> formats = {
        Renderer::getInstance().getSwapchain().getImageFormat(),
        Renderer::getInstance().getGBuffer().getLinearFrame()->getFormat()};
    material.pipeline =
        PipelineBuilder::beginGraphics()
            .addDescriptorSet(
                Renderer::getInstance().getGlobalDescriptorSet()->layout)
            .addDescriptorSet(material.descriptor_set->layout)
            .addShader(UVELON_FILE_RESOURCES("shaders/skybox.vert").string(),
                       VK_SHADER_STAGE_VERTEX_BIT, {})
            .addShader(UVELON_FILE_RESOURCES("shaders/skybox.frag").string(),
                       VK_SHADER_STAGE_FRAGMENT_BIT, {})
            .setDepthTest(false, false)
            .setVertexInputInfo(nullptr, 0, nullptr, 0)
            .setAttachmentFormats(formats.data(), 2, VK_FORMAT_UNDEFINED)
            .build();

    environment.skybox_material =
        Renderer::getInstance().getRenderData().addMaterial(
            std::move(material));
  }

  environment_ = Renderer::getInstance().getRenderData().addEnvironment(
      std::move(environment));
}

}  // namespace Uvelon