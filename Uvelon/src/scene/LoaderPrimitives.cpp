#include "scene/LoaderPrimitives.hpp"

#include <set>

#include "core/BufferCPU.hpp"
#include "rendering/RenderData.hpp"
#include "rendering/Renderer.hpp"
#include "stb_image.h"

namespace Uvelon {

BufferH Accessor::loadGPU(VkBufferUsageFlags usage) {
  char* raw_buffer = view->buffer->data;
  raw_buffer += view->offset + offset;
  switch (view->target) {
    case 34962:
      usage |= VK_BUFFER_USAGE_VERTEX_BUFFER_BIT;
      break;
    case 34963:
      usage |= VK_BUFFER_USAGE_INDEX_BUFFER_BIT;
      break;
  }
  return Renderer::getInstance().getRenderData().allocateBuffer(raw_buffer,
                                                                size, usage);
}

BufferCPU Accessor::loadCPU() {
  char* raw_buffer = view->buffer->data;
  raw_buffer += view->offset + offset;
  BufferCPU buffer(component_type, size);
  memcpy(buffer.raw_buffer.data(), raw_buffer, size);
  return buffer;
}

void VertexData::setAttribute(VertexData::Attribute attribute,
                              std::shared_ptr<Accessor> accessor) {
  assert(!immutable_);
  accessors_[attribute] = accessor;
  generateDescriptions();
}

bool VertexData::containsAttribute(Attribute attribute) {
  assert(!data_only_);
  return !!accessors_[attribute];
}

void VertexData::generateDescriptions() {
  assert(!data_only_);
  binding_descriptions.clear();
  attribute_descriptions.clear();

  std::set<std::shared_ptr<BufferView>> views;
  uint32_t binding;
  uint32_t index = 0, last_valid_index = 0;
  for (std::shared_ptr<Accessor> accessor : accessors_) {
    if (accessor) {
      auto result = views.insert(accessor->view);
      if (result.second) {
        binding = static_cast<uint32_t>(binding_descriptions.size());
        uint32_t stride = static_cast<uint32_t>(accessor->view->stride);
        if (!stride)
          stride = static_cast<uint32_t>(accessor->size / accessor->count);
        binding_descriptions.push_back(
            {binding, stride, VK_VERTEX_INPUT_RATE_VERTEX});
      } else {
        binding =
            static_cast<uint32_t>(std::distance(views.begin(), result.first));
      }
      attribute_descriptions.push_back(
          {index, binding, formats[index],
           static_cast<uint32_t>(accessor->offset)});
      last_valid_index = index;
    } else {
      // Preserve the correct slots by inserting invalid descriptions. This
      // indicates missing attributes.
      attribute_descriptions.push_back({0, 0, VK_FORMAT_UNDEFINED, 0});
    }
    index++;
  }

  // Remove the tail of invalid attribute descriptions to correct the size
  if (last_valid_index == 0 && attribute_descriptions.size() > 0 &&
      attribute_descriptions[0].format == VK_FORMAT_UNDEFINED) {
    attribute_descriptions.clear();
  } else {
    attribute_descriptions.resize(last_valid_index + 1);
  }
}

void VertexData::loadGPU() {
  assert(!data_only_);
  immutable_ = true;
  std::set<std::shared_ptr<BufferView>> views;
  for (std::shared_ptr<Accessor> accessor : accessors_) {
    if (accessor) {
      auto result = views.insert(accessor->view);
      if (result.second) {
        char* raw_buffer = accessor->view->buffer->data;
        raw_buffer += accessor->view->offset;
        buffers.push_back(
            Renderer::getInstance().getRenderData().allocateBuffer(
                raw_buffer, accessor->view->length,
                VK_BUFFER_USAGE_VERTEX_BUFFER_BIT));
      }
    }
  }
}

void VertexData::loadCPU() {
  assert(!data_only_);
  immutable_ = true;
  std::set<std::shared_ptr<BufferView>> views;
  for (std::shared_ptr<Accessor> accessor : accessors_) {
    if (accessor) {
      auto result = views.insert(accessor->view);
      if (result.second) {
        char* raw_buffer = accessor->view->buffer->data;
        raw_buffer += accessor->view->offset;
        views_cpu_.emplace_back(accessor->view->length);
        memcpy(views_cpu_.back().raw_buffer.data(), raw_buffer,
               accessor->view->length);
      }
    }
  }
}

void VertexData::getEntry(VertexData::Attribute attribute, uint32_t index,
                          float* result) {
  assert(!binding_descriptions.empty() && !views_cpu_.empty());

  auto& attribute_description = attribute_descriptions[attribute];
  auto& binding_description =
      binding_descriptions[attribute_description.binding];

  uint32_t byte_offset =
      attribute_description.offset + binding_description.stride * index;

  float* first = reinterpret_cast<float*>(
      views_cpu_[attribute_description.binding].raw_buffer.data() +
      byte_offset);
  memcpy(result, first, num_floats[attribute] * sizeof(float));
}

uint32_t VertexData::getNumEntries(Attribute attribute) {
  assert(!views_cpu_.empty());
  if (!accessors_[attribute]) return 0;
  return static_cast<uint32_t>(accessors_[attribute]->count);
}

ImageData::ImageData(ImageData&& o)
    : data(o.data),
      extent(o.extent),
      size(o.size),
      usage(o.usage),
      image_gpu(o.image_gpu) {
  o.data = nullptr;
}

ImageData& ImageData::operator=(ImageData&& o) {
  data = o.data;
  extent = o.extent;
  size = o.size;
  usage = o.usage;
  image_gpu = o.image_gpu;
  o.data = nullptr;
  return *this;
}

ImageH ImageData::bakeFormat(VkFormat format) {
  if (image_gpu) return image_gpu;
  assert(data);
  image_gpu = Renderer::getInstance().getRenderData().addImage(
      Image(data, size, extent, format, usage, true));
  stbi_image_free(data);
  data = nullptr;
  return image_gpu;
}

ImageData::~ImageData() {
  if (data) stbi_image_free(data);
}

}  // namespace Uvelon