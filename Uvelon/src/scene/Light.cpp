#include "scene/Light.hpp"

#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#define GLM_FORCE_LEFT_HANDED
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/glm.hpp>
#include <glm/gtx/euler_angles.hpp>

#include "rendering/RenderData.hpp"
#include "rendering/Renderer.hpp"
#include "scene/Node.hpp"

namespace Uvelon {

Light::Light() {
  setSpotConeAngles(spot_inner_cone_angle_, spot_outer_cone_angle_);
}

void Light::setSpotConeAngles(float inner, float outer) {
  spot_inner_cone_angle_ = inner;
  spot_outer_cone_angle_ = outer;

  spot_angle_scale_ =
      1.0f / std::max(0.001f, std::cos(spot_inner_cone_angle_) -
                                  std::cos(spot_outer_cone_angle_));
  spot_angle_offset_ = -std::cos(spot_outer_cone_angle_) * spot_angle_scale_;
}

LightPrimitive Light::constructPrimitive() {
  LightPrimitive primitive;
  // TODO: This uses a translation and rotation separate from the node, to allow
  // for modification of the rotation using euler. This is obviously
  // not great, especially when using a light imported through glTF.
  primitive.model_matrix =
      glm::translate(glm::mat4(1.0f), translation) *
      parent_.lock()->getTransform().getModelMatrix() *
      glm::eulerAngleYXZ(rotation.y, rotation.x, rotation.z);
  primitive.color = color;
  primitive.type = static_cast<uint32_t>(type);
  primitive.intensity = intensity;
  primitive.spot_light_angle_scale = spot_angle_scale_;
  primitive.spot_light_angle_offset = spot_angle_offset_;
  if (shadows) {
    if (!shadow_map_ || type != shadow_map_type_) {
      bool cube = type == Type::POINT || type == Type::SPOT;
      uint32_t dim = cube ? 1024 : 2048;
      Image image = Image({dim, dim, 1}, VK_FORMAT_D16_UNORM,
                          VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT |
                              VK_IMAGE_USAGE_SAMPLED_BIT,
                          cube ? 6 : 1, 1, cube);
      shadow_map_ =
          Renderer::getInstance().getRenderData().addImage(std::move(image));
      shadow_map_type_ = type;
    }
    primitive.shadow_map = shadow_map_;
  }
  return primitive;
}

}  // namespace Uvelon