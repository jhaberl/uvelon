#include "scene/Animation.hpp"

#include "scene/Node.hpp"
#include "utils/Logger.hpp"

namespace Uvelon {

void Animation::update(float delta) {
  if (active_) {
    time_ += delta / 1000.0f * speed_;

    checkForEnd();

    auto it = channels_.begin();
    while (it != channels_.end()) {
      auto target_node = it->target_node.lock();
      if (target_node) {
        updateChannel(*it, target_node);
        it++;
      } else {
        it = channels_.erase(it);
      }
    }
  }
}

void Animation::checkForEnd() {
  bool all_ended = true;
  for (auto channel : channels_) {
    auto& sampler = samplers_[channel.sampler_index];
    if (time_ < sampler.input.at<float>(sampler.input.size<float>() - 1)) {
      all_ended = false;
    }
  }
  if (all_ended) {
    if (loop_)
      setTime(0.0f);
    else
      setActive(false);
  }
}

float Animation::getLength() {
  float length = 0.0f;
  for (auto& sampler : samplers_) {
    length = std::max(length,
                      sampler.input.at<float>(sampler.input.size<float>() - 1));
  }
  return length;
}

void Animation::updateChannel(Channel& channel,
                              std::shared_ptr<Node> target_node) {
  Sampler& sampler = samplers_[channel.sampler_index];
  if (sampler.component_type != 5126) return;

  float result[4];

  // Find previous and next time in input
  bool in_range = false;
  size_t index_lower = 0, index_upper = 1;
  size_t num_keyframes = sampler.input.size<float>();
  if (num_keyframes == 0) return;
  float time_lower, time_upper;

  for (size_t i = 0; i < num_keyframes - 1; i++) {
    float this_time = sampler.input.at<float>(i);
    float next_time = sampler.input.at<float>(i + 1);
    if (time_ >= this_time && time_ < next_time) {
      time_lower = this_time;
      time_upper = next_time;
      index_lower = i;
      index_upper = i + 1;
      in_range = true;
      break;
    }
  }
  if (!in_range) return;  // TODO: Set start/end data

  if (channel.target_path == Channel::Path::ROTATION &&
      sampler.interpolation == Sampler::Interpolation::LINEAR) {
    // Slerp
    float interpolation_value =
        (time_ - time_lower) / (time_upper - time_lower);
    glm::quat previous_quat;
    previous_quat[0] = sampler.output.at<float>(index_lower * sampler.type + 0);
    previous_quat[1] = sampler.output.at<float>(index_lower * sampler.type + 1);
    previous_quat[2] = sampler.output.at<float>(index_lower * sampler.type + 2);
    previous_quat[3] = sampler.output.at<float>(index_lower * sampler.type + 3);

    glm::quat next_quat;
    next_quat[0] = sampler.output.at<float>(index_upper * sampler.type + 0);
    next_quat[1] = sampler.output.at<float>(index_upper * sampler.type + 1);
    next_quat[2] = sampler.output.at<float>(index_upper * sampler.type + 2);
    next_quat[3] = sampler.output.at<float>(index_upper * sampler.type + 3);

    glm::quat slerped =
        glm::slerp(previous_quat, next_quat, interpolation_value);
    target_node->getTransform().setRotation(slerped);
  } else {
    for (int component = 0; component < sampler.type; component++) {
      switch (sampler.interpolation) {
        case Sampler::Interpolation::LINEAR: {
          float interpolation_value =
              (time_ - time_lower) / (time_upper - time_lower);
          float previous_point =
              sampler.output.at<float>(index_lower * sampler.type + component);
          float next_point =
              sampler.output.at<float>(index_upper * sampler.type + component);
          result[component] =
              previous_point +
              interpolation_value * (next_point - previous_point);
          break;
        }
        case Sampler::Interpolation::STEP: {
          result[component] =
              sampler.output.at<float>(index_lower * sampler.type + component);
          break;
        }
        case Sampler::Interpolation::CUBICSPLINE: {
          float keyframe_delta = time_upper - time_lower;
          size_t offset_lower = index_lower * sampler.type * 3;
          size_t offset_upper = index_upper * sampler.type * 3;
          size_t offset_in_tangent = 0, offset_point = sampler.type,
                 offset_out_tangent = sampler.type * 2;
          float previous_tangent =
              keyframe_delta *
              sampler.output.at<float>(offset_lower + offset_out_tangent +
                                       component);
          float next_tangent =
              keyframe_delta *
              sampler.output.at<float>(offset_upper + offset_in_tangent +
                                       component);
          float previous_point =
              sampler.output.at<float>(offset_lower + offset_point + component);
          float next_point =
              sampler.output.at<float>(offset_upper + offset_point + component);

          float t = (time_ - time_lower) / (time_upper - time_lower);
          float t2 = t * t;
          float t3 = t2 * t;

          result[component] = (2 * t3 - 3 * t2 + 1) * previous_point +
                              (t3 - 2 * t2 + t) * previous_tangent +
                              (-2 * t3 + 3 * t2) * next_point +
                              (t3 - t2) * next_tangent;
          break;
        }
      }
    }

    // Set property on target
    switch (channel.target_path) {
      case Channel::Path::TRANSLATION: {
        target_node->getTransform().setTranslation(
            glm::vec3(result[0], result[1], result[2]));
        break;
      }
      case Channel::Path::ROTATION: {
        target_node->getTransform().setRotation(glm::normalize(
            glm::quat(result[3], result[0], result[1], result[2])));
        break;
      }
      case Channel::Path::SCALE: {
        target_node->getTransform().setScale(
            glm::vec3(result[0], result[1], result[2]));
        break;
      }
      case Channel::Path::WEIGHTS: {
        // TODO
        break;
      }
    }
  }
}

}  // namespace Uvelon