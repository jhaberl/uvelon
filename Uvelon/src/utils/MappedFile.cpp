#include "utils/MappedFile.hpp"

#include "utils/Logger.hpp"

#ifdef WIN32
#include <windows.h>

#include <climits>
#undef ERROR  // definition from wingi.h breaks logging
#elif defined(__unix__) || (defined(__APPLE__) && defined(__MACH__))
#include <errno.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>
#endif

namespace Uvelon {

MappedFile::MappedFile(const std::string& path, bool write, uint64_t size)
    : path_(path), write_(write), size(size) {
  UVELON_LOG_DEBUG("Open file " + path_);
  init();

  if (data) {
#ifdef _MSC_VER
    if (this->size < INT_MAX)
#else
    if (true)
#endif
    {
      stream_buffer_ = std::make_unique<MemoryBuffer>(
          static_cast<char*>(data), static_cast<char*>(data) + this->size);
      stream = std::make_unique<std::istream>(stream_buffer_.get());
    } else {
      UVELON_LOG_WARNING("MappedFile stream unavailable due to file size!");
    }
  }
}

MappedFile::MappedFile(MappedFile&& o)
    : path_(o.path_), write_(o.write_), size(o.size) {
  file_handle_ = o.file_handle_;
  file_mapping_ = o.file_mapping_;
  fd_ = o.fd_;
  stream_buffer_ = std::move(o.stream_buffer_);
  stream = std::move(o.stream);

  o.file_handle_ = nullptr;
  o.file_mapping_ = nullptr;
  o.fd_ = 0;
}

MappedFile& MappedFile::operator=(MappedFile&& o) { return o; }

#ifdef WIN32
void MappedFile::init() {
  file_handle_ = CreateFileA(
      path_.c_str(), write_ ? GENERIC_READ | GENERIC_WRITE : GENERIC_READ, 0,
      nullptr, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, nullptr);
  if (GetLastError()) {
    logError();
    return;
  }
  if (!size) {
    DWORD size_high;
    DWORD size_low = GetFileSize(file_handle_, &size_high);
    if (GetLastError()) {
      logError();
      return;
    }
    size = (static_cast<uint64_t>(size_high) << 32) | size_low;
  }
  file_mapping_ = CreateFileMappingA(file_handle_, nullptr,
                                     write_ ? PAGE_READWRITE : PAGE_READONLY, 0,
                                     0, nullptr);
  if (GetLastError()) {
    logError();
    return;
  }
  data = MapViewOfFile(file_mapping_,
                       write_ ? FILE_MAP_ALL_ACCESS : FILE_MAP_READ, 0, 0, 0);
  if (GetLastError()) {
    logError();
    return;
  }
}
#elif defined(__unix__) || (defined(__APPLE__) && defined(__MACH__))
void MappedFile::init() {
  fd_ = open(path_.c_str(), write_ ? O_RDWR : O_RDONLY);
  if (fd_ == -1) {
    logError();
    return;
  }
  struct stat file_stats;
  int result = fstat(fd_, &file_stats);
  if (result == -1) {
    logError();
    return;
  }
  if (!size) size = static_cast<size_t>(file_stats.st_size);
  data = mmap(nullptr, size, write_ ? PROT_READ | PROT_WRITE : PROT_READ,
              MAP_SHARED, fd_, 0);
  if (data == MAP_FAILED) {
    data = nullptr;
    logError();
    return;
  }
}
#endif

void MappedFile::logError() {
  char* error_message = nullptr;
#ifdef WIN32
  FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM, 0,
                GetLastError(), 0, (LPSTR)&error_message, 0, nullptr);
#elif defined(__unix__) || (defined(__APPLE__) && defined(__MACH__))
  error_message = strerror(errno);
#endif

  UVELON_LOG_ERROR("Memory map failed (" + path_ +
                   "): " + std::string(error_message));

#ifdef WIN32
  LocalFree(error_message);
#endif
}

MappedFile::~MappedFile() {
  UVELON_LOG_DEBUG("Close file " + path_);
  stream.reset();
  stream_buffer_.reset();
#ifdef WIN32
  if (data) UnmapViewOfFile(data);
  if (file_mapping_) CloseHandle(file_mapping_);
  if (file_handle_) CloseHandle(file_handle_);
#elif defined(__unix__) || (defined(__APPLE__) && defined(__MACH__))
  if (data) munmap(data, size);
  if (fd_) close(fd_);
#endif
}

// Memory buffer

MemoryBuffer::MemoryBuffer(char* begin, char* end) { setg(begin, begin, end); }

MemoryBuffer::pos_type MemoryBuffer::seekoff(off_type _Off,
                                             std::ios_base::seekdir _Way,
                                             std::ios_base::openmode _Mode) {
  switch (_Way) {
    case std::ios_base::beg:
      setg(eback(), eback() + _Off, egptr());
      break;
    case std::ios_base::end:
      setg(eback(), egptr() - _Off, egptr());
      break;
    case std::ios_base::cur:
      gbump(static_cast<int>(_Off));
      break;
  }
  return gptr() - eback();
}

MemoryBuffer::pos_type MemoryBuffer::seekpos(pos_type _Pos,
                                             std::ios_base::openmode _Mode) {
  return seekoff(_Pos, std::ios_base::beg, _Mode);
}

}  // namespace Uvelon