#include "utils/TangentGenerator.hpp"

#include "utils/Logger.hpp"
#include "weldmesh.h"

#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#define GLM_FORCE_LEFT_HANDED
#include <glm/glm.hpp>

namespace Uvelon {

TangentGenerator::TangentGenerator(BufferCPU&& indices,
                                   VertexData&& vertex_data)
    : indices_(std::move(indices)),
      in_vertex_data_(std::move(vertex_data)),
      tangents_(getNumFaces() * 3 * 4 * sizeof(float)) {
  interface_.m_getNumFaces = [](const SMikkTSpaceContext* pContext) {
    return static_cast<TangentGenerator*>(pContext->m_pUserData)->getNumFaces();
  };
  interface_.m_getNumVerticesOfFace = [](const SMikkTSpaceContext* pContext,
                                         const int iFace) {
    return static_cast<TangentGenerator*>(pContext->m_pUserData)
        ->getNumVerticesOfFace(iFace);
  };
  interface_.m_getPosition = [](const SMikkTSpaceContext* pContext,
                                float fvPosOut[], const int iFace,
                                const int iVert) {
    return static_cast<TangentGenerator*>(pContext->m_pUserData)
        ->getPosition(fvPosOut, iFace, iVert);
  };
  interface_.m_getNormal = [](const SMikkTSpaceContext* pContext,
                              float fvNormOut[], const int iFace,
                              const int iVert) {
    return static_cast<TangentGenerator*>(pContext->m_pUserData)
        ->getNormal(fvNormOut, iFace, iVert);
  };
  interface_.m_getTexCoord = [](const SMikkTSpaceContext* pContext,
                                float fvTexcOut[], const int iFace,
                                const int iVert) {
    return static_cast<TangentGenerator*>(pContext->m_pUserData)
        ->getTexCoord(fvTexcOut, iFace, iVert);
  };
  interface_.m_setTSpaceBasic = [](const SMikkTSpaceContext* pContext,
                                   const float fvTangent[], const float fSign,
                                   const int iFace, const int iVert) {
    return static_cast<TangentGenerator*>(pContext->m_pUserData)
        ->setTSpaceBasic(fvTangent, fSign, iFace, iVert);
  };
  interface_.m_setTSpace = nullptr;
}

TangentGenerator::Result TangentGenerator::run() {
  TangentGenerator::Result result;

  in_vertex_data_.loadCPU();

  SMikkTSpaceContext context;
  context.m_pInterface = &interface_;
  context.m_pUserData = this;
  bool success = genTangSpaceDefault(&context);
  if (!success) {
    UVELON_LOG_ERROR("Tangent generation failed!");
    return {};
  }

  bool available[3] = {in_vertex_data_.getNumEntries(VertexData::POSITION) > 0,
                       in_vertex_data_.getNumEntries(VertexData::NORMAL) > 0,
                       in_vertex_data_.getNumEntries(VertexData::UV) > 0};

  int num_vertices = getNumFaces() * 3;
  VertexData temp;
  int num_floats = (available[0] ? temp.num_floats[VertexData::POSITION] : 0) +
                   (available[1] ? temp.num_floats[VertexData::NORMAL] : 0) +
                   temp.num_floats[VertexData::TANGENT] +
                   (available[2] ? temp.num_floats[VertexData::UV] : 0);

  BufferCPU new_indices(num_vertices * sizeof(int));
  BufferCPU new_vertices(num_vertices * num_floats * sizeof(float));

  BufferCPU old_vertices(num_vertices * num_floats * sizeof(float));
  uint32_t offset;
  for (int i = 0; i < num_vertices; i++) {
    offset = 0;
    int face = i / 3;
    int vert = i % 3;
    if (available[0]) {
      getPosition(&old_vertices.at<float>(i * num_floats + offset), face, vert);
      offset += 3;
    }
    if (available[1]) {
      getNormal(&old_vertices.at<float>(i * num_floats + offset), face, vert);
      offset += 3;
    }
    for (int j = 0; j < 4; j++)
      old_vertices.at<float>(i * num_floats + offset + j) =
          tangents_.at<float>(i * 4 + j);
    offset += 4;
    if (available[2]) {
      getTexCoord(&old_vertices.at<float>(i * num_floats + offset), face, vert);
      offset += 2;
    }
  }

  int new_vertex_count =
      WeldMesh(new_indices.data<int>(), new_vertices.data<float>(),
               old_vertices.data<float>(), num_vertices, num_floats);

  new_vertices.resize<float>(new_vertex_count * num_floats);

  std::vector<VkVertexInputBindingDescription> bindings = {
      {0, static_cast<uint32_t>(num_floats * sizeof(float)),
       VK_VERTEX_INPUT_RATE_VERTEX}};
  std::vector<VkVertexInputAttributeDescription> attributes;
  offset = 0;
  if (available[0]) {
    attributes.push_back({0, 0, in_vertex_data_.formats[0], offset});
    offset += in_vertex_data_.num_floats[0] * sizeof(float);
  }
  if (available[1]) {
    attributes.push_back({1, 0, in_vertex_data_.formats[1], offset});
    offset += in_vertex_data_.num_floats[1] * sizeof(float);
  }
  attributes.push_back({2, 0, in_vertex_data_.formats[2], offset});
  offset += in_vertex_data_.num_floats[2] * sizeof(float);
  if (available[2]) {
    attributes.push_back({3, 0, in_vertex_data_.formats[3], offset});
    offset += in_vertex_data_.num_floats[3] * sizeof(float);
  }
  return {std::move(new_indices),
          {{new_vertices.toGPU(VK_BUFFER_USAGE_VERTEX_BUFFER_BIT)},
           bindings,
           attributes}};
}

int TangentGenerator::getNumFaces() {
  if (indices_.empty()) {
    return in_vertex_data_.getNumEntries(VertexData::POSITION) / 3;
  } else {
    uint64_t bytes_per_index = 4;
    if (indices_.component_type == 5123)
      bytes_per_index = 2;
    else if (indices_.component_type == 5121)
      bytes_per_index = 1;
    return static_cast<int>(indices_.size<char>() / bytes_per_index / 3);
  }
}

int TangentGenerator::getNumVerticesOfFace(const int iFace) { return 3; }

uint32_t TangentGenerator::getIndex(const int iFace, const int iVert) {
  uint32_t query = iFace * 3 + iVert;

  if (indices_.empty()) return query;

  switch (indices_.component_type) {
    case 5121:
      return indices_.at<uint8_t>(query);
      break;
    case 5123:
      return indices_.at<uint16_t>(query);
      break;
    case 5125:
      return indices_.at<uint32_t>(query);
      break;
  }
  UVELON_LOG_WARNING("Unsupported index type: " +
                     std::to_string(indices_.component_type));
  return query;
}

void TangentGenerator::getPosition(float fvPosOut[], const int iFace,
                                   const int iVert) {
  if (in_vertex_data_.getNumEntries(VertexData::POSITION) == 0) {
    fvPosOut[0] = fvPosOut[1] = fvPosOut[2] = 0.0f;
    return;
  }

  uint32_t index = getIndex(iFace, iVert);
  in_vertex_data_.getEntry(VertexData::POSITION, index, fvPosOut);
}

void TangentGenerator::getNormal(float fvNormOut[], const int iFace,
                                 const int iVert) {
  if (in_vertex_data_.getNumEntries(VertexData::NORMAL) == 0) {
    // No normals, generate them per face for flat shading
    glm::vec3 a, b, c;
    getPosition(&a.x, iFace, 0);
    getPosition(&b.x, iFace, 1);
    getPosition(&c.x, iFace, 2);
    glm::vec3 normal = glm::normalize(glm::cross(b - a, c - a));
    fvNormOut[0] = normal.x;
    fvNormOut[1] = normal.y;
    fvNormOut[2] = normal.z;
    return;
  }

  uint32_t index = getIndex(iFace, iVert);
  in_vertex_data_.getEntry(VertexData::NORMAL, index, fvNormOut);
}

void TangentGenerator::getTexCoord(float fvTexcOut[], const int iFace,
                                   const int iVert) {
  if (in_vertex_data_.getNumEntries(VertexData::UV) == 0) {
    fvTexcOut[0] = fvTexcOut[1] = 0.0f;
    return;
  }

  uint32_t index = getIndex(iFace, iVert);
  in_vertex_data_.getEntry(VertexData::UV, index, fvTexcOut);
}

void TangentGenerator::setTSpaceBasic(const float fvTangent[],
                                      const float fSign, const int iFace,
                                      const int iVert) {
  uint32_t index = (iFace * 3 + iVert) * 4;
  tangents_.at<float>(index + 0) = fvTangent[0];
  tangents_.at<float>(index + 1) = fvTangent[1];
  tangents_.at<float>(index + 2) = fvTangent[2];
  // https://github.com/KhronosGroup/glTF-Sample-Models/issues/174
  tangents_.at<float>(index + 3) = -fSign;
}

}  // namespace Uvelon