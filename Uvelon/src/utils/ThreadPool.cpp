#include "utils/ThreadPool.hpp"

#include <SDL3/SDL_cpuinfo.h>

#include "utils/Logger.hpp"

namespace Uvelon {

void ThreadPool::init(int num_threads) {
  running_ = true;

  if (num_threads <= 0) {
    num_threads = SDL_GetNumLogicalCPUCores();
  }

  for (int i = 0; i < num_threads; i++) {
    threads_.emplace_back([this] {
      while (true) {
        std::function<void()> job;
        {
          auto lock = std::unique_lock<std::mutex>(m_queue_);
          cv_job_queued_.wait(lock,
                              [this] { return !running_ || !jobs_.empty(); });
          if (!running_) return;
          job = jobs_.front();
          jobs_.pop();
          active_jobs_++;
        }
        job();
        {
          auto lock = std::lock_guard<std::mutex>(m_queue_);
          active_jobs_--;
          if (!running_ || (active_jobs_ == 0 && jobs_.empty()))
            cv_job_done_.notify_all();
        }
      }
    });
  }
}

void ThreadPool::enqueue(const std::function<void()>& job) {
  {
    auto lock = std::lock_guard<std::mutex>(m_queue_);
    jobs_.push(job);
  }
  cv_job_queued_.notify_one();
}

void ThreadPool::destroy() {
  {
    auto lock = std::lock_guard<std::mutex>(m_queue_);
    if (!running_) return;
    running_ = false;
  }
  cv_job_queued_.notify_all();

#ifndef __cpp_lib_jthread
  for (auto& thread : threads_) thread.join();
#endif
}

void ThreadPool::waitIdle() {
  while (true) {
    auto lock = std::unique_lock<std::mutex>(m_queue_);
    cv_job_done_.wait(lock, [this] {
      return !running_ || (active_jobs_ == 0 && jobs_.empty());
    });
    return;
  }
}

}  // namespace Uvelon