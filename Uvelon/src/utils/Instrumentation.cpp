#include "utils/Instrumentation.hpp"

#include <vulkan/vulkan.h>

#include "core/CVarManager.hpp"
#include "rendering/Utils.hpp"

namespace Uvelon {

Timer::Timer() : stopped_(true) {}

Timer::Timer(const std::string& name, const char* file, int line,
             Reporting reporting)
    : name_(name),
      file_(file),
      line_(line),
      stopped_(false),
      reporting_(reporting) {
  start();
}

Timer::~Timer() { stop(); }

inline void Timer::start() {
  start_ = std::chrono::high_resolution_clock::now();
}

inline void Timer::stop() {
  if (stopped_) return;
  auto stop = std::chrono::high_resolution_clock::now();
  stopped_ = true;

  long long nanoseconds =
      std::chrono::duration_cast<std::chrono::nanoseconds>(stop - start_)
          .count();

  // Report
  switch (reporting_) {
    case PRINT:
#ifdef __APPLE__
      printf("[PROFILER] (%s:%d) %s: %'lldns\n", file_.c_str(), line_,
             name_.c_str(), nanoseconds);
#else
      std::cout << "[PROFILER] (" << file_ << ":" << line_ << ") " << name_
                << ": " << nanoseconds << "ns\n";
#endif
      break;
    case CVAR:
      CVarManager::setVar(std::string("instrumentation.ms.") + name_,
                          nanoseconds / static_cast<double>(1000000), true);
      break;
  }
  // TODO: More reporting options
}

// GPU

VkDevice GPUTimer::logical_device_ = nullptr;
VkQueryPool GPUTimer::query_pool_ = nullptr;
uint32_t GPUTimer::timestamp_count_ = 0;
std::vector<GPUTimer::GPUTimerEntry> GPUTimer::entries_ =
    std::vector<GPUTimer::GPUTimerEntry>();
std::vector<uint64_t> GPUTimer::results_buffer_ = std::vector<uint64_t>();
float GPUTimer::timestamp_period_ = 0.0f;

void GPUTimer::init(VkPhysicalDevice physical_device, VkDevice logical_device) {
  VkPhysicalDeviceProperties properties;
  vkGetPhysicalDeviceProperties(physical_device, &properties);
  bool all_queues = properties.limits.timestampComputeAndGraphics;
  if (!all_queues)
    UVELON_LOG_WARNING(
        "Vulkan timestamps not supported for either compute and graphics");
  timestamp_period_ = properties.limits.timestampPeriod;

  VkQueryPoolCreateInfo info = {VK_STRUCTURE_TYPE_QUERY_POOL_CREATE_INFO};
  info.queryType = VK_QUERY_TYPE_TIMESTAMP;
  info.queryCount = static_cast<uint32_t>(UVELON_MAX_TIMESTAMP_QUERY_COUNT);

  logical_device_ = logical_device;

  VK_CHECK(vkCreateQueryPool(logical_device, &info, nullptr, &query_pool_));
}

void GPUTimer::destroy() {
  vkDestroyQueryPool(logical_device_, query_pool_, nullptr);
}

uint32_t GPUTimer::start(VkCommandBuffer cmd, const std::string& name) {
  uint32_t result = static_cast<uint32_t>(entries_.size());
  uint32_t timestamp_start = timestamp_count_++;
  vkCmdWriteTimestamp(cmd, VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, query_pool_,
                      timestamp_start);
  entries_.push_back({name, timestamp_start, 0});
  return result;
}

void GPUTimer::stop(VkCommandBuffer cmd, uint32_t timer) {
  uint32_t timestamp_end = timestamp_count_++;
  vkCmdWriteTimestamp(cmd, VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT, query_pool_,
                      timestamp_end);
  entries_[timer].timestamp_end = timestamp_end;
}

void GPUTimer::collect(Reporting reporting) {
  results_buffer_.resize(timestamp_count_);
  VkResult result;
  do {
    result = vkGetQueryPoolResults(
        logical_device_, query_pool_, 0, timestamp_count_,
        results_buffer_.size() * sizeof(uint64_t), results_buffer_.data(),
        sizeof(uint64_t), VK_QUERY_RESULT_64_BIT);
  } while (result == VK_NOT_READY);

  if (result == VK_SUCCESS) {
    for (GPUTimerEntry& entry : entries_) {
      uint64_t nanoseconds =
          static_cast<uint64_t>((results_buffer_[entry.timestamp_end] -
                                 results_buffer_[entry.timestamp_start]) *
                                timestamp_period_);
      switch (reporting) {
        case PRINT:
#ifdef __APPLE__
          printf("[PROFILER|GPU] %s: %'lldns\n", entry.name.c_str(),
                 nanoseconds);
#else
          std::cout << "[PROFILER|GPU] " << entry.name << ": " << nanoseconds
                    << "ns\n";
#endif
          break;
        case CVAR:
          CVarManager::setVar(std::string("instrumentation.ms.") + entry.name,
                              nanoseconds / static_cast<double>(1000000), true);
          break;
      }
    }
  } else {
    UVELON_LOG_ERROR("Getting query pool results failed!");
  }

  // Reset
  // https://github.com/KhronosGroup/Vulkan-ValidationLayers/issues/8233
  vkResetQueryPool(logical_device_, query_pool_, 0, timestamp_count_);
  entries_.clear();
  timestamp_count_ = 0;
}

}  // namespace Uvelon