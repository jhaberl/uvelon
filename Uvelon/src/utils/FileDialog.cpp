#include "utils/FileDialog.hpp"

#include <imgui.h>
#include <imgui_stdlib.h>

#include "utils/Logger.hpp"

namespace Uvelon {

bool FileDialog::pickDir(const std::string& id, std::filesystem::path& result,
                         const std::filesystem::path& path) {
#ifdef HAVE_NFD
  bool ret = FileWizard::getInstance().pickFolder(result, path.string());
  if (!ret) result = "";
  return true;
#else
  if (checkRequest(id)) {
    result = "";
    if (!selected_.empty()) result = selected_;
    return true;
  } else {
    if (!initRequest(id, path, nullptr, 0, true, false)) return false;
  }
  return false;
#endif
}

bool FileDialog::open(const std::string& id, std::filesystem::path& result,
                      FileWizard::FilterItem* filter, int count,
                      const std::filesystem::path& path) {
#ifdef HAVE_NFD
  bool ret = FileWizard::getInstance().openDialog(result, filter, count,
                                                  path.string());
  if (!ret) result = "";
  return true;
#else
  if (checkRequest(id)) {
    result = "";
    if (!selected_.empty()) result = selected_;
    return true;
  } else {
    if (!initRequest(id, path, filter, count, false, true)) return false;
  }
  return false;
#endif
}

bool FileDialog::save(const std::string& id, std::filesystem::path& result,
                      FileWizard::FilterItem* filter, int count,
                      const std::string& name,
                      const std::filesystem::path& path) {
#ifdef HAVE_NFD
  bool ret = FileWizard::getInstance().saveDialog(result, filter, count, name,
                                                  path.string());
  if (!ret) result = "";
  return true;
#else
  if (checkRequest(id)) {
    result = "";
    if (!selected_.empty() && !filename_.empty()) {
      if (std::filesystem::is_directory(selected_)) {
        result = selected_.append(filename_);
      } else {
        result = selected_.parent_path().append(filename_);
      }
    }
    return true;
  } else {
    if (!initRequest(id, path, filter, count, true, true)) return false;
    filename_ = name;
  }

  return false;
#endif
}

bool FileDialog::checkRequest(const std::string& id) {
  bool done = active_id_ == id && handled_;
  if (done) {
    active_id_ = "";
    handled_ = false;
  }
  return done;
}

bool FileDialog::initRequest(const std::string& id,
                             const std::filesystem::path& path,
                             FileWizard::FilterItem* filter, int filter_count,
                             bool selectable_dir, bool selectable_file) {
  if (!active_id_.empty() || path.empty()) return false;
  active_id_ = id;
  handled_ = false;
  path_ = path;
  // TODO: Add filter support
  updateTree();
  selected_ = "";
  selectable_dir_ = selectable_dir;
  selectable_file_ = selectable_file;
  return true;
}

void FileDialog::draw() {
  if (isActive()) {
    ImGui::Begin("File Dialog", nullptr,
                 ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize |
                     ImGuiWindowFlags_NoCollapse |
                     ImGuiWindowFlags_AlwaysAutoResize);
    if (ImGui::Button("OK")) {
      handled_ = true;
    }
    ImGui::SameLine();
    if (ImGui::Button("Cancel")) {
      selected_ = "";
      handled_ = true;
    }
    if (selectable_dir_ && selectable_file_)
      ImGui::InputText("Filename", &filename_);
    ImVec2& size = ImGui::GetMainViewport()->WorkSize;
    ImGui::SetNextWindowSizeConstraints({0.0f, 0.0f},
                                        {size.x * 0.75f, size.y * 0.75f});
    ImGui::BeginChild(
        "fdchild", {0, 0},
        ImGuiChildFlags_AutoResizeX | ImGuiChildFlags_AutoResizeY);
    drawNode(root_);
    ImGui::EndChild();
    ImGui::End();
  }
}

bool FileDialog::isActive() { return !active_id_.empty(); }

void FileDialog::drawNode(const FileNode& node) {
  ImGui::PushID(&node);
  ImGuiTreeNodeFlags additional_flags =
      selected_ == node.path ? ImGuiTreeNodeFlags_Selected : 0;
  if (node.dir) {
    additional_flags |=
        node.path == root_.path ? ImGuiTreeNodeFlags_DefaultOpen : 0;
    if (selectable_dir_)
      additional_flags |=
          ImGuiTreeNodeFlags_OpenOnArrow | ImGuiTreeNodeFlags_OpenOnDoubleClick;
    bool open =
        ImGui::TreeNodeEx(node.path.filename().string().c_str(),
                          ImGuiTreeNodeFlags_SpanFullWidth | additional_flags);
    if (selectable_dir_ && ImGui::IsItemClicked()) {
      if (selected_ == node.path)
        selected_ = "";
      else
        selected_ = node.path;
    }
    if (open) {
      for (const FileNode& child : node.children) {
        drawNode(child);
      }
      ImGui::TreePop();
    }
  } else {
    if (ImGui::TreeNodeEx(
            node.path.filename().string().c_str(),
            ImGuiTreeNodeFlags_NoTreePushOnOpen | ImGuiTreeNodeFlags_Leaf |
                ImGuiTreeNodeFlags_SpanFullWidth | additional_flags)) {
    }
    if (selectable_file_ && ImGui::IsItemClicked()) {
      if (selected_ == node.path)
        selected_ = "";
      else {
        selected_ = node.path;
        // When saving, adopt filename
        filename_ = node.path.filename().string();
      }
    }
  }
  ImGui::PopID();
}

void FileDialog::updateTree() {
  root_.path = path_;
  root_.children.clear();
  populateChildren(root_);
}

void FileDialog::populateChildren(FileNode& node) {
  node.dir = false;
  try {
    node.dir = std::filesystem::is_directory(node.path);
    if (node.dir) {
      for (auto const& entry : std::filesystem::directory_iterator(node.path)) {
        node.children.emplace_back(entry.path());
        populateChildren(node.children.back());
      }
    }
  } catch (std::exception& ex) {
    UVELON_LOG_WARNING(ex.what());
  }
}

}  // namespace Uvelon