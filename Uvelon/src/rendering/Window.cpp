#include "rendering/Window.hpp"

#include <SDL3/SDL_vulkan.h>
#include <vulkan/vulkan.h>

#include "rendering/Renderer.hpp"
#include "utils/Logger.hpp"

void SDL_WindowDeleter::operator()(SDL_Window* window) {
  SDL_DestroyWindow(window);
}

namespace Uvelon {

Window::Window(const std::string& title, int width, int height, bool maximized,
               bool fullscreen)
    : title_(title),
      width_(width),
      height_(height),
      maximized_(maximized),
      fullscreen_(fullscreen) {
  if (!SDL_Init(SDL_INIT_VIDEO))
    UVELON_LOG_ERROR("SDL_Init failed: " + std::string(SDL_GetError()));
}

Window::~Window() {
  if (opened_) SDL_DestroyWindow(sdl_window_.get());
}

void Window::open() {
  opened_ = true;

  SDL_WindowFlags window_flags =
      static_cast<SDL_WindowFlags>(SDL_WINDOW_VULKAN | SDL_WINDOW_RESIZABLE);
  window_flags = static_cast<SDL_WindowFlags>(
      window_flags | (maximized_ ? SDL_WINDOW_MAXIMIZED : 0) |
      (fullscreen_ ? SDL_WINDOW_FULLSCREEN : 0));
  sdl_window_ = std::unique_ptr<SDL_Window, SDL_WindowDeleter>(
      SDL_CreateWindow(title_.c_str(), width_, height_, window_flags),
      SDL_WindowDeleter());
}

void Window::handleEvent(SDL_Event& event) {
  switch (event.type) {
    case SDL_EVENT_WINDOW_PIXEL_SIZE_CHANGED: {
      width_ = event.window.data1;
      height_ = event.window.data2;
      Renderer::getInstance().notifyWindowSizeChanged();
      break;
    }
    case SDL_EVENT_WINDOW_RESTORED: {
      // Window restored after being minimized. Does not cause a size changed
      // event
      Renderer::getInstance().notifyWindowSizeChanged();
      break;
    }
    default:
      return;
  }
}

void Window::createSurface(VkInstance instance) {
  if (!SDL_Vulkan_CreateSurface(sdl_window_.get(), instance, nullptr,
                                &surface_))
    UVELON_LOG_ERROR("Surface creation failed: " + std::string(SDL_GetError()));
}

}  // namespace Uvelon