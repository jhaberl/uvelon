#include "rendering/Pipeline.hpp"

#include <filesystem>
#include <fstream>
#include <memory>
#include <sstream>

#include "core/FileWizard.hpp"
#include "rendering/GBuffer.hpp"
#include "rendering/Primitives.hpp"
#include "rendering/RenderData.hpp"
#include "rendering/Renderer.hpp"
#include "rendering/Utils.hpp"
#include "scene/LoaderPrimitives.hpp"
#include "utils/Logger.hpp"

namespace Uvelon {

ShaderCache::~ShaderCache() { destroy(); }

void ShaderCache::destroy() {
  VkDevice device = Renderer::getInstance().getDevice();

  auto it = shader_modules_.begin();
  while (it != shader_modules_.end()) {
    VkShaderModule shader = std::get<1>(*it);
    vkDestroyShaderModule(device, shader, nullptr);
    it++;
  }
  shader_modules_.clear();
}

PipelineBuilder PipelineBuilder::fromExisting(Pipeline& existing) {
  PipelineBuilder result;
  result.pipeline_data_ = existing.pipeline_data;
  result.from_existing_ = true;
  result.existing_ = &existing;
  // Shaders are not compiled in pipeline_data, need to be recompiled. This is
  // fine because this method is meant for hot reloading shaders anyway
  for (auto& shader : result.pipeline_data_.shaders) {
    auto shader_module =
        Renderer::getInstance().getShaderCache().createShaderModule(shader);
    VkPipelineShaderStageCreateInfo stage_info = {
        VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO};
    stage_info.stage = shader.stage;
    stage_info.module = shader_module;
    stage_info.pName = "main";
    result.shader_stages_.push_back(stage_info);
  }
  return result;
}

PipelineBuilder& PipelineBuilder::setAttachmentFormats(VkFormat* color_formats,
                                                       uint32_t count,
                                                       VkFormat depth_format,
                                                       uint32_t view_mask) {
  assert(!pipeline_data_.compute);
  // Copy the couple handles we get right away to avoid bugs that are hard to
  // find...
  pipeline_data_.format_color.clear();
  pipeline_data_.format_color.reserve(count);
  for (uint32_t i = 0; i < count; i++)
    pipeline_data_.format_color.push_back(color_formats[i]);

  pipeline_data_.format_depth = depth_format;
  pipeline_data_.view_mask = view_mask;

  pipeline_data_.format_set = true;
  return *this;
}

PipelineBuilder& PipelineBuilder::setVertexInputInfo(
    VkVertexInputBindingDescription* binding_descriptions,
    uint32_t binding_count,
    VkVertexInputAttributeDescription* attribute_descriptions,
    uint32_t attribute_count) {
  assert(!pipeline_data_.compute);
  pipeline_data_.vertex_input_bindings.clear();
  pipeline_data_.vertex_input_bindings.reserve(binding_count);
  for (uint32_t i = 0; i < binding_count; i++)
    pipeline_data_.vertex_input_bindings.push_back(binding_descriptions[i]);

  pipeline_data_.vertex_input_attributes.clear();
  pipeline_data_.vertex_input_attributes.reserve(attribute_count);
  for (uint32_t i = 0; i < attribute_count; i++)
    pipeline_data_.vertex_input_attributes.push_back(attribute_descriptions[i]);

  pipeline_data_.vertex_input_set = true;
  return *this;
}

PipelineBuilder& PipelineBuilder::setTopology(VkPrimitiveTopology topology) {
  assert(!pipeline_data_.compute);
  pipeline_data_.topology = topology;
  return *this;
}

PipelineBuilder& PipelineBuilder::setFrontFace(VkFrontFace front_face) {
  assert(!pipeline_data_.compute);
  pipeline_data_.front_face = front_face;
  return *this;
}

PipelineBuilder& PipelineBuilder::addDynamicState(
    VkDynamicState dynamic_state) {
  assert(!pipeline_data_.compute);
  pipeline_data_.dynamic_states.push_back(dynamic_state);
  return *this;
}

PipelineBuilder& PipelineBuilder::setDepthTest(bool depth_test_enable,
                                               bool depth_write_enable) {
  assert(!pipeline_data_.compute);
  assert(!depth_test_enable && depth_write_enable);
  pipeline_data_.depth_test_enable = depth_test_enable;
  pipeline_data_.depth_write_enable = depth_write_enable;
  return *this;
}

VkShaderModule ShaderCache::createShaderModule(
    const PipelineData::Shader& shader) {
  // Create a shader module or reuse if in cache
  auto cached = std::find_if(
      shader_modules_.begin(), shader_modules_.end(),
      [&shader](std::pair<PipelineData::Shader, VkShaderModule>& element) {
        return element.first == shader;
      });
  if (cached != shader_modules_.end()) return cached->second;

  auto compiled_shader = compileShader(shader);
  VkShaderModuleCreateInfo shader_info = {
      VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO};
  shader_info.codeSize = compiled_shader.size();
  shader_info.pCode = reinterpret_cast<uint32_t*>(compiled_shader.data());

  shader_modules_.emplace_back(shader, VK_NULL_HANDLE);
  VkShaderModule* shader_module = &shader_modules_.back().second;
  VK_CHECK(vkCreateShaderModule(Renderer::getInstance().getDevice(),
                                &shader_info, nullptr, shader_module));
  return *shader_module;
}

std::vector<char> ShaderCache::compileShader(
    const PipelineData::Shader& shader) {
  if (!shader_compiler_.IsValid()) {
    UVELON_LOG_WARNING("Invalid shader compiler!");
    return {};
  }

  // Check shader type based on filename
  shaderc_shader_kind kind;
  std::string fp(shader.path);
  fp = fp.substr(fp.find_last_of(".") + 1);
  // TODO: Can we use shaderc's MapStageNameToForcedKind?
  // Alternatively, may want to use the shader parameter
  if (fp == "frag") {
    kind = shaderc_glsl_fragment_shader;
  } else if (fp == "vert") {
    kind = shaderc_glsl_vertex_shader;
  } else if (fp == "comp") {
    kind = shaderc_glsl_compute_shader;
  } else if (fp == "geom") {
    kind = shaderc_glsl_geometry_shader;
  } else if (fp == "mesh") {
    kind = shaderc_glsl_mesh_shader;
  } else {
    UVELON_LOG_WARNING("Unsupported shader type: " + fp);
    return {};
  }

  // Load shader file
  std::ifstream file(shader.path, std::ios::in | std::ios::binary);
  if (!file.is_open()) {
    UVELON_LOG_ERROR("Shader file not found: " + shader.path);
    abort();
  }

  file.seekg(0, std::ios::end);
  size_t file_size = file.tellg();
  file.seekg(0, std::ios::beg);
  std::string content(file_size, 0);
  file.read(content.data(), file_size);

  // Compile shader to SPIR-V
  shaderc::CompileOptions options;
  options.SetIncluder(std::make_unique<ShaderIncludeHandler>());
  // TODO: Update to 1.3
  options.SetTargetEnvironment(shaderc_target_env_vulkan,
                               shaderc_env_version_vulkan_1_2);
  options.SetOptimizationLevel(shaderc_optimization_level_performance);
  for (auto& macro : shader.macros) {
    options.AddMacroDefinition(std::get<0>(macro), std::get<1>(macro));
  }

  UVELON_LOG_DEBUG("Compiling " + shader.path);

  auto preprocess_result = shader_compiler_.PreprocessGlsl(
      content, kind, shader.path.c_str(), options);
  if (preprocess_result.GetCompilationStatus() !=
      shaderc_compilation_status_success) {
    UVELON_LOG_ERROR("Shader preprocessing failed for " + shader.path + ": \n" +
                     preprocess_result.GetErrorMessage());
    abort();
  }

  auto compile_result =
      shader_compiler_.CompileGlslToSpv(std::string(preprocess_result.begin()),
                                        kind, shader.path.c_str(), options);
  if (compile_result.GetCompilationStatus() !=
      shaderc_compilation_status_success) {
    UVELON_LOG_ERROR("Shader compilation failed for " + shader.path + ": \n" +
                     compile_result.GetErrorMessage());
    abort();
  }

  size_t size =
      (compile_result.end() - compile_result.begin()) * sizeof(uint32_t);
  std::vector<char> result(size);

  memcpy(result.data(), compile_result.begin(), size);

  return result;
}

shaderc_include_result* ShaderIncludeHandler::GetInclude(
    const char* requested_source, shaderc_include_type type,
    const char* requesting_source, size_t include_depth) {
  std::filesystem::path path_requested(requested_source);
  if (path_requested.is_relative()) {
    path_requested =
        std::filesystem::path(requesting_source).parent_path() / path_requested;
  }

  Info* info;

  // Read file content
  std::ifstream in(path_requested, std::ios::in | std::ios::binary);
  if (in.is_open()) {
    info = new Info{path_requested.string(), {}};

    in.seekg(0, std::ios::end);
    size_t file_size = in.tellg();
    in.seekg(0, std::ios::beg);
    info->content.resize(file_size);
    in.read(info->content.data(), file_size);
  } else {
    UVELON_LOG_ERROR("Unable to open included shader: " +
                     std::string(requested_source));
    info = new Info{"", {}};
  }

  return new shaderc_include_result{
      info->source_name.data(), info->source_name.size(), info->content.data(),
      info->content.size(), info};
}

void ShaderIncludeHandler::ReleaseInclude(shaderc_include_result* data) {
  Info* info = static_cast<Info*>(data->user_data);
  delete info;
  delete data;
}

void PipelineBuilder::createLayout(Pipeline& result) {
  if (from_existing_ && existing_->layout) {
    vkDestroyPipelineLayout(Renderer::getInstance().getDevice(),
                            existing_->layout, nullptr);
    existing_->layout = nullptr;
  }

  VkPipelineLayoutCreateInfo layout_info = {
      VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO};
  layout_info.setLayoutCount =
      static_cast<uint32_t>(pipeline_data_.descriptor_sets.size());
  layout_info.pSetLayouts = pipeline_data_.descriptor_sets.data();
  layout_info.pushConstantRangeCount =
      static_cast<uint32_t>(pipeline_data_.push_constants.size());
  layout_info.pPushConstantRanges = pipeline_data_.push_constants.data();
  VK_CHECK(vkCreatePipelineLayout(Renderer::getInstance().getDevice(),
                                  &layout_info, nullptr, &result.layout));
}

PipelineH PipelineBuilder::buildGraphics() {
  Pipeline result;
  result.pipeline_data = pipeline_data_;

  createLayout(result);

  // Pipeline
  VkGraphicsPipelineCreateInfo pipeline_info = {
      VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO};

  // Attachment formats for dynamic rendering
  if (!pipeline_data_.format_set) {
    VkFormat* col_f;
    uint32_t formats;
    VkFormat depth_f;
    Renderer::getInstance().getGBuffer().retrieveFormats(col_f, formats,
                                                         depth_f);
    setAttachmentFormats(col_f, formats, depth_f);
  }
  VkPipelineRenderingCreateInfo rendering_info = {
      VK_STRUCTURE_TYPE_PIPELINE_RENDERING_CREATE_INFO};
  rendering_info.viewMask = pipeline_data_.view_mask;
  rendering_info.colorAttachmentCount =
      static_cast<uint32_t>(pipeline_data_.format_color.size());
  rendering_info.pColorAttachmentFormats = pipeline_data_.format_color.data();
  rendering_info.depthAttachmentFormat = pipeline_data_.format_depth;
  pipeline_info.pNext = &rendering_info;

  // Shaders
  pipeline_info.stageCount = static_cast<uint32_t>(shader_stages_.size());
  pipeline_info.pStages = shader_stages_.data();

  if (!pipeline_data_.vertex_input_set) {
    // Create temporary binding/attribute descriptions to get rid of warnings
    VertexData temp;
    pipeline_data_.vertex_input_bindings.push_back(
        {0, 0, VK_VERTEX_INPUT_RATE_VERTEX});
    for (uint32_t i = 0; i < VertexData::max_attributes; i++) {
      pipeline_data_.vertex_input_attributes.push_back(
          {i, 0, temp.formats[i], 0});
    }
  }
  VkPipelineVertexInputStateCreateInfo vertex_input_info = {
      VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO};
  vertex_input_info.vertexBindingDescriptionCount =
      static_cast<uint32_t>(pipeline_data_.vertex_input_bindings.size());
  vertex_input_info.pVertexBindingDescriptions =
      pipeline_data_.vertex_input_bindings.data();
  vertex_input_info.vertexAttributeDescriptionCount =
      static_cast<uint32_t>(pipeline_data_.vertex_input_attributes.size());
  vertex_input_info.pVertexAttributeDescriptions =
      pipeline_data_.vertex_input_attributes.data();
  pipeline_info.pVertexInputState = &vertex_input_info;

  VkPipelineInputAssemblyStateCreateInfo input_assembly_info = {
      VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO};
  input_assembly_info.topology = pipeline_data_.topology;
  input_assembly_info.primitiveRestartEnable = false;
  pipeline_info.pInputAssemblyState = &input_assembly_info;

  pipeline_info.pTessellationState = nullptr;

  VkPipelineViewportStateCreateInfo viewport_info = {
      VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO};
  viewport_info.viewportCount = 1;
  viewport_info.scissorCount = 1;
  pipeline_info.pViewportState = &viewport_info;

  VkPipelineRasterizationStateCreateInfo rasterization_info = {
      VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO};
  rasterization_info.depthClampEnable = false;
  rasterization_info.rasterizerDiscardEnable = false;
  rasterization_info.polygonMode = VK_POLYGON_MODE_FILL;
  rasterization_info.cullMode = VK_CULL_MODE_BACK_BIT;
  rasterization_info.frontFace = pipeline_data_.front_face;
  rasterization_info.depthBiasEnable = false;
  rasterization_info.lineWidth = 1.0f;
  pipeline_info.pRasterizationState = &rasterization_info;

  VkPipelineMultisampleStateCreateInfo multisample_info = {
      VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO};
  multisample_info.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
  multisample_info.sampleShadingEnable = false;
  pipeline_info.pMultisampleState = &multisample_info;

  VkPipelineDepthStencilStateCreateInfo depth_stencil_info = {
      VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO};
  depth_stencil_info.depthTestEnable = pipeline_data_.depth_test_enable;
  depth_stencil_info.depthWriteEnable = pipeline_data_.depth_write_enable;
  depth_stencil_info.depthCompareOp =
      VK_COMPARE_OP_GREATER_OR_EQUAL;  // Reverse z buffer
  depth_stencil_info.depthBoundsTestEnable = false;
  depth_stencil_info.stencilTestEnable = false;
  pipeline_info.pDepthStencilState = &depth_stencil_info;

  VkPipelineColorBlendStateCreateInfo color_blend_info = {
      VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO};
  color_blend_info.logicOpEnable = false;
  color_blend_info.logicOp = VK_LOGIC_OP_COPY;
  color_blend_info.attachmentCount = rendering_info.colorAttachmentCount;
  std::vector<VkPipelineColorBlendAttachmentState> color_blend_atts(
      color_blend_info.attachmentCount);
  for (uint32_t i = 0; i < color_blend_info.attachmentCount; i++) {
    // Check if blend state is supported for the selected format
    VkFormatProperties properties;
    vkGetPhysicalDeviceFormatProperties(
        Renderer::getInstance().getPhysicalDevice(),
        pipeline_data_.format_color[i], &properties);
    bool supports_blending =
        (properties.bufferFeatures | properties.linearTilingFeatures |
         properties.optimalTilingFeatures) &
        VK_FORMAT_FEATURE_COLOR_ATTACHMENT_BLEND_BIT;

    color_blend_atts[i].blendEnable = supports_blending;
    color_blend_atts[i].srcColorBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA;
    color_blend_atts[i].dstColorBlendFactor =
        VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
    color_blend_atts[i].colorBlendOp = VK_BLEND_OP_ADD;
    color_blend_atts[i].srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
    color_blend_atts[i].dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
    color_blend_atts[i].alphaBlendOp = VK_BLEND_OP_ADD;
    color_blend_atts[i].colorWriteMask =
        VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT |
        VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
  }
  color_blend_info.pAttachments = color_blend_atts.data();
  pipeline_info.pColorBlendState = &color_blend_info;

  VkPipelineDynamicStateCreateInfo dynamic_state_info = {
      VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO};
  dynamic_state_info.dynamicStateCount =
      static_cast<uint32_t>(pipeline_data_.dynamic_states.size());
  dynamic_state_info.pDynamicStates = pipeline_data_.dynamic_states.data();
  pipeline_info.pDynamicState = &dynamic_state_info;

  pipeline_info.layout = result.layout;
  pipeline_info.renderPass = nullptr;  // Dynamic rendering
  pipeline_info.subpass = 1;
  pipeline_info.flags = VK_PIPELINE_CREATE_ALLOW_DERIVATIVES_BIT;

  if (from_existing_) {
    pipeline_info.flags |= VK_PIPELINE_CREATE_DERIVATIVE_BIT;
    pipeline_info.basePipelineHandle = existing_->pipeline;
    pipeline_info.basePipelineIndex = -1;
  }

  VK_CHECK(vkCreateGraphicsPipelines(Renderer::getInstance().getDevice(),
                                     nullptr, 1, &pipeline_info, nullptr,
                                     &result.pipeline));

  return Renderer::getInstance().getRenderData().addPipeline(std::move(result));
}

PipelineH PipelineBuilder::buildCompute() {
  Pipeline result;
  result.pipeline_data = pipeline_data_;

  if (from_existing_) {
    result.layout = existing_->layout;
  } else {
    createLayout(result);
  }

  VkComputePipelineCreateInfo pipeline_info = {
      VK_STRUCTURE_TYPE_COMPUTE_PIPELINE_CREATE_INFO};
  pipeline_info.layout = result.layout;
  assert(shader_stages_.size() == 1);
  pipeline_info.stage = *shader_stages_.begin();
  pipeline_info.flags = VK_PIPELINE_CREATE_ALLOW_DERIVATIVES_BIT;

  if (from_existing_) {
    pipeline_info.flags |= VK_PIPELINE_CREATE_DERIVATIVE_BIT;
    pipeline_info.basePipelineHandle = existing_->pipeline;
    pipeline_info.basePipelineIndex = -1;
  }

  VK_CHECK(vkCreateComputePipelines(Renderer::getInstance().getDevice(),
                                    nullptr, 1, &pipeline_info, nullptr,
                                    &result.pipeline));

  return Renderer::getInstance().getRenderData().addPipeline(std::move(result));
}

PipelineH PipelineBuilder::build() {
  if (pipeline_data_.compute)
    return buildCompute();
  else
    return buildGraphics();
}

PipelineBuilder& PipelineBuilder::addShader(
    const std::string& path, VkShaderStageFlagBits stage,
    std::vector<std::pair<std::string, std::string>> macros) {
  assert(!pipeline_data_.compute ||
         (stage == VK_SHADER_STAGE_COMPUTE_BIT && shader_stages_.empty()));
  PipelineData::Shader shader{path, stage, macros};
  pipeline_data_.shaders.push_back(shader);

  auto shader_module =
      Renderer::getInstance().getShaderCache().createShaderModule(shader);
  VkPipelineShaderStageCreateInfo stage_info = {
      VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO};
  stage_info.stage = stage;
  stage_info.module = shader_module;
  stage_info.pName = "main";
  shader_stages_.push_back(stage_info);
  return *this;
}

PipelineBuilder& PipelineBuilder::addPushConstant(
    VkPushConstantRange push_constant) {
  pipeline_data_.push_constants.push_back(push_constant);
  return *this;
}

PipelineBuilder& PipelineBuilder::addDescriptorSet(
    VkDescriptorSetLayout layout) {
  pipeline_data_.descriptor_sets.push_back(layout);
  return *this;
}

}  // namespace Uvelon