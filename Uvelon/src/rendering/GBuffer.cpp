#include "rendering/GBuffer.hpp"

#include "core/CVarManager.hpp"
#include "core/FileWizard.hpp"
#include "rendering/Descriptor.hpp"
#include "rendering/Image.hpp"
#include "rendering/Pipeline.hpp"
#include "rendering/RenderData.hpp"
#include "rendering/Renderer.hpp"
#include "utils/Logger.hpp"

namespace Uvelon {

void GBuffer::init() {
  // VK_IMAGE_USAGE:
  // If we didn't use dynamic rendering, we could specify
  // VK_IMAGE_USAGE_INPUT_ATTACHMENT_BIT and specify these images as input
  // attachments for more efficient load operations in the shader (since they're
  // all local by definition)
  // VK_FORMAT:
  // Once we have this working and are reasonably sure the g-buffer contains all
  // the required information, we can reduce the image sizes by packing some of
  // the information. Make sure none of the formats are non-linear (srgb)!

  VkExtent2D swapchain_extent =
      Renderer::getInstance().getSwapchain().getExtent();
  RenderData& render_data = Renderer::getInstance().getRenderData();

  VkExtent3D window_extent = {swapchain_extent.width, swapchain_extent.height,
                              1};
  base_color_ = render_data.addImage(
      Image(window_extent, VK_FORMAT_R8G8B8A8_UNORM,
            VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT));
  metal_rough_ = render_data.addImage(
      Image(window_extent, VK_FORMAT_R8G8B8A8_UNORM,
            VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT));
  normal_ = render_data.addImage(
      Image(window_extent, VK_FORMAT_R8G8B8A8_SNORM,
            VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT));
  emissive_ = render_data.addImage(
      Image(window_extent, VK_FORMAT_R8G8B8A8_UNORM,
            VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT));
  depth_ =
      render_data.addImage(Image(window_extent, VK_FORMAT_D32_SFLOAT,
                                 VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT |
                                     VK_IMAGE_USAGE_SAMPLED_BIT));

  formats_color_.clear();
  formats_color_.push_back(base_color_->getFormat());
  formats_color_.push_back(metal_rough_->getFormat());
  formats_color_.push_back(normal_->getFormat());
  formats_color_.push_back(emissive_->getFormat());
  format_depth_ = depth_->getFormat();

  VkClearValue color_clear;
  // color_clear.color = {0.06f, 0.08f, 0.09f, 1.0f};
  color_clear.color = {0.0f, 0.0f, 0.0f, 0.0f};
  VkClearValue depth_clear;
  depth_clear.depthStencil.depth = 0.0f;

  VkRenderingAttachmentInfo info = {
      VK_STRUCTURE_TYPE_RENDERING_ATTACHMENT_INFO};
  info.imageLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
  info.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
  info.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
  info.clearValue = color_clear;

  attachment_infos_color_.clear();
  info.imageView = base_color_->getVkImageView();
  attachment_infos_color_.push_back(info);
  info.imageView = metal_rough_->getVkImageView();
  attachment_infos_color_.push_back(info);
  info.imageView = normal_->getVkImageView();
  attachment_infos_color_.push_back(info);
  info.imageView = emissive_->getVkImageView();
  attachment_infos_color_.push_back(info);

  info.imageLayout = VK_IMAGE_LAYOUT_DEPTH_ATTACHMENT_OPTIMAL;
  info.clearValue = depth_clear;
  info.imageView = depth_->getVkImageView();
  attachment_info_depth_ = info;

  // Create sampler
  sampler_ = Renderer::getInstance().getRenderData().createSampler(
      VK_FILTER_NEAREST, VK_FILTER_NEAREST, VK_SAMPLER_MIPMAP_MODE_NEAREST,
      VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE,
      VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE);

  // Create descriptor set
  Material material;

  // Lights
  light_buffer_ = Renderer::getInstance().getRenderData().allocateBuffer(
      sizeof(LightPrimitive) * MAX_LIGHTS, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
      true);
  memset(light_buffer_->allocation_info.pMappedData, 0, light_buffer_->size);

  // IBL
  ibl_sampler_ = Renderer::getInstance().getRenderData().createSampler(
      VK_FILTER_LINEAR, VK_FILTER_LINEAR, VK_SAMPLER_MIPMAP_MODE_LINEAR,
      VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE,
      VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE);
  float rgba_black[4] = {0.0f, 0.0f, 0.0f, 1.0f};
  std::vector<VkImageSubresourceLayers> subs;
  std::vector<VkBufferImageCopy> regions;
  for (int i = 0; i < 6; i++) {
    subs.push_back({});
    subs.back().aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    subs.back().layerCount = 1;
    subs.back().baseArrayLayer = i;
    regions.push_back({0, 0, 0, subs.back(), {0, 0, 0}, {1, 1, 1}});
  }
  ibl_placeholder_cube_ = Renderer::getInstance().getRenderData().addImage(
      Image(rgba_black, 4 * sizeof(float), {1, 1, 1},
            VK_FORMAT_R32G32B32A32_SFLOAT, VK_IMAGE_USAGE_SAMPLED_BIT, 6, 1,
            regions, true));
  ibl_placeholder_2d_ = Renderer::getInstance().getRenderData().addImage(
      Image(rgba_black, 4 * sizeof(float), {1, 1, 1},
            VK_FORMAT_R32G32B32A32_SFLOAT, VK_IMAGE_USAGE_SAMPLED_BIT));

  shadow_map_sampler_ = Renderer::getInstance().getRenderData().createSampler(
      VK_FILTER_NEAREST, VK_FILTER_NEAREST, VK_SAMPLER_MIPMAP_MODE_NEAREST,
      VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE,
      VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE, false, true);

  // SSR
  linear_frame_ = Renderer::getInstance().getRenderData().addImage(Image(
      window_extent, VK_FORMAT_R8G8B8A8_UNORM,
      VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT));
  linear_frame_ssr_ = Renderer::getInstance().getRenderData().addImage(
      Image(window_extent, VK_FORMAT_R8G8B8A8_UNORM,
            VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_STORAGE_BIT |
                VK_IMAGE_USAGE_SAMPLED_BIT));
  linear_frame_ssr_blurred_ = Renderer::getInstance().getRenderData().addImage(
      Image({window_extent.width / 10, window_extent.height / 10, 1},
            VK_FORMAT_R8G8B8A8_UNORM,
            VK_IMAGE_USAGE_STORAGE_BIT | VK_IMAGE_USAGE_SAMPLED_BIT));
  linear_frame_ssr_valid_ = false;
  ssr_sampler_ = Renderer::getInstance().getRenderData().createSampler(
      VK_FILTER_LINEAR, VK_FILTER_LINEAR, VK_SAMPLER_MIPMAP_MODE_LINEAR,
      VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE,
      VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE);
  Material compute_material;
  compute_material.descriptor_set =
      DescriptorBuilder::begin()
          .addImage(VK_SHADER_STAGE_COMPUTE_BIT, linear_frame_ssr_,
                    VK_IMAGE_LAYOUT_GENERAL, true)
          .addImage(VK_SHADER_STAGE_COMPUTE_BIT, linear_frame_ssr_blurred_,
                    VK_IMAGE_LAYOUT_GENERAL, true)
          .build();
  compute_material.pipeline =
      PipelineBuilder::beginCompute()
          .addDescriptorSet(compute_material.descriptor_set->layout)
          .addShader(UVELON_FILE_RESOURCES("shaders/blur.comp").string(),
                     VK_SHADER_STAGE_COMPUTE_BIT)
          .build();
  ssr_compute_ = Renderer::getInstance().getRenderData().addMaterial(
      std::move(compute_material));

  // Data
  gb_data_buffer_ = Renderer::getInstance().getRenderData().allocateBuffer(
      sizeof(GBufferData), VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, true, true);

  material.descriptor_set =
      DescriptorBuilder::begin()
          .addSampler(VK_SHADER_STAGE_FRAGMENT_BIT, sampler_)
          .addImage(VK_SHADER_STAGE_FRAGMENT_BIT, base_color_,
                    VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL)
          .addImage(VK_SHADER_STAGE_FRAGMENT_BIT, metal_rough_,
                    VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL)
          .addImage(VK_SHADER_STAGE_FRAGMENT_BIT, normal_,
                    VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL)
          .addImage(VK_SHADER_STAGE_FRAGMENT_BIT, emissive_,
                    VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL)
          .addImage(VK_SHADER_STAGE_FRAGMENT_BIT, depth_,
                    VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL)
          .addBuffer(VK_SHADER_STAGE_FRAGMENT_BIT,
                     VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, light_buffer_, 0,
                     light_buffer_->size)
          .addSampler(VK_SHADER_STAGE_FRAGMENT_BIT, shadow_map_sampler_)
          .addSparseImageArray(VK_SHADER_STAGE_FRAGMENT_BIT, MAX_LIGHTS)
          .addSampler(VK_SHADER_STAGE_FRAGMENT_BIT, ibl_sampler_)
          .addImage(VK_SHADER_STAGE_FRAGMENT_BIT,
                    last_environment_ && last_environment_->diffuse
                        ? last_environment_->diffuse
                        : ibl_placeholder_cube_,
                    VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL)
          .addImage(VK_SHADER_STAGE_FRAGMENT_BIT,
                    last_environment_ && last_environment_->specular
                        ? last_environment_->specular
                        : ibl_placeholder_cube_,
                    VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL)
          .addImage(VK_SHADER_STAGE_FRAGMENT_BIT,
                    last_environment_ && last_environment_->brdf
                        ? last_environment_->brdf
                        : ibl_placeholder_2d_,
                    VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL)
          .addBuffer(VK_SHADER_STAGE_ALL_GRAPHICS,
                     VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, gb_data_buffer_, 0,
                     gb_data_buffer_->size)
          .addImage(VK_SHADER_STAGE_FRAGMENT_BIT, linear_frame_ssr_,
                    VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL)
          .addImage(VK_SHADER_STAGE_FRAGMENT_BIT, linear_frame_ssr_blurred_,
                    VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL)
          .addSampler(VK_SHADER_STAGE_FRAGMENT_BIT, ssr_sampler_)
          .build();

  // Create pipeline
  std::array<VkFormat, 2> attachment_formats = {
      Renderer::getInstance().getSwapchain().getImageFormat(),
      linear_frame_->getFormat()};
  material.pipeline =
      PipelineBuilder::beginGraphics()
          .setAttachmentFormats(attachment_formats.data(), 2,
                                VK_FORMAT_UNDEFINED)
          .setVertexInputInfo(nullptr, 0, nullptr, 0)
          .addShader(UVELON_FILE_RESOURCES("shaders/gbuffer.vert").string(),
                     VK_SHADER_STAGE_VERTEX_BIT,
                     {{"UVELON_MAX_LIGHTS", std::to_string(MAX_LIGHTS)}})
          .addShader(UVELON_FILE_RESOURCES("shaders/gbuffer.frag").string(),
                     VK_SHADER_STAGE_FRAGMENT_BIT,
                     {{"UVELON_MAX_LIGHTS", std::to_string(MAX_LIGHTS)}})
          .addDescriptorSet(
              Renderer::getInstance().getGlobalDescriptorSet()->layout)
          .addDescriptorSet(material.descriptor_set->layout)
          .build();

  material_ =
      Renderer::getInstance().getRenderData().addMaterial(std::move(material));
}

void GBuffer::retrieveFormats(VkFormat*& color_formats, uint32_t& count,
                              VkFormat& depth_format) {
  color_formats = formats_color_.data();
  count = static_cast<uint32_t>(formats_color_.size());
  depth_format = format_depth_;
}

void GBuffer::beginRenderTo(VkCommandBuffer cmd) {
  base_color_->transition(cmd, VK_PIPELINE_STAGE_2_NONE, 0,
                          VK_PIPELINE_STAGE_2_COLOR_ATTACHMENT_OUTPUT_BIT,
                          VK_ACCESS_2_COLOR_ATTACHMENT_WRITE_BIT,
                          VK_IMAGE_LAYOUT_UNDEFINED,
                          VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL);
  normal_->transition(cmd, VK_PIPELINE_STAGE_2_NONE, 0,
                      VK_PIPELINE_STAGE_2_COLOR_ATTACHMENT_OUTPUT_BIT,
                      VK_ACCESS_2_COLOR_ATTACHMENT_WRITE_BIT,
                      VK_IMAGE_LAYOUT_UNDEFINED,
                      VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL);
  metal_rough_->transition(cmd, VK_PIPELINE_STAGE_2_NONE, 0,
                           VK_PIPELINE_STAGE_2_COLOR_ATTACHMENT_OUTPUT_BIT,
                           VK_ACCESS_2_COLOR_ATTACHMENT_WRITE_BIT,
                           VK_IMAGE_LAYOUT_UNDEFINED,
                           VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL);
  emissive_->transition(cmd, VK_PIPELINE_STAGE_2_NONE, 0,
                        VK_PIPELINE_STAGE_2_COLOR_ATTACHMENT_OUTPUT_BIT,
                        VK_ACCESS_2_COLOR_ATTACHMENT_WRITE_BIT,
                        VK_IMAGE_LAYOUT_UNDEFINED,
                        VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL);
  depth_->transition(cmd, VK_PIPELINE_STAGE_2_NONE, 0,
                     VK_PIPELINE_STAGE_2_EARLY_FRAGMENT_TESTS_BIT |
                         VK_PIPELINE_STAGE_2_LATE_FRAGMENT_TESTS_BIT,
                     VK_ACCESS_2_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT,
                     VK_IMAGE_LAYOUT_UNDEFINED,
                     VK_IMAGE_LAYOUT_DEPTH_ATTACHMENT_OPTIMAL);

  if (!linear_frame_ssr_valid_) {
    // Will be valid next frame
    linear_frame_ssr_->transition(
        cmd, VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, 0,
        VK_PIPELINE_STAGE_2_FRAGMENT_SHADER_BIT, VK_ACCESS_2_SHADER_READ_BIT,
        VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
    linear_frame_ssr_blurred_->transition(
        cmd, VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, 0,
        VK_PIPELINE_STAGE_2_FRAGMENT_SHADER_BIT, VK_ACCESS_2_SHADER_READ_BIT,
        VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
    linear_frame_ssr_valid_ = true;
  }

  VkRenderingInfoKHR rendering_info = {VK_STRUCTURE_TYPE_RENDERING_INFO_KHR};
  rendering_info.renderArea.offset = {0, 0};
  rendering_info.renderArea.extent =
      Renderer::getInstance().getSwapchain().getExtent();
  rendering_info.layerCount = 1;
  rendering_info.colorAttachmentCount =
      static_cast<uint32_t>(attachment_infos_color_.size());
  rendering_info.pColorAttachments = attachment_infos_color_.data();
  rendering_info.pDepthAttachment = &attachment_info_depth_;
  Renderer::fp_vkCmdBeginRendering(cmd, &rendering_info);
}

void GBuffer::endRenderTo(VkCommandBuffer cmd) {
  Renderer::fp_vkCmdEndRendering(cmd);

  base_color_->transition(cmd, VK_PIPELINE_STAGE_2_COLOR_ATTACHMENT_OUTPUT_BIT,
                          VK_ACCESS_2_COLOR_ATTACHMENT_WRITE_BIT,
                          VK_PIPELINE_STAGE_2_FRAGMENT_SHADER_BIT,
                          VK_ACCESS_2_SHADER_READ_BIT,
                          VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
                          VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
  normal_->transition(cmd, VK_PIPELINE_STAGE_2_COLOR_ATTACHMENT_OUTPUT_BIT,
                      VK_ACCESS_2_COLOR_ATTACHMENT_WRITE_BIT,
                      VK_PIPELINE_STAGE_2_FRAGMENT_SHADER_BIT,
                      VK_ACCESS_2_SHADER_READ_BIT,
                      VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
                      VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
  metal_rough_->transition(cmd, VK_PIPELINE_STAGE_2_COLOR_ATTACHMENT_OUTPUT_BIT,
                           VK_ACCESS_2_COLOR_ATTACHMENT_WRITE_BIT,
                           VK_PIPELINE_STAGE_2_FRAGMENT_SHADER_BIT,
                           VK_ACCESS_2_SHADER_READ_BIT,
                           VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
                           VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
  emissive_->transition(cmd, VK_PIPELINE_STAGE_2_COLOR_ATTACHMENT_OUTPUT_BIT,
                        VK_ACCESS_2_COLOR_ATTACHMENT_WRITE_BIT,
                        VK_PIPELINE_STAGE_2_FRAGMENT_SHADER_BIT,
                        VK_ACCESS_2_SHADER_READ_BIT,
                        VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
                        VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
  depth_->transition(cmd,
                     VK_PIPELINE_STAGE_2_EARLY_FRAGMENT_TESTS_BIT |
                         VK_PIPELINE_STAGE_2_LATE_FRAGMENT_TESTS_BIT,
                     VK_ACCESS_2_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT,
                     VK_PIPELINE_STAGE_2_FRAGMENT_SHADER_BIT,
                     VK_ACCESS_2_SHADER_READ_BIT,
                     VK_IMAGE_LAYOUT_DEPTH_ATTACHMENT_OPTIMAL,
                     VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
}

void GBuffer::render(VkCommandBuffer cmd, DescriptorSetH global_set,
                     std::vector<LightPrimitive>& lights,
                     EnvironmentH environment) {
  memcpy(light_buffer_->allocation_info.pMappedData, lights.data(),
         lights.size() * sizeof(LightPrimitive));
  if (lights.size() < MAX_LIGHTS) {
    memset(static_cast<LightPrimitive*>(
               light_buffer_->allocation_info.pMappedData) +
               lights.size(),
           0, (MAX_LIGHTS - lights.size()) * sizeof(LightPrimitive));
  }
  *static_cast<GBufferData*>(gb_data_buffer_->allocation_info.pMappedData) = {
      last_frame_view_proj_,
      static_cast<float>(CVarManager::getVar("shadows.directional.limit", 1.0)
                             .getValue<double>()),
      static_cast<float>(
          CVarManager::getVar("shadows.far", 1.0).getValue<double>())};
  DescriptorUpdater shadow_map_updater = {};
  for (int i = 0; i < lights.size(); i++) {
    if (lights[i].type && lights[i].shadow_map) {
      bool changed = true;
      for (DescriptorSet::Resource& res :
           material_->descriptor_set->resources) {
        if (res.binding == 8 && res.array_index == i) {
          changed = res.image != lights[i].shadow_map;
          break;
        }
      }
      if (changed) {
        shadow_map_updater.updateSparseImageArray(
            8, i, lights[i].shadow_map,
            VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
      }
    }
  }
  shadow_map_updater.commit(material_->descriptor_set);

  // Update IBL images
  if (environment != last_environment_) updateEnvironment(environment);

  vkCmdBindDescriptorSets(cmd, VK_PIPELINE_BIND_POINT_GRAPHICS,
                          material_->pipeline->layout, 0, 1, &global_set->set,
                          0, nullptr);
  vkCmdBindDescriptorSets(cmd, VK_PIPELINE_BIND_POINT_GRAPHICS,
                          material_->pipeline->layout, 1, 1,
                          &material_->descriptor_set->set, 0, nullptr);
  vkCmdBindPipeline(cmd, VK_PIPELINE_BIND_POINT_GRAPHICS,
                    material_->pipeline->pipeline);
  vkCmdDraw(cmd, 3, 1, 0, 0);

  // Store frame as input in next frame
  last_frame_view_proj_ = Renderer::getInstance().getGlobalUbo().projection *
                          Renderer::getInstance().getGlobalUbo().view;
}

void GBuffer::prepareSSR(VkCommandBuffer cmd) {
  linear_frame_->transition(
      cmd, VK_PIPELINE_STAGE_2_COLOR_ATTACHMENT_OUTPUT_BIT,
      VK_ACCESS_2_COLOR_ATTACHMENT_WRITE_BIT, VK_PIPELINE_STAGE_2_BLIT_BIT,
      VK_ACCESS_2_TRANSFER_READ_BIT, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
      VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL);
  linear_frame_ssr_->transition(
      cmd, VK_PIPELINE_STAGE_2_FRAGMENT_SHADER_BIT, VK_ACCESS_2_SHADER_READ_BIT,
      VK_PIPELINE_STAGE_2_BLIT_BIT, VK_ACCESS_2_TRANSFER_WRITE_BIT,
      VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);

  VkImageSubresourceLayers subres_src = {};
  subres_src.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
  subres_src.layerCount = 1;
  VkImageSubresourceLayers subres_dst = {};
  subres_dst.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
  subres_dst.layerCount = 1;
  VkExtent3D extent_src = linear_frame_->getExtent();
  VkExtent3D extent_dst = linear_frame_ssr_->getExtent();
  VkImageBlit blit = {};
  blit.srcOffsets[0] = {0, 0, 0};
  blit.srcOffsets[1] = {static_cast<int32_t>(extent_src.width),
                        static_cast<int32_t>(extent_src.height), 1};
  blit.srcSubresource = subres_src;
  blit.dstOffsets[0] = {0, 0, 0};
  blit.dstOffsets[1] = {static_cast<int32_t>(extent_dst.width),
                        static_cast<int32_t>(extent_dst.height), 1};
  blit.dstSubresource = subres_dst;
  vkCmdBlitImage(
      cmd, linear_frame_->getVkImage(), VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
      linear_frame_ssr_->getVkImage(), VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1,
      &blit, VK_FILTER_NEAREST);

  linear_frame_ssr_->transition(
      cmd, VK_PIPELINE_STAGE_2_BLIT_BIT, VK_ACCESS_2_TRANSFER_WRITE_BIT,
      VK_PIPELINE_STAGE_2_COMPUTE_SHADER_BIT, VK_ACCESS_2_SHADER_READ_BIT,
      VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_GENERAL);
  linear_frame_ssr_blurred_->transition(
      cmd, VK_PIPELINE_STAGE_2_BLIT_BIT, VK_ACCESS_2_TRANSFER_WRITE_BIT,
      VK_PIPELINE_STAGE_2_COMPUTE_SHADER_BIT, VK_ACCESS_2_SHADER_WRITE_BIT,
      VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_GENERAL);
  vkCmdBindDescriptorSets(cmd, VK_PIPELINE_BIND_POINT_COMPUTE,
                          ssr_compute_->pipeline->layout, 0, 1,
                          &ssr_compute_->descriptor_set->set, 0, nullptr);
  vkCmdBindPipeline(cmd, VK_PIPELINE_BIND_POINT_COMPUTE,
                    ssr_compute_->pipeline->pipeline);
  vkCmdDispatch(cmd, static_cast<uint32_t>(std::ceil(extent_dst.width / 16.0f)),
                static_cast<uint32_t>(std::ceil(extent_dst.height / 16.0f)), 1);
  linear_frame_ssr_->transition(
      cmd, VK_PIPELINE_STAGE_2_COMPUTE_SHADER_BIT, VK_ACCESS_2_SHADER_READ_BIT,
      VK_PIPELINE_STAGE_2_FRAGMENT_SHADER_BIT, VK_ACCESS_2_SHADER_READ_BIT,
      VK_IMAGE_LAYOUT_GENERAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
  linear_frame_ssr_blurred_->transition(
      cmd, VK_PIPELINE_STAGE_2_COMPUTE_SHADER_BIT, VK_ACCESS_2_SHADER_WRITE_BIT,
      VK_PIPELINE_STAGE_2_FRAGMENT_SHADER_BIT, VK_ACCESS_2_SHADER_READ_BIT,
      VK_IMAGE_LAYOUT_GENERAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
}

void GBuffer::destroy() {
  base_color_ = ImageH();
  normal_ = ImageH();
  metal_rough_ = ImageH();
  emissive_ = ImageH();
  depth_ = ImageH();
}

GBuffer::~GBuffer() { destroy(); }

void GBuffer::updateEnvironment(EnvironmentH environment) {
  DescriptorUpdater updater;
  if (environment && environment->diffuse)
    updater.updateImage(10, environment->diffuse,
                        VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
  else
    updater.updateImage(10, ibl_placeholder_cube_,
                        VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

  if (environment && environment->specular)
    updater.updateImage(11, environment->specular,
                        VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
  else
    updater.updateImage(11, ibl_placeholder_cube_,
                        VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

  if (environment && environment->brdf)
    updater.updateImage(12, environment->brdf,
                        VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
  else
    updater.updateImage(12, ibl_placeholder_2d_,
                        VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

  updater.commit(material_->descriptor_set);
  last_environment_ = environment;
}

}  // namespace Uvelon