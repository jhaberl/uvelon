#include "rendering/RenderData.hpp"

#include "core/CVarManager.hpp"
#include "rendering/Image.hpp"
#include "rendering/Renderer.hpp"
#include "rendering/Utils.hpp"
#include "utils/Instrumentation.hpp"
#include "utils/Logger.hpp"

namespace Uvelon {

template <typename T>
DataStorage<T>::DataStorage(std::function<void(T&)>&& destructor)
    : destructor_(destructor) {}

template <typename T>
DataStorage<T>::~DataStorage() {
  destroy();
}

template <typename T>
void DataStorage<T>::setDestructor(std::function<void(T&)>&& destructor) {
  destructor_ = destructor;
}

template <typename T>
void DataStorage<T>::destroy() {
  for (auto it = storage_.begin(); it != storage_.end(); it++) {
    T& element = **it;
    destructor_(element);
  }
  storage_.clear();
}

template <typename T>
void DataStorage<T>::gc() {
  auto it = storage_.begin();
  while (it != storage_.end()) {
    if (it->ref_count_ <= 0) {
      destructor_(**it);
      it = storage_.erase(it);
    } else {
      it++;
    }
  }
}

template <typename T>
RenderDataHandle<T> DataStorage<T>::add(T&& t) {
  auto& ref =
      storage_.template emplace_back<DataStorageContainer<T>>(std::move(t));
  return RenderDataHandle<T>(&ref);
}

RenderData::RenderData() {
  // MeshPrimitives does not use a destructor since all its contents are
  // destroyed in other destructors anyway

  buffers_.setDestructor([](AllocatedBuffer& buffer) {
    vmaDestroyBuffer(Renderer::getInstance().getAllocator(), buffer.buffer,
                     buffer.allocation);
  });
  samplers_.setDestructor([](VkSampler& sampler) {
    vkDestroySampler(Renderer::getInstance().getDevice(), sampler, nullptr);
  });
  images_.setDestructor([](Image& image) { image.destroy(); });
  pipelines_.setDestructor([](Pipeline& pipeline) {
    vkDestroyPipeline(Renderer::getInstance().getDevice(), pipeline.pipeline,
                      nullptr);
    vkDestroyPipelineLayout(Renderer::getInstance().getDevice(),
                            pipeline.layout, nullptr);
  });
  descriptor_sets_.setDestructor([](DescriptorSet& ds) {
    // We can only destroy the descriptor set layout because the cache does
    // nothing for now
    // TODO: Change once we cache descriptor set layouts
    // Also, this whole destructor thing might make freeing the descriptor sets
    // individually slow
    vkFreeDescriptorSets(Renderer::getInstance().getDevice(), ds.pool, 1,
                         &ds.set);
    vkDestroyDescriptorSetLayout(Renderer::getInstance().getDevice(), ds.layout,
                                 nullptr);
  });
}

RenderData::~RenderData() { destroy(); }

void RenderData::destroy() {
  auto& renderer = Renderer::getInstance();

  mesh_primitives_.destroy();
  descriptor_sets_.destroy();
  buffers_.destroy();
  samplers_.destroy();
  images_.destroy();
  materials_.destroy();
  pipelines_.destroy();
}

void RenderData::gc() {
  buffers_.gc();
  samplers_.gc();
  images_.gc();
  pipelines_.gc();
  materials_.gc();
  mesh_primitives_.gc();
  descriptor_sets_.gc();
  gaussian_splats_.gc();
}

BufferH RenderData::allocateBuffer(void* data, uint64_t size,
                                   VkBufferUsageFlags usage) {
  // Create actual buffer
  BufferH result =
      allocateBuffer(size, VK_BUFFER_USAGE_TRANSFER_DST_BIT | usage, false);

  // Create staging buffer
  VkBufferCreateInfo staging_info = {VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO};
  staging_info.size = size;
  staging_info.usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT;
  staging_info.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

  VmaAllocationCreateInfo staging_alloc = {};
  staging_alloc.usage = VMA_MEMORY_USAGE_CPU_ONLY;
  staging_alloc.flags = VMA_ALLOCATION_CREATE_MAPPED_BIT;
  staging_alloc.requiredFlags = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT |
                                VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;

  VkBuffer staging_buffer;
  VmaAllocation staging_allocation;
  VmaAllocationInfo staging_alloc_info;
  VkResult staging_alloc_result = VK_SUCCESS;
  do {
    staging_alloc_result = vmaCreateBuffer(
        Renderer::getInstance().getAllocator(), &staging_info, &staging_alloc,
        &staging_buffer, &staging_allocation, &staging_alloc_info);
  } while (staging_alloc_result == VK_ERROR_OUT_OF_DEVICE_MEMORY &&
           (staging_info.size /= 2) >= 4096);
  VK_CHECK(staging_alloc_result);

  if (staging_info.size < size) {
    UVELON_LOG_WARNING("Using staging buffer of size " +
                       std::to_string(staging_info.size) +
                       " for buffer of size " + std::to_string(size));
  }

  for (uint32_t i = 0; i < size / staging_info.size; i++) {
    // Copy data to staging buffer
    memcpy(staging_alloc_info.pMappedData,
           static_cast<char*>(data) + i * staging_info.size, staging_info.size);

    // Copy from staging buffer to buffer
    Renderer::getInstance().immediateSubmit([&](VkCommandBuffer cmd) {
      VkBufferCopy copy = {};
      copy.srcOffset = 0;
      copy.dstOffset = i * staging_info.size;
      copy.size = staging_info.size;
      vkCmdCopyBuffer(cmd, staging_buffer, result->buffer, 1, &copy);
    });
  }

  // Delete staging buffer
  vmaDestroyBuffer(Renderer::getInstance().getAllocator(), staging_buffer,
                   staging_allocation);

  return result;
}

BufferH RenderData::allocateBuffer(uint64_t size, VkBufferUsageFlags usage,
                                   bool mapped, bool map_cpu2gpu) {
  VkBufferCreateInfo buffer_info = {VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO};
  buffer_info.size = size;
  buffer_info.usage = usage;
  buffer_info.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

  VmaAllocationCreateInfo buffer_alloc = {};
  if (mapped) {
    buffer_alloc.usage =
        map_cpu2gpu ? VMA_MEMORY_USAGE_CPU_TO_GPU : VMA_MEMORY_USAGE_GPU_TO_CPU;
    buffer_alloc.flags = VMA_ALLOCATION_CREATE_MAPPED_BIT;
  } else {
    buffer_alloc.usage = VMA_MEMORY_USAGE_GPU_ONLY;
  }

  AllocatedBuffer buffer;
  buffer.size = size;
  VK_CHECK(vmaCreateBuffer(Renderer::getInstance().getAllocator(), &buffer_info,
                           &buffer_alloc, &buffer.buffer, &buffer.allocation,
                           &buffer.allocation_info));

  return addBuffer(std::move(buffer));
}

BufferH RenderData::addBuffer(AllocatedBuffer&& buffer) {
  return buffers_.add(std::move(buffer));
}

SamplerH RenderData::createSampler(VkFilter mag_filter, VkFilter min_filter,
                                   VkSamplerMipmapMode mipmap_mode,
                                   VkSamplerAddressMode address_mode_u,
                                   VkSamplerAddressMode address_mode_v,
                                   bool unnormalized_coordinates,
                                   bool compare) {
  VkSamplerCreateInfo sampler_info = {VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO};
  sampler_info.magFilter = mag_filter;
  sampler_info.minFilter = min_filter;
  sampler_info.mipmapMode = mipmap_mode;
  sampler_info.addressModeU = address_mode_u;
  sampler_info.addressModeV = address_mode_v;
  sampler_info.unnormalizedCoordinates = unnormalized_coordinates;
  sampler_info.compareEnable = compare;
  sampler_info.compareOp = VK_COMPARE_OP_GREATER;
  if (!unnormalized_coordinates) sampler_info.maxLod = VK_LOD_CLAMP_NONE;

  VkSampler sampler;
  VK_CHECK(vkCreateSampler(Renderer::getInstance().getDevice(), &sampler_info,
                           nullptr, &sampler));
  return samplers_.add(std::move(sampler));
}

ImageH RenderData::addImage(Image&& image) {
  return images_.add(std::move(image));
}

PipelineH RenderData::addPipeline(Pipeline&& pipeline) {
  return pipelines_.add(std::move(pipeline));
}

MaterialH RenderData::addMaterial(Material&& material) {
  return materials_.add(std::move(material));
}

MeshPrimitiveH RenderData::addMeshPrimitive(MeshPrimitive&& mesh_primitive) {
  return mesh_primitives_.add(std::move(mesh_primitive));
}

DescriptorSetH RenderData::addDescriptorSet(DescriptorSet&& descriptor_set) {
  return descriptor_sets_.add(std::move(descriptor_set));
}

GaussianSplatsH RenderData::addGaussianSplats(
    GaussianSplats&& gaussian_splats) {
  return gaussian_splats_.add(std::move(gaussian_splats));
}

EnvironmentH RenderData::addEnvironment(Environment&& environment) {
  return environments_.add(std::move(environment));
}

void RenderData::hotReloadShaders() {
  bool& trigger =
      CVarManager::getVar("shaders.hot_reload", false).getValue<bool>();
  if (trigger) {
    UVELON_PROFILE("Shader hot reload");
    // TODO: Do this in a separate thread
    Renderer::getInstance().getShaderCache().destroy();

    auto material_it = materials_.storage_.begin();
    while (material_it != materials_.storage_.end()) {
      Material& material = **material_it;
      PipelineH old_pipeline = material.pipeline;
      material.pipeline = PipelineBuilder::fromExisting(*old_pipeline).build();
      // Remove layout from old layout to avoid it being destroyed in gc
      old_pipeline->layout = VK_NULL_HANDLE;
      material_it++;
    }

    auto gs_it = gaussian_splats_.storage_.begin();
    while (gs_it != gaussian_splats_.storage_.end()) {
      GaussianSplats& splats = **gs_it;

      PipelineH old_pipeline = splats.sort_pipe_depth;
      splats.sort_pipe_depth =
          PipelineBuilder::fromExisting(*old_pipeline).build();
      old_pipeline->layout = VK_NULL_HANDLE;

      old_pipeline = splats.sort_pipe_hist;
      splats.sort_pipe_hist =
          PipelineBuilder::fromExisting(*old_pipeline).build();
      old_pipeline->layout = VK_NULL_HANDLE;

      old_pipeline = splats.sort_pipe_sort;
      splats.sort_pipe_sort =
          PipelineBuilder::fromExisting(*old_pipeline).build();
      old_pipeline->layout = VK_NULL_HANDLE;

      if (splats.reduction_pipe) {
        old_pipeline = splats.reduction_pipe;
        splats.reduction_pipe =
            PipelineBuilder::fromExisting(*old_pipeline).build();
        old_pipeline->layout = VK_NULL_HANDLE;
      }

      gs_it++;
    }
    trigger = false;
  }
}

}  // namespace Uvelon