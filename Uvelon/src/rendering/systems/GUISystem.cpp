#include "rendering/systems/GUISystem.hpp"

#include "backends/imgui_impl_sdl3.h"
#include "backends/imgui_impl_vulkan.h"
#include "implot.h"
#include "rendering/Renderer.hpp"
#include "rendering/Utils.hpp"
#include "rendering/Window.hpp"

namespace Uvelon {

void GUISystem::init() {
  auto& renderer = Renderer::getInstance();

  VkDescriptorPoolSize pool_sizes[] = {
      {VK_DESCRIPTOR_TYPE_SAMPLER, 1000},
      {VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 1000},
      {VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE, 1000},
      {VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, 1000},
      {VK_DESCRIPTOR_TYPE_UNIFORM_TEXEL_BUFFER, 1000},
      {VK_DESCRIPTOR_TYPE_STORAGE_TEXEL_BUFFER, 1000},
      {VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 1000},
      {VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, 1000},
      {VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC, 1000},
      {VK_DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC, 1000},
      {VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT, 1000}};

  VkDescriptorPoolCreateInfo pool_info = {
      VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO};
  pool_info.flags = VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT;
  pool_info.maxSets = 1000;
  pool_info.poolSizeCount = static_cast<uint32_t>(std::size(pool_sizes));
  pool_info.pPoolSizes = pool_sizes;

  VK_CHECK(vkCreateDescriptorPool(renderer.getDevice(), &pool_info, nullptr,
                                  &pool_));

  uint32_t graphics_queue_family, compute_queue_family;
  renderer.getQueueFamilies(graphics_queue_family, compute_queue_family);

  ImGui::CreateContext();
  ImPlot::CreateContext();

  ImGui_ImplSDL3_InitForVulkan(renderer.getInitConfig().window->getWindow());
  ImGui_ImplVulkan_InitInfo imgui_info = {};
  imgui_info.Instance = renderer.getVkInstance();
  imgui_info.PhysicalDevice = renderer.getPhysicalDevice();
  imgui_info.Device = renderer.getDevice();
  imgui_info.QueueFamily = graphics_queue_family;
  imgui_info.Queue = renderer.getQueueGraphics();
  imgui_info.DescriptorPool = pool_;
  imgui_info.MinImageCount = renderer.getSwapchain().getNumFrames();
  imgui_info.ImageCount = renderer.getSwapchain().getNumFrames();
  imgui_info.UseDynamicRendering = true;
  imgui_info.PipelineRenderingCreateInfo = {
      VK_STRUCTURE_TYPE_PIPELINE_RENDERING_CREATE_INFO};
  imgui_info.PipelineRenderingCreateInfo.colorAttachmentCount = 1;
  VkFormat color_format = renderer.getSwapchain().getImageFormat();
  imgui_info.PipelineRenderingCreateInfo.pColorAttachmentFormats =
      &color_format;
  ImGui_ImplVulkan_Init(&imgui_info);
  ImGui_ImplVulkan_CreateFontsTexture();
}

void GUISystem::destroy() {
  ImPlot::CreateContext();
  ImGui::CreateContext();

  ImGui_ImplVulkan_Shutdown();
  vkDestroyDescriptorPool(Renderer::getInstance().getDevice(), pool_, nullptr);
}

void GUISystem::draw(VkCommandBuffer cmd, Stage stage,
                     FilteredIterator<RenderObject> begin,
                     FilteredIterator<RenderObject> end) {
  if (stage == Stage::SWAPCHAIN_FINAL) {
    ImGui_ImplVulkan_RenderDrawData(ImGui::GetDrawData(), cmd);
  }
}

}  // namespace Uvelon