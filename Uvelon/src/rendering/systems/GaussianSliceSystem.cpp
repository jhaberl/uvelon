#include "rendering/systems/GaussianSliceSystem.hpp"

#define STB_IMAGE_WRITE_IMPLEMENTATION

#include <fstream>

#include "core/CVarManager.hpp"
#include "core/FileWizard.hpp"
#include "rendering/Descriptor.hpp"
#include "rendering/RenderData.hpp"
#include "rendering/Renderer.hpp"
#include "stb_image.h"
#include "stb_image_write.h"
#include "utils/Instrumentation.hpp"
#include "utils/Logger.hpp"

namespace Uvelon {

GaussianSliceSystem::GaussianSliceSystem() : RenderSystem() {
  // Init cvars
  CVarManager::setVar("gaussian.slice.export", false, false);
  CVarManager::setVar("gaussian.slice.num_slices.x", 1000, false);
  CVarManager::setVar("gaussian.slice.num_slices.y", 1000, false);
  CVarManager::setVar("gaussian.slice.num_slices.z", 1000, false);
  CVarManager::setVar("gaussian.slice.bounds.x", 3.0, false);
  CVarManager::setVar("gaussian.slice.bounds.y", 3.0, false);
  CVarManager::setVar("gaussian.slice.bounds.z", 3.0, false);
  CVarManager::setVar("gaussian.slice.opacity", 0.5, false);
  CVarManager::setVar("gaussian.slice.path", "./slices", false);
}

bool GaussianSliceSystem::canDraw(RenderObject& render_object) {
  return render_object.gaussian_splats &&
         !render_object.gaussian_splats->proxy_mesh;
}

// TODO: Stop blocking the application during this (low priority)
void GaussianSliceSystem::prepare(VkCommandBuffer dontuse,
                                  FilteredIterator<RenderObject> begin,
                                  FilteredIterator<RenderObject> end) {
  bool& export_slices =
      CVarManager::getVar("gaussian.slice.export").getValue<bool>();

  // TODO: Better support for multiple simultaneous scenes

  if (export_slices) {
    uint32_t dims_x = static_cast<uint32_t>(
        CVarManager::getVar("gaussian.slice.num_slices.x").getValue<int32_t>());
    uint32_t dims_y = static_cast<uint32_t>(
        CVarManager::getVar("gaussian.slice.num_slices.y").getValue<int32_t>());
    uint32_t dims_z = static_cast<uint32_t>(
        CVarManager::getVar("gaussian.slice.num_slices.z").getValue<int32_t>());
    float bounds_x = static_cast<float>(
        CVarManager::getVar("gaussian.slice.bounds.x").getValue<double>());
    float bounds_y = static_cast<float>(
        CVarManager::getVar("gaussian.slice.bounds.y").getValue<double>());
    float bounds_z = static_cast<float>(
        CVarManager::getVar("gaussian.slice.bounds.z").getValue<double>());
    float min_opacity = static_cast<float>(
        CVarManager::getVar("gaussian.slice.opacity").getValue<double>());
    std::string& out_path =
        CVarManager::getVar("gaussian.slice.path").getValue<std::string>();

    std::array<VkDeviceSize, 3> vertex_offsets = {0, 0, 0};
    auto& render_computer = Renderer::getInstance().getRenderComputer();
    std::ofstream out_file(out_path, std::ios::binary);
    if (!out_file.is_open()) {
      UVELON_LOG_WARNING("Could not open " + out_path);
      export_slices = false;
      return;
    }

    auto target_image = Renderer::getInstance().getRenderData().addImage(Image(
        {dims_x, dims_y, 1}, VK_FORMAT_R8_UNORM,
        VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT));
    auto staging_buffer =
        Renderer::getInstance().getRenderData().allocateBuffer(
            dims_x * dims_y, VK_BUFFER_USAGE_TRANSFER_DST_BIT, true, false);
    GaussianSliceExtra extra;
    extra.width = dims_x;
    extra.height = dims_y;
    extra.bounds_x = bounds_x;
    extra.bounds_y = bounds_y;
    extra.min_opacity = min_opacity;
    auto extra_buffer = Renderer::getInstance().getRenderData().allocateBuffer(
        sizeof(GaussianSliceExtra), VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, true);

    VkPushConstantRange push_constant = {};
    push_constant.size = sizeof(glm::mat4);  // Global UBO
    push_constant.stageFlags = VK_SHADER_STAGE_ALL_GRAPHICS;

    // Render
    auto it = begin;
    while (it != end) {
      UVELON_LOG_INFO("Exporting slices to file");
      auto splats = it->gaussian_splats;

      auto extra_ds = DescriptorBuilder::begin()
                          .addBuffer(VK_SHADER_STAGE_ALL_GRAPHICS,
                                     VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
                                     extra_buffer, 0, extra_buffer->size)
                          .build();
      auto format = target_image->getFormat();
      auto pipeline =
          PipelineBuilder::beginGraphics()
              .addPushConstant(push_constant)
              .addDescriptorSet(
                  Renderer::getInstance().getGlobalDescriptorSet()->layout)
              .addDescriptorSet(splats->material->descriptor_set->layout)
              .addDescriptorSet(extra_ds->layout)
              .addShader(UVELON_FILE_RESOURCES(
                             "shaders/gaussian_splatting/gsplat_slice.vert")
                             .string(),
                         VK_SHADER_STAGE_VERTEX_BIT)
              .addShader(UVELON_FILE_RESOURCES(
                             "shaders/gaussian_splatting/gsplat_slice.frag")
                             .string(),
                         VK_SHADER_STAGE_FRAGMENT_BIT)
              .setVertexInputInfo(nullptr, 0, nullptr, 0)
              .setTopology(VK_PRIMITIVE_TOPOLOGY_POINT_LIST)
              .setAttachmentFormats(&format, 1, VK_FORMAT_UNDEFINED)
              .build();

      for (uint32_t slice = 0; slice < dims_z; slice++) {
        UVELON_PROFILE(std::to_string(slice));
        extra.z = -bounds_z / 2 + bounds_z / dims_z * slice;
        memcpy(extra_buffer->allocation_info.pMappedData, &extra,
               sizeof(GaussianSliceExtra));

        auto cmd = render_computer.getCommandBuffer();

        VkViewport viewport = {
            0.0f, 0.0f, static_cast<float>(dims_x), static_cast<float>(dims_y),
            0.0f, 1.0f};
        VkRect2D scissor = {{0, 0}, {dims_x, dims_y}};
        vkCmdSetViewport(cmd, 0, 1, &viewport);
        vkCmdSetScissor(cmd, 0, 1, &scissor);

        VkImageSubresourceRange subres_target = {};
        subres_target.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        subres_target.levelCount = target_image->getMipLevels();
        subres_target.layerCount = target_image->getArrayLayers();
        Image::transitionVkImage(
            cmd, target_image->getVkImage(), VK_PIPELINE_STAGE_2_NONE, 0,
            VK_PIPELINE_STAGE_2_COLOR_ATTACHMENT_OUTPUT_BIT,
            VK_ACCESS_2_COLOR_ATTACHMENT_WRITE_BIT, VK_IMAGE_LAYOUT_UNDEFINED,
            VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL, subres_target);

        VkClearValue color_clear;
        color_clear.color = {0.0f};
        VkRenderingAttachmentInfo color_info_;
        color_info_ = {VK_STRUCTURE_TYPE_RENDERING_ATTACHMENT_INFO};
        color_info_.imageLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
        color_info_.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
        color_info_.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
        color_info_.clearValue = color_clear;
        color_info_.imageView = target_image->getVkImageView();
        VkRenderingInfoKHR rendering_info = {
            VK_STRUCTURE_TYPE_RENDERING_INFO_KHR};
        rendering_info.renderArea.offset = {0, 0};
        rendering_info.renderArea.extent = {dims_x, dims_y};
        rendering_info.layerCount = 1;
        rendering_info.colorAttachmentCount = 1;
        rendering_info.pColorAttachments = &color_info_;
        Renderer::fp_vkCmdBeginRendering(cmd, &rendering_info);

        // Rendering
        vkCmdBindPipeline(cmd, VK_PIPELINE_BIND_POINT_GRAPHICS,
                          pipeline->pipeline);
        vkCmdBindDescriptorSets(
            cmd, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline->layout, 0, 1,
            &Renderer::getInstance().getGlobalDescriptorSet()->set, 0, nullptr);
        vkCmdBindDescriptorSets(
            cmd, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline->layout, 1, 1,
            &splats->material->descriptor_set->set, 0, nullptr);
        vkCmdBindDescriptorSets(cmd, VK_PIPELINE_BIND_POINT_GRAPHICS,
                                pipeline->layout, 2, 1, &extra_ds->set, 0,
                                nullptr);
        vkCmdPushConstants(cmd, pipeline->layout, VK_SHADER_STAGE_ALL_GRAPHICS,
                           0, sizeof(glm::mat4), &it->transform);
        vkCmdBindIndexBuffer(cmd, splats->indices->buffer, 0,
                             VK_INDEX_TYPE_UINT32);
        vkCmdDrawIndexed(cmd,
                         static_cast<uint32_t>(splats->num_inmemory_current), 1,
                         0, 0, 0);

        Renderer::fp_vkCmdEndRendering(cmd);

        // Copy image to buffer and save
        Image::transitionVkImage(
            cmd, target_image->getVkImage(),
            VK_PIPELINE_STAGE_2_COLOR_ATTACHMENT_OUTPUT_BIT,
            VK_ACCESS_2_COLOR_ATTACHMENT_WRITE_BIT,
            VK_PIPELINE_STAGE_2_TRANSFER_BIT, VK_ACCESS_2_TRANSFER_READ_BIT,
            VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL, VK_IMAGE_LAYOUT_GENERAL,
            subres_target);

        VkImageSubresourceLayers subresource = {};
        subresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        subresource.mipLevel = 0;
        subresource.baseArrayLayer = 0;
        subresource.layerCount = 1;

        VkBufferImageCopy img_copy = {};
        img_copy.bufferOffset = 0;
        img_copy.bufferRowLength = 0;
        img_copy.bufferImageHeight = 0;
        img_copy.imageSubresource = subresource;
        img_copy.imageOffset = {0, 0, 0};
        img_copy.imageExtent = {dims_x, dims_y, 1};
        vkCmdCopyImageToBuffer(cmd, target_image->getVkImage(),
                               VK_IMAGE_LAYOUT_GENERAL, staging_buffer->buffer,
                               1, &img_copy);
        render_computer.submit(cmd, {}, {}, true);

        int len;
        unsigned char* out_png = stbi_write_png_to_mem(
            static_cast<unsigned char*>(
                staging_buffer->allocation_info.pMappedData),
            dims_x, dims_x, dims_y, 1, &len);
        out_file.write(reinterpret_cast<char*>(out_png), len);
      }
      it++;
    }

    out_file.close();

    export_slices = false;
  }
}

}  // namespace Uvelon