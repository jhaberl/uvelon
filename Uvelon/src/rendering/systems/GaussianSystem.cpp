#include "rendering/systems/GaussianSystem.hpp"

#include <algorithm>
#ifndef __APPLE__
#include <execution>
#endif

#ifdef __APPLE__
#include "TargetConditionals.h"
#endif

#include "core/CVarManager.hpp"
#include "rendering/Renderer.hpp"
#include "utils/Instrumentation.hpp"
#include "utils/Logger.hpp"
#include "utils/MappedFile.hpp"

namespace Uvelon {

GaussianSystem::GaussianSystem() {
  copy_pool_.init(GAUSSIAN_MAX_PAGES_PER_FRAME);
}
GaussianSystem::~GaussianSystem() { copy_pool_.destroy(); }

bool GaussianSystem::canDraw(RenderObject& object) {
  return object.gaussian_splats;
}

void GaussianSystem::prepare(VkCommandBuffer cmd,
                             FilteredIterator<RenderObject> begin,
                             FilteredIterator<RenderObject> end) {
  bool gsplat_vmem = false;
  for (auto it = begin; it != end; it++) {
    bool require_sort = true;
    frustum_vis_ = false;
    if (it->gaussian_splats->proxy_mesh) {
      require_sort = processVmem(cmd, *it);
      gsplat_vmem = true;
    }

    // if (require_sort)
    sortByDepth(cmd, *it);
  }
  CVarManager::setVar("gaussian.vmem", gsplat_vmem);
  frame_count_++;
}

bool GaussianSystem::processVmem(VkCommandBuffer cmd, RenderObject& ro) {
  auto timer_proxy_render_reduce =
      UVELON_START_TIMER("proxy_render_reduce", Reporting::CVAR);
  bool made_changes = false;
  auto& render_computer = Renderer::getInstance().getRenderComputer();
  std::array<VkDeviceSize, 1> vertex_offsets = {0};

  auto splats = ro.gaussian_splats;
  auto image_extent = splats->proxy_image->getExtent();
  auto primitive = splats->proxy_mesh;

  uint32_t pagesize_bytes = splats->page_size * GAUSSIAN_PER_GAUSSIAN_BYTES;

  uint32_t max_pages =
      static_cast<uint32_t>(splats->num_total / splats->page_size + 1);

  // Render image with page ids using proxy mesh
  VkCommandBuffer render_now_cmd = render_computer.getCommandBuffer();

  VkViewport viewport = {0.0f,
                         0.0f,
                         static_cast<float>(image_extent.width),
                         static_cast<float>(image_extent.height),
                         0.0f,
                         1.0f};
  VkRect2D scissor = {{0, 0},
                      {static_cast<uint32_t>(image_extent.width),
                       static_cast<uint32_t>(image_extent.height)}};
  vkCmdSetViewport(render_now_cmd, 0, 1, &viewport);
  vkCmdSetScissor(render_now_cmd, 0, 1, &scissor);

  frustum_vis_ =
      CVarManager::getVar("gaussian.vmem.frustum_vis", false).getValue<bool>();
  if (frustum_vis_) {
    GlobalUniformBuffer global_replacement =
        Renderer::getInstance().getGlobalUbo();
    for (int i = 0; i < 4; i++) {
      for (int j = 0; j < 4; j++) {
        global_replacement.view[i][j] = static_cast<float>(
            CVarManager::getVar("gaussian.vmem.frustum_vis.view." +
                                std::to_string(i) + "." + std::to_string(j))
                .getValue<double>());
        global_replacement.projection[i][j] = static_cast<float>(
            CVarManager::getVar("gaussian.vmem.frustum_vis.projection." +
                                std::to_string(i) + "." + std::to_string(j))
                .getValue<double>());
      }
    }
    global_replacement.inv_view_projection =
        glm::inverse(global_replacement.projection * global_replacement.view);
    memcpy(Renderer::getInstance()
               .getGlobalUboBuffer()
               ->allocation_info.pMappedData,
           &global_replacement, sizeof(GlobalUniformBuffer));
  }

  VkRenderingAttachmentInfo info = {
      VK_STRUCTURE_TYPE_RENDERING_ATTACHMENT_INFO};
  info.imageLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
  info.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
  info.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
  info.clearValue = {0.0f, 0.0f, 0.0f, 0.0f};
  info.imageView = splats->proxy_image->getVkImageView();
  VkRenderingAttachmentInfo depth_info = {
      VK_STRUCTURE_TYPE_RENDERING_ATTACHMENT_INFO};
  depth_info.imageLayout = VK_IMAGE_LAYOUT_DEPTH_ATTACHMENT_OPTIMAL;
  depth_info.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
  depth_info.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
  depth_info.clearValue = {0.0f, 0};
  depth_info.imageView = splats->proxy_image_depth->getVkImageView();

  splats->proxy_image->transition(
      render_now_cmd, VK_PIPELINE_STAGE_2_TOP_OF_PIPE_BIT, 0,
      VK_PIPELINE_STAGE_2_COLOR_ATTACHMENT_OUTPUT_BIT,
      VK_ACCESS_2_COLOR_ATTACHMENT_WRITE_BIT, VK_IMAGE_LAYOUT_UNDEFINED,
      VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL);
  splats->proxy_image_depth->transition(
      render_now_cmd, VK_PIPELINE_STAGE_2_TOP_OF_PIPE_BIT, 0,
      VK_PIPELINE_STAGE_2_EARLY_FRAGMENT_TESTS_BIT |
          VK_PIPELINE_STAGE_2_LATE_FRAGMENT_TESTS_BIT,
      VK_ACCESS_2_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT, VK_IMAGE_LAYOUT_UNDEFINED,
      VK_IMAGE_LAYOUT_DEPTH_ATTACHMENT_OPTIMAL);

  VkRenderingInfoKHR rendering_info = {VK_STRUCTURE_TYPE_RENDERING_INFO_KHR};
  rendering_info.renderArea.offset = {0, 0};
  rendering_info.renderArea.extent = {image_extent.width, image_extent.height};
  rendering_info.layerCount = 1;
  rendering_info.colorAttachmentCount = 1;
  rendering_info.pColorAttachments = &info;
  rendering_info.pDepthAttachment = &depth_info;
  Renderer::fp_vkCmdBeginRendering(render_now_cmd, &rendering_info);
  vkCmdBindDescriptorSets(
      render_now_cmd, VK_PIPELINE_BIND_POINT_GRAPHICS,
      primitive->material->pipeline->layout, 0, 1,
      &Renderer::getInstance().getGlobalDescriptorSet()->set, 0, nullptr);
  vkCmdBindDescriptorSets(render_now_cmd, VK_PIPELINE_BIND_POINT_GRAPHICS,
                          primitive->material->pipeline->layout, 1, 1,
                          &primitive->material->descriptor_set->set, 0,
                          nullptr);
  vkCmdBindPipeline(render_now_cmd, VK_PIPELINE_BIND_POINT_GRAPHICS,
                    primitive->material->pipeline->pipeline);
  vkCmdPushConstants(render_now_cmd, primitive->material->pipeline->layout,
                     VK_SHADER_STAGE_ALL_GRAPHICS | VK_SHADER_STAGE_COMPUTE_BIT,
                     0, sizeof(glm::mat4), &ro.transform);
  vkCmdBindVertexBuffers(render_now_cmd, 0, 1,
                         &primitive->vertex_buffers[0]->buffer,
                         vertex_offsets.data());
  vkCmdBindIndexBuffer(render_now_cmd, primitive->indices->buffer, 0,
                       primitive->index_type);
  vkCmdDrawIndexed(render_now_cmd, primitive->index_count, 1, 0, 0, 0);
  Renderer::fp_vkCmdEndRendering(render_now_cmd);

  // Functions as barrier too
  splats->proxy_image->transition(
      render_now_cmd, VK_PIPELINE_STAGE_2_COLOR_ATTACHMENT_OUTPUT_BIT,
      VK_ACCESS_2_COLOR_ATTACHMENT_WRITE_BIT,
      VK_PIPELINE_STAGE_2_COMPUTE_SHADER_BIT, VK_ACCESS_2_MEMORY_READ_BIT,
      VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
      VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
  splats->proxy_image_depth->transition(
      render_now_cmd,
      VK_PIPELINE_STAGE_2_EARLY_FRAGMENT_TESTS_BIT |
          VK_PIPELINE_STAGE_2_LATE_FRAGMENT_TESTS_BIT,
      VK_ACCESS_2_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT,
      VK_PIPELINE_STAGE_2_COMPUTE_SHADER_BIT, VK_ACCESS_2_MEMORY_READ_BIT,
      VK_IMAGE_LAYOUT_DEPTH_ATTACHMENT_OPTIMAL,
      VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

  // Reduce image to determine required pages
  std::array<VkDescriptorSet, 2> descriptor_sets = {
      Renderer::getInstance().getGlobalDescriptorSet()->set,
      ro.gaussian_splats->reduction_ds->set};
  vkCmdBindDescriptorSets(render_now_cmd, VK_PIPELINE_BIND_POINT_COMPUTE,
                          ro.gaussian_splats->reduction_pipe->layout, 0,
                          static_cast<uint32_t>(descriptor_sets.size()),
                          descriptor_sets.data(), 0, nullptr);
  vkCmdBindPipeline(render_now_cmd, VK_PIPELINE_BIND_POINT_COMPUTE,
                    ro.gaussian_splats->reduction_pipe->pipeline);
  vkCmdDispatch(render_now_cmd,
                static_cast<uint32_t>(ceil(image_extent.width / 16.0f)),
                static_cast<uint32_t>(ceil(image_extent.height / 16.0f)), 1);
  memset(ro.gaussian_splats->reduction_result->allocation_info.pMappedData, 0,
         max_pages * sizeof(uint32_t));
  render_computer.submit(render_now_cmd, {}, {}, true);
  UVELON_END_TIMER(timer_proxy_render_reduce);

  auto timer_pagetable_update =
      UVELON_START_TIMER("pagetable_update", Reporting::CVAR);
  if (frustum_vis_) {
    copy_pool_.enqueue([this]() {
      memcpy(Renderer::getInstance()
                 .getGlobalUboBuffer()
                 ->allocation_info.pMappedData,
             &Renderer::getInstance().getGlobalUbo(),
             sizeof(GlobalUniformBuffer));
    });
  }

  // Update page table
  uint32_t* reduction_result = static_cast<uint32_t*>(
      splats->reduction_result->allocation_info.pMappedData);

  // Determine required pages based on reduction result
  reduction_result_cpu_.resize(max_pages);
  memcpy(reduction_result_cpu_.data(), reduction_result,
         max_pages * sizeof(uint32_t));
  // Convert distances to LOD level + 1
  const glm::mat4& projection =
      Renderer::getInstance().getGlobalUbo().projection;
  uint32_t far =
      static_cast<uint32_t>(std::ceil(-projection[3][2] / projection[2][2])) *
      1000;
  int32_t lod_thresholds[3] = {
      CVarManager::getVar("gaussian.vmem.lod.threshold1", 800)
          .getValue<int32_t>(),
      CVarManager::getVar("gaussian.vmem.lod.threshold2", 1250)
          .getValue<int32_t>(),
      CVarManager::getVar("gaussian.vmem.lod.threshold3", 1500)
          .getValue<int32_t>()};
  uint64_t stats_pages_needed = 0;
  for (uint32_t page = 0; page < max_pages; page++) {
    uint32_t linear_depth = reduction_result_cpu_[page];
    if (linear_depth > 0) {
      linear_depth = far - linear_depth;
      uint32_t desired_lod = 1;
      if (linear_depth > static_cast<uint32_t>(lod_thresholds[2])) {
        desired_lod = 4;
      } else if (linear_depth > static_cast<uint32_t>(lod_thresholds[1])) {
        desired_lod = 3;
      } else if (linear_depth > static_cast<uint32_t>(lod_thresholds[0])) {
        desired_lod = 2;
      } else {
        desired_lod = 1;
      }
      reduction_result_cpu_[page] =
          std::min(desired_lod, splats->lod_levels + 1);
      stats_pages_needed++;
    }
  }

  // Reset free physical pages
  while (free_physical_pages_.size() < splats->lod_levels + 1)
    free_physical_pages_.emplace_back();
  for (auto& free : free_physical_pages_) free.clear();

  // Go through page table and check which physical pages are still required,
  // which can be modified
  for (uint32_t physical_page = 0; physical_page < splats->page_table.size();
       physical_page++) {
    auto& pagetable_entry = splats->page_table[physical_page];
    bool pte_modified = false;
    unsigned char num_required = 0;

    for (uint32_t subpage_index = 0;
         subpage_index < static_cast<uint32_t>(pow(2, pagetable_entry.level));
         subpage_index++) {
      if (pagetable_entry.page_ids[subpage_index]) {
        // Check if page is required
        bool was_required = pteIsRequired(pagetable_entry, subpage_index);
        bool required =
            (reduction_result_cpu_[pagetable_entry.page_ids[subpage_index]] -
             1) == pagetable_entry.level;
        pte_modified |= was_required != required;

        pteSetRequired(pagetable_entry, subpage_index, required);
        if (required) {  // Page in memory is required
          // No longer needs to be transferred
          reduction_result_cpu_[pagetable_entry.page_ids[subpage_index]] = 0;
          pagetable_entry.last_used = frame_count_;
          num_required++;
          stats_pages_needed--;
        }
      }
    }

    if (num_required == 0) {
      // No present LOD subpages are required, could change page's LOD level
      free_physical_pages_[0].emplace_back(pagetable_entry.last_used,
                                           physical_page);
    } else {
      for (int i = 0;
           i < static_cast<int>(pow(2, pagetable_entry.level)) - num_required;
           i++)
        free_physical_pages_[pagetable_entry.level].emplace_back(
            pagetable_entry.last_used, physical_page);
    }

    made_changes |= pte_modified;
  }

  // LRU for free physical pages
  for (uint32_t i = 0; i < splats->lod_levels + 1; i++) {
#ifndef __APPLE__
    std::sort(std::execution::parallel_unsequenced_policy(),
              free_physical_pages_[i].begin(), free_physical_pages_[i].end());
#else
    std::sort(free_physical_pages_[i].begin(), free_physical_pages_[i].end());
#endif
  }

  // Go through all required pages that are not present and move them to the
  // GPU via staging buffer
  uint32_t free_physical_page_index[4]{0, 0, 0, 0};
  uint32_t staging_buffer_offset = 0;
  copy_regions_.clear();
  for (uint32_t required_page = 1; required_page < max_pages;
       required_page++) {  // Never require page 0
    if (!reduction_result_cpu_[required_page]) continue;

    // Page is required but not present in memory
    // Copy to staging buffer
    uint32_t level = reduction_result_cpu_[required_page] - 1;
    uint32_t pagesize_bytes_lod =
        pagesize_bytes / static_cast<uint32_t>(pow(2, level));

    if (staging_buffer_offset + pagesize_bytes_lod >
        GAUSSIAN_MAX_PAGES_PER_FRAME * splats->page_size *
            GAUSSIAN_PER_GAUSSIAN_BYTES) {
      break;  // Staging buffer does not have enough space remaining
    }

    if (free_physical_page_index[level] == free_physical_pages_[level].size() &&
        free_physical_page_index[0] == free_physical_pages_[0].size())
      break;  // No more free pages (this LOD level or top)!

    uint32_t lod_offset = 0;
    for (uint32_t i = 0; i < level; i++)
      lod_offset += (splats->num_pages * pagesize_bytes) /
                    static_cast<uint32_t>(pow(2, i));

    // Perform copy to staging buffer asynchronously, await before submitting
    void* copy_src = static_cast<char*>(splats->source_file->data) +
                     splats->header_size + lod_offset +
                     pagesize_bytes_lod * (required_page - 1);
    void* copy_dst =
        static_cast<char*>(
            splats->vmem_staging_buffer->allocation_info.pMappedData) +
        staging_buffer_offset;
    size_t copy_len = pagesize_bytes_lod;
    copy_pool_.enqueue([copy_src, copy_dst, copy_len] {
      memcpy(copy_dst, copy_src, copy_len);
    });
    stats_pages_needed--;

    // Update page table
    uint32_t physical_page_index = 0;
    uint32_t physical_page_lod_offset = 0;
    if (level > 0 &&
        free_physical_page_index[level] < free_physical_pages_[level].size()) {
      // Using page already set to the correct LOD level
      physical_page_index =
          free_physical_pages_[level][free_physical_page_index[level]].second;

      auto& pagetable_entry = splats->page_table[physical_page_index];
      for (int i = 0; i < static_cast<int>(pow(2, level)); i++) {
        if (!pteIsRequired(pagetable_entry, i)) {
          physical_page_lod_offset = i * pagesize_bytes_lod;
          pagetable_entry.page_ids[i] = required_page;
          pteSetRequired(pagetable_entry, i, true);
          pagetable_entry.last_used = frame_count_;
          break;
        }
      }

      free_physical_page_index[level]++;
    } else {
      // No free pages on this LOD level, use a top level page
      physical_page_index =
          free_physical_pages_[0][free_physical_page_index[0]].second;

      auto& pagetable_entry = splats->page_table[physical_page_index];
      pagetable_entry.level = level;
      memset(&pagetable_entry.page_ids, 0,
             static_cast<size_t>(pow(2, level)) * sizeof(uint32_t));
      memset(&pagetable_entry.required, 0,
             static_cast<size_t>(pow(2, level)) * sizeof(bool));
      pagetable_entry.page_ids[0] = required_page;
      pteSetRequired(pagetable_entry, 0, true);
      pagetable_entry.last_used = frame_count_;

      for (int i = 1; i < static_cast<int>(pow(2, level)); i++)
        free_physical_pages_[level].emplace_back(pagetable_entry.last_used,
                                                 physical_page_index);

      free_physical_page_index[0]++;
    }

    // Add command to copy from staging buffer to actual buffer
    copy_regions_.emplace_back();
    copy_regions_.back().srcOffset = staging_buffer_offset;
    copy_regions_.back().dstOffset =
        physical_page_index * pagesize_bytes + physical_page_lod_offset;
    copy_regions_.back().size = pagesize_bytes_lod;

    staging_buffer_offset += pagesize_bytes_lod;
  }
  if (!copy_regions_.empty()) {
    made_changes = true;
    vkCmdCopyBuffer(cmd, splats->vmem_staging_buffer->buffer,
                    splats->gaussian_buffer->buffer,
                    static_cast<uint32_t>(copy_regions_.size()),
                    copy_regions_.data());

    // Barrier for transfer
    VkBufferMemoryBarrier2 barrier_transfer = {
        VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER_2};
    barrier_transfer.buffer = splats->sort_buf_packed[1]->buffer;
    barrier_transfer.size = VK_WHOLE_SIZE;
    barrier_transfer.srcStageMask = VK_PIPELINE_STAGE_2_TRANSFER_BIT;
    barrier_transfer.srcAccessMask = VK_ACCESS_2_TRANSFER_WRITE_BIT;
    barrier_transfer.dstStageMask = VK_PIPELINE_STAGE_2_COMPUTE_SHADER_BIT;
    barrier_transfer.dstAccessMask = VK_ACCESS_2_MEMORY_READ_BIT;
    VkDependencyInfo dep_transfer = {VK_STRUCTURE_TYPE_DEPENDENCY_INFO};
    dep_transfer.bufferMemoryBarrierCount = 1;
    dep_transfer.pBufferMemoryBarriers = &barrier_transfer;
    Renderer::fp_vkCmdPipelineBarrier2(cmd, &dep_transfer);
  }

  // Write physical pages to second buffer used for sorting. The depth sort
  // compute shader will construct the full index buffer.
  // TODO: Deal with LOD properly
  index2_cpu_.clear();
  splats->num_inmemory_current = 0;
  uint32_t stats_inmemory_current[4] = {0, 0, 0, 0};
  for (uint32_t physical_page = 0; physical_page < splats->page_table.size();
       physical_page++) {
    if (splats->page_table[physical_page].required) {
      index2_cpu_.push_back(physical_page);
      splats->num_inmemory_current += splats->page_size;
      stats_inmemory_current[splats->page_table[physical_page].level]++;
    }
  }
  if (!index2_cpu_.empty()) {
    uint32_t* index_staging_buffer = static_cast<uint32_t*>(
        splats->index_staging_buffer->allocation_info.pMappedData);
    memcpy(index_staging_buffer, index2_cpu_.data(),
           splats->num_inmemory_current / splats->page_size * sizeof(uint32_t));
    VkBufferCopy copy = {};
    copy.srcOffset = 0;
    copy.dstOffset = 0;
    copy.size = index2_cpu_.size() * sizeof(uint32_t);
    vkCmdCopyBuffer(cmd, splats->index_staging_buffer->buffer,
                    splats->sort_buf_packed[1]->buffer, 1, &copy);

    // Barrier for depth sort
    VkBufferMemoryBarrier2 barrier_depth_sort = {
        VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER_2};
    barrier_depth_sort.buffer = splats->sort_buf_packed[1]->buffer;
    barrier_depth_sort.size = VK_WHOLE_SIZE;
    barrier_depth_sort.srcStageMask = VK_PIPELINE_STAGE_2_TRANSFER_BIT;
    barrier_depth_sort.srcAccessMask = VK_ACCESS_2_MEMORY_WRITE_BIT;
    barrier_depth_sort.dstStageMask = VK_PIPELINE_STAGE_2_COMPUTE_SHADER_BIT;
    barrier_depth_sort.dstAccessMask = VK_ACCESS_2_MEMORY_READ_BIT;
    VkDependencyInfo dep_depth_sort = {VK_STRUCTURE_TYPE_DEPENDENCY_INFO};
    dep_depth_sort.bufferMemoryBarrierCount = 1;
    dep_depth_sort.pBufferMemoryBarriers = &barrier_depth_sort;
    Renderer::fp_vkCmdPipelineBarrier2(cmd, &dep_depth_sort);
  }

  // Adaptive LOD
  if (CVarManager::getVar("gaussian.vmem.lod.adaptive", true)
          .getValue<bool>()) {
    double& step = CVarManager::getVar("gaussian.vmem.lod.adaptive.step", 10.0)
                       .getValue<double>();
    uint64_t in_memory_pages = splats->num_inmemory_current / splats->page_size;
    float ratio =
        static_cast<float>(in_memory_pages) / GAUSSIAN_MAX_INMEMORY_PAGES;
    if (GAUSSIAN_MAX_INMEMORY_PAGES >= splats->num_pages) ratio = 0.1f;

    if (ratio >= 0.8) {
      int32_t new_th1 = lod_thresholds[0] - static_cast<int32_t>(step);
      CVarManager::setVar("gaussian.vmem.lod.threshold1", std::max(new_th1, 0));

      if (frame_count_ - last_down_frame_ < 10 && new_th1 > 0) {
        if (frame_count_ - last_up_frame_ > 60)
          step = std::max(1.0, step * 1.1);
        else
          step = step * 0.9;
      }
      last_down_frame_ = frame_count_;
    } else if (ratio <= 0.5 &&
               (stats_inmemory_current[1] || stats_inmemory_current[2] ||
                stats_inmemory_current[3])) {
      CVarManager::setVar("gaussian.vmem.lod.threshold1",
                          lod_thresholds[0] + static_cast<int32_t>(step));
      if (frame_count_ - last_up_frame_ < 10) {
        if (frame_count_ - last_down_frame_ > 60)
          step = std::max(1.0, step * 1.1);
        else
          step = step * 0.9;
      }
      last_up_frame_ = frame_count_;
    }

    CVarManager::setVar("gaussian.vmem.lod.threshold2",
                        static_cast<int32_t>(lod_thresholds[0] * 1.5f));
    CVarManager::setVar("gaussian.vmem.lod.threshold3",
                        static_cast<int32_t>(lod_thresholds[0] * 2.0f));
  }

  CVarManager::setVar("gaussian.vmem.stats.total",
                      static_cast<int32_t>(splats->num_total));
  CVarManager::setVar("gaussian.vmem.stats.page_size",
                      static_cast<int32_t>(splats->page_size));
  CVarManager::setVar(
      "gaussian.vmem.stats.inmemory.max.pages",
      static_cast<int32_t>(splats->num_inmemory_max / splats->page_size));
  CVarManager::setVar(
      "gaussian.vmem.stats.inmemory.current.pages",
      static_cast<int32_t>(splats->num_inmemory_current / splats->page_size));
  for (int i = 0; i < 4; i++) {
    CVarManager::setVar(
        "gaussian.vmem.stats.inmemory.current.pages." + std::to_string(i),
        static_cast<int32_t>(stats_inmemory_current[i]), false);
  }
  CVarManager::setVar("gaussian.vmem.stats.total.pages",
                      static_cast<int32_t>(splats->num_pages));
  CVarManager::setVar("gaussian.vmem.stats.inmemory.copy",
                      static_cast<int32_t>(staging_buffer_offset), false);
  // Reduce by 1 to account for zero page
  CVarManager::setVar("gaussian.vmem.stats.missing",
                      static_cast<int32_t>(stats_pages_needed - 1), false);
  UVELON_END_TIMER(timer_pagetable_update);

  return made_changes;
}

// TODO: The queue ownership transfer does not currently work. We acquire and
// release the buffers but the graphics queue needs corresponding
// release/acquire commands!
void GaussianSystem::sortByDepth(VkCommandBuffer cmd, RenderObject& ro) {
  GaussianSplats& splats = *ro.gaussian_splats;
  uint32_t graphics_queue_family, compute_queue_family;
  Renderer::getInstance().getQueueFamilies(graphics_queue_family,
                                           compute_queue_family);

  VkCommandBuffer compute_cmd = cmd;

  std::array<VkDescriptorSet, 2> descriptor_sets = {
      Renderer::getInstance().getGlobalDescriptorSet()->set,
      splats.sort_ds->set};

  // Barrier to draw after depth sorting is done
  VkBufferMemoryBarrier2 barrier_indices[4] = {
      {VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER_2},
      {VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER_2},
      {VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER_2},
      {VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER_2}};
  // 0 W->R
  barrier_indices[0].buffer = splats.sort_buf_packed[0]->buffer;
  barrier_indices[0].size = VK_WHOLE_SIZE;
  barrier_indices[0].srcStageMask = VK_PIPELINE_STAGE_2_COMPUTE_SHADER_BIT;
  barrier_indices[0].srcAccessMask = VK_ACCESS_2_MEMORY_WRITE_BIT;
  barrier_indices[0].dstStageMask = VK_PIPELINE_STAGE_2_COMPUTE_SHADER_BIT;
  barrier_indices[0].dstAccessMask = VK_ACCESS_2_MEMORY_READ_BIT;
  // 1 R->W
  barrier_indices[1].buffer = splats.sort_buf_packed[1]->buffer;
  barrier_indices[1].size = VK_WHOLE_SIZE;
  barrier_indices[1].srcStageMask = VK_PIPELINE_STAGE_2_COMPUTE_SHADER_BIT;
  barrier_indices[1].srcAccessMask = 0;
  barrier_indices[1].dstStageMask = VK_PIPELINE_STAGE_2_COMPUTE_SHADER_BIT;
  barrier_indices[1].dstAccessMask = 0;
  // 0 R->W
  barrier_indices[2].buffer = splats.sort_buf_packed[1]->buffer;
  barrier_indices[2].size = VK_WHOLE_SIZE;
  barrier_indices[2].srcStageMask = VK_PIPELINE_STAGE_2_COMPUTE_SHADER_BIT;
  barrier_indices[2].srcAccessMask = VK_ACCESS_2_MEMORY_WRITE_BIT;
  barrier_indices[2].dstStageMask = VK_PIPELINE_STAGE_2_COMPUTE_SHADER_BIT;
  barrier_indices[2].dstAccessMask = VK_ACCESS_2_MEMORY_READ_BIT;
  // 1 W->R
  barrier_indices[3].buffer = splats.sort_buf_packed[0]->buffer;
  barrier_indices[3].size = VK_WHOLE_SIZE;
  barrier_indices[3].srcStageMask = VK_PIPELINE_STAGE_2_COMPUTE_SHADER_BIT;
  barrier_indices[3].srcAccessMask = 0;
  barrier_indices[3].dstStageMask = VK_PIPELINE_STAGE_2_COMPUTE_SHADER_BIT;
  barrier_indices[3].dstAccessMask = 0;
  VkBufferMemoryBarrier2 barrier_hist[2] = {
      {VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER_2},
      {VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER_2}};
  // W->R
  barrier_hist[0].buffer = splats.sort_buf_hist->buffer;
  barrier_hist[0].size = VK_WHOLE_SIZE;
  barrier_hist[0].srcStageMask = VK_PIPELINE_STAGE_2_COMPUTE_SHADER_BIT;
  barrier_hist[0].srcAccessMask = VK_ACCESS_2_MEMORY_WRITE_BIT;
  barrier_hist[0].dstStageMask = VK_PIPELINE_STAGE_2_COMPUTE_SHADER_BIT;
  barrier_hist[0].dstAccessMask = VK_ACCESS_2_MEMORY_READ_BIT;
  // R->W
  barrier_hist[1].buffer = splats.sort_buf_hist->buffer;
  barrier_hist[1].size = VK_WHOLE_SIZE;
  barrier_hist[1].srcStageMask = VK_PIPELINE_STAGE_2_COMPUTE_SHADER_BIT;
  barrier_hist[1].srcAccessMask = 0;
  barrier_hist[1].dstStageMask = VK_PIPELINE_STAGE_2_COMPUTE_SHADER_BIT;
  barrier_hist[1].dstAccessMask = 0;

  // Prepare metadata for all iterations
  GaussianSplatsSortData sort_data = {};
  sort_data.num_splats = static_cast<uint32_t>(splats.num_inmemory_current);
  sort_data.elements_per_wg = splats.sort_elements_per_wg;
  sort_data.page_size = splats.proxy_mesh ? splats.page_size : 0;
  memcpy(splats.sort_buf_data->allocation_info.pMappedData, &sort_data,
         sizeof(GaussianSplatsSortData));

  GaussianSplatsPC push_constant = {};
  push_constant.model_matrix = ro.transform;
  push_constant.iteration = 0;

  auto timer = UVELON_START_TIMER_GPU(compute_cmd, "depth_sort");

  // Record command buffer
  vkCmdBindDescriptorSets(compute_cmd, VK_PIPELINE_BIND_POINT_COMPUTE,
                          splats.sort_pipe_depth->layout, 0,
                          static_cast<uint32_t>(descriptor_sets.size()),
                          descriptor_sets.data(), 0, nullptr);
  vkCmdPushConstants(compute_cmd, splats.material->pipeline->layout,
                     VK_SHADER_STAGE_ALL_GRAPHICS | VK_SHADER_STAGE_COMPUTE_BIT,
                     0, sizeof(GaussianSplatsPC), &push_constant);

  // Update distance from camera
  vkCmdBindPipeline(compute_cmd, VK_PIPELINE_BIND_POINT_COMPUTE,
                    splats.sort_pipe_depth->pipeline);
  vkCmdDispatch(compute_cmd, splats.sort_num_workgroups, 1, 1);

  VkDependencyInfo dep0 = {VK_STRUCTURE_TYPE_DEPENDENCY_INFO};
  dep0.bufferMemoryBarrierCount = 2;
  dep0.pBufferMemoryBarriers = &barrier_indices[0];
  Renderer::fp_vkCmdPipelineBarrier2(compute_cmd, &dep0);

  // Radix sort in 4 iterations
  const int iterations = 4;
  for (int i = 0; i < iterations; i++) {
    if (i != 0) {
      push_constant.iteration = i;
      vkCmdPushConstants(
          compute_cmd, splats.material->pipeline->layout,
          VK_SHADER_STAGE_ALL_GRAPHICS | VK_SHADER_STAGE_COMPUTE_BIT, 0,
          sizeof(GaussianSplatsPC), &push_constant);
    }

    vkCmdBindPipeline(compute_cmd, VK_PIPELINE_BIND_POINT_COMPUTE,
                      splats.sort_pipe_hist->pipeline);
    vkCmdDispatch(compute_cmd, splats.sort_num_workgroups, 1, 1);

    VkDependencyInfo dep1 = {VK_STRUCTURE_TYPE_DEPENDENCY_INFO};
    dep1.bufferMemoryBarrierCount = 1;
    dep1.pBufferMemoryBarriers = &barrier_hist[0];
    Renderer::fp_vkCmdPipelineBarrier2(compute_cmd, &dep1);

    vkCmdBindPipeline(compute_cmd, VK_PIPELINE_BIND_POINT_COMPUTE,
                      splats.sort_pipe_sort->pipeline);
    vkCmdDispatch(compute_cmd, splats.sort_num_workgroups, 1, 1);

    VkBufferMemoryBarrier2 dep2_barriers[3] = {
        barrier_hist[1], barrier_indices[i % 2 == 0 ? 2 : 0],
        barrier_indices[i % 2 == 1 ? 3 : 1]};
    VkDependencyInfo dep2 = {VK_STRUCTURE_TYPE_DEPENDENCY_INFO};
    dep2.bufferMemoryBarrierCount = 3;
    dep2.pBufferMemoryBarriers = &dep2_barriers[0];
    Renderer::fp_vkCmdPipelineBarrier2(compute_cmd, &dep2);
  }

  VkBufferCopy buffer_copy = {};
  buffer_copy.size = splats.indices->size;
  vkCmdCopyBuffer(compute_cmd, splats.sort_buf_packed[0]->buffer,
                  splats.indices->buffer, 1, &buffer_copy);

  VkBufferMemoryBarrier2 barrier_main_index = {
      VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER_2};
  barrier_main_index.buffer = splats.indices->buffer;
  barrier_main_index.size = VK_WHOLE_SIZE;
  barrier_main_index.srcStageMask = VK_PIPELINE_STAGE_2_COMPUTE_SHADER_BIT;
  barrier_main_index.srcAccessMask = VK_ACCESS_2_MEMORY_WRITE_BIT;
  barrier_main_index.dstStageMask = VK_PIPELINE_STAGE_2_INDEX_INPUT_BIT;
  barrier_main_index.dstAccessMask = VK_ACCESS_2_MEMORY_READ_BIT;
  VkDependencyInfo dep_main_index = {VK_STRUCTURE_TYPE_DEPENDENCY_INFO};
  dep_main_index.bufferMemoryBarrierCount = 1;
  dep_main_index.pBufferMemoryBarriers = &barrier_main_index;
  Renderer::fp_vkCmdPipelineBarrier2(compute_cmd, &dep_main_index);
  UVELON_END_TIMER_GPU(compute_cmd, timer);
}

void GaussianSystem::draw(VkCommandBuffer cmd, Stage stage,
                          FilteredIterator<RenderObject> begin,
                          FilteredIterator<RenderObject> end) {
  if (stage == Stage::SWAPCHAIN_FINAL) {
    std::array<VkDeviceSize, 3> vertex_offsets = {0, 0, 0};
    auto global_ds = Renderer::getInstance().getGlobalDescriptorSet();

    GaussianSplatsPC push_constant = {};

#if defined(__APPLE__) && TARGET_OS_IPHONE
    // Workaround: https://github.com/KhronosGroup/MoltenVK/issues/1585#issuecomment-1117720933
    Renderer::getInstance().getSwapchain().stopRenderTo(cmd);
    auto timer = UVELON_START_TIMER_GPU(cmd, "gaussian_render");
    Renderer::getInstance().getSwapchain().resumeRenderTo(cmd);
#else
    auto timer = UVELON_START_TIMER_GPU(cmd, "gaussian_render");
#endif
    for (auto it = begin; it != end; it++) {
      GaussianSplats& splats = *it->gaussian_splats;
      // Rendering
      push_constant.model_matrix = it->transform;
      if (splats.num_inmemory_current > 0) {
        vkCmdBindPipeline(cmd, VK_PIPELINE_BIND_POINT_GRAPHICS,
                          splats.material->pipeline->pipeline);
        vkCmdBindDescriptorSets(cmd, VK_PIPELINE_BIND_POINT_GRAPHICS,
                                splats.material->pipeline->layout, 0, 1,
                                &global_ds->set, 0, nullptr);
        vkCmdBindDescriptorSets(cmd, VK_PIPELINE_BIND_POINT_GRAPHICS,
                                splats.material->pipeline->layout, 1, 1,
                                &splats.material->descriptor_set->set, 0,
                                nullptr);
        vkCmdPushConstants(
            cmd, splats.material->pipeline->layout,
            VK_SHADER_STAGE_ALL_GRAPHICS | VK_SHADER_STAGE_COMPUTE_BIT, 0,
            sizeof(GaussianSplatsPC), &push_constant);
        vkCmdBindIndexBuffer(cmd, splats.indices->buffer, 0,
                             VK_INDEX_TYPE_UINT32);
        vkCmdDrawIndexed(cmd,
                         static_cast<uint32_t>(splats.num_inmemory_current), 1,
                         0, 0, 0);
      }
    }
    UVELON_END_TIMER_GPU(cmd, timer);

    {
      // If a gsplat scene with vmem is active it may still be copying data to
      // the staging buffer. Once the main command buffer is submitted, the data
      // will be copied and accessed by the index buffer. This is our last
      // opportunity to wait for the transfer to complete on the CPU.
      // TODO: May want to replace this with a barrier if we can
      UVELON_PROFILE("staging_copy_overhead", Reporting::CVAR);
      copy_pool_.waitIdle();
    }
  }
}

}  // namespace Uvelon