#include "rendering/systems/ShadowMapSystem.hpp"

#include <algorithm>
#include <execution>
#include <numbers>

#include "core/CVarManager.hpp"
#include "core/FileWizard.hpp"
#include "rendering/Descriptor.hpp"
#include "rendering/Pipeline.hpp"
#include "rendering/RenderData.hpp"
#include "rendering/Renderer.hpp"
#include "utils/Logger.hpp"

#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#define GLM_FORCE_LEFT_HANDED
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/string_cast.hpp>

namespace Uvelon {
ShadowMapSystem::ShadowMapSystem() {
  uniform_buffer_ = Renderer::getInstance().getRenderData().allocateBuffer(
      sizeof(ShadowMapUniform), VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, true, true);

  light_ds_ = DescriptorBuilder::begin()
                  .addBuffer(VK_SHADER_STAGE_ALL_GRAPHICS,
                             VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, uniform_buffer_,
                             0, uniform_buffer_->size)
                  .build();

#ifndef __APPLE__
  VkVertexInputBindingDescription vertex_binding;
  vertex_binding.binding = 0;
  vertex_binding.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;
  vertex_binding.stride = sizeof(glm::vec3);  // Ignore, uses dynamic state
  VkVertexInputAttributeDescription vertex_attribute;
  vertex_attribute.binding = 0;
  vertex_attribute.format = VK_FORMAT_R32G32B32_SFLOAT;
  vertex_attribute.location = 0;
  vertex_attribute.offset = 0;

  Material temp_mat;
  temp_mat.pipeline =
      createPipelineBuilder(Light::Type::DIR)
          .addDynamicState(VK_DYNAMIC_STATE_VERTEX_INPUT_BINDING_STRIDE)
          .setVertexInputInfo(&vertex_binding, 1, &vertex_attribute, 1)
          .build();
  directional_ =
      Renderer::getInstance().getRenderData().addMaterial(std::move(temp_mat));

  temp_mat = Material();
  temp_mat.pipeline =
      createPipelineBuilder(Light::Type::POINT)
          .addDynamicState(VK_DYNAMIC_STATE_VERTEX_INPUT_BINDING_STRIDE)
          .setVertexInputInfo(&vertex_binding, 1, &vertex_attribute, 1)
          .build();
  cube_ =
      Renderer::getInstance().getRenderData().addMaterial(std::move(temp_mat));
#endif

  CVarManager::setVar("shadows.directional.limit", 20.0, true);
  CVarManager::setVar("shadows.perspective.near", 0.01, true);
  CVarManager::setVar("shadows.far", 100.0, true);
}

PipelineBuilder ShadowMapSystem::createPipelineBuilder(Light::Type type) {
  VkPushConstantRange pc = {};
  pc.offset = 0;
  pc.size = sizeof(ShadowMapPushConstant);
  pc.stageFlags = VK_SHADER_STAGE_ALL_GRAPHICS;

  if (type == Light::Type::DIR) {
    return PipelineBuilder::beginGraphics()
        .setAttachmentFormats(nullptr, 0, VK_FORMAT_D16_UNORM, 1)
        .addShader(UVELON_FILE_RESOURCES("shaders/shadow_map.vert").string(),
                   VK_SHADER_STAGE_VERTEX_BIT,
                   {{"UVELON_MAX_LIGHTS", std::to_string(MAX_LIGHTS)}})
        .addShader(
            UVELON_FILE_RESOURCES("shaders/shadow_map_dir.frag").string(),
            VK_SHADER_STAGE_FRAGMENT_BIT,
            {{"UVELON_MAX_LIGHTS", std::to_string(MAX_LIGHTS)}})
        .addPushConstant(pc)
        .addDescriptorSet(light_ds_->layout);
  } else {
    return PipelineBuilder::beginGraphics()
        .setAttachmentFormats(nullptr, 0, VK_FORMAT_D16_UNORM, 0b00111111)
        .addShader(UVELON_FILE_RESOURCES("shaders/shadow_map.vert").string(),
                   VK_SHADER_STAGE_VERTEX_BIT,
                   {{"UVELON_MAX_LIGHTS", std::to_string(MAX_LIGHTS)}})
        .addShader(
            UVELON_FILE_RESOURCES("shaders/shadow_map_point.frag").string(),
            VK_SHADER_STAGE_FRAGMENT_BIT,
            {{"UVELON_MAX_LIGHTS", std::to_string(MAX_LIGHTS)}})
        .addPushConstant(pc)
        .addDescriptorSet(light_ds_->layout);
  }
}

bool ShadowMapSystem::canDraw(LightPrimitive& light) {
  return light.shadow_map;
}

void ShadowMapSystem::prepare(VkCommandBuffer cmd,
                              FilteredIterator<LightPrimitive> begin,
                              FilteredIterator<LightPrimitive> end) {
#ifndef __APPLE__
  std::sort(
      std::execution::parallel_unsequenced_policy(), begin, end,
      [](LightPrimitive& a, LightPrimitive& b) { return a.type < b.type; });
#else
  std::sort(begin, end, [](LightPrimitive& a, LightPrimitive& b) {
    return a.type < b.type;
  });
#endif
}

void ShadowMapSystem::draw(VkCommandBuffer cmd, Stage stage,
                           FilteredIterator<LightPrimitive> begin,
                           FilteredIterator<LightPrimitive> end) {
  if (stage == RenderSystem::Stage::SHADOWMAP) {
    assert(render_objects_);

    PipelineH bound_pipeline;
    uint32_t light_index = 0;
    for (auto it = begin; it != end; it++) {
      it->shadow_map->transition(
          cmd, VK_PIPELINE_STAGE_2_NONE, 0,
          VK_PIPELINE_STAGE_2_EARLY_FRAGMENT_TESTS_BIT |
              VK_PIPELINE_STAGE_2_LATE_FRAGMENT_TESTS_BIT,
          VK_ACCESS_2_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT,
          VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_DEPTH_ATTACHMENT_OPTIMAL);

      glm::mat4 projection;
      float directional_limit;
      float far = static_cast<float>(
          CVarManager::getVar("shadows.far").getValue<double>());
      switch (it->type) {
        case Light::Type::DIR: {
          directional_limit = static_cast<float>(
              CVarManager::getVar("shadows.directional.limit")
                  .getValue<double>());
          projection = glm::ortho(-directional_limit, directional_limit,
                                  -directional_limit, directional_limit, far,
                                  0.0f);  // Reverse z
          break;
        }
        case Light::Type::POINT:
        case Light::Type::SPOT: {
          float near =
              static_cast<float>(CVarManager::getVar("shadows.perspective.near")
                                     .getValue<double>());
          projection =
              glm::perspective(static_cast<float>(std::numbers::pi / 2.0f),
                               1.0f, far, near);  // Reverse z
          break;
        }
      }
      glm::mat4 view;
      for (int i = 0; i < (it->type == Light::Type::DIR ? 1 : 6); i++) {
        switch (it->type) {
          case Light::Type::DIR: {
            view = glm::inverse(glm::mat4(glm::mat3(it->model_matrix)));
            view[3] = glm::vec4(0.0f, 0.0f, directional_limit, 1.0f);
            break;
          }
          case Light::Type::POINT:
          case Light::Type::SPOT: {
            glm::vec3 light_pos = glm::vec3(it->model_matrix[3]);
            view =
                glm::lookAt(light_pos, light_pos + center_offset_[i], up_[i]);
            break;
          }
        }
        static_cast<ShadowMapUniform*>(
            uniform_buffer_->allocation_info.pMappedData)
            ->light_projection[light_index][i] = projection * view;
      }
      static_cast<ShadowMapUniform*>(
          uniform_buffer_->allocation_info.pMappedData)
          ->far = far;

      // Start rendering
      VkRenderingAttachmentInfo attachment_info = {
          VK_STRUCTURE_TYPE_RENDERING_ATTACHMENT_INFO};
      attachment_info.clearValue.depthStencil.depth = 0.0f;
      attachment_info.imageLayout = VK_IMAGE_LAYOUT_DEPTH_ATTACHMENT_OPTIMAL;
      attachment_info.imageView = it->shadow_map->getVkImageView();
      attachment_info.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
      attachment_info.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
      VkRenderingInfo render_info = {VK_STRUCTURE_TYPE_RENDERING_INFO};
      render_info.pDepthAttachment = &attachment_info;
      render_info.renderArea.extent = {it->shadow_map->getExtent().width,
                                       it->shadow_map->getExtent().height};
      render_info.viewMask =
          it->type == Light::Type::DIR ? 0b00000001 : 0b00111111;
      Renderer::fp_vkCmdBeginRendering(cmd, &render_info);

      PipelineH required_pipeline;
#ifndef __APPLE__
      required_pipeline = it->type == Light::Type::DIR ? directional_->pipeline
                                                       : cube_->pipeline;
#endif
      for (RenderObject& object : *render_objects_) {
        if (object.mesh_primitive) {
#ifdef __APPLE__
          required_pipeline =
              it->type == Light::Type::DIR
                  ? object.mesh_primitive->mat_sm_directional->pipeline
                  : object.mesh_primitive->mat_sm_cube->pipeline;
#endif
          if (required_pipeline != bound_pipeline) {
            vkCmdBindPipeline(cmd, VK_PIPELINE_BIND_POINT_GRAPHICS,
                              required_pipeline->pipeline);
            bound_pipeline = required_pipeline;

            vkCmdBindDescriptorSets(cmd, VK_PIPELINE_BIND_POINT_GRAPHICS,
                                    bound_pipeline->layout, 0, 1,
                                    &light_ds_->set, 0, nullptr);

            VkViewport viewport = {
                0.0f,
                0.0f,
                static_cast<float>(it->shadow_map->getExtent().width),
                static_cast<float>(it->shadow_map->getExtent().height),
                0.0f,
                1.0f};
            VkRect2D scissor = {
                {0, 0},
                {static_cast<uint32_t>(it->shadow_map->getExtent().width),
                 static_cast<uint32_t>(it->shadow_map->getExtent().height)}};
            vkCmdSetViewport(cmd, 0, 1, &viewport);
            vkCmdSetScissor(cmd, 0, 1, &scissor);
          }

          ShadowMapPushConstant pc;
          pc.model_matrix = object.transform;
          pc.light_index = light_index;

          vkCmdPushConstants(cmd, bound_pipeline->layout,
                             VK_SHADER_STAGE_ALL_GRAPHICS, 0,
                             sizeof(ShadowMapPushConstant), &pc);
          vkCmdBindIndexBuffer(cmd, object.mesh_primitive->indices->buffer, 0,
                               object.mesh_primitive->index_type);
          // With dynamic stride we create pipelines for all primitives. On
          // Apple devices we create pipelines for each primitive instead to
          // deal with lack of support on some of the devices.
#ifdef __APPLE__
          std::vector<VkDeviceSize> vertex_offsets = {0, 0, 0, 0};
          std::vector<VkBuffer> vertex_buffers;
          vertex_buffers.reserve(object.mesh_primitive->vertex_buffers.size());
          for (BufferH buffer : object.mesh_primitive->vertex_buffers)
            vertex_buffers.push_back(buffer->buffer);
          while (vertex_buffers.size() > vertex_offsets.size())
            vertex_offsets.push_back(0);
          vkCmdBindVertexBuffers(cmd, 0,
                                 static_cast<uint32_t>(vertex_buffers.size()),
                                 vertex_buffers.data(), vertex_offsets.data());
#else
          VkDeviceSize offset =
              object.mesh_primitive->attribute_description[0].offset;
          VkDeviceSize stride =
              object.mesh_primitive
                  ->binding_description
                      [object.mesh_primitive->attribute_description[0].binding]
                  .stride;
          vkCmdBindVertexBuffers2(
              cmd, 0, 1, &object.mesh_primitive->vertex_buffers[0]->buffer,
              &offset, nullptr, &stride);
#endif
          vkCmdDrawIndexed(cmd, object.mesh_primitive->index_count, 1, 0, 0, 0);
        }
      }
      Renderer::fp_vkCmdEndRendering(cmd);
      it->shadow_map->transition(
          cmd,
          VK_PIPELINE_STAGE_2_EARLY_FRAGMENT_TESTS_BIT |
              VK_PIPELINE_STAGE_2_LATE_FRAGMENT_TESTS_BIT,
          VK_ACCESS_2_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT,
          VK_PIPELINE_STAGE_2_FRAGMENT_SHADER_BIT, VK_ACCESS_2_SHADER_READ_BIT,
          VK_IMAGE_LAYOUT_DEPTH_ATTACHMENT_OPTIMAL,
          VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
      light_index++;
    }
  }
}

}  // namespace Uvelon