#include "rendering/systems/SkyboxSystem.hpp"

#include "rendering/Renderer.hpp"

namespace Uvelon {

bool SkyboxSystem::canDraw(RenderObject& render_object) {
  return render_object.environment && render_object.environment->skybox;
}

void SkyboxSystem::draw(VkCommandBuffer cmd, Stage stage,
                        FilteredIterator<RenderObject> begin,
                        FilteredIterator<RenderObject> end) {
  if (stage == Stage::SWAPCHAIN) {
    auto global_ds = Renderer::getInstance().getGlobalDescriptorSet();
    for (auto it = begin; it != end; it++) {
      Environment& env = *it->environment;
      vkCmdBindDescriptorSets(cmd, VK_PIPELINE_BIND_POINT_GRAPHICS,
                              env.skybox_material->pipeline->layout, 0, 1,
                              &global_ds->set, 0, nullptr);
      vkCmdBindDescriptorSets(cmd, VK_PIPELINE_BIND_POINT_GRAPHICS,
                              env.skybox_material->pipeline->layout, 1, 1,
                              &env.skybox_material->descriptor_set->set, 0,
                              nullptr);
      vkCmdBindPipeline(cmd, VK_PIPELINE_BIND_POINT_GRAPHICS,
                        env.skybox_material->pipeline->pipeline);
      vkCmdDraw(cmd, 36, 1, 0, 0);
    }
  }
}

}  // namespace Uvelon