#include "rendering/systems/MeshPrimitiveSystem.hpp"

#include <algorithm>
#ifndef __APPLE__
#include <execution>
#endif

#include "rendering/Renderer.hpp"
#include "utils/Logger.hpp"

namespace Uvelon {

bool MeshPrimitiveSystem::canDraw(RenderObject& object) {
  return object.mesh_primitive;
}

void MeshPrimitiveSystem::prepare(VkCommandBuffer cmd,
                                  FilteredIterator<RenderObject> begin,
                                  FilteredIterator<RenderObject> end) {
  // Sort by material to reduce the number of binds
#ifndef __APPLE__
  std::sort(std::execution::parallel_unsequenced_policy(), begin, end,
            [](RenderObject& a, RenderObject& b) {
              return a.mesh_primitive->material < a.mesh_primitive->material;
            });
#else
  std::sort(begin, end, [](RenderObject& a, RenderObject& b) {
    return a.mesh_primitive->material < a.mesh_primitive->material;
  });
#endif
}

void MeshPrimitiveSystem::draw(VkCommandBuffer cmd, Stage stage,
                               FilteredIterator<RenderObject> begin,
                               FilteredIterator<RenderObject> end) {
  if (stage == Stage::GBUFFER) {
    std::vector<VkDeviceSize> vertex_offsets = {0, 0, 0, 0};
    auto global_ds = Renderer::getInstance().getGlobalDescriptorSet();

    MaterialH last_material;
    for (auto it = begin; it != end; it++) {
      MeshPrimitive& primitive = *(it->mesh_primitive);
      std::vector<VkBuffer> vertex_buffers;
      vertex_buffers.reserve(primitive.vertex_buffers.size());
      for (BufferH buffer : primitive.vertex_buffers)
        vertex_buffers.push_back(buffer->buffer);
      while (vertex_buffers.size() > vertex_offsets.size())
        vertex_offsets.push_back(0);

      if (primitive.material != last_material) {
        if (!last_material) {
          // Bind global descriptor set once per frame. All pipeline layouts are
          // required to have the descriptor set bound as the first set
          vkCmdBindDescriptorSets(cmd, VK_PIPELINE_BIND_POINT_GRAPHICS,
                                  primitive.material->pipeline->layout, 0, 1,
                                  &global_ds->set, 0, nullptr);
        }

        vkCmdBindDescriptorSets(cmd, VK_PIPELINE_BIND_POINT_GRAPHICS,
                                primitive.material->pipeline->layout, 1, 1,
                                &primitive.material->descriptor_set->set, 0,
                                nullptr);
        vkCmdBindPipeline(cmd, VK_PIPELINE_BIND_POINT_GRAPHICS,
                          primitive.material->pipeline->pipeline);
        last_material = primitive.material;
      }

      vkCmdPushConstants(cmd, primitive.material->pipeline->layout,
                         VK_SHADER_STAGE_ALL, 0, sizeof(glm::mat4),
                         &it->transform);
      vkCmdBindVertexBuffers(cmd, 0,
                             static_cast<uint32_t>(vertex_buffers.size()),
                             vertex_buffers.data(), vertex_offsets.data());
      vkCmdBindIndexBuffer(cmd, primitive.indices->buffer, 0,
                           primitive.index_type);
      vkCmdDrawIndexed(cmd, primitive.index_count, 1, 0, 0, 0);
    }
  }
}

}  // namespace Uvelon