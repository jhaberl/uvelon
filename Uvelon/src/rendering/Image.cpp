#include "rendering/Image.hpp"

#include <vk_mem_alloc.h>

#include <cassert>

#include "rendering/Renderer.hpp"
#include "rendering/Utils.hpp"
#include "utils/Logger.hpp"

namespace Uvelon {

Image::Image(VkExtent3D extent, VkFormat format, VkImageUsageFlags usage,
             uint32_t array_layers, uint32_t mip_levels, bool cubemap)
    : extent_(extent),
      format_(format),
      array_layers_(array_layers),
      mip_levels_(mip_levels) {
  auto& renderer = Renderer::getInstance();
  auto& allocator = renderer.getAllocator();
  auto device = renderer.getDevice();

  VkImageCreateInfo img_info = {VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO};
  img_info.imageType = VK_IMAGE_TYPE_2D;
  img_info.format = format_;
  img_info.extent = extent_;
  img_info.mipLevels = mip_levels_;
  img_info.arrayLayers = array_layers_;
  img_info.samples = VK_SAMPLE_COUNT_1_BIT;
  img_info.tiling = VK_IMAGE_TILING_OPTIMAL;
  img_info.usage = usage;
  img_info.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
  if (cubemap) img_info.flags = VK_IMAGE_CREATE_CUBE_COMPATIBLE_BIT;

  VmaAllocationCreateInfo alloc_info = {};
  alloc_info.usage = VMA_MEMORY_USAGE_GPU_ONLY;
  alloc_info.requiredFlags =
      VkMemoryPropertyFlags(VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

  VK_CHECK(vmaCreateImage(allocator, &img_info, &alloc_info, &image_,
                          &allocation_, nullptr));

  VkImageViewCreateInfo view_info = {VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO};
  view_info.image = image_;
  view_info.viewType =
      array_layers_ == 1
          ? VK_IMAGE_VIEW_TYPE_2D
          : (cubemap ? VK_IMAGE_VIEW_TYPE_CUBE : VK_IMAGE_VIEW_TYPE_2D_ARRAY);
  view_info.format = format_;
  view_info.subresourceRange.aspectMask =
      isDepthImage() ? VK_IMAGE_ASPECT_DEPTH_BIT : VK_IMAGE_ASPECT_COLOR_BIT;
  view_info.subresourceRange.baseMipLevel = 0;
  view_info.subresourceRange.levelCount = mip_levels_;
  view_info.subresourceRange.baseArrayLayer = 0;
  view_info.subresourceRange.layerCount = array_layers_;
  VK_CHECK(vkCreateImageView(device, &view_info, nullptr, &view_));
}

Image::Image(void* data, uint64_t size, VkExtent3D extent, VkFormat format,
             VkImageUsageFlags usage, uint32_t array_layers,
             uint32_t mip_levels, std::vector<VkBufferImageCopy> regions,
             bool cubemap, bool generate_mips)
    : Image(extent, format,
            usage | VK_IMAGE_USAGE_TRANSFER_DST_BIT |
                (generate_mips ? VK_IMAGE_USAGE_TRANSFER_SRC_BIT : 0),
            array_layers,
            generate_mips ? calculateMaxMipLevels(extent) : mip_levels,
            cubemap) {
  VkBufferCreateInfo staging_info = {VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO};
  staging_info.size = size;
  staging_info.usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT;
  staging_info.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

  VmaAllocationCreateInfo staging_alloc = {};
  staging_alloc.usage = VMA_MEMORY_USAGE_CPU_ONLY;
  staging_alloc.flags = VMA_ALLOCATION_CREATE_MAPPED_BIT;
  staging_alloc.requiredFlags = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT |
                                VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;

  VkBuffer staging_buffer;
  VmaAllocation staging_allocation;
  VmaAllocationInfo staging_alloc_info;
  VK_CHECK(vmaCreateBuffer(Renderer::getInstance().getAllocator(),
                           &staging_info, &staging_alloc, &staging_buffer,
                           &staging_allocation, &staging_alloc_info));

  // Copy data to staging buffer
  memcpy(staging_alloc_info.pMappedData, data, size);

  Renderer::getInstance().immediateSubmit([&](VkCommandBuffer cmd) {
    transition(cmd, VK_PIPELINE_STAGE_2_TOP_OF_PIPE_BIT, 0,
               VK_PIPELINE_STAGE_2_TRANSFER_BIT, VK_ACCESS_2_MEMORY_WRITE_BIT,
               VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);

    vkCmdCopyBufferToImage(
        cmd, staging_buffer, image_, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
        static_cast<uint32_t>(regions.size()), regions.data());

    if (generate_mips && mip_levels_ > 1) {
      // Transition first mip level to read, other levels can remain the same
      VkImageSubresourceRange subres_first_mip = {};
      subres_first_mip.aspectMask = isDepthImage() ? VK_IMAGE_ASPECT_DEPTH_BIT
                                                   : VK_IMAGE_ASPECT_COLOR_BIT;
      subres_first_mip.levelCount = 1;
      subres_first_mip.layerCount = array_layers_;
      transitionVkImage(
          cmd, image_, VK_PIPELINE_STAGE_2_TRANSFER_BIT,
          VK_ACCESS_2_TRANSFER_WRITE_BIT, VK_PIPELINE_STAGE_2_TRANSFER_BIT,
          VK_ACCESS_2_TRANSFER_READ_BIT, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
          VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, subres_first_mip);

      std::vector<VkImageBlit> blits(mip_levels_ - 1);

      VkImageSubresourceLayers blit_src = {};
      blit_src.aspectMask = isDepthImage() ? VK_IMAGE_ASPECT_DEPTH_BIT
                                           : VK_IMAGE_ASPECT_COLOR_BIT;
      blit_src.mipLevel = 0;
      blit_src.layerCount = array_layers_;
      for (uint32_t i = 1; i < mip_levels_; i++) {
        VkImageSubresourceLayers blit_dst = {};
        blit_dst.aspectMask = blit_src.aspectMask;
        blit_dst.mipLevel = i;
        blit_dst.layerCount = array_layers;
        VkImageBlit blit = {};
        blit.srcSubresource = blit_src;
        blit.srcOffsets[1] = {static_cast<int>(extent.width),
                              static_cast<int>(extent.height),
                              static_cast<int>(extent.depth)};
        blit.dstSubresource = blit_dst;
        blit.dstOffsets[1] = {
            static_cast<int>(std::floor(extent.width / std::pow(2, i))),
            static_cast<int>(std::floor(extent.height / std::pow(2, i))),
            static_cast<int>(
                extent.depth)};  // TODO: Is this correct for the depth?
        blits[i - 1] = blit;
      }
      vkCmdBlitImage(cmd, image_, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, image_,
                     VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
                     static_cast<uint32_t>(blits.size()), blits.data(),
                     VK_FILTER_LINEAR);

      transitionVkImage(
          cmd, image_, VK_PIPELINE_STAGE_2_TRANSFER_BIT,
          VK_ACCESS_2_TRANSFER_READ_BIT,
          VK_PIPELINE_STAGE_2_FRAGMENT_SHADER_BIT, VK_ACCESS_2_MEMORY_READ_BIT,
          VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
          VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, subres_first_mip);
      VkImageSubresourceRange subres_mips = {};
      subres_mips.aspectMask = isDepthImage() ? VK_IMAGE_ASPECT_DEPTH_BIT
                                              : VK_IMAGE_ASPECT_COLOR_BIT;
      subres_mips.baseMipLevel = 1;
      subres_mips.levelCount = VK_REMAINING_MIP_LEVELS;
      subres_mips.layerCount = array_layers_;
      transitionVkImage(cmd, image_, VK_PIPELINE_STAGE_2_TRANSFER_BIT,
                        VK_ACCESS_2_TRANSFER_WRITE_BIT,
                        VK_PIPELINE_STAGE_2_FRAGMENT_SHADER_BIT,
                        VK_ACCESS_2_MEMORY_READ_BIT,
                        VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
                        VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, subres_mips);
    } else {
      transition(
          cmd, VK_PIPELINE_STAGE_2_TRANSFER_BIT, VK_ACCESS_2_TRANSFER_WRITE_BIT,
          VK_PIPELINE_STAGE_2_FRAGMENT_SHADER_BIT, VK_ACCESS_2_MEMORY_READ_BIT,
          VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
          VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
    }
  });

  // Delete staging buffer
  vmaDestroyBuffer(Renderer::getInstance().getAllocator(), staging_buffer,
                   staging_allocation);
}

std::vector<VkBufferImageCopy> Image::defaultStagingCopy(VkExtent3D extent) {
  VkImageSubresourceLayers subresource = {};
  subresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
  subresource.mipLevel = 0;
  subresource.layerCount = 1;
  subresource.baseArrayLayer = 0;

  VkBufferImageCopy img_copy = {};
  img_copy.bufferOffset = 0;
  img_copy.bufferRowLength = 0;
  img_copy.bufferImageHeight = 0;
  img_copy.imageSubresource = subresource;
  img_copy.imageOffset = {0, 0, 0};
  img_copy.imageExtent = extent;
  return {img_copy};
}

uint32_t Image::calculateMaxMipLevels(VkExtent3D extent) {
  return static_cast<uint32_t>(
      std::floor(std::log2(
          std::max(extent.width, std::max(extent.height, extent.depth)))) +
      1);
}

Image::Image(void* data, uint64_t size, VkExtent3D extent, VkFormat format,
             VkImageUsageFlags usage, bool generate_mips)
    : Image(data, size, extent, format, usage | VK_IMAGE_USAGE_TRANSFER_DST_BIT,
            1, 1, defaultStagingCopy(extent), false, generate_mips) {}

Image::Image(const Image&& o) {
  image_ = o.image_;
  view_ = o.view_;
  allocation_ = o.allocation_;
  extent_ = o.extent_;
  format_ = o.format_;
  array_layers_ = o.array_layers_;
  mip_levels_ = o.mip_levels_;
  cubemap_ = o.cubemap_;
  destroyed_ = o.destroyed_;

  o.destroyed_ = true;
}

Image Image::operator=(const Image&& o) { return Image(std::move(o)); }

Image::~Image() { destroy(); }

void Image::destroy() {
  if (!destroyed_) {
    auto& renderer = Renderer::getInstance();
    auto& allocator = renderer.getAllocator();
    auto device = renderer.getDevice();

    vkDestroyImageView(device, view_, nullptr);
    vmaDestroyImage(allocator, image_, allocation_);
    destroyed_ = true;
  }
}

bool Image::isDepthImage() {
  return format_ == VK_FORMAT_D32_SFLOAT ||
         format_ == VK_FORMAT_D32_SFLOAT_S8_UINT ||
         format_ == VK_FORMAT_D24_UNORM_S8_UINT ||
         format_ == VK_FORMAT_D16_UNORM ||
         format_ == VK_FORMAT_D16_UNORM_S8_UINT;
}

void Image::transition(VkCommandBuffer cmd, VkPipelineStageFlags2 src_stage,
                       VkAccessFlags2 src_access,
                       VkPipelineStageFlags2 dst_stage,
                       VkAccessFlags2 dst_access, VkImageLayout old_layout,
                       VkImageLayout new_layout) {
  // Would be nice to keep track of the layout here but transitionVkImage
  // records a pipeline barrier command to the command buffer, so the layout
  // doesn't actually change until at some point after command buffer
  // submission.
  VkImageSubresourceRange subresource = {};
  subresource.aspectMask =
      isDepthImage() ? VK_IMAGE_ASPECT_DEPTH_BIT : VK_IMAGE_ASPECT_COLOR_BIT;
  subresource.baseMipLevel = 0;
  subresource.levelCount = mip_levels_;
  subresource.baseArrayLayer = 0;
  subresource.layerCount = array_layers_;
  transitionVkImage(cmd, image_, src_stage, src_access, dst_stage, dst_access,
                    old_layout, new_layout, subresource);
}

void Image::transitionVkImage(
    VkCommandBuffer cmd, VkImage image, VkPipelineStageFlags2 src_stage,
    VkAccessFlags2 src_access, VkPipelineStageFlags2 dst_stage,
    VkAccessFlags2 dst_access, VkImageLayout old_layout,
    VkImageLayout new_layout, VkImageSubresourceRange subresource) {
  VkImageMemoryBarrier2KHR barrier = {
      VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER_2_KHR};
  barrier.srcStageMask = src_stage;
  barrier.srcAccessMask = src_access;
  barrier.dstStageMask = dst_stage;
  barrier.dstAccessMask = dst_access;
  barrier.oldLayout = old_layout;
  barrier.newLayout = new_layout;
  // TODO: What do we do about srcQueueFamilyIndex/dstQueueFamilyIndex
  barrier.image = image;
  barrier.subresourceRange = subresource;

  VkDependencyInfoKHR dependency_info = {VK_STRUCTURE_TYPE_DEPENDENCY_INFO_KHR};
  dependency_info.imageMemoryBarrierCount = 1;
  dependency_info.pImageMemoryBarriers = &barrier;

  Renderer::fp_vkCmdPipelineBarrier2(cmd, &dependency_info);
}

}  // namespace Uvelon