#include "rendering/Utils.hpp"

namespace Uvelon {

void ShutdownQueue::pushFunction(std::function<void()>&& function) {
  functions.push_back(function);
}

void ShutdownQueue::performShutdown() {
  auto it = functions.rbegin();
  while (it != functions.rend()) {
    (*it)();
    it++;
  }
  functions.clear();
}

}