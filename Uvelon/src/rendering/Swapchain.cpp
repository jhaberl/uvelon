#include "rendering/Swapchain.hpp"

#include "VkBootstrap.h"
#include "core/CVarManager.hpp"
#include "rendering/Renderer.hpp"
#include "rendering/Utils.hpp"
#include "rendering/Window.hpp"
#include "utils/Instrumentation.hpp"

namespace Uvelon {

Swapchain::Swapchain(VkDevice device, const Window& window,
                     VkPhysicalDevice physical_device, VkQueue graphics_queue,
                     uint32_t graphics_queue_family)
    : device_(device),
      window_(window),
      physical_device_(physical_device),
      graphics_queue_(graphics_queue),
      graphics_queue_family_(graphics_queue_family) {}

Swapchain::~Swapchain() { destroy(); }

void Swapchain::destroy() {
  swapchain_shutdown_queue_->performShutdown();
  framedata_shutdown_queue_->performShutdown();
}

// May want to stop rendering directly into the swapchain image at some
// point:
// https://vkguide.dev/docs/new_vkguide/chapter_2/vulkan_new_rendering/
void Swapchain::init() {
  // Destroy an existing swapchain if it exists
  // This is so we can call this to recreate the swapchain on the fly
  // (Window size change, settings change)
  bool recreating = false;
  if (swapchain_) {
    swapchain_shutdown_queue_->performShutdown();
    recreating = true;
  } else {
    swapchain_shutdown_queue_ = std::make_unique<ShutdownQueue>();
    initFrameData();
  }

  // Determine surface extent
  VkSurfaceCapabilitiesKHR surface_capabilities;
  vkGetPhysicalDeviceSurfaceCapabilitiesKHR(
      physical_device_, window_.getSurface(), &surface_capabilities);
  extent_ = surface_capabilities.currentExtent;

  minimized_ = extent_.width == 0 && extent_.height == 0;
  if (minimized_) return;

  // Create swapchain
  vsync_ = CVarManager::getVar("vsync", true).getValue<bool>();

  vkb::SwapchainBuilder swapchain_builder{physical_device_, device_,
                                          window_.getSurface()};
  vkb::Swapchain swapchain =
      swapchain_builder.set_desired_format({VK_FORMAT_R8G8B8A8_UNORM})
          .set_desired_present_mode(vsync_ ? VK_PRESENT_MODE_FIFO_KHR
                                           : VK_PRESENT_MODE_MAILBOX_KHR)
          .set_desired_extent(extent_.width, extent_.height)
          .add_image_usage_flags(VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT)
          .build()
          .value();
  swapchain_ = swapchain.swapchain;
  image_format_ = swapchain.image_format;
  images_ = swapchain.get_images().value();
  views_ = swapchain.get_image_views().value();
  swapchain_shutdown_queue_->pushFunction([&]() {
    for (int i = 0; i < views_.size(); i++) {
      vkDestroyImageView(device_, views_[i], nullptr);
    }
    vkDestroySwapchainKHR(device_, swapchain_, nullptr);
  });

  if (!recreating)
    UVELON_LOG_INFO("Swapchain image format: " + std::to_string(image_format_));

  VkClearValue color_clear;
  color_clear.color = {0.0f, 0.0f, 0.0f, 1.0f};

  color_info_ = {VK_STRUCTURE_TYPE_RENDERING_ATTACHMENT_INFO};
  color_info_.imageLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
  color_info_.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
  color_info_.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
  color_info_.clearValue = color_clear;

  color_linear_info_ = color_info_;

  color_resume_info_ = color_info_;
  color_resume_info_.loadOp = VK_ATTACHMENT_LOAD_OP_LOAD;

  color_linear_resume_info_ = color_linear_info_;
  color_linear_resume_info_.loadOp = VK_ATTACHMENT_LOAD_OP_LOAD;

  if (recreating) notify_recreated_();
}

void Swapchain::initFrameData() {
  framedata_shutdown_queue_ = std::make_unique<ShutdownQueue>();

  VkSemaphoreCreateInfo sem_info = {VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO};

  VkFenceCreateInfo fence_info = {VK_STRUCTURE_TYPE_FENCE_CREATE_INFO};
  fence_info.flags = VK_FENCE_CREATE_SIGNALED_BIT;

  VkCommandPoolCreateInfo pool_info = {
      VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO};
  pool_info.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
  pool_info.queueFamilyIndex = graphics_queue_family_;

  for (int i = 0; i < INFLIGHT_FRAMES; i++) {
    auto& frame_data = frames_[i];

    // Synchronization primitives
    VK_CHECK(
        vkCreateSemaphore(device_, &sem_info, nullptr, &frame_data.sem_ready));
    VK_CHECK(
        vkCreateSemaphore(device_, &sem_info, nullptr, &frame_data.sem_done));
    VK_CHECK(
        vkCreateFence(device_, &fence_info, nullptr, &frame_data.fence_render));

    // Command pool, command buffer
    VK_CHECK(vkCreateCommandPool(device_, &pool_info, nullptr,
                                 &frame_data.cmd_pool));
    VkCommandBufferAllocateInfo buffer_info = {
        VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO};
    buffer_info.commandPool = frame_data.cmd_pool;
    buffer_info.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    buffer_info.commandBufferCount = 1;
    VK_CHECK(vkAllocateCommandBuffers(device_, &buffer_info,
                                      &frame_data.cmd_buffer));

    framedata_shutdown_queue_->pushFunction([&]() {
      vkDestroyCommandPool(device_, frame_data.cmd_pool, nullptr);
      vkDestroyFence(device_, frame_data.fence_render, nullptr);
      vkDestroySemaphore(device_, frame_data.sem_done, nullptr);
      vkDestroySemaphore(device_, frame_data.sem_ready, nullptr);
    });
  }
}

void Swapchain::waitForFrame() {
  assert(!minimized_ &&
         "Attempted to interact with swapchain while minimized!");

  VK_CHECK(vkWaitForFences(device_, 1, &frames_[frame_index_].fence_render,
                           true, -1));
}

bool Swapchain::acquireNextImage() {
  assert(!minimized_ &&
         "Attempted to interact with swapchain while minimized!");

  bool vsync = CVarManager::getVar("vsync", true).getValue<bool>();
  if (vsync != vsync_) {
    recreate();
    return false;
  }

  auto result = vkAcquireNextImageKHR(device_, swapchain_, -1,
                                      frames_[frame_index_].sem_ready, nullptr,
                                      &image_index_);
  if (result == VK_ERROR_OUT_OF_DATE_KHR) {
    recreate();
    return false;
  } else if (result != VK_SUCCESS && result != VK_SUBOPTIMAL_KHR) {
    UVELON_LOG_ERROR("Error attempting to acquire image from swapchain");
    abort();
  }

  VK_CHECK(vkResetFences(device_, 1, &frames_[frame_index_].fence_render));
  return true;
}

void Swapchain::beginRenderTo(VkCommandBuffer cmd, ImageH linear_out) {
  VkImageSubresourceRange subresource = {};
  subresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
  subresource.levelCount = 1;
  subresource.layerCount = 1;
  Image::transitionVkImage(
      cmd, getImage(), VK_PIPELINE_STAGE_2_NONE, 0,
      VK_PIPELINE_STAGE_2_COLOR_ATTACHMENT_OUTPUT_BIT,
      VK_ACCESS_2_COLOR_ATTACHMENT_WRITE_BIT, VK_IMAGE_LAYOUT_UNDEFINED,
      VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL, subresource);

  color_info_.imageView = getImageView();
  if (linear_out) {
    color_linear_info_.imageView = linear_out->getVkImageView();
    linear_out->transition(cmd, VK_PIPELINE_STAGE_2_TOP_OF_PIPE_BIT, 0,
                           VK_PIPELINE_STAGE_2_COLOR_ATTACHMENT_OUTPUT_BIT,
                           VK_ACCESS_2_COLOR_ATTACHMENT_WRITE_BIT,
                           VK_IMAGE_LAYOUT_UNDEFINED,
                           VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL);
  }
  std::array<VkRenderingAttachmentInfo, 2> info = {color_info_,
                                                   color_linear_info_};

  VkRenderingInfoKHR rendering_info = {VK_STRUCTURE_TYPE_RENDERING_INFO_KHR};
  rendering_info.renderArea.offset = {0, 0};
  rendering_info.renderArea.extent = extent_;
  rendering_info.layerCount = 1;
  rendering_info.colorAttachmentCount = linear_out ? 2 : 1;
  rendering_info.pColorAttachments = info.data();
  Renderer::fp_vkCmdBeginRendering(cmd, &rendering_info);
}

void Swapchain::resumeRenderTo(VkCommandBuffer cmd, ImageH linear_out) {
  // Transitions need to be done ahead of calling this!

  color_resume_info_.imageView = getImageView();
  if (linear_out) {
    color_linear_resume_info_.imageView = linear_out->getVkImageView();
  }
  std::array<VkRenderingAttachmentInfo, 2> info = {color_resume_info_,
                                                   color_linear_resume_info_};

  VkRenderingInfoKHR rendering_info = {VK_STRUCTURE_TYPE_RENDERING_INFO_KHR};
  rendering_info.renderArea.offset = {0, 0};
  rendering_info.renderArea.extent = extent_;
  rendering_info.layerCount = 1;
  rendering_info.colorAttachmentCount = linear_out ? 2 : 1;
  rendering_info.pColorAttachments = info.data();
  Renderer::fp_vkCmdBeginRendering(cmd, &rendering_info);
}

void Swapchain::stopRenderTo(VkCommandBuffer cmd) {
  Renderer::fp_vkCmdEndRendering(cmd);
}

void Swapchain::endRenderTo(VkCommandBuffer cmd) {
  Renderer::fp_vkCmdEndRendering(cmd);

  VkImageSubresourceRange subresource = {};
  subresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
  subresource.levelCount = 1;
  subresource.layerCount = 1;
  Image::transitionVkImage(cmd, getImage(),
                           VK_PIPELINE_STAGE_2_COLOR_ATTACHMENT_OUTPUT_BIT,
                           VK_ACCESS_2_COLOR_ATTACHMENT_WRITE_BIT,
                           VK_PIPELINE_STAGE_2_BOTTOM_OF_PIPE_BIT, 0,
                           VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
                           VK_IMAGE_LAYOUT_PRESENT_SRC_KHR, subresource);
}

void Swapchain::submitPresent() {
  assert(!minimized_ &&
         "Attempted to interact with swapchain while minimized!");

  auto& frame_data = frames_[frame_index_];
  auto& cmd = frame_data.cmd_buffer;

  // Submit command to queue
  VkSubmitInfo submit_info = {VK_STRUCTURE_TYPE_SUBMIT_INFO};
  submit_info.waitSemaphoreCount = 1;
  submit_info.pWaitSemaphores = &frame_data.sem_ready;
  VkPipelineStageFlags wait_dst_stage_mask[] = {
      VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT};
  submit_info.pWaitDstStageMask = wait_dst_stage_mask;
  submit_info.commandBufferCount = 1;
  submit_info.pCommandBuffers = &cmd;
  submit_info.signalSemaphoreCount = 1;
  submit_info.pSignalSemaphores = &frame_data.sem_done;
  VK_CHECK(
      vkQueueSubmit(graphics_queue_, 1, &submit_info, frame_data.fence_render));

  // Present once buffer is done executing
  VkPresentInfoKHR present_info = {VK_STRUCTURE_TYPE_PRESENT_INFO_KHR};
  present_info.waitSemaphoreCount = 1;
  present_info.pWaitSemaphores = &frame_data.sem_done;
  present_info.swapchainCount = 1;
  present_info.pSwapchains = &swapchain_;
  present_info.pImageIndices = &image_index_;
  auto result = vkQueuePresentKHR(graphics_queue_, &present_info);
  if (result == VK_ERROR_OUT_OF_DATE_KHR) {
    recreate();
    return;
  } else if (result != VK_SUCCESS && result != VK_SUBOPTIMAL_KHR) {
    UVELON_LOG_ERROR("Error attempting to present");
    abort();
  }

  frame_index_ = (frame_index_) % INFLIGHT_FRAMES;
}

}  // namespace Uvelon