#include "rendering/Computer.hpp"

#include <thread>

#include "rendering/Renderer.hpp"
#include "rendering/Utils.hpp"

namespace Uvelon {

void Computer::init(VkQueue compute_queue, uint32_t compute_queue_family) {
  queue_ = compute_queue;

  VkCommandPoolCreateInfo cpcreate_info = {
      VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO};
  cpcreate_info.queueFamilyIndex = compute_queue_family;
  cpcreate_info.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
  VK_CHECK(vkCreateCommandPool(Renderer::getInstance().getDevice(),
                               &cpcreate_info, nullptr, &cmd_pool_));
}

Computer::~Computer() { destroy(); }

void Computer::destroy() {
  std::lock_guard<std::mutex> lock(m_free_);
  if (!destroyed_) {
    destroyed_ = true;
    vkDestroyCommandPool(Renderer::getInstance().getDevice(), cmd_pool_,
                         nullptr);
    free_cmds_.clear();
    for (VkFence fence : free_fences_) {
      vkDestroyFence(Renderer::getInstance().getDevice(), fence, nullptr);
    }
    free_fences_.clear();
  }
}

VkCommandBuffer Computer::getCommandBuffer() {
  VkCommandBuffer result;

  VkCommandBufferBeginInfo cmd_begin_info = {
      VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO};
  cmd_begin_info.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

  {
    std::lock_guard<std::mutex> lock(m_free_);
    if (!free_cmds_.empty()) {
      result = free_cmds_.back();
      free_cmds_.pop_back();
      VK_CHECK(vkResetCommandBuffer(result, 0));
      VK_CHECK(vkBeginCommandBuffer(result, &cmd_begin_info));
      return result;
    }
  }

  VkCommandBufferAllocateInfo cballoc_info = {
      VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO};
  cballoc_info.commandBufferCount = 1;
  cballoc_info.commandPool = cmd_pool_;
  cballoc_info.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
  VK_CHECK(vkAllocateCommandBuffers(Renderer::getInstance().getDevice(),
                                    &cballoc_info, &result));
  VK_CHECK(vkBeginCommandBuffer(result, &cmd_begin_info));
  return result;
}

void Computer::submit(VkCommandBuffer cmd, std::vector<VkSemaphore> wait_sem,
                      std::vector<VkSemaphore> signal_sem, bool await) {
  VkFence fence;
  {
    std::lock_guard<std::mutex> lock(m_free_);
    if (!free_fences_.empty()) {
      fence = free_fences_.back();
      free_fences_.pop_back();
      VK_CHECK(vkResetFences(Renderer::getInstance().getDevice(), 1, &fence));
    } else {
      VkFenceCreateInfo fence_info = {VK_STRUCTURE_TYPE_FENCE_CREATE_INFO};
      VK_CHECK(vkCreateFence(Renderer::getInstance().getDevice(), &fence_info,
                             nullptr, &fence));
    }
  }

  VK_CHECK(vkEndCommandBuffer(cmd));

  VkSubmitInfo submit_info = {VK_STRUCTURE_TYPE_SUBMIT_INFO};
  submit_info.commandBufferCount = 1;
  submit_info.pCommandBuffers = &cmd;
  submit_info.waitSemaphoreCount = static_cast<uint32_t>(wait_sem.size());
  submit_info.pWaitSemaphores = wait_sem.data();
  submit_info.signalSemaphoreCount = static_cast<uint32_t>(signal_sem.size());
  submit_info.pSignalSemaphores = signal_sem.data();
  VK_CHECK(vkQueueSubmit(queue_, 1, &submit_info, fence));

  if (await) {
    VK_CHECK(vkWaitForFences(Renderer::getInstance().getDevice(), 1, &fence,
                             VK_TRUE, -1));
    std::lock_guard<std::mutex> lock(m_free_);
    free_cmds_.push_back(cmd);
    free_fences_.push_back(fence);
  } else {
    std::thread t = std::thread([this, cmd, fence]() {
      VK_CHECK(vkWaitForFences(Renderer::getInstance().getDevice(), 1, &fence,
                               VK_TRUE, -1));
      std::lock_guard<std::mutex> lock(m_free_);
      free_cmds_.push_back(cmd);
      free_fences_.push_back(fence);
    });
    t.detach();
  }
}

}  // namespace Uvelon