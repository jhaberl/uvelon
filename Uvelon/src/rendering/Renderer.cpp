#include "rendering/Renderer.hpp"

#include <chrono>
#include <thread>

#include "VkBootstrap.h"
#include "rendering/Descriptor.hpp"
#include "rendering/GBuffer.hpp"
#include "rendering/Image.hpp"
#include "rendering/Primitives.hpp"
#include "rendering/RenderData.hpp"
#include "rendering/Utils.hpp"
#include "rendering/Window.hpp"
#include "rendering/systems/GUISystem.hpp"
#include "rendering/systems/GaussianSliceSystem.hpp"
#include "rendering/systems/GaussianSystem.hpp"
#include "rendering/systems/MeshPrimitiveSystem.hpp"
#include "rendering/systems/ShadowMapSystem.hpp"
#include "rendering/systems/SkyboxSystem.hpp"
#include "utils/Instrumentation.hpp"
#include "utils/Logger.hpp"

#define VMA_IMPLEMENTATION
#include "vk_mem_alloc.h"

namespace Uvelon {

PFN_vkCmdBeginRenderingKHR Renderer::fp_vkCmdBeginRendering;
PFN_vkCmdEndRenderingKHR Renderer::fp_vkCmdEndRendering;
PFN_vkCmdPipelineBarrier2KHR Renderer::fp_vkCmdPipelineBarrier2;

Renderer& Renderer::getInstance() {
  static Renderer renderer;
  return renderer;
}

void Renderer::init(Renderer::InitConfig config) {
  assert(!initialized_ && "Renderer is already initialized");

  init_config_ = config;
  assert(init_config_.window && "Invalid window passed to initialize renderer");

  UVELON_LOG_INFO("Initializing Renderer");

  shutdown_queue_ = std::make_unique<ShutdownQueue>();
  initVulkan();
  swapchain_ = std::make_unique<Swapchain>(device_, *init_config_.window,
                                           physical_device_, graphics_queue_,
                                           graphics_queue_family_);
  swapchain_->init();
  shutdown_queue_->pushFunction([&]() { swapchain_->destroy(); });

  computer_ = std::make_unique<Computer>();
  computer_->init(compute_queue_, compute_queue_family_);
  shutdown_queue_->pushFunction([&]() { computer_->destroy(); });
  render_computer_ = std::make_unique<Computer>();
  render_computer_->init(graphics_queue_, graphics_queue_family_);
  shutdown_queue_->pushFunction([&]() { render_computer_->destroy(); });

  initImmediateSubmit();

  descriptor_allocator_ = std::make_unique<DescriptorAllocator>();
  shutdown_queue_->pushFunction([&]() { descriptor_allocator_->destroy(); });

  shader_cache_ = std::make_unique<ShaderCache>();
  shutdown_queue_->pushFunction([&]() { shader_cache_->destroy(); });

  render_data_ = std::make_unique<RenderData>();
  shutdown_queue_->pushFunction([&]() { render_data_->destroy(); });

  // Create buffers and descriptor set for per-frame ubo
  global_ubo_buffer_ = render_data_->allocateBuffer(
      sizeof(GlobalUniformBuffer), VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, true);
  global_ds_ =
      DescriptorBuilder::begin()
          .addBuffer(VK_SHADER_STAGE_ALL, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
                     global_ubo_buffer_, 0, sizeof(GlobalUniformBuffer))
          .build();

  g_buffer_ = std::make_unique<GBuffer>();
  g_buffer_->init();
  swapchain_->setRecreateCallback([&]() { g_buffer_->recreate(); });
  shutdown_queue_->pushFunction([&]() { g_buffer_->destroy(); });

  render_systems_.push_back(std::make_unique<SkyboxSystem>());
  render_systems_.push_back(std::make_unique<MeshPrimitiveSystem>());
  render_systems_.push_back(std::make_unique<GaussianSystem>());
  render_systems_.push_back(std::make_unique<GaussianSliceSystem>());
  render_systems_.push_back(std::make_unique<GUISystem>());
  render_system_shadow_map_ = std::make_unique<ShadowMapSystem>();

  for (auto& render_system : render_systems_) {
    render_system->init();
  }
  shutdown_queue_->pushFunction([&]() {
    for (auto& render_system : render_systems_) {
      render_system->destroy();
    }
    render_system_shadow_map_->destroy();
  });

  initialized_ = true;
}

void Renderer::shutdown() {
  if (initialized_) {
    vkDeviceWaitIdle(device_);
    UVELON_PROFILER_DESTROY_GPU();
    shutdown_queue_->performShutdown();

    initialized_ = false;
  }
}

void Renderer::initVulkan() {
  uint32_t api_version;
  VK_CHECK(vkEnumerateInstanceVersion(&api_version));
  UVELON_LOG_INFO("Vulkan API version: " +
                  std::to_string(VK_API_VERSION_MAJOR(api_version)) + "." +
                  std::to_string(VK_API_VERSION_MINOR(api_version)) + "." +
                  std::to_string(VK_API_VERSION_PATCH(api_version)));

// Vulkan instance
#ifdef __APPLE__
  vkb::InstanceBuilder builder(vkGetInstanceProcAddr);
#else
  vkb::InstanceBuilder builder;
#endif
  auto inst_ret =
      builder.set_app_name("Uvelon")
          .request_validation_layers(init_config_.use_validation_layers)
          .use_default_debug_messenger()
          .require_api_version(1, 3, 0)
          .build();
  if (!inst_ret.has_value()) {
    UVELON_LOG_ERROR("Failed to create Vulkan instance: " +
                     inst_ret.error().message());
    abort();
  }
  vkb::Instance vkb_inst = inst_ret.value();
  instance_ = vkb_inst.instance;
  shutdown_queue_->pushFunction(
      [&]() { vkDestroyInstance(instance_, nullptr); });
  debug_messenger_ = vkb_inst.debug_messenger;
  shutdown_queue_->pushFunction([&]() {
    vkb::destroy_debug_utils_messenger(instance_, debug_messenger_);
  });

  // Window surface
  init_config_.window->createSurface(instance_);
  shutdown_queue_->pushFunction([&]() {
    vkDestroySurfaceKHR(instance_, init_config_.window->getSurface(), nullptr);
  });

  // Physical device
  VkPhysicalDeviceVulkan11Features vulkan11_features = {
      VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_1_FEATURES};
  vulkan11_features.multiview = true;
  VkPhysicalDeviceDynamicRenderingFeaturesKHR dyn_features = {
      VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DYNAMIC_RENDERING_FEATURES_KHR};
  dyn_features.dynamicRendering = true;
  VkPhysicalDeviceSynchronization2FeaturesKHR syn_features = {
      VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SYNCHRONIZATION_2_FEATURES_KHR};
  syn_features.synchronization2 = true;
  VkPhysicalDeviceDescriptorIndexingFeatures desc_index_features = {
      VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DESCRIPTOR_INDEXING_FEATURES};
  desc_index_features.descriptorBindingPartiallyBound = true;
  desc_index_features.descriptorBindingVariableDescriptorCount = false;
  desc_index_features.descriptorBindingSampledImageUpdateAfterBind = true;
  VkPhysicalDeviceHostQueryResetFeatures host_query_reset_features = {
      VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_HOST_QUERY_RESET_FEATURES};
  host_query_reset_features.hostQueryReset = true;
  vkb::PhysicalDeviceSelector selector(vkb_inst);
  auto physical_device_result =
      selector.set_minimum_version(1, 2)
          .add_required_extension("VK_KHR_dynamic_rendering")
          .add_required_extension_features(dyn_features)
          .add_required_extension("VK_KHR_synchronization2")
          .add_required_extension_features(syn_features)
          .add_required_extension("VK_EXT_descriptor_indexing")
          .add_required_extension_features(desc_index_features)
          .add_required_extension_features(vulkan11_features)
          .add_required_extension_features(host_query_reset_features)
          .set_surface(init_config_.window->getSurface())
          .select();
  if (!physical_device_result.has_value()) {
    UVELON_LOG_ERROR("Failed to find physical device: " +
                     physical_device_result.error().message());
    abort();
  }
  // glPrimitive_ID in fragment shader
  VkPhysicalDeviceFeatures features = {};
  features.geometryShader = true;
  features.tessellationShader = true;
  physical_device_result.value().enable_features_if_present(features);

  // 8 bit index buffers
  VkPhysicalDeviceIndexTypeUint8FeaturesEXT features_uint8_index = {
      VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_INDEX_TYPE_UINT8_FEATURES_EXT};
  features_uint8_index.indexTypeUint8 = true;
  if (!(physical_device_result.value().enable_extension_if_present(
            "VK_EXT_index_type_uint8") &&
        physical_device_result.value().enable_extension_features_if_present(
            features_uint8_index)))
    UVELON_LOG_WARNING("8 bit indices unsupported");

  physical_device_ = physical_device_result.value().physical_device;
  vkGetPhysicalDeviceProperties(physical_device_, &physical_device_properties_);
  // MoltenVK currently does not support Vulkan 1.3, but it does support the
  // dynamic rendering extension.
  fp_vkCmdBeginRendering = reinterpret_cast<PFN_vkCmdBeginRenderingKHR>(
      vkGetInstanceProcAddr(instance_, "vkCmdBeginRenderingKHR"));
  fp_vkCmdEndRendering = reinterpret_cast<PFN_vkCmdEndRenderingKHR>(
      vkGetInstanceProcAddr(instance_, "vkCmdEndRenderingKHR"));
  fp_vkCmdPipelineBarrier2 = reinterpret_cast<PFN_vkCmdPipelineBarrier2KHR>(
      vkGetInstanceProcAddr(instance_, "vkCmdPipelineBarrier2KHR"));

  VkPhysicalDeviceMemoryProperties memory_properties = {};
  vkGetPhysicalDeviceMemoryProperties(physical_device_, &memory_properties);
  integrated_device_ = memory_properties.memoryHeapCount == 1;

  UVELON_LOG_INFO("Selected device: " + physical_device_result.value().name +
                  " (integrated: " + std::to_string(integrated_device_) + ")");

  // Logical device
  vkb::DeviceBuilder device_builder{physical_device_result.value()};
  vkb::Device device = device_builder.build().value();
  device_ = device.device;
  shutdown_queue_->pushFunction([&]() { vkDestroyDevice(device_, nullptr); });

  UVELON_PROFILER_INIT_GPU(physical_device_, device_);

  // Queue
  graphics_queue_ = device.get_queue(vkb::QueueType::graphics).value();
  graphics_queue_family_ =
      device.get_queue_index(vkb::QueueType::graphics).value();
  auto compute_result = device.get_queue(vkb::QueueType::compute);
  if (compute_result) {
    compute_queue_ = compute_result.value();
    compute_queue_family_ =
        device.get_queue_index(vkb::QueueType::compute).value();
  } else {
    // TODO: Find a compute queue separate from the graphics queue which may
    // also be capable of graphics Gotta figure out how to get vkb to do that
    compute_queue_ = graphics_queue_;
    compute_queue_family_ = graphics_queue_family_;
  }

  // VMA memory allocator
  VmaAllocatorCreateInfo vma_create_info = {};
  vma_create_info.instance = instance_;
  vma_create_info.physicalDevice = physical_device_;
  vma_create_info.device = device_;
  vmaCreateAllocator(&vma_create_info, &allocator_);
  shutdown_queue_->pushFunction([&]() { vmaDestroyAllocator(allocator_); });
}

void Renderer::initImmediateSubmit() {
  VkCommandPoolCreateInfo pool_info = {
      VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO};
  pool_info.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
  pool_info.queueFamilyIndex = graphics_queue_family_;

  VK_CHECK(vkCreateCommandPool(device_, &pool_info, nullptr,
                               &immediate_submit_data.cmd_pool));
  VkCommandBufferAllocateInfo buffer_info = {
      VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO};
  buffer_info.commandPool = immediate_submit_data.cmd_pool;
  buffer_info.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
  buffer_info.commandBufferCount = 1;
  VK_CHECK(vkAllocateCommandBuffers(device_, &buffer_info,
                                    &immediate_submit_data.cmd_buffer));

  VkFenceCreateInfo fence_info = {VK_STRUCTURE_TYPE_FENCE_CREATE_INFO};
  VK_CHECK(vkCreateFence(device_, &fence_info, nullptr,
                         &immediate_submit_data.fence));

  shutdown_queue_->pushFunction([&]() {
    vkDestroyCommandPool(device_, immediate_submit_data.cmd_pool, nullptr);
    vkDestroyFence(device_, immediate_submit_data.fence, nullptr);
  });
}

void Renderer::immediateSubmit(
    std::function<void(VkCommandBuffer cmd)> function) {
  // Reset data
  VK_CHECK(vkResetFences(device_, 1, &immediate_submit_data.fence));
  VK_CHECK(vkResetCommandBuffer(immediate_submit_data.cmd_buffer, 0));

  // Run function with recording command buffer
  VkCommandBufferBeginInfo begin_info = {
      VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO};
  begin_info.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
  VK_CHECK(vkBeginCommandBuffer(immediate_submit_data.cmd_buffer, &begin_info));
  function(immediate_submit_data.cmd_buffer);
  VK_CHECK(vkEndCommandBuffer(immediate_submit_data.cmd_buffer));

  // Submit to queue, wait for execution to complete
  VkSubmitInfo submit_info = {VK_STRUCTURE_TYPE_SUBMIT_INFO};
  submit_info.commandBufferCount = 1;
  submit_info.pCommandBuffers = &immediate_submit_data.cmd_buffer;
  VK_CHECK(vkQueueSubmit(graphics_queue_, 1, &submit_info,
                         immediate_submit_data.fence));
  VK_CHECK(vkWaitForFences(device_, 1, &immediate_submit_data.fence, true,
                           static_cast<uint64_t>(-1)));
}

void Renderer::draw(DrawData& data) {
  render_data_->hotReloadShaders();

  if (swapchain_->isMinimized()) {
    // Window is minimized, simply delay for a bit
    std::this_thread::sleep_for(std::chrono::milliseconds(30));
    return;
  }

  // Wait for this frame to be done rendering
  swapchain_->waitForFrame();

  // Acquire next image from swapchain
  if (!swapchain_->acquireNextImage()) return;

  // Reset and begin command buffer
  auto cmd = swapchain_->getCommandBuffer();
  VK_CHECK(vkResetCommandBuffer(cmd, 0));
  VkCommandBufferBeginInfo cmd_begin_info = {
      VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO};
  cmd_begin_info.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
  VK_CHECK(vkBeginCommandBuffer(cmd, &cmd_begin_info));

  // Update uniform buffer
  global_ubo_.view = data.view_matrix;
  global_ubo_.projection = data.projection_matrix;
  global_ubo_.inv_view_projection =
      glm::inverse(global_ubo_.projection * global_ubo_.view);
  global_ubo_.camera_position = glm::inverse(data.view_matrix)[3];
  global_ubo_.window_size.x =
      static_cast<float>(init_config_.window->getWidth());
  global_ubo_.window_size.y =
      static_cast<float>(init_config_.window->getHeight());
  memcpy(global_ubo_buffer_->allocation_info.pMappedData, &global_ubo_,
         sizeof(GlobalUniformBuffer));

  for (auto& render_system : render_systems_) {
    render_system->prepare(cmd, data.render_objects);
  }

  static_cast<ShadowMapSystem*>(render_system_shadow_map_.get())
      ->setRenderObjects(&data.render_objects);
  render_system_shadow_map_->prepare(cmd, data.light_primitives);
  render_system_shadow_map_->draw(
      cmd, RenderSystem<LightPrimitive>::Stage::SHADOWMAP,
      data.light_primitives);

  // Set viewport dynamically
  VkViewport viewport = {0.0f,
                         0.0f,
                         static_cast<float>(init_config_.window->getWidth()),
                         static_cast<float>(init_config_.window->getHeight()),
                         0.0f,
                         1.0f};
  VkRect2D scissor = {
      {0, 0},
      {static_cast<uint32_t>(init_config_.window->getWidth()),
       static_cast<uint32_t>(init_config_.window->getHeight())}};
  vkCmdSetViewport(cmd, 0, 1, &viewport);
  vkCmdSetScissor(cmd, 0, 1, &scissor);

  g_buffer_->beginRenderTo(cmd);
  for (auto& render_system : render_systems_) {
    render_system->draw(cmd, RenderSystem<RenderObject>::Stage::GBUFFER,
                        data.render_objects);
  }
  g_buffer_->endRenderTo(cmd);

  swapchain_->beginRenderTo(cmd, g_buffer_->getLinearFrame());
  for (auto& render_system : render_systems_) {
    render_system->draw(cmd, RenderSystem<RenderObject>::Stage::SWAPCHAIN,
                        data.render_objects);
  }
  g_buffer_->render(cmd, global_ds_, data.light_primitives,
                    data.environment_ibl);
  swapchain_->stopRenderTo(cmd);
  g_buffer_->prepareSSR(cmd);
  swapchain_->resumeRenderTo(cmd);
  for (auto& render_system : render_systems_) {
    render_system->draw(cmd, RenderSystem<RenderObject>::Stage::SWAPCHAIN_FINAL,
                        data.render_objects);
  }
  swapchain_->endRenderTo(cmd);

  VK_CHECK(vkEndCommandBuffer(cmd));

  // Submit command to queue and present it once the command buffer is done
  // executing
  swapchain_->submitPresent();

  render_data_->gc();
  UVELON_PROFILER_COLLECT_GPU(Reporting::CVAR);
}

}  // namespace Uvelon