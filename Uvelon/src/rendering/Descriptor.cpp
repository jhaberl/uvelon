#include "rendering/Descriptor.hpp"

#include "rendering/RenderData.hpp"
#include "rendering/Renderer.hpp"
#include "rendering/Utils.hpp"

namespace Uvelon {

DescriptorAllocator::~DescriptorAllocator() { destroy(); }

void DescriptorAllocator::destroy() {
  auto device = Renderer::getInstance().getDevice();
  if (current_pool_) vkDestroyDescriptorPool(device, current_pool_, nullptr);
  for (auto pool : available_pools_) {
    vkDestroyDescriptorPool(device, pool, nullptr);
  }
  for (auto pool : used_pools_) {
    vkDestroyDescriptorPool(device, pool, nullptr);
  }

  current_pool_ = nullptr;
  available_pools_.clear();
  used_pools_.clear();
}

void DescriptorAllocator::invalidatePool() {
  VkDescriptorPool new_pool;
  if (available_pools_.size() > 0) {
    // Instead of creating a new pool just reuse an available one
    new_pool = available_pools_.back();
    available_pools_.pop_back();
  } else {
    uint32_t max_sets = 1000;

    std::vector<VkDescriptorPoolSize> adjusted_sizes;
    adjusted_sizes.resize(sizes_.sizes.size());
    for (int i = 0; i < sizes_.sizes.size(); i++) {
      adjusted_sizes[i].type = sizes_.sizes[i].first;
      adjusted_sizes[i].descriptorCount =
          static_cast<uint32_t>(sizes_.sizes[i].second * max_sets);
    }

    VkDescriptorPoolCreateInfo pool_info = {
        VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO};
    pool_info.flags = VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT |
                      VK_DESCRIPTOR_POOL_CREATE_UPDATE_AFTER_BIND_BIT;
    pool_info.maxSets = max_sets;
    pool_info.poolSizeCount = static_cast<uint32_t>(adjusted_sizes.size());
    pool_info.pPoolSizes = adjusted_sizes.data();

    VK_CHECK(vkCreateDescriptorPool(Renderer::getInstance().getDevice(),
                                    &pool_info, nullptr, &new_pool));
  }
  if (current_pool_ != VK_NULL_HANDLE) used_pools_.push_back(current_pool_);
  current_pool_ = new_pool;
}

VkDescriptorPool DescriptorAllocator::allocate(VkDescriptorSet& set,
                                               VkDescriptorSetLayout layout) {
  if (current_pool_ == VK_NULL_HANDLE) invalidatePool();

  VkDescriptorSetAllocateInfo allocate_info = {
      VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO};
  allocate_info.descriptorPool = current_pool_;
  allocate_info.descriptorSetCount = 1;
  allocate_info.pSetLayouts = &layout;
  VkResult result = vkAllocateDescriptorSets(
      Renderer::getInstance().getDevice(), &allocate_info, &set);
  switch (result) {
    case VK_SUCCESS:
      return current_pool_;
    case VK_ERROR_FRAGMENTED_POOL:
    case VK_ERROR_OUT_OF_POOL_MEMORY:
      break;
    default:
      UVELON_LOG_ERROR("Allocating descriptor pool failed!");
      abort();
      return VK_NULL_HANDLE;
  }

  invalidatePool();
  VK_CHECK(vkAllocateDescriptorSets(Renderer::getInstance().getDevice(),
                                    &allocate_info, &set));
  return current_pool_;
}

// ---

VkDescriptorSetLayout DescriptorLayoutCache::create(
    VkDescriptorSetLayoutCreateInfo layout_info) {
  VkDescriptorSetLayout layout;
  VK_CHECK(vkCreateDescriptorSetLayout(Renderer::getInstance().getDevice(),
                                       &layout_info, nullptr, &layout));

  // TODO: Actually cache/retrieve things. Note that this changes when we can
  // destroy a layout.

  return layout;
}

// ---

uint32_t DescriptorBuilder::addBinding(VkDescriptorType descriptor_type,
                                       VkShaderStageFlags stage, int count,
                                       VkDescriptorBindingFlags flags) {
  uint32_t index = static_cast<uint32_t>(bindings_.size());
  VkDescriptorSetLayoutBinding binding = {};
  binding.binding = index;
  binding.descriptorType = descriptor_type;
  binding.descriptorCount = count;
  binding.stageFlags = stage;
  bindings_.push_back(binding);
  binding_flags_.push_back(flags);
  return index;
}

DescriptorBuilder& DescriptorBuilder::addBuffer(VkShaderStageFlags stage,
                                                VkDescriptorType type,
                                                BufferH buffer, size_t offset,
                                                size_t range) {
  uint32_t binding = addBinding(type, stage, 1);
  updater_.updateBuffer(binding, type, buffer, offset, range);
  return *this;
}

DescriptorBuilder& DescriptorBuilder::addSampler(VkShaderStageFlags stage,
                                                 SamplerH sampler) {
  uint32_t binding = addBinding(VK_DESCRIPTOR_TYPE_SAMPLER, stage, 1);
  updater_.updateSampler(binding, sampler);
  return *this;
}

DescriptorBuilder& DescriptorBuilder::addImage(VkShaderStageFlags stage,
                                               ImageH image,
                                               VkImageLayout layout,
                                               bool storage) {
  uint32_t binding = addBinding(storage ? VK_DESCRIPTOR_TYPE_STORAGE_IMAGE
                                        : VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE,
                                stage, 1);
  updater_.updateImage(binding, image, layout, storage);
  return *this;
}

DescriptorBuilder& DescriptorBuilder::addCombinedImageSampler(
    VkShaderStageFlags stage, ImageH image, VkImageLayout layout,
    SamplerH sampler) {
  uint32_t binding =
      addBinding(VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, stage, 1);
  updater_.updateCombinedImageSampler(binding, image, layout, sampler);
  return *this;
}

DescriptorBuilder& DescriptorBuilder::addSparseImageArray(
    VkShaderStageFlags stage, int count) {
  addBinding(VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE, stage, count,
             VK_DESCRIPTOR_BINDING_PARTIALLY_BOUND_BIT |
                 VK_DESCRIPTOR_BINDING_UPDATE_AFTER_BIND_BIT);
  return *this;
}

DescriptorSetH DescriptorBuilder::build() {
  DescriptorSet result;
  VkDescriptorSetLayoutCreateInfo layout_info = {
      VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO};
  layout_info.bindingCount = static_cast<uint32_t>(bindings_.size());
  layout_info.pBindings = bindings_.data();
  layout_info.flags =
      VK_DESCRIPTOR_SET_LAYOUT_CREATE_UPDATE_AFTER_BIND_POOL_BIT;

  VkDescriptorSetLayoutBindingFlagsCreateInfo flag_info = {
      VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_BINDING_FLAGS_CREATE_INFO};
  flag_info.bindingCount = static_cast<uint32_t>(binding_flags_.size());
  flag_info.pBindingFlags = binding_flags_.data();
  layout_info.pNext = &flag_info;

  auto& descriptor_allocator = Renderer::getInstance().getDescriptorAllocator();
  result.layout = descriptor_allocator.getLayoutCache().create(layout_info);
  result.pool = descriptor_allocator.allocate(result.set, result.layout);

  DescriptorSetH descriptor_set =
      Renderer::getInstance().getRenderData().addDescriptorSet(
          std::move(result));
  updater_.commit(descriptor_set);

  return descriptor_set;
}

void DescriptorUpdater::addWrite(uint32_t index,
                                 VkDescriptorType descriptor_type,
                                 VkDescriptorBufferInfo* buffer_info,
                                 VkDescriptorImageInfo* image_info,
                                 uint32_t array_index) {
  VkWriteDescriptorSet write = {VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET};
  write.dstBinding = index;
  write.dstArrayElement = array_index;
  write.descriptorCount = 1;
  write.descriptorType = descriptor_type;
  write.pImageInfo = image_info;
  write.pBufferInfo = buffer_info;
  writes_.push_back(write);
}

DescriptorUpdater& DescriptorUpdater::updateBuffer(uint32_t index,
                                                   VkDescriptorType type,
                                                   BufferH buffer,
                                                   size_t offset,
                                                   size_t range) {
  // Maybe change this to be dynamic for offsets when binding
  VkDescriptorBufferInfo info = {};
  info.buffer = buffer->buffer;
  info.offset = offset;
  info.range = range;
  buffer_infos_.push_back(info);
  updated_resources_.push_back({index, 0, ImageH(), SamplerH(), buffer});
  addWrite(index, type, &buffer_infos_.back(), nullptr);
  return *this;
}

DescriptorUpdater& DescriptorUpdater::updateSampler(uint32_t index,
                                                    SamplerH sampler) {
  VkDescriptorImageInfo info = {};
  info.sampler = *sampler;
  info.imageView = nullptr;
  info.imageLayout =
      VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;  // TODO: Can we set this to
                                                 // none?
  image_infos_.push_back(info);
  updated_resources_.push_back({index, 0, ImageH(), sampler, BufferH()});
  addWrite(index, VK_DESCRIPTOR_TYPE_SAMPLER, nullptr, &image_infos_.back());
  return *this;
}

DescriptorUpdater& DescriptorUpdater::updateImage(uint32_t index, ImageH image,
                                                  VkImageLayout layout,
                                                  bool storage) {
  VkDescriptorImageInfo info = {};
  info.imageView = image->getVkImageView();
  info.sampler = nullptr;
  info.imageLayout = layout;
  image_infos_.push_back(info);
  updated_resources_.push_back({index, 0, image, SamplerH(), BufferH()});
  addWrite(index,
           storage ? VK_DESCRIPTOR_TYPE_STORAGE_IMAGE
                   : VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE,
           nullptr, &image_infos_.back());
  return *this;
}

DescriptorUpdater& DescriptorUpdater::updateCombinedImageSampler(
    uint32_t index, ImageH image, VkImageLayout layout, SamplerH sampler) {
  VkDescriptorImageInfo info = {};
  info.imageView = image->getVkImageView();
  info.imageLayout = layout;
  info.sampler = *sampler;
  image_infos_.push_back(info);
  updated_resources_.push_back({index, 0, image, sampler, BufferH()});

  addWrite(index, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, nullptr,
           &image_infos_.back());
  return *this;
}

DescriptorUpdater& DescriptorUpdater::updateSparseImageArray(
    uint32_t index, uint32_t array_index, ImageH image, VkImageLayout layout) {
  VkDescriptorImageInfo info = {};
  info.imageView = image->getVkImageView();
  info.imageLayout = layout;
  image_infos_.push_back(info);
  updated_resources_.push_back(
      {index, array_index, image, SamplerH(), BufferH()});
  addWrite(index, VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE, nullptr,
           &image_infos_.back(), array_index);
  return *this;
}

void DescriptorUpdater::commit(DescriptorSetH descriptor_set) {
  if (writes_.empty()) return;
  for (auto& write : writes_) {
    write.dstSet = descriptor_set->set;
  }
  vkUpdateDescriptorSets(Renderer::getInstance().getDevice(),
                         static_cast<uint32_t>(writes_.size()), writes_.data(),
                         0, nullptr);

  for (DescriptorSet::Resource& resource : updated_resources_) {
    updateResource(descriptor_set, resource);
  }
}

void DescriptorUpdater::updateResource(DescriptorSetH descriptor_set,
                                       DescriptorSet::Resource resource) {
  std::erase_if(descriptor_set->resources,
                [resource](DescriptorSet::Resource& o) {
                  return o.binding == resource.binding &&
                         o.array_index == resource.array_index;
                });
  descriptor_set->resources.push_back(resource);
}

}  // namespace Uvelon