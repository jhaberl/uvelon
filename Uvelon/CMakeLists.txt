cmake_minimum_required(VERSION 3.2)

set(PROJECT_NAME Uvelon)
project(${PROJECT_NAME})

file(GLOB_RECURSE UVELON_SRC "${CMAKE_CURRENT_SOURCE_DIR}/src/*.cpp")

set(CMAKE_WINDOWS_EXPORT_ALL_SYMBOLS ON)
add_library(${PROJECT_NAME} STATIC ${UVELON_SRC})
target_include_directories(${PROJECT_NAME} PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}/include")
target_link_libraries(${PROJECT_NAME} PUBLIC vulkan vkbootstrap VulkanMemoryAllocator mikktspace weldmesh glm imgui implot stb_image sdl3 shaderc nlohmann nfd)

if (APPLE)
    find_library(FOUNDATION_LIB CoreFoundation)
    target_link_libraries(${PROJECT_NAME} INTERFACE ${FOUNDATION_LIB})
endif ()