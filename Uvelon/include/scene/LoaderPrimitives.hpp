#pragma once

#include <memory>

#include "rendering/Primitives.hpp"
#include "utils/MappedFile.hpp"

namespace Uvelon {

class BufferCPU;

struct Buffer {
  std::shared_ptr<MappedFile> file;

  char* data;
  size_t size;

  Buffer(std::shared_ptr<MappedFile> file, size_t offset, size_t size)
      : file(file), size(size), data(static_cast<char*>(file->data) + offset) {}
};

struct BufferView {
  std::shared_ptr<Buffer> buffer;
  size_t offset, length, stride;
  int target;

  BufferView(std::shared_ptr<Buffer> buffer, size_t offset, size_t length,
             size_t stride, int target)
      : buffer(buffer),
        offset(offset),
        length(length),
        stride(stride),
        target(target) {}
};

struct Accessor {
  std::shared_ptr<BufferView> view;
  size_t offset, count;
  int component_type;
  int type;

  size_t size;

  Accessor(std::shared_ptr<BufferView> view, size_t offset, size_t count,
           int component_type, int type)
      : view(view),
        offset(offset),
        count(count),
        component_type(component_type),
        type(type) {}

  BufferH loadGPU(VkBufferUsageFlags usage = 0);
  BufferCPU loadCPU();
};

struct VertexData {
  // This needs to match the vertex data expected in mtv.vert
  static const uint32_t max_attributes = 4;
  enum Attribute { POSITION, NORMAL, TANGENT, UV };
  const VkFormat formats[max_attributes]{
      VK_FORMAT_R32G32B32_SFLOAT, VK_FORMAT_R32G32B32_SFLOAT,
      VK_FORMAT_R32G32B32A32_SFLOAT, VK_FORMAT_R32G32_SFLOAT};
  const uint32_t num_floats[max_attributes]{3, 3, 4, 2};

  std::vector<BufferH> buffers;
  std::vector<VkVertexInputBindingDescription> binding_descriptions;
  std::vector<VkVertexInputAttributeDescription> attribute_descriptions;

  VertexData() {
    for (int i = 0; i < 8; i++) accessors_[i] = nullptr;
  }
  VertexData(
      std::vector<BufferH> buffers,
      std::vector<VkVertexInputBindingDescription> binding_descriptions,
      std::vector<VkVertexInputAttributeDescription> attribute_descriptions)
      : buffers(buffers),
        binding_descriptions(binding_descriptions),
        attribute_descriptions(attribute_descriptions),
        data_only_(true),
        immutable_(true) {
    for (int i = 0; i < 8; i++) accessors_[i] = nullptr;
  }
  VertexData(VertexData&& o)
      : buffers(std::move(o.buffers)),
        binding_descriptions(std::move(o.binding_descriptions)),
        attribute_descriptions(std::move(o.attribute_descriptions)),
        immutable_(o.immutable_),
        data_only_(o.data_only_),
        views_cpu_(std::move(o.views_cpu_)) {
    for (int i = 0; i < 8; i++) accessors_[i] = o.accessors_[i];
  }

  VertexData& operator=(VertexData&& o) {
    buffers = std::move(o.buffers);
    binding_descriptions = std::move(o.binding_descriptions);
    attribute_descriptions = std::move(o.attribute_descriptions);
    immutable_ = o.immutable_;
    data_only_ = o.data_only_;
    views_cpu_ = std::move(o.views_cpu_);
    return *this;
  }

  void setAttribute(Attribute attribute, std::shared_ptr<Accessor> accessor);
  bool containsAttribute(Attribute attribute);

  void loadGPU();
  void loadCPU();

  void getEntry(Attribute attribute, uint32_t index, float* result);
  uint32_t getNumEntries(Attribute attribute);

 private:
  bool immutable_{false}, data_only_{false};

  std::shared_ptr<Accessor> accessors_[8];
  std::vector<BufferCPU> views_cpu_;

  void generateDescriptions();
};

inline bool operator==(const VertexData& lhs, const VertexData& rhs) {
  bool same = true;
  same &= lhs.buffers.size() == rhs.buffers.size();
  same &= lhs.binding_descriptions.size() == rhs.binding_descriptions.size();
  same &=
      lhs.attribute_descriptions.size() == rhs.attribute_descriptions.size();
  for (size_t i = 0; i < lhs.buffers.size() && same; i++) {
    same &= lhs.buffers[i] == rhs.buffers[i];
  }
  for (size_t i = 0; i < lhs.binding_descriptions.size() && same; i++) {
    same &= memcmp(&lhs.binding_descriptions[i], &rhs.binding_descriptions[i],
                   sizeof(VkVertexInputBindingDescription)) == 0;
  }
  for (size_t i = 0; i < lhs.attribute_descriptions.size() && same; i++) {
    same &=
        memcmp(&lhs.attribute_descriptions[i], &rhs.attribute_descriptions[i],
               sizeof(VkVertexInputAttributeDescription)) == 0;
  }
  return same;
}

typedef unsigned char stbi_uc;
// Used to store image data until it is first used and we can infer the required
// format. Before first use, data is valid and image_gpu is nullptr. After first
// use, data is nullptr and image_gpu is valid.
struct ImageData {
  stbi_uc* data{nullptr};
  VkExtent3D extent;
  uint64_t size;
  VkImageUsageFlagBits usage;

  ImageH image_gpu;

  ImageData() {}
  ImageData(stbi_uc* data, VkExtent3D extent, uint64_t size,
            VkImageUsageFlagBits usage)
      : data(data), extent(extent), size(size), usage(usage) {}
  ImageData(ImageH image_gpu) : image_gpu(image_gpu) {}

  ImageData(const ImageData& o) = delete;
  ImageData(ImageData&& o);
  ImageData& operator=(const ImageData& o) = delete;
  ImageData& operator=(ImageData&& o);

  ~ImageData();

  // Transfer data from CPU to GPU with the correct format
  ImageH bakeFormat(VkFormat format);
};

struct Texture {
  std::shared_ptr<ImageData> image_data;
  SamplerH sampler;

  Texture() {}
  Texture(std::shared_ptr<ImageData> image_data, SamplerH sampler)
      : image_data(image_data), sampler(sampler) {}
};

enum PlaceholderTexture { NONE, WHITE, NORMAL_FLAT, BLACK, TRANSPARENT };

}  // namespace Uvelon