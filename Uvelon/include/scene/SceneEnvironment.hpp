#pragma once
#include <string>

#include "rendering/Primitives.hpp"

namespace Uvelon {

class SceneEnvironment {
 public:
  SceneEnvironment(const std::string& folder_path);

  EnvironmentH getEnvironment() const { return environment_; }

 private:
  EnvironmentH environment_;

  SceneEnvironment() {}
};

}  // namespace Uvelon