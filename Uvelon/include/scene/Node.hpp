#pragma once
#include <memory>
#include <string>
#include <vector>

#include "Camera.hpp"
#include "GaussianSplatsNode.hpp"
#include "Light.hpp"
#include "Mesh.hpp"
#include "Scene.hpp"

#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#define GLM_FORCE_LEFT_HANDED
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/glm.hpp>
#include <glm/gtx/quaternion.hpp>

namespace Uvelon {

struct RenderObject;

class Transform {
 public:
  void setTranslation(const glm::vec3& translation);
  void setRotation(const glm::quat& rotation);
  void setScale(const glm::vec3& scale);

  void setDirty() {
    dirty_ = true;
  }  // Force dirty flag (e.g. after attaching to new parent node)
  bool isDirty() const { return dirty_; }

  const glm::vec3& getTranslation() const { return translation_; }
  const glm::quat& getRotation() const { return rotation_; }
  const glm::vec3& getScale() const { return scale_; }

  const glm::mat4& getModelMatrix() const;

  void updateModelMatrix();
  void updateModelMatrix(const glm::mat4& parent_model_matrix);

 private:
  bool dirty_{true};

  glm::vec3 translation_{0.0f, 0.0f, 0.0f};
  glm::quat rotation_{1.0f, 0.0f, 0.0f, 0.0f};  // wxyz
  glm::vec3 scale_{1.0f, 1.0f, 1.0f};

  glm::mat4 model_matrix_;  // Global

  glm::mat4 calculateLocalMatrix();
};

class Node : public std::enable_shared_from_this<Node> {
  friend class Scene;

 public:
  Node();
  Node(const std::string& name);
  Node(const std::string& name, std::shared_ptr<Mesh> mesh);
  Node(const std::string& name, std::shared_ptr<Camera> camera);
  Node(const std::string& name, std::shared_ptr<Light> light);

  static void attach(std::shared_ptr<Node> parent, std::shared_ptr<Node> child);

  void detachChild(std::shared_ptr<Node> child);

  void update(bool force = false);
  void addRenderObjects(std::vector<RenderObject>& data);
  bool getActiveCamera(std::shared_ptr<Node>& camera_parent);
  void addCameraNodes(std::vector<std::shared_ptr<Node>>& camera_nodes);

  const std::string& getName() const { return name_; }
  Transform& getTransform() { return transform_; }

  std::shared_ptr<Node> getParent() { return parent_.lock(); }
  std::vector<std::shared_ptr<Node>>& getChildren() { return children_; }

  void setMesh(std::shared_ptr<Mesh> mesh) { mesh_ = mesh; }
  void setCamera(std::shared_ptr<Camera> camera) {
    camera_ = camera;
    if (camera_) camera_->setParent(shared_from_this());
  }
  void setGaussianSplats(std::shared_ptr<GaussianSplatsNode> splats) {
    splats_ = splats;
  }
  void setLight(std::shared_ptr<Light> light) {
    light_ = light;
    if (light_) light_->setParent(shared_from_this());
  }

  std::shared_ptr<Mesh> getMesh() { return mesh_; }
  std::shared_ptr<Camera> getCamera() { return camera_; }
  std::shared_ptr<GaussianSplatsNode> getGaussianSplats() { return splats_; }
  std::shared_ptr<Light> getLight() { return light_; }

 private:
  std::string name_;
  std::weak_ptr<Node> parent_;
  std::vector<std::shared_ptr<Node>> children_;

  Transform transform_;

  std::shared_ptr<Mesh> mesh_;
  std::shared_ptr<Camera> camera_;
  std::shared_ptr<GaussianSplatsNode> splats_;
  std::shared_ptr<Light> light_;
};

}  // namespace Uvelon