#pragma once

#include <filesystem>
#include <memory>
#include <string>

#include "rendering/Primitives.hpp"

namespace Uvelon {

class Node;
class MappedFile;
class Scene;

class GSplatLoader {
 public:
  GSplatLoader(const std::string& path);

  std::unique_ptr<Scene> load();

 private:
  std::shared_ptr<MappedFile> file_;
  std::filesystem::path fs_path_;

  GaussianSplats gs_;

  VkPushConstantRange push_constant_;

  bool readHeader();

  bool loadProxyMesh();
  void createVmemResources();

  void loadFullDataClassic();

  void createGaussianMaterial();
  void createDepthSortResources();
};

}  // namespace Uvelon