#pragma once

#include "rendering/Primitives.hpp"

namespace Uvelon {

class Node;

class Light {
 public:
  enum Type { NONE, DIR, POINT, SPOT };

  glm::vec3 translation{0.0f, 0.0f, 0.0f};
  glm::vec3 rotation{0.0f, 0.0f, 0.0f};
  glm::vec3 color{1.0f, 1.0f, 1.0f};
  Type type{POINT};
  float intensity{1.0f};
  bool shadows{true};

  Light();

  void setParent(std::shared_ptr<Node> parent) {
    parent_ = std::weak_ptr(parent);
  }

  float getSpotInnerConeAngle() const { return spot_inner_cone_angle_; }
  float getSpotOuterConeAngle() const { return spot_outer_cone_angle_; }
  void setSpotConeAngles(float inner, float outer);

  float getSpotAngleScale() const { return spot_angle_scale_; }
  float getSpotAngleOffset() const { return spot_angle_offset_; }

  LightPrimitive constructPrimitive();

 private:
  std::weak_ptr<Node> parent_;

  ImageH shadow_map_;
  Type shadow_map_type_{NONE};

  float spot_inner_cone_angle_{0.0f};
  float spot_outer_cone_angle_{3.1415f / 4.0f};
  float spot_angle_scale_;
  float spot_angle_offset_;
};

}  // namespace Uvelon