#pragma once

#include <memory>

#include "rendering/Primitives.hpp"

namespace Uvelon {

class Node;

class Mesh {
 public:
  Mesh(const std::string& name, std::vector<MeshPrimitiveH> primitives)
      : name_(name), render_primitives_(primitives) {}

  std::vector<MeshPrimitiveH>& getPrimitives() { return render_primitives_; }

 private:
  std::string name_;
  std::vector<MeshPrimitiveH> render_primitives_;
};

}  // namespace Uvelon