#pragma once

#include <memory>

#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#define GLM_FORCE_LEFT_HANDED
#include <glm/glm.hpp>

#define UVELON_UP glm::vec3(0.0f, 1.0f, 0.0f)

namespace Uvelon {

class Node;

class Camera {
 public:
  enum CameraType { PERSPECTIVE, ORTHOGRAPHIC };

  CameraType getType() const { return type_; }
  const glm::mat4 computeViewMatrix() const;
  const glm::mat4& getProjectionMatrix() const { return projection_matrix_; };

  virtual void computeProjection() = 0;

  void lookAt(const glm::vec3& eye, const glm::vec3& center);
  void setParent(std::shared_ptr<Node> parent) {
    parent_ = std::weak_ptr(parent);
  }

 protected:
  Camera(CameraType type);

  glm::mat4 projection_matrix_;

 private:
  CameraType type_;
  std::weak_ptr<Node> parent_;
};

class PerspectiveCamera : public Camera {
 public:
  PerspectiveCamera(float aspect_ratio, float yfov, float znear);
  PerspectiveCamera(float aspect_ratio, float yfov, float znear, float zfar);

  virtual void computeProjection();

  float getAspectRatio() const { return aspect_ratio_; }
  float getYFOV() const { return yfov_; }
  float getZNear() const { return znear_; }
  float getZFar() const { return zfar_; }

  float* modifyAspectRatio() { return &aspect_ratio_; }
  float* modifyYFOV() { return &yfov_; }
  float* modifyZNear() { return &znear_; }
  float* modifyZFar() { return &zfar_; }

 private:
  float aspect_ratio_;
  float yfov_;
  float znear_;
  float zfar_;
};

class OrthographicCamera : public Camera {
 public:
  OrthographicCamera(float xmag, float ymag, float znear, float zfar);

  virtual void computeProjection();

  float getXMag() const { return xmag_; }
  float getYMag() const { return ymag_; }
  float getZNear() const { return znear_; }
  float getZFar() const { return zfar_; }

  float* modifyXMag() { return &xmag_; }
  float* modifyYMag() { return &ymag_; }
  float* modifyZNear() { return &znear_; }
  float* modifyZFar() { return &zfar_; }

 private:
  float xmag_;
  float ymag_;
  float znear_;
  float zfar_;
};

}  // namespace Uvelon