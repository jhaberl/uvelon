#pragma once
#include <memory>
#include <vector>

#include "core/BufferCPU.hpp"
#include "scene/Loader.hpp"

namespace Uvelon {

class Node;

class Animation {
  friend class Loader;

 public:
  struct Sampler {
    enum Interpolation { LINEAR, STEP, CUBICSPLINE };

    BufferCPU input;  // Keyframe timestamps [s], always scalar float
    Interpolation interpolation;
    BufferCPU output;
    int type;            // Equivalent to number of elements (up to 4)
    int component_type;  // Currently only support float (5126)

    Sampler(BufferCPU&& input, Interpolation interpolation, BufferCPU&& output,
            int type, int component_type)
        : input(std::move(input)),
          interpolation(interpolation),
          output(std::move(output)),
          type(type),
          component_type(component_type) {}
  };
  struct Channel {
    enum Path { TRANSLATION, ROTATION, SCALE, WEIGHTS };

    int sampler_index;
    std::weak_ptr<Node> target_node;
    Path target_path;

    Channel(int sampler_index, std::weak_ptr<Node> target_node,
            Path target_path)
        : sampler_index(sampler_index),
          target_node(target_node),
          target_path(target_path) {}
  };

  Animation() {}
  Animation(std::string name, std::vector<Sampler>&& samplers,
            std::vector<Channel>&& channels)
      : name_(name),
        samplers_(std::move(samplers)),
        channels_(std::move(channels)) {}

  const std::string& getName() const { return name_; }

  void update(float delta);

  void setActive(bool active) { active_ = active; }
  bool& getActive() { return active_; }

  void setSpeed(float speed) { speed_ = std::max(speed, 0.0f); }
  float& getSpeed() { return speed_; }

  void setTime(float time) { time_ = std::max(time, 0.0f); }
  float& getTime() { return time_; }
  float getLength();

  void setLoop(float loop) { loop_ = loop; }
  bool& getLoop() { return loop_; }

 private:
  std::string name_;
  std::vector<Sampler> samplers_;
  std::vector<Channel> channels_;

  bool active_{false};
  float time_{0.0f};
  float speed_{1.0f};
  bool loop_{false};

  void checkForEnd();
  void updateChannel(Channel& channel, std::shared_ptr<Node> target_node);
};

}  // namespace Uvelon