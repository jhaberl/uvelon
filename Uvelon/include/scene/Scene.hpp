#pragma once
#include <memory>
#include <vector>

namespace Uvelon {

class Node;
struct RenderObject;
class Animation;
struct LightPrimitive;
class SceneEnvironment;

class Scene {
  friend class Application;

 public:
  Scene();
  Scene(const std::shared_ptr<Node> root);
  Scene(const std::shared_ptr<Node> root,
        std::vector<std::shared_ptr<Animation>> animations);
  Scene(const std::shared_ptr<Node> root,
        std::vector<std::shared_ptr<Animation>> animations,
        std::shared_ptr<SceneEnvironment> environment);

  const std::shared_ptr<Node> getRoot() const { return root_; }
  std::vector<std::shared_ptr<Animation>>& getAnimations() {
    return animations_;
  }

  bool getActiveCamera(std::shared_ptr<Node>& camera_node) const;
  void setActiveCamera(std::shared_ptr<Node> camera) {
    active_camera_ = std::weak_ptr<Node>(camera);
  }
  void addCameraNodes(std::vector<std::shared_ptr<Node>>& camera_nodes,
                      std::shared_ptr<Node> node = nullptr) const;
  void addLightNodes(std::vector<std::shared_ptr<Node>>& light_nodes,
                     std::shared_ptr<Node> node = nullptr) const;
  void setEnvironment(std::shared_ptr<SceneEnvironment> environment) {
    environment_ = environment;
  }
  const std::shared_ptr<SceneEnvironment> getEnvironment() const {
    return environment_;
  }

 private:
  const std::shared_ptr<Node> root_;
  std::weak_ptr<Node> active_camera_;
  std::vector<std::shared_ptr<Animation>> animations_;
  std::shared_ptr<SceneEnvironment> environment_;

  void preUpdate(float delta);
  void update(float delta);
  void addRenderObjects(std::vector<RenderObject>& render_objects,
                        std::shared_ptr<Node> node = nullptr) const;
  void addLightPrimitives(std::vector<LightPrimitive>& light_primitives,
                          std::shared_ptr<Node> node = nullptr) const;
};

}  // namespace Uvelon