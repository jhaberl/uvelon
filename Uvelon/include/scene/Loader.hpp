#pragma once

#include <functional>
#include <memory>

#include "json.hpp"
#include "rendering/Primitives.hpp"
#include "scene/LoaderPrimitives.hpp"
#include "utils/MappedFile.hpp"

namespace Uvelon {

class Scene;
class Node;
class Mesh;
class Camera;
class Animation;
class BufferCPU;
class Light;

/***
 * Currently only loads GLTF files
 */
class Loader {
 public:
  // Optional functions to handle extras
  std::function<void(
      nlohmann::json, MeshPrimitive&, std::vector<std::shared_ptr<Accessor>>,
      std::vector<std::shared_ptr<BufferView>>,
      std::vector<std::shared_ptr<Buffer>>, std::vector<MaterialH>)>
      ef_mesh_primitive;

  Loader(const std::string& path);

  std::vector<std::string> queryScenes(int& default_scene);

  // Limitations:
  // - Error handling could be a lot better
  // - There are lots of unsupported parts of the spec. In the best case we log
  //    a warning. In the worst case we crash...
  //    Known unsupported:
  //    - Meshes without all of index/position/normal/uv (TODO)
  //    - Meshes without materials (TODO)
  //    - Meshes with topology (primitive mode) other than triangle list
  //    - Skinning
  //    - Morph targets
  // - This currently does not support loading asynchronously
  // - Camera loading is currently untested (TODO)
  std::shared_ptr<Scene> load(int scene_id = -1);

 private:
  bool glb_;
  std::shared_ptr<MappedFile> file_;
  std::filesystem::path fs_path_;

  uint32_t glb_json_offset_, glb_json_length_;
  uint32_t glb_buffer_offset_, glb_buffer_length_;

  nlohmann::json getJson();

  std::vector<std::shared_ptr<Buffer>> loadBuffers(
      const nlohmann::json& gltf_buffers);
  std::vector<std::shared_ptr<BufferView>> loadBufferViews(
      const nlohmann::json& gltf_views,
      std::vector<std::shared_ptr<Buffer>>& buffers);
  std::vector<std::shared_ptr<Accessor>> loadAccessors(
      const nlohmann::json& gltf_accessors,
      std::vector<std::shared_ptr<BufferView>>& views);

  std::vector<SamplerH> loadSamplers(const nlohmann::json& gltf_samplers);
  void addPlaceholderSampler(std::vector<SamplerH>& samplers);
  std::vector<std::shared_ptr<ImageData>> loadImages(
      const nlohmann::json& gltf_images,
      std::vector<std::shared_ptr<BufferView>>& views);
  std::vector<Texture> loadTextures(const nlohmann::json& gltf_textures,
                                    std::vector<SamplerH>& samplers,
                                    std::vector<std::shared_ptr<ImageData>>& images);
  void addPlaceholderTextures(std::vector<Texture>& textures);
  std::vector<MaterialH> loadMaterials(const nlohmann::json& gltf_materials,
                                       std::vector<Texture>& textures);

  std::vector<std::shared_ptr<Mesh>> loadMeshes(
      const nlohmann::json& gltf_meshes,
      std::vector<std::shared_ptr<Accessor>>& accessors,
      std::vector<std::shared_ptr<BufferView>>& buffer_views,
      std::vector<std::shared_ptr<Buffer>>& buffers,
      std::vector<MaterialH>& materials);

  std::vector<std::shared_ptr<Camera>> loadCameras(
      const nlohmann::json& gltf_cameras);

  std::vector<std::shared_ptr<Light>> loadLights(const nlohmann::json& gltf);
  std::vector<std::shared_ptr<Animation>> loadAnimations(
      const nlohmann::json& gltf,
      std::vector<std::shared_ptr<Accessor>>& accessors,
      std::vector<std::shared_ptr<Node>>& nodes);

  std::vector<std::shared_ptr<Node>> loadNodes(
      const nlohmann::json& gltf_nodes,
      const std::vector<std::shared_ptr<Mesh>>& meshes,
      const std::vector<std::shared_ptr<Camera>>& cameras,
      const std::vector<std::shared_ptr<Light>>& lights);
  std::shared_ptr<Node> loadSceneRoot(
      const nlohmann::json& gltf, const std::string& path,
      const std::vector<std::shared_ptr<Node>>& nodes, int& scene);
  void fixWindingOrder(std::shared_ptr<Node> node, bool update = true);
};

}  // namespace Uvelon