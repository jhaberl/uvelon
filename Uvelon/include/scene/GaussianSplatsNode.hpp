#pragma once

#include "rendering/Primitives.hpp"

namespace Uvelon {

class GaussianSplatsNode {
 public:
  GaussianSplatsNode(GaussianSplatsH gaussian_splats)
      : gaussian_splats_(gaussian_splats) {}

  GaussianSplatsH getHandle() { return gaussian_splats_; }

 private:
  GaussianSplatsH gaussian_splats_;  // Only stores the primitive used to render
};

}  // namespace Uvelon