#pragma once

#include <stdexcept>
#include <vector>

namespace Uvelon {

template <typename T>
struct RenderDataHandle;
struct AllocatedBuffer;
typedef RenderDataHandle<AllocatedBuffer> BufferH;
typedef uint32_t VkBufferUsageFlags;

class BufferCPU {
 public:
  std::vector<char> raw_buffer;
  int component_type;

  BufferCPU() : raw_buffer(0) {}
  BufferCPU(size_t size_bytes) : raw_buffer(size_bytes) {}
  BufferCPU(int component_type, size_t size_bytes)
      : raw_buffer(size_bytes), component_type(component_type) {}
  ~BufferCPU() {}
  BufferCPU(const BufferCPU& o);
  BufferCPU(BufferCPU&& o)
      : raw_buffer(std::move(o.raw_buffer)), component_type(o.component_type) {}

  BufferCPU& operator=(BufferCPU& o) = delete;
  inline BufferCPU& operator=(BufferCPU&& o) {
    raw_buffer = std::move(o.raw_buffer);
    component_type = o.component_type;
    return *this;
  }

  template <typename T>
  inline T* data() {
    return reinterpret_cast<T*>(raw_buffer.data());
  }

  template <typename T>
  inline void resize(size_t size) {
    return raw_buffer.resize(size * sizeof(T));
  }

  template <typename T>
  inline size_t size() {
    return raw_buffer.size() / sizeof(T);
  }

  inline bool empty() { return raw_buffer.empty(); }

  template <typename T>
  inline T& at(const size_t index) {
    if ((index + 1) * sizeof(T) > raw_buffer.size())
      throw std::out_of_range("Invalid index for CPU buffer");
    return *(reinterpret_cast<T*>(raw_buffer.data()) + index);
  }

  BufferH toGPU(VkBufferUsageFlags usage);
};

}  // namespace Uvelon