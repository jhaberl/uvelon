#pragma once

#define UVELON_VERSION_MAJOR 0
#define UVELON_VERSION_MINOR 0
#define UVELON_VERSION_REVISION 1

#include <memory>
#include <string>

#include "rendering/Renderer.hpp"
#include "rendering/Window.hpp"

namespace Uvelon {

class Scene;
class Node;

class Application {
 public:
  struct Config {
    std::string title{"Uvelon"};
    int width{900}, height{600};
    bool maximized{false}, fullscreen{false};
    Renderer::InitConfig renderer_config;
  };

  Application(Config& config);
  virtual ~Application();

  virtual void run() final;

  virtual void init() = 0;
  virtual void update(float delta) = 0;
  virtual void drawUI(float delta) = 0;
  virtual void resize(int width, int height){};

 protected:
  Config& config_;
  Window window_;

  std::shared_ptr<Scene> scene_;
};

}  // namespace Uvelon