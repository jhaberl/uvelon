#pragma once
#include <SDL3/SDL_events.h>

#include <vector>

namespace Uvelon {

class Input {
 public:
  static void startFrame();
  static void handleEvent(SDL_Event& event, bool ignore_mouse = false,
                          bool ignore_keyboard = false);

  static bool isKeyDown(SDL_Scancode code);
  static void getMousePosition(float& x, float& y);
  static bool isMouseButtonDown(uint8_t button);
  static void getScroll(float& x, float& y);

 private:
  static std::vector<SDL_Scancode> down_keys_;
  static float mouse_x_, mouse_y_;
  static std::vector<uint8_t> down_mouse_buttons_;
  static float scroll_x_, scroll_y_;
};

}  // namespace Uvelon