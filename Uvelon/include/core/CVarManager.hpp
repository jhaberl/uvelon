#pragma once

#include <string>

namespace Uvelon {

struct CVarInternal;

struct CVar {
  CVar(CVarInternal* var) : var_(var) {}
  bool isValid();
  std::string getName();
  std::string getDescription();
  template <typename T>
  T& getValue();

 private:
  CVarInternal* var_;
};

class CVarManager {
 public:
  template <typename T>
  static void setVar(std::string name, T value, bool allow_draw = true,
                     std::string description = "");
  static CVar getVar(std::string name);
  template <typename T>
  static CVar getVar(std::string name, T default_value);
  static void draw();

 protected:
  virtual void setVarInternal(CVarInternal&& var) = 0;
  virtual CVar getVarInternal(std::string name) = 0;
  virtual void drawInternal() = 0;

 private:
  static CVarManager& getInstance();
};

}  // namespace Uvelon