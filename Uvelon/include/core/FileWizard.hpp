#pragma once

#include <filesystem>

#ifdef __APPLE__
#include "TargetConditionals.h"
#endif

#if !defined(__APPLE__) || !TARGET_OS_IPHONE
// Is not iOS
#define HAVE_NFD
#endif

#define UVELON_FILE_RESOURCES(relative_path)   \
  ::Uvelon::FileWizard::getInstance().getPath( \
      ::Uvelon::FileWizard::StorageType::RESOURCES, relative_path)
#define UVELON_FILE_WORKSPACE(relative_path)   \
  ::Uvelon::FileWizard::getInstance().getPath( \
      ::Uvelon::FileWizard::StorageType::WORKSPACE, relative_path)
#define UVELON_FILE_FILESYSTEM(relative_path)  \
  ::Uvelon::FileWizard::getInstance().getPath( \
      ::Uvelon::FileWizard::StorageType::FILESYSTEM, relative_path)

namespace Uvelon {

class FileWizard {
 public:
  enum StorageType { RESOURCES, WORKSPACE, FILESYSTEM };

  struct FilterItem {
    const char* name;
    const char* extension;
  };

  static FileWizard& getInstance() {
    static FileWizard instance;
    return instance;
  }

  std::filesystem::path getPath(StorageType storage_type,
                                const std::string& relative_path);

  bool pickFolder(std::filesystem::path& result,
                  const std::string& default_path);

  bool openDialog(std::filesystem::path& result, const FilterItem* filter,
                  int filter_count, const std::string& default_path);

  bool saveDialog(std::filesystem::path& result, const FilterItem* filter,
                  int filter_count, const std::string& name,
                  const std::string& default_path);

 private:
  FileWizard();
  ~FileWizard();

  std::filesystem::path apple_resources_path_;
  std::filesystem::path apple_workspace_path_;
};

}  // namespace Uvelon