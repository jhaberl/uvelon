#pragma once

#include <SDL3/SDL.h>
#include <vulkan/vulkan.h>

#include <memory>
#include <string>
#include <thread>

struct SDL_Window;
struct SDL_WindowDeleter {
  void operator()(SDL_Window* window);
};

namespace Uvelon {

class Window {
 public:
  Window(const std::string& title, int width, int height,
         bool maximized = false, bool fullscreen = false);
  virtual ~Window();

  void open();
  void handleEvent(SDL_Event& event);

  void createSurface(VkInstance instance);

  VkSurfaceKHR getSurface() const { return surface_; }
  SDL_Window* getWindow() const { return sdl_window_.get(); }
  int getWidth() const { return width_; }
  int getHeight() const { return height_; }

 private:
  std::string title_;
  int width_;
  int height_;
  bool opened_{false};

  // Initial settings, not updated
  bool maximized_;
  bool fullscreen_;

  std::unique_ptr<SDL_Window, SDL_WindowDeleter> sdl_window_;
  VkSurfaceKHR surface_;
};

}  // namespace Uvelon