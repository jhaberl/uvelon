#pragma once

#include <vulkan/vulkan.h>

#include <vector>

VK_DEFINE_HANDLE(VmaAllocation)

namespace Uvelon {

class Image {
 public:
  Image(VkExtent3D extent, VkFormat format, VkImageUsageFlags usage,
        uint32_t array_layers = 1, uint32_t mip_levels = 1,
        bool cubemap = false);
  Image(void* data, uint64_t size, VkExtent3D extent, VkFormat format,
        VkImageUsageFlags usage, uint32_t array_layers, uint32_t mip_levels,
        std::vector<VkBufferImageCopy> regions, bool cubemap = false,
        bool generate_mips = false);
  Image(void* data, uint64_t size, VkExtent3D extent, VkFormat format,
        VkImageUsageFlags usage, bool generate_mips = false);

  Image(const Image&& o);
  Image operator=(const Image&& o);

  ~Image();
  void destroy();

  VkImage getVkImage() { return image_; }
  VkImageView getVkImageView() { return view_; }
  VkFormat getFormat() { return format_; }
  VkExtent3D getExtent() { return extent_; }
  uint32_t getMipLevels() { return mip_levels_; }
  uint32_t getArrayLayers() { return array_layers_; }

  void transition(VkCommandBuffer cmd, VkPipelineStageFlags2 src_stage,
                  VkAccessFlags2 src_access, VkPipelineStageFlags2 dst_stage,
                  VkAccessFlags2 dst_access, VkImageLayout old_layout,
                  VkImageLayout new_layout);

  static void transitionVkImage(
      VkCommandBuffer cmd, VkImage image, VkPipelineStageFlags2 src_stage,
      VkAccessFlags2 src_access, VkPipelineStageFlags2 dst_stage,
      VkAccessFlags2 dst_access, VkImageLayout old_layout,
      VkImageLayout new_layout, VkImageSubresourceRange subresource);

 private:
  VkImage image_;
  VkImageView view_;
  VmaAllocation allocation_;
  VkExtent3D extent_;
  VkFormat format_;
  uint32_t array_layers_;
  uint32_t mip_levels_;
  bool cubemap_;

  mutable bool destroyed_{false};

  bool isDepthImage();

  uint32_t calculateMaxMipLevels(VkExtent3D extent);

  static std::vector<VkBufferImageCopy> defaultStagingCopy(VkExtent3D extent);
};

}  // namespace Uvelon