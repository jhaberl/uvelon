#pragma once

#include "rendering/Primitives.hpp"

#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#define GLM_FORCE_LEFT_HANDED
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

namespace Uvelon {

class GBuffer {
 public:
  void init();
  void recreate() { init(); }

  void retrieveFormats(VkFormat*& color_formats, uint32_t& count,
                       VkFormat& depth_format);

  void beginRenderTo(VkCommandBuffer cmd);
  void endRenderTo(VkCommandBuffer cmd);
  void render(VkCommandBuffer cmd, DescriptorSetH global_set,
              std::vector<LightPrimitive>& lights, EnvironmentH environment);

  void prepareSSR(VkCommandBuffer cmd);

  ImageH getLinearFrame() { return linear_frame_; }
  ImageH getLinearFrameSSR() { return linear_frame_ssr_; }

  void destroy();
  ~GBuffer();

 private:
  SamplerH sampler_;
  ImageH base_color_;
  ImageH metal_rough_;
  ImageH normal_;
  ImageH emissive_;
  ImageH depth_;

  // IBL
  EnvironmentH last_environment_;
  SamplerH ibl_sampler_;
  ImageH ibl_placeholder_cube_, ibl_placeholder_2d_;

  // Shadow maps
  SamplerH shadow_map_sampler_;

  // SSR
  ImageH linear_frame_;
  // Last frame in linear colorspace + blurred
  ImageH linear_frame_ssr_, linear_frame_ssr_blurred_;
  bool linear_frame_ssr_valid_;
  glm::mat4 last_frame_view_proj_;
  SamplerH ssr_sampler_;
  MaterialH ssr_compute_;

#pragma pack(push, 1)
  struct GBufferData {
    glm::mat4 last_frame_view_proj;
    float shadow_map_limit;
    float shadow_map_far;
  };
#pragma pack(pop)
  BufferH gb_data_buffer_;

  std::vector<VkFormat> formats_color_;
  VkFormat format_depth_;
  std::vector<VkRenderingAttachmentInfo> attachment_infos_color_;
  VkRenderingAttachmentInfo attachment_info_depth_;

  MaterialH material_;

  BufferH light_buffer_;

  void updateEnvironment(EnvironmentH environment);
};

}  // namespace Uvelon