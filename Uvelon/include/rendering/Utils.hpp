#pragma once
#include <deque>
#include <functional>
#include <string>

#include "utils/Logger.hpp"

#define VK_CHECK(x)                                             \
  do {                                                          \
    VkResult err = x;                                           \
    if (err) {                                                  \
      UVELON_LOG_ERROR(std::string("Detected Vulkan error: ") + \
                       std::to_string(err));                    \
      abort();                                                  \
    }                                                           \
  } while (0)

namespace Uvelon {

struct ShutdownQueue {
  std::deque<std::function<void()>> functions;

  void pushFunction(std::function<void()>&& function);
  void performShutdown();
};

}  // namespace Uvelon