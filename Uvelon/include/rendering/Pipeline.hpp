#pragma once
#include <vulkan/vulkan.h>

#include <shaderc/shaderc.hpp>
#include <vector>

#include "rendering/Primitives.hpp"

namespace Uvelon {

class ShaderIncludeHandler : public shaderc::CompileOptions::IncluderInterface {
 public:
  shaderc_include_result* GetInclude(const char* requested_source,
                                     shaderc_include_type type,
                                     const char* requesting_source,
                                     size_t include_depth) override;

  void ReleaseInclude(shaderc_include_result* data) override;

 private:
  struct Info {
    std::string source_name;
    std::vector<char> content;
  };
};

class ShaderCache {
 public:
  ~ShaderCache();
  void destroy();

  VkShaderModule createShaderModule(const PipelineData::Shader& shader);

 private:
  shaderc::Compiler shader_compiler_;

  std::vector<std::pair<PipelineData::Shader, VkShaderModule>> shader_modules_;

  std::vector<char> compileShader(const PipelineData::Shader& shader);
};

class PipelineBuilder {
 public:
  static PipelineBuilder beginGraphics() { return {}; }
  static PipelineBuilder beginCompute() {
    PipelineBuilder result;
    result.pipeline_data_.compute = true;
    return result;
  }
  // Create new pipeline from existing. Cannot recreate layout at this point
  static PipelineBuilder fromExisting(Pipeline& existing);

  PipelineH build();

  // Set the color and depth attachment formats. Defaults to those of the
  // g-buffer, if not modified
  PipelineBuilder& setAttachmentFormats(VkFormat* color_formats, uint32_t count,
                                        VkFormat depth_format,
                                        uint32_t view_mask = 0);

  // Set the vertex input info descriptions. Defaults to MeshPrimitive's if not
  // modified
  PipelineBuilder& setVertexInputInfo(
      VkVertexInputBindingDescription* binding_descriptions,
      uint32_t binding_count,
      VkVertexInputAttributeDescription* attribute_descriptions,
      uint32_t attribute_count);
  PipelineBuilder& setTopology(VkPrimitiveTopology topology);
  PipelineBuilder& setFrontFace(VkFrontFace front_face);
  PipelineBuilder& addDynamicState(VkDynamicState dynamic_state);
  PipelineBuilder& setDepthTest(bool depth_test_enable,
                                bool depth_write_enable);

  PipelineBuilder& addShader(
      const std::string& path, VkShaderStageFlagBits stage,
      std::vector<std::pair<std::string, std::string>> macros = {});
  PipelineBuilder& addPushConstant(VkPushConstantRange push_constant);
  PipelineBuilder& addDescriptorSet(VkDescriptorSetLayout layout);

 private:
  std::vector<VkPipelineShaderStageCreateInfo> shader_stages_;

  bool from_existing_{false};
  Pipeline* existing_;

  PipelineData pipeline_data_;

  void createLayout(Pipeline& result);

  PipelineH buildGraphics();
  PipelineH buildCompute();
};

}  // namespace Uvelon