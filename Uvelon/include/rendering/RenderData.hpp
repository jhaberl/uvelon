#pragma once

#include <functional>
#include <list>
#include <memory>

#include "rendering/Primitives.hpp"

namespace Uvelon {

struct ShutdownQueue;
class Image;

template <typename T>
class DataStorage {
 public:
  DataStorage() = default;
  DataStorage(std::function<void(T&)>&& destructor);
  ~DataStorage();
  void destroy();

  void gc();

  RenderDataHandle<T> add(T&& t);

  void setDestructor(std::function<void(T&)>&& destructor);

 private:
  friend class RenderData;
  std::list<DataStorageContainer<T>> storage_;
  std::function<void(T&)> destructor_{[](T&) {}};
};

class RenderData {
 public:
  RenderData();
  ~RenderData();

  void destroy();

  RenderData operator=(const RenderData&&) = delete;
  RenderData(const RenderData&&) = delete;

  // Garbage collection... This entire setup is pretty sketchy, not a huge fan
  void gc();

  BufferH allocateBuffer(void* data, uint64_t size, VkBufferUsageFlags usage);
  BufferH allocateBuffer(uint64_t size, VkBufferUsageFlags usage,
                         bool mapped = false, bool map_cpu2gpu = true);
  BufferH addBuffer(AllocatedBuffer&& buffer);

  ImageH addImage(Image&& image);
  SamplerH createSampler(VkFilter mag_filter, VkFilter min_filter,
                         VkSamplerMipmapMode mipmap_mode,
                         VkSamplerAddressMode address_mode_u,
                         VkSamplerAddressMode address_mode_v,
                         bool unnormalized_coordinates = false,
                         bool compare = false);

  PipelineH addPipeline(Pipeline&& pipeline);
  MaterialH addMaterial(Material&& material);
  MeshPrimitiveH addMeshPrimitive(MeshPrimitive&& mesh_primitive);
  DescriptorSetH addDescriptorSet(DescriptorSet&& descriptor_set);
  GaussianSplatsH addGaussianSplats(GaussianSplats&& gaussian_splats);
  EnvironmentH addEnvironment(Environment&& environment);

  void hotReloadShaders();

 private:
  DataStorage<AllocatedBuffer> buffers_;
  DataStorage<VkSampler> samplers_;
  DataStorage<Image> images_;
  DataStorage<Pipeline> pipelines_;
  DataStorage<Material> materials_;
  DataStorage<MeshPrimitive> mesh_primitives_;
  DataStorage<DescriptorSet> descriptor_sets_;
  DataStorage<GaussianSplats> gaussian_splats_;
  DataStorage<Environment> environments_;
};

}  // namespace Uvelon