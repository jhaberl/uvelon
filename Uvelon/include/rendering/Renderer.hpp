#pragma once

#include <vulkan/vulkan.h>

#include <functional>
#include <memory>
#include <vector>

#include "rendering/Computer.hpp"
#include "rendering/Pipeline.hpp"
#include "rendering/Primitives.hpp"
#include "rendering/Swapchain.hpp"
#include "rendering/systems/RenderSystem.hpp"
#include "vk_mem_alloc.h"

#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#define GLM_ENABLE_EXPERIMENTAL
#define GLM_FORCE_LEFT_HANDED
#include <glm/glm.hpp>

namespace Uvelon {

class Window;
struct ShutdownQueue;
class RenderData;
struct RenderObject;
class DescriptorAllocator;
class ShaderCache;
class GBuffer;

#pragma pack(push, 1)
struct GlobalUniformBuffer {
  glm::mat4 view;
  glm::mat4 projection;
  glm::mat4 inv_view_projection;
  glm::vec4 camera_position;
  glm::vec2 window_size;
};
#pragma pack(pop)

struct DrawData {
  std::vector<RenderObject> render_objects;
  std::vector<LightPrimitive> light_primitives;
  EnvironmentH environment_ibl;
  glm::mat4 view_matrix{1.0f}, projection_matrix{1.0f};
};

class Renderer {
 public:
  struct InitConfig {
    Window* window{nullptr};  // Architecture could be better for this
    bool use_validation_layers{true};
  };

  static Renderer& getInstance();

  void init(Renderer::InitConfig init_config);
  void notifyWindowSizeChanged() { swapchain_->init(); }
  void draw(DrawData& data);
  void shutdown();

  void immediateSubmit(std::function<void(VkCommandBuffer cmd)> function);

  InitConfig& getInitConfig() { return init_config_; }
  VkInstance getVkInstance() const { return instance_; }
  VkPhysicalDevice getPhysicalDevice() const { return physical_device_; }
  VkDevice getDevice() const { return device_; }
  VmaAllocator& getAllocator() { return allocator_; }
  RenderData& getRenderData() const { return *render_data_; }
  Swapchain& getSwapchain() const { return *swapchain_; }
  DescriptorAllocator& getDescriptorAllocator() const {
    return *descriptor_allocator_;
  }
  ShaderCache& getShaderCache() const { return *shader_cache_; }
  DescriptorSetH getGlobalDescriptorSet() const { return global_ds_; }
  GBuffer& getGBuffer() const { return *g_buffer_; }
  Computer& getComputer() const { return *computer_; }
  Computer& getRenderComputer() const { return *render_computer_; }
  VkQueue getQueueGraphics() const { return graphics_queue_; }
  VkQueue getQueueCompute() const { return compute_queue_; }
  void getQueueFamilies(uint32_t& graphics, uint32_t& compute) const {
    graphics = graphics_queue_family_;
    compute = compute_queue_family_;
  }
  const GlobalUniformBuffer& getGlobalUbo() const { return global_ubo_; }
  BufferH getGlobalUboBuffer() const { return global_ubo_buffer_; }
  bool isIntegratedDevice() const { return integrated_device_; }
  RenderSystem<LightPrimitive>& getShadowMapSystem() {
    return *render_system_shadow_map_;
  }

  // Function pointers for extensions
  static PFN_vkCmdBeginRenderingKHR fp_vkCmdBeginRendering;
  static PFN_vkCmdEndRenderingKHR fp_vkCmdEndRendering;
  static PFN_vkCmdPipelineBarrier2KHR fp_vkCmdPipelineBarrier2;

 private:
  Renderer::InitConfig init_config_;
  bool initialized_{false};

  std::unique_ptr<ShutdownQueue> shutdown_queue_;

  VkInstance instance_;
  VkDebugUtilsMessengerEXT debug_messenger_;

  VkPhysicalDevice physical_device_;
  VkPhysicalDeviceProperties physical_device_properties_;
  VkDevice device_;
  VkQueue graphics_queue_, compute_queue_;
  uint32_t graphics_queue_family_, compute_queue_family_;
  bool integrated_device_{false};

  VmaAllocator allocator_;

  std::unique_ptr<Swapchain> swapchain_;
  std::unique_ptr<Computer> computer_;
  std::unique_ptr<Computer> render_computer_;
  std::unique_ptr<RenderData> render_data_;
  std::unique_ptr<DescriptorAllocator> descriptor_allocator_;
  std::unique_ptr<ShaderCache> shader_cache_;

  // Immediate submit
  struct ImmediateSubmitData {
    VkCommandPool cmd_pool;
    VkCommandBuffer cmd_buffer;
    VkFence fence;
  } immediate_submit_data;

  GlobalUniformBuffer global_ubo_;
  BufferH global_ubo_buffer_;
  DescriptorSetH global_ds_;

  std::unique_ptr<GBuffer> g_buffer_;

  // Render systems
  std::vector<std::unique_ptr<RenderSystem<RenderObject>>> render_systems_;
  std::unique_ptr<RenderSystem<LightPrimitive>> render_system_shadow_map_;

  Renderer() = default;

  void initVulkan();
  void initImmediateSubmit();
};

}  // namespace Uvelon