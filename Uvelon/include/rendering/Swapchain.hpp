#pragma once

#include <vulkan/vulkan.h>

#include <functional>
#include <memory>
#include <vector>

#include "rendering/Image.hpp"
#include "rendering/Primitives.hpp"

namespace Uvelon {

class Window;
struct ShutdownQueue;

constexpr int INFLIGHT_FRAMES = 2;

class Swapchain {
 public:
  Swapchain(VkDevice device, const Window& window,
            VkPhysicalDevice physical_device, VkQueue graphics_queue,
            uint32_t graphics_queue_family);

  void init();
  void recreate() { init(); }

  void initFrameData();

  ~Swapchain();
  void destroy();

  void waitForFrame();
  bool acquireNextImage();
  void submitPresent();

  void beginRenderTo(VkCommandBuffer cmd, ImageH linear_out = ImageH());
  void endRenderTo(VkCommandBuffer cmd);

  // No image transitions
  void stopRenderTo(VkCommandBuffer cmd);
  void resumeRenderTo(VkCommandBuffer cmd, ImageH linear_out = ImageH());

  void setRecreateCallback(std::function<void()> callback) {
    notify_recreated_ = callback;
  }

  bool isMinimized() { return minimized_; }
  VkExtent2D getExtent() { return extent_; }
  VkCommandBuffer getCommandBuffer() {
    return frames_[frame_index_].cmd_buffer;
  }
  VkImage getImage() { return images_[image_index_]; }
  VkFormat getImageFormat() { return image_format_; }
  VkImageView getImageView() { return views_[image_index_]; }
  uint32_t getNumFrames() const { return INFLIGHT_FRAMES; }

 private:
  VkDevice device_;
  const Window& window_;
  VkPhysicalDevice physical_device_;
  VkQueue graphics_queue_;
  uint32_t graphics_queue_family_;
  VkExtent2D extent_;

  bool minimized_{false};
  bool vsync_{true};

  std::function<void()> notify_recreated_;

  VkSwapchainKHR swapchain_{nullptr};
  std::vector<VkImage> images_;
  std::vector<VkImageView> views_;
  VkFormat image_format_;

  std::unique_ptr<ShutdownQueue> swapchain_shutdown_queue_;
  std::unique_ptr<ShutdownQueue> framedata_shutdown_queue_;

  struct FrameData {
    VkSemaphore sem_ready, sem_done;
    VkFence fence_render;

    VkCommandPool cmd_pool;
    VkCommandBuffer cmd_buffer;
  } frames_[INFLIGHT_FRAMES];

  uint32_t frame_index_{0}, image_index_{0};

  VkRenderingAttachmentInfo color_info_, color_linear_info_, color_resume_info_,
      color_linear_resume_info_;
};

}  // namespace Uvelon