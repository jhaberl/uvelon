#pragma once

#include <vulkan/vulkan.h>

#include <list>
#include <rendering/Primitives.hpp>
#include <unordered_map>
#include <vector>

/**
 * Mainly taken from
 * https://vkguide.dev/docs/extra-chapter/abstracting_descriptors/
 */

namespace Uvelon {

class DescriptorLayoutCache {
 public:
  struct DescriptorLayoutInfo {
    std::vector<VkDescriptorSetLayoutBinding> bindings;

    bool operator==(const DescriptorLayoutInfo& other) const;
    size_t hash() const;
  };

  VkDescriptorSetLayout create(VkDescriptorSetLayoutCreateInfo layout_info);

 private:
  struct DescriptorLayoutHash {
    size_t operator()(const DescriptorLayoutInfo& info) const {
      return info.hash();
    }
  };

  std::unordered_map<DescriptorLayoutInfo, VkDescriptorSetLayout,
                     DescriptorLayoutHash>
      cache_;
};

class DescriptorAllocator {
 public:
  struct DescriptorPoolSizes {
    std::vector<std::pair<VkDescriptorType, float>> sizes = {
        {VK_DESCRIPTOR_TYPE_SAMPLER, 0.5f},
        {VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 4.f},
        {VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE, 4.f},
        {VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, 1.f},
        {VK_DESCRIPTOR_TYPE_UNIFORM_TEXEL_BUFFER, 1.f},
        {VK_DESCRIPTOR_TYPE_STORAGE_TEXEL_BUFFER, 1.f},
        {VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 2.f},
        {VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, 2.f},
        {VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC, 1.f},
        {VK_DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC, 1.f},
        {VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT, 0.5f}};
  };

  ~DescriptorAllocator();
  void destroy();

  VkDescriptorPool allocate(VkDescriptorSet& set, VkDescriptorSetLayout layout);
  DescriptorLayoutCache& getLayoutCache() { return layout_cache_; }

 private:
  DescriptorLayoutCache layout_cache_;

  VkDescriptorPool current_pool_{VK_NULL_HANDLE};

  DescriptorPoolSizes sizes_;
  std::vector<VkDescriptorPool> available_pools_;
  std::vector<VkDescriptorPool> used_pools_;

  void invalidatePool();
};

class DescriptorUpdater {
 public:
  DescriptorUpdater& updateBuffer(uint32_t index, VkDescriptorType type,
                                  BufferH buffer, size_t offset, size_t range);
  DescriptorUpdater& updateSampler(uint32_t index, SamplerH sampler);
  DescriptorUpdater& updateImage(uint32_t index, ImageH image,
                                 VkImageLayout layout, bool storage = false);
  DescriptorUpdater& updateCombinedImageSampler(uint32_t index, ImageH image,
                                                VkImageLayout layout,
                                                SamplerH sampler);
  DescriptorUpdater& updateSparseImageArray(uint32_t index,
                                            uint32_t array_index, ImageH image,
                                            VkImageLayout layout);

  void commit(DescriptorSetH descriptor_set);

 private:
  std::vector<DescriptorSet::Resource> updated_resources_;

  std::list<VkDescriptorBufferInfo> buffer_infos_;
  std::list<VkDescriptorImageInfo> image_infos_;

  std::vector<VkWriteDescriptorSet> writes_;

  void updateResource(DescriptorSetH descriptor_set,
                      DescriptorSet::Resource resource);

  void addWrite(uint32_t index, VkDescriptorType descriptor_type,
                VkDescriptorBufferInfo* buffer_info,
                VkDescriptorImageInfo* image_info, uint32_t array_index = 0);
};

class DescriptorBuilder {
 public:
  static DescriptorBuilder begin() { return {}; }

  DescriptorBuilder& addBuffer(VkShaderStageFlags stage, VkDescriptorType type,
                               BufferH buffer, size_t offset, size_t range);
  DescriptorBuilder& addSampler(VkShaderStageFlags stage, SamplerH sampler);
  DescriptorBuilder& addImage(VkShaderStageFlags stage, ImageH image,
                              VkImageLayout layout, bool storage = false);
  DescriptorBuilder& addCombinedImageSampler(VkShaderStageFlags stage,
                                             ImageH image, VkImageLayout layout,
                                             SamplerH sampler);

  DescriptorBuilder& addSparseImageArray(VkShaderStageFlags stage, int count);

  DescriptorSetH build();

 private:
  DescriptorUpdater updater_;

  std::vector<VkDescriptorSetLayoutBinding> bindings_;
  std::vector<VkDescriptorBindingFlags> binding_flags_;

  uint32_t addBinding(VkDescriptorType descriptor_type,
                      VkShaderStageFlags stage, int count,
                      VkDescriptorBindingFlags flags = 0);
};

}  // namespace Uvelon