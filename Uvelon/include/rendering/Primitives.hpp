#pragma once

// TODO: Reduce the number of imports in here, more forward declarations and a
// cpp file probably
#include <vulkan/vulkan.h>

#include <array>
#include <atomic>
#include <memory>
#include <string>
#include <unordered_map>
#include <utility>
#include <vector>

#include "core/BufferCPU.hpp"
#include "rendering/Image.hpp"
#include "vk_mem_alloc.h"

#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#define GLM_FORCE_LEFT_HANDED
#include <glm/glm.hpp>

namespace Uvelon {

template <typename T>
struct DataStorageContainer {
  DataStorageContainer(const DataStorageContainer<T>&& other)
      : ref_count_(other.ref_count_.load()), t_(std::move(other.t_)) {}
  DataStorageContainer<T> operator=(const DataStorageContainer<T>& other) =
      delete;

  constexpr T& operator*() { return t_; }
  constexpr T* operator->() const { return &t_; }

 private:
  template <typename R>
  friend struct RenderDataHandle;
  template <typename R>
  friend class DataStorage;

  std::atomic_uint32_t ref_count_;
  T t_;

  DataStorageContainer(T&& t) : t_(std::move(t)) {}
};

template <typename T>
struct RenderDataHandle {
  RenderDataHandle() {}
  RenderDataHandle(const RenderDataHandle& other) noexcept
      : container_(other.container_) {
    if (container_) container_->ref_count_++;
  }
  RenderDataHandle(RenderDataHandle&& other) noexcept
      : container_(other.container_) {
    if (container_) container_->ref_count_++;
  }
  RenderDataHandle operator=(const RenderDataHandle& other) {
    if (container_) container_->ref_count_--;
    container_ = other.container_;
    if (container_) container_->ref_count_++;
    return *this;
  }
  RenderDataHandle& operator=(RenderDataHandle&& other) {
    if (container_) container_->ref_count_--;
    container_ = other.container_;
    if (container_) container_->ref_count_++;
    return *this;
  }

  constexpr T& operator*() { return container_->t_; }
  constexpr T* operator->() { return &container_->t_; }
  constexpr bool operator==(const RenderDataHandle<T>& other) const {
    return container_ == other.container_;
  }
  constexpr bool operator!=(const RenderDataHandle<T>& other) const {
    return !(operator==(other));
  }
  constexpr bool operator<(const RenderDataHandle<T>& other) const {
    return container_ < other.container_;
  }
  constexpr bool operator>(const RenderDataHandle<T>& other) const {
    return container_ > other.container_;
  }
  constexpr operator bool() const { return container_; }

  ~RenderDataHandle() {
    if (container_) {
      container_->ref_count_--;
      container_ = nullptr;
    }
  }

 private:
  template <typename R>
  friend class DataStorage;

  DataStorageContainer<T>* container_{nullptr};

  RenderDataHandle(DataStorageContainer<T>* container) : container_(container) {
    container_->ref_count_++;
  }
};

typedef RenderDataHandle<VkSampler> SamplerH;
typedef RenderDataHandle<Image> ImageH;

struct AllocatedBuffer {
  VkBuffer buffer;
  uint64_t size;
  VmaAllocation allocation;
  VmaAllocationInfo allocation_info;
};
typedef RenderDataHandle<AllocatedBuffer> BufferH;

struct PipelineData {
  struct Shader {
    std::string path;
    VkShaderStageFlagBits stage;
    std::vector<std::pair<std::string, std::string>> macros;

    bool operator==(const Shader& o) const {
      if (path != o.path || stage != o.stage ||
          macros.size() != o.macros.size())
        return false;
      for (auto& macro : macros) {
        if (std::find(o.macros.begin(), o.macros.end(), macro) ==
            o.macros.end())
          return false;
      }
      return true;
    }
  };

  bool compute{false};

  std::vector<Shader> shaders;

  bool format_set{false};
  std::vector<VkFormat> format_color;
  VkFormat format_depth;
  uint32_t view_mask{0};

  bool vertex_input_set{false};
  std::vector<VkVertexInputBindingDescription> vertex_input_bindings;
  std::vector<VkVertexInputAttributeDescription> vertex_input_attributes;

  VkPrimitiveTopology topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
  VkFrontFace front_face = VK_FRONT_FACE_COUNTER_CLOCKWISE;

  bool depth_test_enable{true}, depth_write_enable{true};

  std::vector<VkDynamicState> dynamic_states{VK_DYNAMIC_STATE_VIEWPORT,
                                             VK_DYNAMIC_STATE_SCISSOR};

  std::vector<VkPushConstantRange> push_constants;
  std::vector<VkDescriptorSetLayout> descriptor_sets;
};

struct Pipeline {
  VkPipelineLayout layout;
  VkPipeline pipeline;

  // Data necessary to recreate the pipeline
  PipelineData pipeline_data;
};
typedef RenderDataHandle<Pipeline> PipelineH;

struct DescriptorSet {
  VkDescriptorSet set;
  VkDescriptorSetLayout layout;

  VkDescriptorPool pool;

  struct Resource {
    uint32_t binding;
    uint32_t array_index;
    ImageH image;
    SamplerH sampler;
    BufferH buffer;
  };
  std::vector<Resource> resources;
};
typedef RenderDataHandle<DescriptorSet> DescriptorSetH;

struct Material {
  struct Factors {
    glm::vec4 base_color_factor{1.0f, 1.0f, 1.0f, 1.0f};
    glm::vec3 emissive_factor{0.0f, 0.0f, 0.0f};
    float metallic_factor{1.0f};
    float roughness_factor{1.0f};
  };

  DescriptorSetH descriptor_set;
  PipelineH pipeline;
};
typedef RenderDataHandle<Material> MaterialH;

struct MeshPrimitive {
  std::vector<BufferH> vertex_buffers;

  BufferH indices;
  uint32_t index_count;
  VkIndexType index_type;

  MaterialH material;

  std::vector<VkVertexInputBindingDescription> binding_description;
  std::vector<VkVertexInputAttributeDescription> attribute_description;

#ifdef __APPLE__
  // Requirements for VK_DYNAMIC_STATE_VERTEX_INPUT_BINDING_STRIDE are:
  // macOS 14.0 or iOS/tvOS 17.0, plus either Apple4 or Mac2 GPU. This is a
  // workaround applied to all Apple devices for now.
  MaterialH mat_sm_directional, mat_sm_cube;
#endif
};
typedef RenderDataHandle<MeshPrimitive> MeshPrimitiveH;

#pragma pack(push, 1)
struct GaussianSplatsSortData {
  uint32_t num_splats;
  uint32_t elements_per_wg;
  uint32_t page_size;
};
#pragma pack(pop)

#pragma pack(push, 1)
struct GaussianSplatsPC {
  glm::mat4 model_matrix;
  uint32_t iteration;
};
#pragma pack(pop)

#pragma pack(push, 1)
struct GaussianSliceExtra {
  uint32_t width, height;
  float bounds_x, bounds_y;
  float z;
  float min_opacity;
};
#pragma pack(pop)

// Maximum supported LOD level: 3
struct GaussianPageTableEntry {
  unsigned char level;
  uint32_t last_used{0};
  uint8_t required;
  uint32_t page_ids[8];
};

class MappedFile;

#define GAUSSIAN_ENABLE_GEOMETRY
#define GAUSSIAN_MAX_INMEMORY_PAGES 500
#define GAUSSIAN_MAX_ALLOWED_PAGES 5000
#define GAUSSIAN_COMPONENTS (3 + 48 + 1 + 3 + 4)
#define GAUSSIAN_MAX_PAGES_PER_FRAME 40
#define GAUSSIAN_PER_GAUSSIAN_BYTES (GAUSSIAN_COMPONENTS * sizeof(float))
#ifdef __APPLE__
#define GAUSSIAN_PROXY_RENDER_DIMS {300, 150, 1}
#else
#define GAUSSIAN_PROXY_RENDER_DIMS {600, 400, 1}
#endif
struct GaussianSplats {
  uint64_t num_total, num_inmemory_max, num_inmemory_current;
  BufferH indices;

  // Virtual memory
  std::shared_ptr<MappedFile> source_file;
  uint32_t header_size, page_size, num_pages, lod_levels;
  MeshPrimitiveH proxy_mesh;
  BufferH face_pageid_mapping, page_links;
  ImageH proxy_image;
  ImageH proxy_image_depth;
  SamplerH proxy_sampler;
  BufferH reduction_result;
  DescriptorSetH reduction_ds;
  PipelineH reduction_pipe;
  std::vector<GaussianPageTableEntry> page_table;
  BufferH vmem_staging_buffer, index_staging_buffer;

  BufferH gaussian_buffer;
  MaterialH material;

  // Depth sorting
  uint32_t sort_num_workgroups, sort_elements_per_wg;
  BufferH sort_buf_data, sort_buf_hist, sort_buf_packed[2];
  DescriptorSetH sort_ds;
  PipelineH sort_pipe_depth, sort_pipe_hist, sort_pipe_sort;
};
typedef RenderDataHandle<GaussianSplats> GaussianSplatsH;

struct Environment {
  ImageH skybox, diffuse, brdf, specular;
  MaterialH skybox_material;
};
typedef RenderDataHandle<Environment> EnvironmentH;

struct RenderObject {
  RenderObject(MeshPrimitiveH mesh_primitive, glm::mat4 transform)
      : mesh_primitive(mesh_primitive), transform(transform) {}
  RenderObject(GaussianSplatsH gaussian_splats, glm::mat4 transform)
      : gaussian_splats(gaussian_splats), transform(transform) {}
  RenderObject(EnvironmentH environment) : environment(environment) {}

  MeshPrimitiveH mesh_primitive;
  GaussianSplatsH gaussian_splats;
  EnvironmentH environment;

  glm::mat4 transform;
};

#define MAX_LIGHTS 100
#pragma pack(push, 1)
struct LightPrimitive {
  glm::mat4 model_matrix;
  glm::vec3 color;
  uint32_t type;
  float intensity;
  float spot_light_angle_scale;
  float spot_light_angle_offset;
  ImageH shadow_map;  // 8 bytes
  uint32_t padding1, padding2, padding3;
};
#pragma pack(pop)

}  // namespace Uvelon