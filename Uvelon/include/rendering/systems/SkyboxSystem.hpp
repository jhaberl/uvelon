#pragma once
#include "RenderSystem.hpp"

namespace Uvelon {

class SkyboxSystem : public RenderSystem<RenderObject> {
 protected:
  virtual bool canDraw(RenderObject& render_object) override;
  virtual void draw(VkCommandBuffer cmd, Stage stage,
                    FilteredIterator<RenderObject> begin,
                    FilteredIterator<RenderObject> end) override;
};

}  // namespace Uvelon