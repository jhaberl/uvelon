#pragma once
#include <vector>

#include "rendering/Primitives.hpp"
#include "utils/FilteredIterator.hpp"

namespace Uvelon {

template <typename T>
class RenderSystem {
 public:
  enum Stage { SHADOWMAP, GBUFFER, SWAPCHAIN, SWAPCHAIN_FINAL };

  RenderSystem() {}
  virtual ~RenderSystem(){};

  virtual void init() {}
  virtual void prepare(VkCommandBuffer cmd, std::vector<T>& objects) final {
    prepare(cmd,
            FilteredIterator<T>(
                std::bind(&RenderSystem::canDraw, this, std::placeholders::_1),
                objects.begin(), objects.end()),
            FilteredIterator<T>(
                std::bind(&RenderSystem::canDraw, this, std::placeholders::_1),
                objects.end(), objects.end()));
  }
  virtual void draw(VkCommandBuffer cmd, Stage stage,
                    std::vector<T>& objects) final {
    draw(cmd, stage,
         FilteredIterator<T>(
             std::bind(&RenderSystem::canDraw, this, std::placeholders::_1),
             objects.begin(), objects.end()),
         FilteredIterator<T>(
             std::bind(&RenderSystem::canDraw, this, std::placeholders::_1),
             objects.end(), objects.end()));
                    }
  virtual void destroy() {}

 protected:
  virtual bool canDraw(T& render_object) = 0;
  // Prepare is called outside of a render pass
  virtual void prepare(VkCommandBuffer cmd, FilteredIterator<T> begin,
                       FilteredIterator<T> end) {}
  virtual void draw(VkCommandBuffer cmd, Stage stage, FilteredIterator<T> begin,
                    FilteredIterator<T> end) {}
};

}  // namespace Uvelon