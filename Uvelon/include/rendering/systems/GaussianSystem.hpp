#pragma once

#include "rendering/Primitives.hpp"
#include "rendering/systems/RenderSystem.hpp"
#include "utils/ThreadPool.hpp"

namespace Uvelon {

class GaussianSystem : public RenderSystem<RenderObject> {
 public:
  GaussianSystem();
  virtual ~GaussianSystem() override;

 protected:
  virtual bool canDraw(RenderObject& object) override;
  virtual void prepare(VkCommandBuffer cmd,
                       FilteredIterator<RenderObject> begin,
                       FilteredIterator<RenderObject> end) override;
  virtual void draw(VkCommandBuffer cmd, Stage stage,
                    FilteredIterator<RenderObject> begin,
                    FilteredIterator<RenderObject> end) override;

 private:
  ThreadPool copy_pool_;
  std::vector<uint32_t> reduction_result_cpu_, index2_cpu_;
  // Free physical pages for each lod level
  // Vector per level, pair<last used, page>
  std::vector<std::vector<std::pair<uint32_t, uint32_t>>> free_physical_pages_;
  std::vector<VkBufferCopy> copy_regions_;
  uint32_t frame_count_{0};

  bool frustum_vis_{false};

  // Adaptive LOD
  uint32_t last_up_frame_, last_down_frame_;

  bool processVmem(VkCommandBuffer cmd, RenderObject& ro);
  void sortByDepth(VkCommandBuffer cmd, RenderObject& ro);

  inline bool pteIsRequired(GaussianPageTableEntry& entry, uint32_t index) {
    return (entry.required >> index) & 0x1;
  }

  inline void pteSetRequired(GaussianPageTableEntry& entry, uint32_t index,
                             bool required) {
    // Set bit at index to 0, then set to required
    entry.required = (entry.required & ~(0x1 << index)) | (!!required << index);
  }
};

}  // namespace Uvelon