#pragma once
#include "RenderSystem.hpp"

namespace Uvelon {

// Export gaussian splats as B/W alpha slices for marching cubes
class GaussianSliceSystem : public RenderSystem<RenderObject> {
 public:
  GaussianSliceSystem();

 protected:
  bool canDraw(RenderObject& render_object) override;
  virtual void prepare(VkCommandBuffer cmd,
                       FilteredIterator<RenderObject> begin,
                       FilteredIterator<RenderObject> end) override;
};

}  // namespace Uvelon