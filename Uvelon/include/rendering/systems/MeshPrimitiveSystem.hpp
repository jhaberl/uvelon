#pragma once
#include "rendering/systems/RenderSystem.hpp"

namespace Uvelon {

class MeshPrimitiveSystem : public RenderSystem<RenderObject> {
 protected:
  virtual bool canDraw(RenderObject& object) override;
  virtual void prepare(VkCommandBuffer cmd,
                       FilteredIterator<RenderObject> begin,
                       FilteredIterator<RenderObject> end) override;
  virtual void draw(VkCommandBuffer cmd, Stage stage,
                    FilteredIterator<RenderObject> begin,
                    FilteredIterator<RenderObject> end) override;
};

}  // namespace Uvelon