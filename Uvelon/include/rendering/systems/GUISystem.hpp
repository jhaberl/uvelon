#pragma once
#include "rendering/systems/RenderSystem.hpp"

namespace Uvelon {

class GUISystem : public RenderSystem<RenderObject> {
 public:
  virtual void init() override;
  virtual void destroy() override;

 protected:
  virtual bool canDraw(RenderObject& render_object) override { return false; }
  virtual void draw(VkCommandBuffer cmd, Stage stage,
                    FilteredIterator<RenderObject> begin,
                    FilteredIterator<RenderObject> end) override;

 private:
  VkDescriptorPool pool_;
};

}  // namespace Uvelon