#pragma once
#include "RenderSystem.hpp"
#include "rendering/Pipeline.hpp"
#include "rendering/Primitives.hpp"
#include "scene/Light.hpp"

namespace Uvelon {

class ShadowMapSystem : public RenderSystem<LightPrimitive> {
 public:
  ShadowMapSystem();

  // TODO: The design of this is just terrible
  void setRenderObjects(std::vector<RenderObject>* render_objects) {
    render_objects_ = render_objects;
  }

  // Create a PipelineBuilder initialized with everything necessary for a light
  // type except for the binding/input attributes.
  PipelineBuilder createPipelineBuilder(Light::Type type);

 protected:
  virtual bool canDraw(LightPrimitive& render_object) override;
  virtual void prepare(VkCommandBuffer cmd,
                       FilteredIterator<LightPrimitive> begin,
                       FilteredIterator<LightPrimitive> end) override;
  virtual void draw(VkCommandBuffer cmd, Stage stage,
                    FilteredIterator<LightPrimitive> begin,
                    FilteredIterator<LightPrimitive> end) override;

 private:
#pragma pack(push, 1)
  struct ShadowMapUniform {
    glm::mat4 light_projection[MAX_LIGHTS][6];
    float far;
  };

  struct ShadowMapPushConstant {
    glm::mat4 model_matrix;
    uint32_t light_index;
  };
#pragma pack(pop)

  BufferH uniform_buffer_;
  DescriptorSetH light_ds_;
  MaterialH directional_, cube_;

  glm::vec3 center_offset_[6] = {{-1.0f, 0.0f, 0.0f}, {1.0f, 0.0f, 0.0f},
                                 {0.0f, -1.0f, 0.0f}, {0.0f, 1.0f, 0.0f},
                                 {0.0f, 0.0f, -1.0f}, {0.0f, 0.0f, 1.0f}};
  glm::vec3 up_[6] = {{0.0f, 1.0f, 0.0f},  {0.0, 1.0f, 0.0f},
                      {0.0f, 0.0f, -1.0f}, {0.0f, 0.0f, 1.0f},
                      {0.0f, 1.0f, 0.0f},  {0.0f, 1.0f, 0.0f}};
  std::array<VkDeviceSize, 3> vertex_offsets_ = {0, 0, 0};

  std::vector<RenderObject>* render_objects_{nullptr};
};

}  // namespace Uvelon