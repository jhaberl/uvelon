#pragma once

#include <vulkan/vulkan.h>

#include <mutex>
#include <vector>

namespace Uvelon {

class Computer {
 public:
  ~Computer();

  void init(VkQueue compute_queue, uint32_t compute_queue_family);
  void destroy();

  VkCommandBuffer getCommandBuffer();
  void submit(VkCommandBuffer cmd, std::vector<VkSemaphore> wait_sem,
              std::vector<VkSemaphore> signal_sem, bool await);

 private:
  VkQueue queue_;

  VkCommandPool cmd_pool_;
  std::mutex m_free_;
  std::vector<VkCommandBuffer> free_cmds_;
  std::vector<VkFence> free_fences_;

  bool destroyed_{false};
};

}  // namespace Uvelon