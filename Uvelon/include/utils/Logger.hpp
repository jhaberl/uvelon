#pragma once

#include <iostream>
#include <string>

#define __FILENAME_WIN__ \
  (strrchr(__FILE__, '\\') ? strrchr(__FILE__, '\\') + 1 : __FILE__)
#define __FILENAME__                                                   \
  (strrchr(__FILENAME_WIN__, '/') ? strrchr(__FILENAME_WIN__, '/') + 1 \
                                  : __FILENAME_WIN__)

#define UVELON_LOG_TRACE(message)                                              \
  ::Uvelon::Logger::getInstance().log(::Uvelon::Logger::Level::TRACE, message, \
                                      __FILENAME__, __LINE__)
#ifndef NDEBUG
#define UVELON_LOG_DEBUG(message)                                              \
  ::Uvelon::Logger::getInstance().log(::Uvelon::Logger::Level::DEBUG, message, \
                                      __FILENAME__, __LINE__)
#else
#define UVELON_LOG_DEBUG(message) ((void)0)
#endif
#define UVELON_LOG_INFO(message)                                              \
  ::Uvelon::Logger::getInstance().log(::Uvelon::Logger::Level::INFO, message, \
                                      __FILENAME__, __LINE__)
#define UVELON_LOG_WARNING(message)                                     \
  ::Uvelon::Logger::getInstance().log(::Uvelon::Logger::Level::WARNING, \
                                      message, __FILENAME__, __LINE__)
#define UVELON_LOG_ERROR(message)                                              \
  ::Uvelon::Logger::getInstance().log(::Uvelon::Logger::Level::ERROR, message, \
                                      __FILENAME__, __LINE__)

namespace Uvelon {

class Logger {
 public:
  enum Level { TRACE, DEBUG, INFO, WARNING, ERROR };

  static Logger getInstance() {
    static Logger instance;
    return instance;
  }

  inline void log(Level log_level, std::string message, const char* file,
                  int line) {
    std::cout << "[" << level_strings_[log_level] << "] (" << file << ":"
              << line << ") " << message << "\n";
  }

 private:
  std::string level_strings_[5] = {"TRACE", "DEBUG", "INFO", "WARNING",
                                   "ERROR"};

  Logger() {}
};

}  // namespace Uvelon