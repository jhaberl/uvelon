#pragma once
#include <chrono>
#include <string>

#include "Logger.hpp"

#define UVELON_ENABLE_PROFILING
// #define UVELON_ENABLE_GPU_PROFILING

#ifdef UVELON_ENABLE_PROFILING
#define CONCAT(s1, s2) s1##s2
#define EXPANDCONCAT(s1, s2) CONCAT(s1, s2)
#define GET_MACRO12(_1, _2, NAME, ...) NAME

#define UVELON_PROFILE1(name) \
  ::Uvelon::Timer EXPANDCONCAT(timer1_, __LINE__)(name, __FILENAME__, __LINE__)
#define UVELON_PROFILE2(name, reporting)                              \
  ::Uvelon::Timer EXPANDCONCAT(timer2_, __LINE__)(name, __FILENAME__, \
                                                  __LINE__, reporting)
#define UVELON_PROFILE(...) \
  GET_MACRO12(__VA_ARGS__, UVELON_PROFILE2, UVELON_PROFILE1)(__VA_ARGS__)

#define UVELON_START_TIMER1(name) ::Uvelon::Timer(name, __FILENAME__, __LINE__)
#define UVELON_START_TIMER2(name, reporting) \
  ::Uvelon::Timer(name, __FILENAME__, __LINE__, reporting)
#define UVELON_START_TIMER(...)                                      \
  GET_MACRO12(__VA_ARGS__, UVELON_START_TIMER2, UVELON_START_TIMER1) \
  (__VA_ARGS__)
#define UVELON_END_TIMER(timer) timer.stop()
#else
#define UVELON_PROFILE(...) ((void)0)

#define UVELON_START_TIMER(...) ::Uvelon::Timer()
#define UVELON_END_TIMER(timer) ((void)0)
#endif

#ifdef UVELON_ENABLE_GPU_PROFILING
#define UVELON_PROFILER_INIT_GPU(physical_device, logical_device) \
  ::Uvelon::GPUTimer::init(physical_device, logical_device)
#define UVELON_START_TIMER_GPU(cmd, name) ::Uvelon::GPUTimer::start(cmd, name)
#define UVELON_END_TIMER_GPU(cmd, timer) ::Uvelon::GPUTimer::stop(cmd, timer)
#define UVELON_PROFILER_COLLECT_GPU(reporting) \
  ::Uvelon::GPUTimer::collect(reporting)
#define UVELON_PROFILER_DESTROY_GPU() ::Uvelon::GPUTimer::destroy()
#else
#define UVELON_PROFILER_INIT_GPU(physical_device, logical_device) ((void)0)
#define UVELON_START_TIMER_GPU(cmd, name) 0
#define UVELON_END_TIMER_GPU(cmd, timer) ((void)0)
#define UVELON_PROFILER_COLLECT_GPU(reporting) ((void)0)
#define UVELON_PROFILER_DESTROY_GPU() ((void)0)
#endif

#define UVELON_MAX_TIMESTAMP_QUERY_COUNT 1000

struct VkPhysicalDevice_T;
typedef VkPhysicalDevice_T* VkPhysicalDevice;
struct VkDevice_T;
typedef VkDevice_T* VkDevice;
struct VkCommandBuffer_T;
typedef VkCommandBuffer_T* VkCommandBuffer;
struct VkQueryPool_T;
typedef VkQueryPool_T* VkQueryPool;

namespace Uvelon {

enum Reporting { PRINT, CVAR };

class Timer {
 public:
  Timer();
  Timer(const std::string& name, const char* file, int line,
        Reporting reporting = PRINT);
  ~Timer();

  void start();
  void stop();

 private:
  const std::string name_;
  const std::string file_;
  int line_;
  bool stopped_{false};
  const Reporting reporting_{PRINT};

  std::chrono::time_point<std::chrono::high_resolution_clock> start_;
};

class GPUTimer {
 public:
  static void init(VkPhysicalDevice physical_device, VkDevice logical_device);
  static void destroy();

  static uint32_t start(VkCommandBuffer cmd, const std::string& name);
  static void stop(VkCommandBuffer cmd, uint32_t timer);

  static void collect(Reporting reporting);

 private:
  struct GPUTimerEntry {
    const std::string name;
    uint32_t timestamp_start, timestamp_end;
  };

  static VkDevice logical_device_;
  static VkQueryPool query_pool_;
  static uint32_t timestamp_count_;
  static std::vector<GPUTimerEntry> entries_;
  static std::vector<uint64_t> results_buffer_;
  static float timestamp_period_;
};

}  // namespace Uvelon