#pragma once
#include <functional>
#include <mutex>
#include <queue>
#include <thread>

namespace Uvelon {

// Based on https://stackoverflow.com/a/32593825
class ThreadPool {
 public:
  ~ThreadPool() { destroy(); }

  void init(int num_threads = 0);
  void enqueue(const std::function<void()>& job);
  void destroy();

  void waitIdle();

 private:
  bool running_{false};
#ifdef __cpp_lib_jthread
  std::vector<std::jthread> threads_;
#else  // MacOS...
  std::vector<std::thread> threads_;
#endif

  std::mutex m_queue_;  // jobs_, active_jobs_
  std::condition_variable cv_job_queued_, cv_job_done_;

  std::queue<std::function<void()>> jobs_;
  int active_jobs_{0};
};

}  // namespace Uvelon