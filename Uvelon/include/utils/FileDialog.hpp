#pragma once

#include <filesystem>
#include <string>

#include "core/FileWizard.hpp"

namespace Uvelon {

class FileDialog {
 public:
  bool pickDir(const std::string& id, std::filesystem::path& result,
               const std::filesystem::path& path);
  bool open(const std::string& id, std::filesystem::path& result,
            FileWizard::FilterItem* filter, int count,
            const std::filesystem::path& path);
  bool save(const std::string& id, std::filesystem::path& result,
            FileWizard::FilterItem* filter, int count, const std::string& name,
            const std::filesystem::path& path);

  void draw();

  bool isActive();

 private:
  struct FileNode {
    std::filesystem::path path;
    bool dir;
    std::vector<FileNode> children;

    FileNode(std::filesystem::path path) : path(path) {}
  };

  // Set
  std::string active_id_;
  bool handled_{false};
  std::filesystem::path path_;
  bool selectable_dir_, selectable_file_;

  // Updated
  FileNode root_{""};
  std::filesystem::path selected_;
  std::string filename_;

  bool checkRequest(const std::string& id);
  bool initRequest(const std::string& id, const std::filesystem::path& path,
                   FileWizard::FilterItem* filter, int filter_count,
                   bool selectable_dir, bool selectable_file);

  void drawNode(const FileNode& node);

  void updateTree();
  void populateChildren(FileNode& node);
};

}  // namespace Uvelon