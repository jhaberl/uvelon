#pragma once
#include "core/BufferCPU.hpp"
#include "mikktspace.h"
#include "scene/LoaderPrimitives.hpp"

namespace Uvelon {

class TangentGenerator {
 public:
  struct Result {
    BufferCPU indices;
    VertexData vertex_data;
  };

  TangentGenerator(BufferCPU&& indices, VertexData&& vertex_data);

  Result run();

  int getNumFaces();
  int getNumVerticesOfFace(const int iFace);
  void getPosition(float fvPosOut[], const int iFace, const int iVert);
  void getNormal(float fvNormOut[], const int iFace, const int iVert);
  void getTexCoord(float fvTexcOut[], const int iFace, const int iVert);

  uint32_t getIndex(const int iFace, const int iVert);

  void setTSpaceBasic(const float fvTangent[], const float fSign,
                      const int iFace, const int iVert);

 private:
  SMikkTSpaceInterface interface_;

  BufferCPU indices_;
  VertexData in_vertex_data_;

  BufferCPU tangents_;
};

}  // namespace Uvelon