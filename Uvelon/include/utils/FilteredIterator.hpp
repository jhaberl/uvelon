#pragma once
#include <functional>
#include <vector>

namespace Uvelon {

template <typename T>
struct FilteredIterator {
  using iterator_category = std::random_access_iterator_tag;
  using difference_type = int32_t;
  using value_type = T;

  typedef std::function<bool(T&)> FilterPredicate;

  explicit FilteredIterator() = default;
  explicit FilteredIterator(FilterPredicate filter,
                            std::vector<T>::iterator begin,
                            std::vector<T>::iterator end)
      : filter_(filter), begin_(begin), current_(begin), end_(end) {
    while (current_ != end_ && !filter(*current_)) {
      current_++;
    }
  }

  T& operator*() const { return *current_; }
  bool operator==(FilteredIterator o) const { return current_ == o.current_; }
  T* operator->() const { return &(*current_); }

  // Forward
  FilteredIterator& operator++() {
    do {
      current_++;
    } while (current_ != end_ && !filter_(*current_));
    return *this;
  }
  FilteredIterator operator++(int) {
    FilteredIterator pre_increment_result(*this);
    do {
      current_++;
    } while (current_ != end_ && !filter_(*current_));
    return pre_increment_result;
  }
  // Bidirectional
  FilteredIterator& operator--() {
    do {
      current_--;
    } while (current_ != begin_ && !filter_(*current_));
    return *this;
  }
  FilteredIterator operator--(int) {
    FilteredIterator pre_decrement_result(*this);
    do {
      current_--;
    } while (current_ != begin_ && !filter_(*current_));
    return pre_decrement_result;
  }
  // Random access
  FilteredIterator& operator+=(int n) {
    while (current_ != end_ && n != 0) {
      if (n > 0) {
        ++(*this);
        n--;
      } else {
        --(*this);
        n++;
      }
    }
    return *this;
  }
  FilteredIterator& operator-=(int n) { return *this += -n; }
  FilteredIterator operator+(int n) const {
    FilteredIterator result(*this);
    result += n;
    return result;
  }
  FilteredIterator operator-(int n) const { return *this + -n; }
  friend FilteredIterator operator+(int n, const FilteredIterator& rhs) {
    return rhs + n;
  }
  friend difference_type operator-(const FilteredIterator<T>& lhs,
                                   const FilteredIterator<T>& rhs) {
    difference_type result = static_cast<difference_type>(0);
    FilteredIterator<T> temp(lhs);
    while (temp != rhs) {
      if (temp < rhs)
        temp++;
      else
        temp--;
      result++;
    }
    return result;
  }
  T& operator[](int n) const { return *(*this + n); }
  friend bool operator<(const FilteredIterator<T>& lhs,
                        const FilteredIterator<T>& rhs) {
    return lhs.current_ < rhs.current_;
  }
  friend bool operator>(const FilteredIterator<T>& lhs,
                        const FilteredIterator<T>& rhs) {
    return rhs < lhs;
  }
  friend bool operator<=(const FilteredIterator<T>& lhs,
                         const FilteredIterator<T>& rhs) {
    return !(lhs > rhs);
  }
  friend bool operator>=(const FilteredIterator<T>& lhs,
                         const FilteredIterator<T>& rhs) {
    return !(lhs < rhs);
  }

 private:
  FilterPredicate filter_;
  std::vector<T>::iterator begin_, current_, end_;
};

}  // namespace Uvelon