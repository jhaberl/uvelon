#pragma once

#include <istream>
#include <memory>
#include <streambuf>
#include <string>

namespace Uvelon {

// https://stackoverflow.com/a/57862858
// Thanks Microsoft: https://github.com/microsoft/STL/issues/388
class MemoryBuffer : public std::streambuf {
 public:
  MemoryBuffer(char* begin, char* end);
  virtual pos_type seekoff(off_type _Off, std::ios_base::seekdir _Way,
                           std::ios_base::openmode _Mode =
                               std::ios_base::in | std::ios_base::out) override;
  virtual pos_type seekpos(
      pos_type, std::ios_base::openmode = std::ios_base::in |
                                          std::ios_base::out) override;
};

class MappedFile {
 public:
  void* data;
  uint64_t size;

  // Does not work for files larger than INT_MAX bytes in MSVC
  // https://github.com/microsoft/STL/issues/388
  std::unique_ptr<std::istream> stream;

  MappedFile(const std::string& path, bool write = false, uint64_t size = 0);
  ~MappedFile();

  // No copies
  MappedFile(const MappedFile&) = delete;
  MappedFile& operator=(const MappedFile&) = delete;

  MappedFile(MappedFile&&);
  MappedFile& operator=(MappedFile&&);

 private:
  const std::string path_;
  const bool write_{false};

  void* file_handle_;
  void* file_mapping_;

  int fd_;

  std::unique_ptr<MemoryBuffer> stream_buffer_;

  void init();
  void logError();
};

}  // namespace Uvelon