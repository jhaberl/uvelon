# Uvelon

Vulkan toy renderer. Also used for my master's thesis.

## Features
- Custom glTF 2.0 Importer
- Deferred Rendering
- Physically-Based Rendering (Cook-Torrance)
- Punctual lights (directional, point, spot)
- Normal Maps
- Shadow Mapping
- Skyboxes
- Image-Based Lighting
- Screen-Space Reflections
- High Dynamic Range
- Tone Mapping (ACES, Reinhard, Uncharted 2)
- 3D Gaussian Splatting
- Virtual Memory for 3D Gaussian Splatting (Master's Thesis)
- Shader Hot-Reload
- Support for Windows and MacOS

### Future
- SSAO
- FXAA (and perhaps TAA)
- Skinned animation (w/ compute skinning)
- Bloom
- Compute culling
- GI

## Supported models
The custom glTF loader only supports a subset of the glTF 2.0 specification. The following models are known to work. Note that even if the models are able to load and be displayed, some features may be unsupported. `gltf-samples` refers to the [glTF sample assets repository](https://github.com/KhronosGroup/glTF-Sample-Assets).

- gltf-samples/ABeautifulGame (no KHR_materials_transmission/KHR_materials_volume)
- gltf-samples/AnimatedCube
- gltf-samples/AntiqueCamera
- gltf-samples/Avocado
- gltf-samples/BarramundiFish
- gltf-samples/BoomBox
- gltf-samples/BoomBoxWithAxes
- gltf-samples/Box
- gltf-samples/BoxAnimated
- gltf-samples/BoxTextured
- gltf-samples/BoxTexturedNonPowerOfTwo
- gltf-samples/CesiumMilkTruck
- `TODO`

## Setup
### Structure
- `Uvelon` is the core of the renderer
- `UvelonPlayground` is used to run the renderer

### Requirements
#### Vulkan
- Version 1.2
- VK_KHR_dynamic_rendering
- VK_KHR_synchronization2
- VK_EXT_index_type_uint8 (optional, 8 bit indices)

### Libraries
- glm
- Dear ImGui
- nativefiledialog-extended
- nlohmann/json
- SDL
- shaderc
- stb_image
- stb_image_write
- VkBootstrap
- VMA
- Volk