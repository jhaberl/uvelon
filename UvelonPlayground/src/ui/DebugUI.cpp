#include "ui/DebugUI.hpp"

#include <imgui.h>
#include <imgui_stdlib.h>
#include <implot.h>

#include "core/CVarManager.hpp"
#include "rendering/Primitives.hpp"
#include "scene/Node.hpp"
#include "scene/Scene.hpp"
#include "utils/Logger.hpp"

#ifdef __APPLE__
#include <TargetConditionals.h>
#if TARGET_OS_IPHONE
#include <filesystem>
#endif
#endif

void DebugUI::update(float delta) {
  hist_fps_.addEntry(1000.0f / delta);

  if (Uvelon::CVarManager::getVar("gaussian.vmem", false).getValue<bool>()) {
    int32_t gsplat_inmem_pages =
        Uvelon::CVarManager::getVar(
            "gaussian.vmem.stats.inmemory.current.pages", 0)
            .getValue<int32_t>();
    hist_gsplat_inmem_pages_.addEntry(static_cast<float>(gsplat_inmem_pages));

    int32_t gsplat_copy_bytes =
        Uvelon::CVarManager::getVar("gaussian.vmem.stats.inmemory.copy", 0)
            .getValue<int32_t>();
    hist_gsplat_copy_.addEntry(static_cast<float>(gsplat_copy_bytes));

    int32_t gsplat_lod_th1 =
        Uvelon::CVarManager::getVar("gaussian.vmem.lod.threshold1", 0)
            .getValue<int32_t>();
    hist_gsplat_lod_thresh_.addEntry(static_cast<float>(gsplat_lod_th1));

    int32_t gsplat_missing =
        Uvelon::CVarManager::getVar("gaussian.vmem.stats.missing", 0)
            .getValue<int32_t>();
    hist_gsplat_missing_.addEntry(static_cast<float>(gsplat_missing));
  }
}

void DebugUI::draw() {
  // ImPlot::ShowDemoWindow();
  if (ImGui::Begin("Debug")) {
    plotLine("FPS", hist_fps_, 0.0f);

    if (Uvelon::CVarManager::getVar("gaussian.vmem", false).getValue<bool>()) {
      plotLine("Pages In Memory", hist_gsplat_inmem_pages_, 0.0f,
               GAUSSIAN_MAX_INMEMORY_PAGES);

      int32_t pagesize =
          Uvelon::CVarManager::getVar("gaussian.vmem.stats.page_size", 0)
              .getValue<int32_t>();
      plotLine("Bytes Copied", hist_gsplat_copy_, 0.0f,
               static_cast<float>(GAUSSIAN_MAX_PAGES_PER_FRAME *
                                  GAUSSIAN_PER_GAUSSIAN_BYTES * pagesize));

      plotLine("LOD Threshold", hist_gsplat_lod_thresh_, 0.0f);

      plotLine("Pages Missing", hist_gsplat_missing_, 0.0f);

      const char* inmemory_current_labels[4] = {"Level 0", "Level 1", "Level 2",
                                                "Level 3"};
      float inmemory_current[4];
      for (int i = 0; i < 4; i++) {
        inmemory_current[i] = static_cast<float>(
            Uvelon::CVarManager::getVar(
                "gaussian.vmem.stats.inmemory.current.pages." +
                    std::to_string(i),
                0)
                .getValue<int32_t>());
      }
      plotStackedBar("In memory", inmemory_current_labels, inmemory_current, 4,
                     "", "Physical Pages", -0.5, 0.5, 0.0,
                     static_cast<double>(GAUSSIAN_MAX_INMEMORY_PAGES));

      const char* times_labels[5] = {
          "Proxy Render & Reduce", "Pagetable Update", "Staging Copy Overhead",
          "Depth Sort", "Render"};
      float times[5];
      times[0] =
          static_cast<float>(Uvelon::CVarManager::getVar(
                                 "instrumentation.ms.proxy_render_reduce", 0.0)
                                 .getValue<double>());
      times[1] =
          static_cast<float>(Uvelon::CVarManager::getVar(
                                 "instrumentation.ms.pagetable_update", 0.0)
                                 .getValue<double>());
      times[2] = static_cast<float>(
          Uvelon::CVarManager::getVar(
              "instrumentation.ms.staging_copy_overhead", 0.0)
              .getValue<double>());
      times[3] = static_cast<float>(
          Uvelon::CVarManager::getVar("instrumentation.ms.depth_sort", 0.0)
              .getValue<double>());
      times[4] = static_cast<float>(
          Uvelon::CVarManager::getVar("instrumentation.ms.gaussian_render", 0.0)
              .getValue<double>());
      plotStackedBar("Performance", times_labels, times, 5, "", "Time [ms]",
                     -0.5, 0.5, 0.0, 16.6);

      if (Uvelon::CVarManager::getVar("gsplat.temp_file_out", false)
              .getValue<bool>()) {
        if (!temp_file_out_.is_open()) createOutFile();
        temp_file_out_ << times[0] << "," << times[1] << "," << times[2] << ","
                       << times[3] << "," << times[4] << ","
                       << inmemory_current[0] << "," << inmemory_current[1]
                       << "," << inmemory_current[2] << ","
                       << inmemory_current[3] << ","
                       << hist_gsplat_copy_.getLast() << ","
                       << hist_fps_.getLast() << ","
                       << hist_gsplat_lod_thresh_.getLast() << ","
                       << hist_gsplat_missing_.getLast() << "\n";
      } else if (temp_file_out_.is_open()) {
        temp_file_out_.close();
      }
    } else if (playground_.getScene()->getRoot()->getName().ends_with(".ply")) {
      const char* times_labels[5] = {
          "Proxy Render & Reduce", "Pagetable Update", "Staging Copy Overhead",
          "Depth Sort", "Render"};
      float times[5] = {0.0f, 0.0f, 0.0f, 0.0f, 0.0f};
      times[3] = static_cast<float>(
          Uvelon::CVarManager::getVar("instrumentation.ms.depth_sort", 0.0)
              .getValue<double>());
      times[4] = static_cast<float>(
          Uvelon::CVarManager::getVar("instrumentation.ms.gaussian_render", 0.0)
              .getValue<double>());
      plotStackedBar("Performance", times_labels, times, 5, "", "Time [ms]",
                     -0.5, 0.5, 0.0, 16.6);

      if (Uvelon::CVarManager::getVar("gsplat.temp_file_out", false)
              .getValue<bool>()) {
        if (!temp_file_out_.is_open()) createOutFile();
        temp_file_out_ << times[0] << "," << times[1] << "," << times[2] << ","
                       << times[3] << "," << times[4] << "," << 0 << "," << 0
                       << "," << 0 << "," << 0 << "," << 0.0f << ","
                       << hist_fps_.getLast() << "," << 0.0f << "," << 0
                       << "\n";
      } else if (temp_file_out_.is_open()) {
        temp_file_out_.close();
      }
    }
  }
  ImGui::End();
}

void DebugUI::createOutFile() {
#if defined(__APPLE__) && TARGET_OS_IPHONE
  // Place in documents folder to give access via file sharing
  std::filesystem::create_directory(UVELON_FILE_WORKSPACE("Documents"));
  auto path = UVELON_FILE_WORKSPACE("Documents/temp_file_out.csv");
#else
  auto path = UVELON_FILE_WORKSPACE("temp_file_out.csv");
#endif
  temp_file_out_ = std::ofstream(path);
}

void DebugUI::plotLine(const std::string& label, HistoryData& history,
                       float min, float max) {
  ImGui::PlotLines((label + ": " + std::to_string(history.getLast())).c_str(),
                   getHistoryEntry, &history, history.max_size, 0, nullptr, min,
                   max);
  /*if (ImPlot::BeginPlot(label.c_str())) {
    ImPlot::SetupAxes("x", "y", ImPlotAxisFlags_AutoFit,
                      ImPlotAxisFlags_AutoFit);
    ImPlot::PlotLineG(label.c_str(), getHistoryEntryImPlot, &history,
                      history.max_size, 0);
    //ImPlot::PlotBarsG(label.c_str(), getHistoryEntryImPlot, &history, 1, 1.0);
    ImPlot::EndPlot();
  }*/
}

void DebugUI::plotStackedBar(const std::string& title, const char** labels,
                             const float* values, int items,
                             const std::string& label_x,
                             const std::string& label_y, double x_min,
                             double x_max, double y_min, double y_max) {
  if (ImPlot::BeginPlot(title.c_str())) {
    ImPlot::SetupAxes(label_x.c_str(), label_y.c_str(), 0, 0);
    ImPlot::SetupAxesLimits(x_min, x_max, y_min, y_max);
    ImPlot::PlotBarGroups(labels, values, items, 1, 1.0, 0,
                          ImPlotBarGroupsFlags_Stacked);
    ImPlot::EndPlot();
  }
}

void DebugUI::HistoryData::addEntry(float entry) {
  data[next++] = entry;
  if (next >= max_size) next = 0;
}

float DebugUI::HistoryData::getLast() {
  int idx = next - 1;
  if (idx < 0) idx += max_size;
  return data[idx];
}

float DebugUI::HistoryData::getAverage() {
  float result = 0.0f;
  for (uint32_t i = 0; i < max_size; i++) {
    result += data[i];
  }
  result /= max_size;
  return result;
}

float DebugUI::getHistoryEntry(void* data, int idx) {
  HistoryData* history = static_cast<HistoryData*>(data);
  idx += history->next;
  if (static_cast<uint32_t>(idx) >= history->max_size) idx -= history->max_size;
  return history->data[idx];
}

ImPlotPoint DebugUI::getHistoryEntryImPlot(int idx, void* data) {
  HistoryData* history = static_cast<HistoryData*>(data);
  int idx_in = idx;
  idx += history->next;
  if (static_cast<uint32_t>(idx) >= history->max_size) idx -= history->max_size;
  return {static_cast<double>(idx_in), history->data[idx]};
}