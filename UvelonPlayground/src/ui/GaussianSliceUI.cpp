#include "ui/GaussianSliceUI.hpp"

#include <imgui.h>
#include <imgui_stdlib.h>

#include "core/CVarManager.hpp"
#include "scene/Node.hpp"
#include "scene/Scene.hpp"

void GaussianSliceUI::draw() {
  if (playground_.getScene()->getRoot()->getGaussianSplats() &&
      !playground_.getScene()
           ->getRoot()
           ->getGaussianSplats()
           ->getHandle()
           ->proxy_mesh) {
    if (ImGui::Begin("Export Slices")) {
      ImGui::InputInt("#Slices X", &Uvelon::CVarManager::getVar(
                                        "gaussian.slice.num_slices.x")
                                        .getValue<int32_t>());
      ImGui::InputInt("#Slices Y", &Uvelon::CVarManager::getVar(
                                        "gaussian.slice.num_slices.y")
                                        .getValue<int32_t>());
      ImGui::InputInt("#Slices Z", &Uvelon::CVarManager::getVar(
                                        "gaussian.slice.num_slices.z")
                                        .getValue<int32_t>());

      ImGui::InputDouble("Bounds X",
                         &Uvelon::CVarManager::getVar("gaussian.slice.bounds.x")
                              .getValue<double>());
      ImGui::InputDouble("Bounds Y",
                         &Uvelon::CVarManager::getVar("gaussian.slice.bounds.y")
                              .getValue<double>());
      ImGui::InputDouble("Bounds Z",
                         &Uvelon::CVarManager::getVar("gaussian.slice.bounds.z")
                              .getValue<double>());
      double min = 0.0, max = 1.0;
      ImGui::SliderScalar("Opacity", ImGuiDataType_Double,
                          &Uvelon::CVarManager::getVar("gaussian.slice.opacity")
                               .getValue<double>(),
                          &min, &max);
      ImGui::InputText("Output Path",
                       &Uvelon::CVarManager::getVar("gaussian.slice.path")
                            .getValue<std::string>());
      if (ImGui::Button("Run")) {
        Uvelon::CVarManager::getVar("gaussian.slice.export").getValue<bool>() =
            true;
      }
    }
    ImGui::End();
  }
}