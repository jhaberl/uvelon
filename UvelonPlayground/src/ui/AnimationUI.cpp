#include "ui/AnimationUI.hpp"

#include <imgui.h>
#include <imgui_stdlib.h>

#include "scene/Animation.hpp"
#include "scene/Scene.hpp"

void AnimationUI::draw() {
  if (ImGui::Begin("Animations")) {
    auto selected = selected_.lock();
    if (ImGui::BeginCombo("Animation", selected
                                           ? (selected->getName().empty()
                                                  ? "[ANIMATION]"
                                                  : selected->getName().c_str())
                                           : "")) {
      int index = 0;
      for (auto animation : playground_.getScene()->getAnimations()) {
        std::string name = animation->getName();
        if (ImGui::Selectable(
                name.empty() ? ("[ANIMATION]##" + std::to_string(index)).c_str()
                             : name.c_str(),
                animation == selected)) {
          selected_ = animation;
        }
        index++;
      }
      ImGui::EndCombo();
    }
    if (auto selected = selected_.lock()) {
      ImGui::Checkbox("Loop", &selected->getLoop());
      // ImGui::InputFloat("Time", &selected_->getTime());
      ImGui::SliderFloat("Time", &selected->getTime(), 0.0f,
                         selected->getLength());
      ImGui::SameLine();
      if (ImGui::Button("Reset##Time")) selected->setTime(0.0f);
      ImGui::InputFloat("Speed", &selected->getSpeed());
      ImGui::SameLine();
      if (ImGui::Button("Reset##Speed")) selected->setSpeed(1.0f);
      if (ImGui::Button("Play")) selected->setActive(true);
      ImGui::SameLine();
      if (ImGui::Button("Pause")) selected->setActive(false);
    }
  }
  ImGui::End();
}