#include "ui/LightUI.hpp"

#include <imgui.h>
#include <imgui_stdlib.h>

#include <numbers>

#include "scene/Node.hpp"

void LightUI::draw() {
  if (ImGui::Begin("Lights")) {
    auto selected = selected_.lock();
    std::string selected_name;
    if (selected) {
      if (selected->getName().empty()) {
        if (selected_index_ >= 0)
          selected_name = std::to_string(selected_index_);
      } else {
        selected_name = selected->getName();
      }
    }

    if (ImGui::BeginCombo("Light", selected_name.c_str())) {
      playground_.getScene()->addLightNodes(light_nodes);
      int index = 0;
      for (auto node : light_nodes) {
        if (ImGui::Selectable((node->getName().empty() ? std::to_string(index)
                                                       : node->getName())
                                  .c_str(),
                              selected == node)) {
          selected_ = node;
          selected_index_ = index;
        }
        index++;
      }
      num_lights_ = index;
      light_nodes.clear();
      ImGui::EndCombo();
    }
    if (auto selected = selected_.lock()) {
      std::shared_ptr<Uvelon::Light> light = selected->getLight();
      ImGui::ColorEdit3("Color", &light->color[0]);
      ImGui::InputFloat("Intensity", &light->intensity);
      ImGui::Checkbox("Cast Shadows", &light->shadows);
      if (ImGui::BeginCombo(
              "Type",
              light->type == Uvelon::Light::Type::DIR
                  ? "Directional"
                  : (light->type == Uvelon::Light::Type::POINT ? "Point"
                                                               : "Spot"))) {
        for (int i = Uvelon::Light::Type::DIR;
             i < Uvelon::Light::Type::SPOT + 1; i++) {
          if (ImGui::Selectable(
                  i == Uvelon::Light::Type::DIR
                      ? "Directional"
                      : (i == Uvelon::Light::Type::POINT ? "Point" : "Spot"),
                  i == light->type)) {
            light->type = static_cast<Uvelon::Light::Type>(i);
          }
        }
        ImGui::EndCombo();
      }
      if (light->type != Uvelon::Light::Type::DIR) {
        ImGui::DragFloat3("Translation", &light->translation[0], 0.1f);
      }
      if (light->type != Uvelon::Light::Type::POINT) {
        ImGui::DragFloat3("Rotation", &light->rotation[0], 0.01f,
                          -static_cast<float>(std::numbers::pi),
                          static_cast<float>(std::numbers::pi));
      }
      if (light->type == Uvelon::Light::Type::SPOT) {
        bool change = false;
        float inner_angle = light->getSpotInnerConeAngle();
        float outer_angle = light->getSpotOuterConeAngle();
        if (ImGui::DragFloat("Angle Inner", &inner_angle, 0.01f,
                             -static_cast<float>(std::numbers::pi),
                             static_cast<float>(std::numbers::pi)))
          change = true;
        if (ImGui::DragFloat("Angle Outer", &outer_angle, 0.01f,
                             -static_cast<float>(std::numbers::pi),
                             static_cast<float>(std::numbers::pi)))
          change = true;
        if (change) {
          light->setSpotConeAngles(inner_angle, outer_angle);
        }
      }
      if (ImGui::Button("Delete")) {
        selected->getParent()->detachChild(selected);
        num_lights_--;
        selected_index_ = -1;
      }
    } else {
      selected_index_ = -1;
    }
    if (ImGui::Button("Add")) {
      auto light_node = std::make_shared<Uvelon::Node>("");
      light_node->setLight(std::make_shared<Uvelon::Light>());
      Uvelon::Node::attach(playground_.getScene()->getRoot(), light_node);
      selected_ = light_node;
      selected_index_ = num_lights_;
      num_lights_++;
    }
    if (ImGui::Button("Clear")) {
      playground_.getScene()->addLightNodes(light_nodes);
      for (auto node : light_nodes) {
        node->getParent()->detachChild(node);
      }
      light_nodes.clear();
      num_lights_ = 0;
      selected_index_ = -1;
    }
  }
  ImGui::End();
}