#include "ui/SceneUI.hpp"

#include <imgui.h>
#include <imgui_stdlib.h>

#include "core/FileWizard.hpp"
#include "scene/GSplatLoader.hpp"
#include "scene/Loader.hpp"
#include "scene/Scene.hpp"
#include "scene/SceneEnvironment.hpp"
#include "utils/Instrumentation.hpp"
#include "utils/Logger.hpp"

void SceneUI::draw() {
  if (ImGui::Begin("Scene")) {
    if (ImGui::Button("Open File") || playground_.getFileDialog().isActive()) {
      std::filesystem::path file_path;
      Uvelon::FileWizard::FilterItem filter[3] = {
          {"All Supported", "gltf,glb,ply"},
          {"glTF Model", "gltf,glb"},
          {"3DGS Scene", "ply"}};
      bool result = playground_.getFileDialog().open(
          "scene_open", file_path, filter, 3, UVELON_FILE_WORKSPACE(""));
      if (result && !file_path.empty()) {
        model_selected_file_ = file_path;

        model_selected_file_scenes_.clear();
        // Query contained scenes in glTF
        // For gaussian splatting don't query for scenes
        if (model_selected_file_.extension() != ".ply") {
          Uvelon::Loader loader(model_selected_file_.string());
          model_selected_file_scenes_ =
              loader.queryScenes(model_selected_scene_);
        }
      }
    }

    ImGui::TextWrapped("%s",
                       model_selected_file_.has_filename()
                           ? model_selected_file_.filename().string().c_str()
                           : "");

    if (!model_selected_file_.empty()) {
      if (!model_selected_file_scenes_.empty()) {
        if (ImGui::BeginCombo(
                "Scene",
                model_selected_file_scenes_[model_selected_scene_].c_str())) {
          for (int i = 0; i < model_selected_file_scenes_.size(); i++) {
            bool selected = false;
            if (ImGui::Selectable(model_selected_file_scenes_[i].c_str(),
                                  &selected)) {
              model_selected_scene_ = i;
            }
          }
          ImGui::EndCombo();
        }
      }

      if (ImGui::Button("Load##Scene")) {
        if (model_selected_file_.extension() == ".ply") {
          // Gaussian Splatting
          UVELON_PROFILE("Load ply: " + model_selected_file_.string());
          playground_.setScene(
              Uvelon::GSplatLoader(model_selected_file_.string()).load());
        } else {
          // glTF
          UVELON_PROFILE(
              "Load gltf: " + model_selected_file_.string() +
              " scene: " + model_selected_file_scenes_[model_selected_scene_]);
          playground_.setScene(Uvelon::Loader(model_selected_file_.string())
                                   .load(model_selected_scene_));
        }

        // Force the creation of a default camera if none is in the scene
        std::shared_ptr<Uvelon::Node> temp;
        if (!playground_.getScene()->getActiveCamera(temp)) {
          playground_.createDefaultCamera();
        }
      }
      ImGui::SameLine();
      if (ImGui::Button("Unload")) {
        playground_.setScene(std::make_unique<Uvelon::Scene>());
      }
    }

    ImGui::Separator();
    ImGui::Text("Environment");
    if (ImGui::Button("Load##Environment") ||
        playground_.getFileDialog().isActive()) {
      std::filesystem::path folder_path;
      bool result = playground_.getFileDialog().pickDir(
          "environment_load", folder_path, UVELON_FILE_WORKSPACE(""));
      if (result && !folder_path.empty()) {
        UVELON_PROFILE("Load environment");
        playground_.getScene()->setEnvironment(
            std::make_shared<Uvelon::SceneEnvironment>(folder_path.string()));
      }
    }
    ImGui::SameLine();
    if (ImGui::Button("Remove")) {
      playground_.getScene()->setEnvironment(nullptr);
    }
  }
  ImGui::End();
}