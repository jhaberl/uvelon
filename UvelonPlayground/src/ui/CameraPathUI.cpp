#include "ui/CameraPathUI.hpp"

#include <imgui.h>
#include <imgui_stdlib.h>

#include <fstream>

#include "core/BufferCPU.hpp"
#include "core/CVarManager.hpp"
#include "core/FileWizard.hpp"
#include "scene/Animation.hpp"
#include "scene/Node.hpp"
#include "scene/Scene.hpp"
#include "utils/Logger.hpp"
#include "utils/MappedFile.hpp"

CameraPathUI::CameraPathUI(UvelonPlayground& playground) : UI(playground) {
  Uvelon::CVarManager::getVar("gsplat.temp_file_out.with_path", false);
}

void CameraPathUI::update(float delta) {
  if (in_playback_) {
    if (!inserted_animation_->getActive()) {
      // No longer in playback
      stopAnimation();
    } else if (!playground_.getCamLocked()) {
      std::shared_ptr<Uvelon::Node> active_camera;
      if (playground_.getScene()->getActiveCamera(active_camera)) {
        if (active_camera == camera_.lock()) {
          // Camera lock cancelled on camera playing back path
          stopAnimation();
        }
      }
    }
  }
}

void CameraPathUI::draw() {
  if (ImGui::Begin("Camera Path")) {
    if (ImGui::Button("New")) {
      stopAnimation();
      selected_keyframe_ = -1;
      current_ = std::make_unique<CameraPath>();
    }
    ImGui::SameLine();
    if (ImGui::Button("Open") || playground_.getFileDialog().isActive()) {
      std::filesystem::path file_path;
      Uvelon::FileWizard::FilterItem filter[1] = {{"Camera Path File", "csv"}};

      bool result = playground_.getFileDialog().open(
          "cam_path_open", file_path, filter, 1, UVELON_FILE_WORKSPACE(""));
      if (result && !file_path.empty()) {
        openFile(file_path.string());
      }
    }
    if (current_) {
      ImGui::SameLine();
      if (ImGui::Button("Save") || playground_.getFileDialog().isActive()) {
        // TODO: Remove file dialog, save only to fixed folder in workspace
        std::filesystem::path file_path;
        Uvelon::FileWizard::FilterItem filter[1] = {
            {"Camera Path File", "csv"}};
        bool result = playground_.getFileDialog().save(
            "cam_path_save", file_path, filter, 1, "camera_path.csv",
            UVELON_FILE_WORKSPACE(""));
        if (result && !file_path.empty()) {
          current_->toFile(file_path.string());
        }
      }
    }
    ImGui::Separator();

    if (current_) {
      if (ImGui::Checkbox("Hide UI", &hide_ui_)) {
        if (in_playback_) playground_.getUIShow() = !hide_ui_;
      }
      if (ImGui::Button("Play")) startAnimation();
      ImGui::SameLine();
      if (ImGui::Button("Stop")) stopAnimation();
      ImGui::Separator();

      if (ImGui::BeginCombo("Keyframe",
                            selected_keyframe_ > -1
                                ? std::to_string(selected_keyframe_).c_str()
                                : "")) {
        for (int i = 0; i < current_->keyframes_.size(); i++) {
          bool selected = selected_keyframe_ == i;
          if (ImGui::Selectable(std::to_string(i).c_str(), &selected)) {
            selected_keyframe_ = i;
          }
        }
        ImGui::EndCombo();
      }
      if (selected_keyframe_ > -1) {
        ImGui::InputFloat(
            "Time (offset) [s]",
            &current_->keyframes_[selected_keyframe_].time_offset);
        if (ImGui::Button("Up")) moveKeyframe(true);
        ImGui::SameLine();
        if (ImGui::Button("Down")) moveKeyframe(false);
        ImGui::SameLine();
        if (ImGui::Button("Delete")) deleteKeyframe();
      }

      if (ImGui::Button("Add")) addKeyframe();
    }
  }
  ImGui::End();
}

void CameraPathUI::openFile(const std::string& path) {
  stopAnimation();
  current_ =
      std::make_unique<CameraPath>(CameraPath::fromFile(std::string(path)));
  selected_keyframe_ = static_cast<int>(current_->keyframes_.size()) - 1;
}

float CameraPath::consume(std::string& line) {
  size_t pos = line.find(",");
  float result = strtof(line.substr(0, pos).c_str(), nullptr);
  line = line.substr(pos + 1);
  return result;
}

CameraPath CameraPath::fromFile(const std::string& path) {
  CameraPath result;

  Uvelon::MappedFile file(path);
  for (std::string line; std::getline(*file.stream, line);) {
    if (line.size() > 3) {
      KeyFrame keyframe;
      keyframe.time_offset = consume(line);
      for (int i = 0; i < 3; i++) keyframe.translation[i] = consume(line);
      for (int i = 0; i < 4; i++) keyframe.rotation[i] = consume(line);
      result.keyframes_.push_back(keyframe);
    }
  }

  return result;
}

void CameraPath::toFile(const std::string& path) {
  std::ofstream file(path, std::ios::out | std::ios::trunc);
  if (file.is_open()) {
    for (KeyFrame& keyframe : keyframes_) {
      file << std::to_string(keyframe.time_offset);
      for (int i = 0; i < 3; i++) {
        file << ",";
        file << std::to_string(keyframe.translation[i]);
      }
      for (int i = 0; i < 4; i++) {
        file << ",";
        file << std::to_string(keyframe.rotation[i]);
      }
      file << "\n";
    }
    file.close();
  } else {
    UVELON_LOG_WARNING("Could not open file: " + path);
  }
}

std::shared_ptr<Uvelon::Animation> CameraPath::toAnimation(
    std::shared_ptr<Uvelon::Node> camera_node) {
  std::string name = "UVELON_CAMERA_PATH";
  std::vector<Uvelon::Animation::Sampler> samplers;
  std::vector<Uvelon::Animation::Channel> channels;

  Uvelon::BufferCPU buffer_time(5126, keyframes_.size() * sizeof(float));
  Uvelon::BufferCPU buffer_translation(5126,
                                       keyframes_.size() * sizeof(glm::vec3));
  Uvelon::BufferCPU buffer_rotation(5126,
                                    keyframes_.size() * sizeof(glm::quat));
  float time = 0.0f;
  for (uint32_t i = 0; i < keyframes_.size(); i++) {
    auto& keyframe = keyframes_[i];
    buffer_time.at<float>(i) = (time += keyframe.time_offset);
    for (uint32_t j = 0; j < 3; j++)
      buffer_translation.at<float>(i * 3 + j) = keyframe.translation[j];

    for (uint32_t j = 0; j < 4; j++)
      buffer_rotation.at<float>(i * 4 + j) = keyframe.rotation[j];
  }

  samplers.push_back({Uvelon::BufferCPU(buffer_time),
                      Uvelon::Animation::Sampler::Interpolation::LINEAR,
                      std::move(buffer_translation), 3, 5126});
  samplers.push_back({std::move(buffer_time),
                      Uvelon::Animation::Sampler::Interpolation::LINEAR,
                      std::move(buffer_rotation), 4, 5126});
  channels.push_back({0, std::weak_ptr<Uvelon::Node>(camera_node),
                      Uvelon::Animation::Channel::Path::TRANSLATION});
  channels.push_back({1, std::weak_ptr<Uvelon::Node>(camera_node),
                      Uvelon::Animation::Channel::Path::ROTATION});

  return std::make_shared<Uvelon::Animation>(name, std::move(samplers),
                                             std::move(channels));
}

void CameraPathUI::moveKeyframe(bool up) {
  if (up) {
    if (selected_keyframe_ > 0) {
      CameraPath::KeyFrame temp = current_->keyframes_[selected_keyframe_];
      current_->keyframes_[selected_keyframe_] =
          current_->keyframes_[selected_keyframe_ - 1];
      current_->keyframes_[selected_keyframe_ - 1] = temp;
      selected_keyframe_--;
    }
  } else {
    if (selected_keyframe_ < current_->keyframes_.size() - 1) {
      CameraPath::KeyFrame temp = current_->keyframes_[selected_keyframe_];
      current_->keyframes_[selected_keyframe_] =
          current_->keyframes_[selected_keyframe_ + 1];
      current_->keyframes_[selected_keyframe_ + 1] = temp;
      selected_keyframe_++;
    }
  }
}

void CameraPathUI::addKeyframe() {
  std::shared_ptr<Uvelon::Node> camera;
  if (playground_.getScene()->getActiveCamera(camera)) {
    CameraPath::KeyFrame frame;
    frame.translation = camera->getTransform().getTranslation();
    frame.rotation = camera->getTransform().getRotation();

    if (selected_keyframe_ > -1) {
      frame.time_offset = 1.0f;

      // Insert
      current_->keyframes_.insert(
          current_->keyframes_.begin() + selected_keyframe_ + 1, frame);

      selected_keyframe_++;
    } else {
      // First keyframe
      frame.time_offset = 0.0f;
      current_->keyframes_.push_back(frame);
      selected_keyframe_ = 0;
    }
  }
}

void CameraPathUI::deleteKeyframe() {
  if (selected_keyframe_ > -1) {
    current_->keyframes_.erase(current_->keyframes_.begin() +
                               selected_keyframe_);
    if (selected_keyframe_ >= current_->keyframes_.size()) selected_keyframe_--;
  }
}

void CameraPathUI::startAnimation() {
  std::shared_ptr<Uvelon::Node> camera;
  if (playground_.getScene()->getActiveCamera(camera) &&
      !current_->keyframes_.empty()) {
    stopAnimation();
    auto animation = current_->toAnimation(camera);
    animation->setActive(true);
    playground_.getScene()->getAnimations().push_back(animation);
    inserted_animation_ = playground_.getScene()->getAnimations().back();
    camera_ = camera;

    playground_.getCamLocked() = true;
    playground_.getUIShow() = !hide_ui_;
    in_playback_ = true;

    if (Uvelon::CVarManager::getVar("gsplat.temp_file_out.with_path", false)
            .getValue<bool>()) {
      Uvelon::CVarManager::setVar("gsplat.temp_file_out", true);
    }
  }
}

void CameraPathUI::stopAnimation() {
  std::vector<std::shared_ptr<Uvelon::Animation>>& animations =
      playground_.getScene()->getAnimations();
  if (inserted_animation_)
    animations.erase(
        std::remove(animations.begin(), animations.end(), inserted_animation_),
        animations.end());
  inserted_animation_ = nullptr;
  camera_.reset();
  playground_.getCamLocked() = false;
  playground_.getUIShow() = true;
  in_playback_ = false;

  if (Uvelon::CVarManager::getVar("gsplat.temp_file_out.with_path", false)
          .getValue<bool>()) {
    Uvelon::CVarManager::setVar("gsplat.temp_file_out", false);
  }
}