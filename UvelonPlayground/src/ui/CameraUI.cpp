#include "ui/CameraUI.hpp"

#include <imgui.h>
#include <imgui_stdlib.h>

#include "scene/Node.hpp"
#include "scene/Scene.hpp"

void CameraUI::draw() {
  std::shared_ptr<Uvelon::Node> active_camera;
  bool camera_active = playground_.getScene()->getActiveCamera(active_camera);

  std::vector<std::shared_ptr<Uvelon::Node>> camera_nodes;
  playground_.getScene()->addCameraNodes(camera_nodes);

  // Active camera's name or index
  std::string active_name = "None";
  if (camera_active) {
    if (active_camera->getName().empty()) {
      for (int i = 0; i < camera_nodes.size(); i++) {
        if (camera_nodes[i] == active_camera) {
          active_name = std::to_string(i);
          break;
        }
      }
    } else {
      active_name = active_camera->getName();
    }
  }

  if (ImGui::Begin("Cameras")) {
    ImGui::Checkbox("Fly", &playground_.getCamFly());
    if (playground_.getCamFly()) {
      ImGui::SliderFloat("Fly Speed", &playground_.getCamFlySpeed(), 0.01f,
                         1.0f);
    }
    ImGui::Separator();
    if (ImGui::BeginCombo("Active", active_name.c_str())) {
      int index = 0;
      for (auto node : camera_nodes) {
        bool selected = node == active_camera;
        bool empty = node->getName().empty();
        if (ImGui::Selectable(
                empty ? std::to_string(index).c_str() : node->getName().c_str(),
                &selected)) {
          playground_.getScene()->setActiveCamera(node);
        }
        index++;
      }
      ImGui::EndCombo();
    }

    // Set gui_cam_node to display values as disabled
    if (!gui_cam_edit_ && !gui_cam_new_) gui_cam_node_ = active_camera;

    if (camera_active || gui_cam_edit_ || gui_cam_new_) {
      ImGui::BeginDisabled(!gui_cam_edit_ && !gui_cam_new_);
      bool projection_chosen = gui_cam_node_->getCamera().operator bool();
      bool perspective =
          projection_chosen && gui_cam_node_->getCamera()->getType() ==
                                   Uvelon::Camera::CameraType::PERSPECTIVE;
      if (ImGui::BeginCombo("Projection",
                            projection_chosen
                                ? (perspective ? "Perspective" : "Orthographic")
                                : "")) {
        if (ImGui::Selectable("Perspective", &perspective))
          gui_cam_node_->setCamera(std::make_shared<Uvelon::PerspectiveCamera>(
              static_cast<float>(playground_.getWindow().getWidth()) /
                  playground_.getWindow().getHeight(),
              DEFAULT_FOV, DEFAULT_NEAR, DEFAULT_FAR));

        bool orthographic = projection_chosen && !perspective;
        if (ImGui::Selectable("Orthographic", &orthographic))
          gui_cam_node_->setCamera(std::make_shared<Uvelon::OrthographicCamera>(
              2.0f,
              2.0f / (static_cast<float>(playground_.getWindow().getWidth()) /
                      playground_.getWindow().getHeight()),
              DEFAULT_NEAR, DEFAULT_FAR));
        ImGui::EndCombo();
      }

      if (projection_chosen) {
        bool modified_camera = false;
        if (perspective) {
          auto perspective_cam =
              std::static_pointer_cast<Uvelon::PerspectiveCamera>(
                  gui_cam_node_->getCamera());
          modified_camera |= ImGui::InputFloat(
              "Aspect Ratio", perspective_cam->modifyAspectRatio());
          modified_camera |= ImGui::SliderAngle(
              "FOV Y", perspective_cam->modifyYFOV(), 0.0f, 180.0f);
          modified_camera |=
              ImGui::InputFloat("Near Z", perspective_cam->modifyZNear());
          modified_camera |=
              ImGui::InputFloat("Far Z", perspective_cam->modifyZFar());
        } else {
          auto orthographic_cam =
              std::static_pointer_cast<Uvelon::OrthographicCamera>(
                  gui_cam_node_->getCamera());
          modified_camera |=
              ImGui::InputFloat("Mag X", orthographic_cam->modifyXMag());
          modified_camera |=
              ImGui::InputFloat("Mag Y", orthographic_cam->modifyYMag());
          modified_camera |=
              ImGui::InputFloat("Near Z", orthographic_cam->modifyZNear());
          modified_camera |=
              ImGui::InputFloat("Far Z", orthographic_cam->modifyZFar());
        }
        if (modified_camera) gui_cam_node_->getCamera()->computeProjection();

        if (gui_cam_edit_ || gui_cam_new_) {
          if (ImGui::Button(gui_cam_edit_ ? "Done" : "Create")) {
            if (gui_cam_new_) {
              auto node = std::shared_ptr<Uvelon::Node>(gui_cam_node_);
              Uvelon::Node::attach(playground_.getScene()->getRoot(), node);
            }

            playground_.getScene()->setActiveCamera(gui_cam_node_);

            gui_cam_new_ = false;
            gui_cam_edit_ = false;
            gui_cam_node_ = nullptr;
          }

          if (gui_cam_new_) ImGui::SameLine();
        }
      }
      ImGui::EndDisabled();
    }

    if (gui_cam_new_) {
      if (ImGui::Button("Cancel")) {
        gui_cam_new_ = false;
        gui_cam_node_.reset();
      }
    } else if (!gui_cam_new_ && !gui_cam_edit_) {
      if (ImGui::Button("New")) {
        gui_cam_node_ = std::make_shared<Uvelon::Node>();
        gui_cam_new_ = true;
        gui_cam_edit_ = false;
      }
      ImGui::SameLine();
      if (camera_active) {
        if (ImGui::Button("Edit")) {
          gui_cam_node_ = active_camera;
          gui_cam_edit_ = true;
        }
        ImGui::SameLine();
        if (ImGui::Button("Delete")) {
          gui_cam_edit_ = false;
          gui_cam_new_ = false;
          gui_cam_node_ = nullptr;
          active_camera->getParent()->detachChild(active_camera);
          // Enable another camera
          for (auto& cam : camera_nodes) {
            if (cam != active_camera) {
              playground_.getScene()->setActiveCamera(cam);
              break;
            }
          }
        }
      }
    }
  }
  ImGui::End();
}