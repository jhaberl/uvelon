#include "ui/FrustumVisUI.hpp"

#include <imgui.h>
#include <imgui_stdlib.h>

#include "core/CVarManager.hpp"
#include "scene/Node.hpp"
#include "scene/Scene.hpp"

void FrustumVisUI::update(float delta) {
  bool& enable = Uvelon::CVarManager::getVar("gaussian.vmem.frustum_vis", false)
                     .getValue<bool>();
  enable = enable_;
  if (enable) {
    if (auto selected = selected_.lock()) {
      glm::mat4 view = selected->getCamera()->computeViewMatrix();
      const glm::mat4& projection =
          selected->getCamera()->getProjectionMatrix();
      for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
          Uvelon::CVarManager::setVar("gaussian.vmem.frustum_vis.view." +
                                          std::to_string(i) + "." +
                                          std::to_string(j),
                                      static_cast<double>(view[i][j]), false);
          Uvelon::CVarManager::setVar(
              "gaussian.vmem.frustum_vis.projection." + std::to_string(i) +
                  "." + std::to_string(j),
              static_cast<double>(projection[i][j]), false);
        }
      }
    } else {
      enable = false;
    }
  }
}

void FrustumVisUI::draw() {
  if (playground_.getScene()->getRoot()->getGaussianSplats() &&
      playground_.getScene()
          ->getRoot()
          ->getGaussianSplats()
          ->getHandle()
          ->proxy_mesh) {
    if (ImGui::Begin("View Frustum Visualization")) {
      std::string selected_name;
      if (auto selected = selected_.lock()) {
        if (!selected->getName().empty()) {
          selected_name = selected->getName();
        } else {
          playground_.getScene()->addCameraNodes(camera_nodes_);
          for (int i = 0; i < camera_nodes_.size(); i++) {
            if (camera_nodes_[i] == selected) {
              selected_name = std::to_string(i);
              break;
            }
          }
        }
      }

      if (ImGui::BeginCombo("Camera", selected_name.c_str())) {
        if (camera_nodes_.empty())
          playground_.getScene()->addCameraNodes(camera_nodes_);

        int index = 0;
        for (auto camera_node : camera_nodes_) {
          if (ImGui::Selectable(std::to_string(index).c_str(),
                                camera_node == selected_.lock())) {
            selected_ = camera_node;
          }
          index++;
        }
        ImGui::EndCombo();
      }
      camera_nodes_.clear();

      ImGui::Checkbox("Visualize", &enable_);
    }
    ImGui::End();
  }
}