#include "UvelonPlayground.hpp"

#include <SDL3/SDL_main.h>
#include <imgui.h>
#include <imgui_stdlib.h>

#include "core/CVarManager.hpp"
#include "core/Input.hpp"
#include "rendering/RenderData.hpp"
#include "rendering/Renderer.hpp"
#include "scene/Camera.hpp"
#include "scene/GSplatLoader.hpp"
#include "scene/Loader.hpp"
#include "scene/Node.hpp"
#include "ui/AnimationUI.hpp"
#include "ui/CameraPathUI.hpp"
#include "ui/CameraUI.hpp"
#include "ui/DebugUI.hpp"
#include "ui/FrustumVisUI.hpp"
#include "ui/GaussianSliceUI.hpp"
#include "ui/LightUI.hpp"
#include "ui/SceneUI.hpp"
#include "utils/Instrumentation.hpp"
#include "utils/Logger.hpp"

int main(int argc, char* argv[]) {
  Uvelon::Application::Config config;
  config.title = "Uvelon Playground";
  config.width = 900;
  config.height = 600;
  config.maximized = true;
  config.renderer_config.use_validation_layers = true;

  UvelonPlayground playground(config);
  playground.run();

  return 0;
}

UvelonPlayground::UvelonPlayground(Uvelon::Application::Config& config)
    : Uvelon::Application(config) {
  UVELON_LOG_INFO("Starting Uvelon Playground");
}

UvelonPlayground::~UvelonPlayground() {}

void UvelonPlayground::init() {
  ui_.push_back(std::make_unique<SceneUI>(*this));
  ui_.push_back(std::make_unique<CameraUI>(*this));
  ui_.push_back(std::make_unique<GaussianSliceUI>(*this));
  ui_.push_back(std::make_unique<CameraPathUI>(*this));
  ui_.push_back(std::make_unique<AnimationUI>(*this));
  ui_.push_back(std::make_unique<LightUI>(*this));
  ui_.push_back(std::make_unique<DebugUI>(*this));
  ui_.push_back(std::make_unique<FrustumVisUI>(*this));
}

void UvelonPlayground::update(float delta) {
  // Create a default perspective camera on pressing "C"
  if (Uvelon::Input::isKeyDown(SDL_SCANCODE_C)) {
    if (!cam_default_create_down_) {
      createDefaultCamera();
    }
    cam_default_create_down_ = true;
  } else {
    cam_default_create_down_ = false;
  }

  if (Uvelon::Input::isKeyDown(SDL_SCANCODE_R)) {
    if (!hot_reload_down_) {
      Uvelon::CVarManager::getVar("shaders.hot_reload", false)
          .getValue<bool>() = true;
    }
    hot_reload_down_ = true;
  } else {
    hot_reload_down_ = false;
  }

  // Switch between fly and orbit mode
  if (Uvelon::Input::isKeyDown(SDL_SCANCODE_E)) {
    if (!cam_mode_down_) cam_fly_ = !cam_fly_;
    cam_mode_down_ = true;
  } else {
    cam_mode_down_ = false;
  }

  if (Uvelon::Input::isKeyDown(SDL_SCANCODE_ESCAPE)) {
    ui_show_ = true;
  }

  std::shared_ptr<Uvelon::Node> camera_node;
  if (scene_->getActiveCamera(camera_node)) {
    if (cam_fly_ && !cam_was_fly_) {
      // Switched from orbit to fly mode, set translation to where it was
      const glm::vec3& translation =
          camera_node->getTransform().getTranslation();
      cam_fly_pos_[0] = translation.x;
      cam_fly_pos_[1] = translation.y;
      cam_fly_pos_[2] = translation.z;
    }
    cam_was_fly_ = cam_fly_;

    if (Uvelon::Input::isMouseButtonDown(SDL_BUTTON_RIGHT)) {
      cam_locked_ = false;

      float mouse_pos[2];
      Uvelon::Input::getMousePosition(mouse_pos[0], mouse_pos[1]);

      if (cam_last_mouse[0] != -1.0f) {
        cam_rotate_[0] += (mouse_pos[0] - cam_last_mouse[0]) / 200.0f;
        cam_rotate_[1] += (mouse_pos[1] - cam_last_mouse[1]) / 200.0f;

        while (cam_rotate_[0] > TWO_PI) cam_rotate_[0] -= TWO_PI;
        while (cam_rotate_[0] < 0) cam_rotate_[0] += TWO_PI;
        cam_rotate_[1] = std::max(-PI_HALF + 0.01f,
                                  std::min(PI_HALF - 0.01f, cam_rotate_[1]));
      }
      memcpy(&cam_last_mouse, &mouse_pos, sizeof(int) * 2);

      if (cam_fly_) {
        int forward = 0, right = 0, up = 0;
        forward += Uvelon::Input::isKeyDown(SDL_SCANCODE_W);
        forward -= Uvelon::Input::isKeyDown(SDL_SCANCODE_S);
        right += Uvelon::Input::isKeyDown(SDL_SCANCODE_D);
        right -= Uvelon::Input::isKeyDown(SDL_SCANCODE_A);
        up -= Uvelon::Input::isKeyDown(SDL_SCANCODE_SPACE);
        up += Uvelon::Input::isKeyDown(SDL_SCANCODE_LCTRL);

        if (forward || right || up) {
          auto rotation = glm::eulerAngleY(cam_rotate_[0]) *
                          glm::eulerAngleX(-cam_rotate_[1]);
          glm::vec3 forward_vec =
              glm::vec3(rotation[2]) * static_cast<float>(forward);
          glm::vec3 right_vec =
              glm::vec3(rotation[0]) * static_cast<float>(right);
          glm::vec3 up_vec = glm::vec3(rotation[1]) * static_cast<float>(up);

          glm::vec3 move_vec =
              glm::normalize(forward_vec + right_vec + up_vec) *
              (cam_fly_speed_ * (delta / 16.66f));

          if (Uvelon::Input::isKeyDown(SDL_SCANCODE_LSHIFT)) move_vec *= 2.0f;

          cam_fly_pos_[0] += move_vec[0];
          cam_fly_pos_[1] += move_vec[1];
          cam_fly_pos_[2] += move_vec[2];
        }
      }
    } else {
      cam_last_mouse[0] = -1.0f;
    }

    if (!cam_locked_) {
      if (cam_fly_) {
        camera_node->getTransform().setTranslation(
            glm::vec3(cam_fly_pos_[0], cam_fly_pos_[1], cam_fly_pos_[2]));
        camera_node->getTransform().setRotation(
            glm::eulerAngleY(cam_rotate_[0]) *
            glm::eulerAngleX(-cam_rotate_[1]));
      } else {
        float x_scroll, y_scroll;
        Uvelon::Input::getScroll(x_scroll, y_scroll);
        cam_radius_ -= y_scroll / 20.0f;
        cam_radius_ = std::max(0.01f, cam_radius_);

        camera_node->getCamera()->lookAt(
            glm::vec3(
                cos(-cam_rotate_[0]) * cos(-cam_rotate_[1]) * cam_radius_,
                sin(-cam_rotate_[1]) * cam_radius_,
                sin(-cam_rotate_[0]) * cos(-cam_rotate_[1]) * cam_radius_),
            glm::vec3(0.0f));
      }
    }
  }

  for (auto& ui : ui_) {
    ui->update(delta);
  }
}

void UvelonPlayground::drawUI(float delta) {
  if (ui_show_) {
    Uvelon::CVarManager::draw();
    for (auto& ui : ui_) {
      ui->draw();
    }
    file_dialog_.draw();
  }
}

void UvelonPlayground::createDefaultCamera() {
  auto camera_node = std::make_shared<Uvelon::Node>();
  camera_node->setCamera(std::make_shared<Uvelon::PerspectiveCamera>(
      static_cast<float>(window_.getWidth()) / window_.getHeight(), DEFAULT_FOV,
      DEFAULT_NEAR, DEFAULT_FAR));

  Uvelon::Node::attach(scene_->getRoot(), camera_node);
  scene_->setActiveCamera(camera_node);
}
