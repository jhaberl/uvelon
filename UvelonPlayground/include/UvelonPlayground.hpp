#pragma once

#define NOMINMAX

#include <memory>

#include "core/Application.hpp"
#include "rendering/Window.hpp"
#include "utils/FileDialog.hpp"

#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#define GLM_FORCE_LEFT_HANDED
#include <glm/glm.hpp>
#include <glm/gtx/euler_angles.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtx/string_cast.hpp>

constexpr float PI = glm::pi<float>();
constexpr float PI_HALF = glm::pi<float>() / 2;
constexpr float TWO_PI = glm::pi<float>() * 2;

constexpr float DEFAULT_FOV = glm::radians(90.0f);
constexpr float DEFAULT_NEAR = 0.01f, DEFAULT_FAR = 1000.0f;

class UI;

class UvelonPlayground : public Uvelon::Application {
 public:
  UvelonPlayground(Uvelon::Application::Config& config);
  virtual ~UvelonPlayground();

  virtual void init();
  virtual void update(float delta);
  virtual void drawUI(float delta);

  void setScene(std::shared_ptr<Uvelon::Scene> scene) { scene_ = scene; }
  std::shared_ptr<Uvelon::Scene> getScene() { return scene_; }
  void createDefaultCamera();
  bool& getCamFly() { return cam_fly_; }
  float* getCamFlyPos() { return cam_fly_pos_; }
  float& getCamFlySpeed() { return cam_fly_speed_; }
  Uvelon::Window& getWindow() { return window_; }
  bool& getCamLocked() { return cam_locked_; }
  bool& getUIShow() { return ui_show_; }
  Uvelon::FileDialog& getFileDialog() { return file_dialog_; }

 private:
  // Camera controls
  float cam_last_mouse[2] = {-1.0f, -1.0f};
  float cam_rotate_[2] = {0.0f, 0.0f};
  float cam_radius_{1.0f};
  float cam_fly_pos_[3];
  bool cam_fly_{false}, cam_was_fly_{false};
  float cam_fly_speed_ = 0.1f;
  bool cam_default_create_down_{false}, cam_mode_down_{false};
  bool cam_locked_{false};  // Don't update camera to allow outside control

  // Hot reload
  bool hot_reload_down_{false};

  bool ui_show_{true};

  std::vector<std::unique_ptr<UI>> ui_;

  Uvelon::FileDialog file_dialog_;
};