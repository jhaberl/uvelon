#pragma once
#include "UI.hpp"

class GaussianSliceUI : public UI {
 public:
  GaussianSliceUI(UvelonPlayground& playground) : UI(playground) {}
  virtual void draw() final;
};