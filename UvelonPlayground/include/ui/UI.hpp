#pragma once
#include "../UvelonPlayground.hpp"

class UI {
 public:
  UI(UvelonPlayground& playground) : playground_(playground) {}
  virtual ~UI() {}

  virtual void update(float delta) {}
  virtual void draw() = 0;

 protected:
  UvelonPlayground& playground_;
};