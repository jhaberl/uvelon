#pragma once

#include <fstream>

#include "UI.hpp"

struct ImPlotPoint;

class DebugUI : public UI {
 public:
  DebugUI(UvelonPlayground& playground) : UI(playground) {}
  virtual void update(float delta) final;
  virtual void draw() final;

 private:
  struct HistoryData {
    const uint32_t max_size;

    std::vector<float> data;
    uint32_t next{0};

    HistoryData(uint32_t max_size) : max_size(max_size) {
      data.resize(max_size);
    }
    HistoryData() : HistoryData(1000) {}
    void addEntry(float entry);
    float getLast();
    float getAverage();
  };

  HistoryData hist_fps_;

  // Gsplat virtual memory
  HistoryData hist_gsplat_inmem_pages_;
  HistoryData hist_gsplat_copy_;
  HistoryData hist_gsplat_lod_thresh_;
  HistoryData hist_gsplat_missing_;

  std::ofstream temp_file_out_;

  void plotLine(const std::string& label, HistoryData& history,
                float min = 3.4028235E38F, float max = 3.4028235E38F);
  void plotStackedBar(const std::string& title, const char** labels,
                      const float* values, int items,
                      const std::string& label_x, const std::string& label_y,
                      double x_min, double x_max, double y_min, double y_max);
  static ImPlotPoint getHistoryEntryImPlot(int idx, void* data);
  static float getHistoryEntry(void* data, int idx);

  void createOutFile();
};