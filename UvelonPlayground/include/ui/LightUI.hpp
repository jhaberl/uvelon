#pragma once
#include "UI.hpp"

class LightUI : public UI {
 public:
  LightUI(UvelonPlayground& playground) : UI(playground) {}
  virtual void draw() final;

 private:
  std::vector<std::shared_ptr<Uvelon::Node>> light_nodes;
  std::weak_ptr<Uvelon::Node> selected_;

  // This may be wrong, it's just for display
  int selected_index_;
  int num_lights_ = 0;
};