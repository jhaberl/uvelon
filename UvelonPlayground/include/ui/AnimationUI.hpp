#pragma once
#include "UI.hpp"

namespace Uvelon {
class Animation;
}

class AnimationUI : public UI {
 public:
  AnimationUI(UvelonPlayground& playground) : UI(playground) {}
  virtual void draw() final;

 private:
  std::weak_ptr<Uvelon::Animation> selected_;
};