#pragma once
#include "UI.hpp"

#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#define GLM_FORCE_LEFT_HANDED
#include <glm/glm.hpp>
#include <memory>
#include <string>

namespace Uvelon {
class Animation;
}

class CameraPath {
  friend class CameraPathUI;

 public:
  struct KeyFrame {
    float time_offset;
    glm::vec3 translation;
    glm::quat rotation;
  };

  static float consume(std::string& line);

  static CameraPath fromFile(const std::string& path);
  void toFile(const std::string& path);

  std::shared_ptr<Uvelon::Animation> toAnimation(
      std::shared_ptr<Uvelon::Node> camera_node);

 private:
  std::vector<KeyFrame> keyframes_;
};

class CameraPathUI : public UI {
 public:
  CameraPathUI(UvelonPlayground& playground);

  virtual void update(float delta) final;
  virtual void draw() final;

 private:
  std::unique_ptr<CameraPath> current_;
  int selected_keyframe_ = -1;

  bool in_playback_{false};
  bool hide_ui_{false};
  std::shared_ptr<Uvelon::Animation> inserted_animation_;
  std::weak_ptr<Uvelon::Node> camera_;

  void moveKeyframe(bool up);
  void addKeyframe();
  void deleteKeyframe();

  void openFile(const std::string& path);

  void startAnimation();
  void stopAnimation();
};