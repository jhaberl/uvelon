#pragma once
#include "UI.hpp"

class CameraUI : public UI {
 public:
  CameraUI(UvelonPlayground& playground) : UI(playground) {}
  virtual void draw() final;

 private:
  bool gui_cam_new_{false}, gui_cam_edit_{false};
  std::shared_ptr<Uvelon::Node> gui_cam_node_;
};