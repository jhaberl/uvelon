#pragma once
#include "UI.hpp"

namespace Uvelon {
class Node;
}

class FrustumVisUI : public UI {
 public:
  FrustumVisUI(UvelonPlayground& playground) : UI(playground) {}

  void update(float delta) override;
  void draw() override;

 private:
  std::vector<std::shared_ptr<Uvelon::Node>> camera_nodes_;
  std::weak_ptr<Uvelon::Node> selected_;

  bool enable_{false};
};