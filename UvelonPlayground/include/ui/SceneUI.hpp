#pragma once
#include <filesystem>
#include <string>
#include <vector>

#include "UI.hpp"

class SceneUI : public UI {
 public:
  SceneUI(UvelonPlayground& playground) : UI(playground) {}
  virtual void draw() final;

 private:
  std::filesystem::path model_selected_file_;
  std::vector<std::string> model_selected_file_scenes_;
  int model_selected_scene_;
};