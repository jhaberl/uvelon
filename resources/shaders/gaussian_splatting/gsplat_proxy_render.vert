#version 450
#include "../global_ubo.glsl"

layout (location = 0) in vec3 position;

layout (push_constant) uniform PushConstant {
  mat4 model;
} pc;

void main() {
  gl_Position = ubo.projection * ubo.view * pc.model * vec4(position, 1.0);
}