#version 430

layout (location = 0) flat in vec2 size;
layout (location = 1) flat in vec4 color_opacity;
layout (location = 2) flat in mat2 inv_cov_2d;
#ifdef UVELON_GSPLAT_GEOMETRY
layout (location = 4) in vec2 quad_coord;
#endif

layout (location = 0) out vec4 out_albedo;

void main() {
#ifdef UVELON_GSPLAT_GEOMETRY
  vec2 center_dist = (quad_coord - vec2(0.5)) * size;
#else
  vec2 center_dist = (gl_PointCoord - vec2(0.5)) * size;
#endif
  float sigma = -0.5 * dot(center_dist, inv_cov_2d * center_dist);
  if (sigma >= 0.0) discard;
  float alpha = clamp(color_opacity.a * exp(sigma), 0.0, 1.0);
  if (alpha < 1.0 / 255.0) discard;
  out_albedo = vec4(color_opacity.rgb, alpha);
}