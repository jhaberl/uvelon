#version 430

layout (location = 0) flat in float size;
layout (location = 1) flat in mat2 in_f;

layout (location = 0) out vec4 out_albedo;

void main() {  
  vec2 center_dist = (gl_PointCoord - vec2(0.5)) * size;
  // https://en.wikipedia.org/wiki/Ellipse#General_ellipse_2 -> Implicit representation
  float det1 = determinant(mat2(center_dist, in_f[1]));
  float det2 = determinant(mat2(in_f[0], center_dist));
  float det3 = determinant(mat2(in_f[0], in_f[1]));
  float ellipse = det1 * det1 + det2 * det2 - det3 * det3;
  //vec2 scaled = in_f * center_dist;
  if (ellipse > 0) discard;
  out_albedo = vec4(1.0);
}