layout (set = 1, binding = 0) uniform ComputeData {
  uint num_splats;
  uint elements_per_wg;
  // For vmem:
  // If not zero,, indicates present physical pages are marked in indices2
  uint page_size;
} cd;

#define NUM_COMPONENTS 59
layout (set = 1, binding = 1) readonly buffer GaussianBuffer {
  float gaussian_buffer[];
};
layout (set = 1, binding = 2) buffer Indices {
  uint indices[];
};
layout (set = 1, binding = 3) buffer Indices2 {
  uint indices2[];
};
layout (set = 1, binding = 4) buffer Histograms {
  uint histograms[];
};

layout (push_constant) uniform PushConstant {
  mat4 model;
  uint iteration;
} pc;