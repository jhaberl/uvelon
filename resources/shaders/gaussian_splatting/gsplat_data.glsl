// From reference implementation
#define SH_C0 0.28209479177387814
#define SH_C1 0.4886025119029199
#define SH_C2_0 1.0925484305920792
#define SH_C2_1 -1.0925484305920792
#define SH_C2_2 0.31539156525252005
#define SH_C2_3 -1.0925484305920792
#define SH_C2_4 0.5462742152960396
#define SH_C3_0 -0.5900435899266435
#define SH_C3_1 2.890611442640554
#define SH_C3_2 -0.4570457994644658
#define SH_C3_3 0.3731763325901154
#define SH_C3_4 -0.4570457994644658
#define SH_C3_5 1.445305721320277
#define SH_C3_6 -0.5900435899266435

#define NUM_COMPONENTS 59
layout (set = 1, binding = 0) readonly buffer GaussianBuffer {
  float gaussian_buffer[];
};

#define OFFSET_GAUSSIANS gl_VertexIndex * NUM_COMPONENTS

#define OFFSET_POSITION 0
#define OFFSET_SH 3
#define OFFSET_OPACITY 51
#define OFFSET_SCALE 52
#define OFFSET_ROTATION 55

#define position vec3(gaussian_buffer[OFFSET_GAUSSIANS + OFFSET_POSITION + 0], \
  gaussian_buffer[OFFSET_GAUSSIANS + OFFSET_POSITION + 1], \
  gaussian_buffer[OFFSET_GAUSSIANS + OFFSET_POSITION + 2])
#define sh(index) vec3(gaussian_buffer[OFFSET_GAUSSIANS + OFFSET_SH + index * 3 + 0], \
  gaussian_buffer[OFFSET_GAUSSIANS + OFFSET_SH + index * 3 + 1], \
  gaussian_buffer[OFFSET_GAUSSIANS + OFFSET_SH + index * 3 + 2])
#define opacity gaussian_buffer[OFFSET_GAUSSIANS + OFFSET_OPACITY]
#define scale vec3(gaussian_buffer[OFFSET_GAUSSIANS + OFFSET_SCALE + 0], \
  gaussian_buffer[OFFSET_GAUSSIANS + OFFSET_SCALE + 1], \
  gaussian_buffer[OFFSET_GAUSSIANS + OFFSET_SCALE + 2])
#define rotation vec4(gaussian_buffer[OFFSET_GAUSSIANS + OFFSET_ROTATION + 0], \
  gaussian_buffer[OFFSET_GAUSSIANS + OFFSET_ROTATION + 1], \
  gaussian_buffer[OFFSET_GAUSSIANS + OFFSET_ROTATION + 2], \
  gaussian_buffer[OFFSET_GAUSSIANS + OFFSET_ROTATION + 3])
