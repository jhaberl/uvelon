#version 450

layout (location = 0) out uint out_pageid;

layout (set = 1, binding = 0) readonly buffer FacePageIDMap {
  uint face_pageid_map[];
};

void main() {
  out_pageid = face_pageid_map[gl_PrimitiveID];
}