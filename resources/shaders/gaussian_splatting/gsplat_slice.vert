#version 430
#include "../global_ubo.glsl"
#include "gsplat_data.glsl"

layout (set = 2, binding = 0) uniform GSliceExtra {
  uint width, height;
  float bounds_x, bounds_y;
  float z;
  float min_opacity;
} extra;

layout (push_constant) uniform PushConstant {
  mat4 model;
} pc;

layout (location = 0) flat out float out_size;
layout (location = 1) flat out mat2 out_f;

mat3 quat2rot(vec4 quat) {
  mat3 rot = mat3(
    1.0 - 2.0 * (quat.y * quat.y + quat.z * quat.z), 
      2.0 * (quat.x * quat.y - quat.w * quat.z), 
        2.0 * (quat.x * quat.z + quat.w * quat.y),
		2.0 * (quat.x * quat.y + quat.w * quat.z), 
      1.0 - 2.0 * (quat.x * quat.x + quat.z * quat.z), 
        2.0 * (quat.y * quat.z - quat.w * quat.x),
		2.0 * (quat.x * quat.z - quat.w * quat.y), 
      2.0 * (quat.y * quat.z + quat.w * quat.x), 
        1.0 - 2.0 * (quat.x * quat.x + quat.y * quat.y)
  );

  return rot;
}

void main() {
  if (opacity < extra.min_opacity) {
    gl_Position = vec4(-100.0, -100.0, -100.0, 1.0);
    return;
  }

  mat4 view = mat4(
    1, 0, 0, 0,
    0, 1, 0, 0,
    0, 0, 1, 0,
    0, 0, extra.z, 1
  );
  
  float right = extra.bounds_x / 2, left = -extra.bounds_x / 2;
  float top = extra.bounds_y / 2, bottom = -extra.bounds_y / 2;
  float near = 1.1, far = 0.9;
  mat4 projection = mat4(
    2.0 / (right - left), 0, 0, 0,
    0, 2.0 / (top - bottom), 0, 0,
    0, 0, 1.0 / (far - near), 0,
    -(right + left) / (right - left), -(top + bottom) / (top - bottom), -near / (far - near), 1
  );

  vec4 pre_projection = view * pc.model * vec4(position, 1.0);

  // Intersect splat with plane at z=0
  // https://en.wikipedia.org/wiki/Ellipsoid#Determining_the_ellipse_of_a_plane_section
  // https://mathworld.wolfram.com/HessianNormalForm.html

  mat3 rotmat = transpose(quat2rot(rotation));
  mat3 slice_rot = transpose(rotmat); // Inverse of rotation matrix
  vec3 n = slice_rot * vec3(0, 0, 1);
  float d = pre_projection.z;
  
  // Hessian normal form for plane
  vec3 n_scaled = n * scale;
  vec3 m = normalize(n_scaled);
  float p = d / length(n_scaled);

  if (abs(p) >= 1.0) {
    // No intersection, cull
    gl_Position = vec4(-100.0, -100.0, -100.0, 1.0);
    return;
  }

  // Intersection
  float radius = sqrt(1 - p * p);
  vec3 e0 = p * m;
  vec3 e1, e2;
  if (abs(1.0 - radius) < 0.00001) {
    e1 = vec3(radius, 0, 0);
    e2 = vec3(0, radius, 0);
  } else {
    e1 = radius / sqrt(m.x * m.x + m.y * m.y) * vec3(m.y, -m.x, 0);
    e2 = cross(m, e1);
  }
  // Ellipse center
  vec3 f0 = e0 * scale;
  // Conjugate half diameters
  vec3 f1 = e1 * scale;
  vec3 f2 = e2 * scale;

  // Transform back
  f0 = rotmat * f0;
  f1 = rotmat * f1;
  f2 = rotmat * f2;
  f0.xy += pre_projection.xy;

  // Projection
  gl_Position = projection * vec4(f0.xy, 1.0, 1.0);
  f1.x *= extra.width / extra.bounds_x;
  f1.y *= extra.height / extra.bounds_y;
  f2.x *= extra.width / extra.bounds_x;
  f2.y *= extra.height / extra.bounds_y;

  out_f[0] = f1.xy;
  out_f[1] = f2.xy;
  out_f = inverse(out_f);

  gl_PointSize = out_size = min(150.0, 2.0 * max(length(f1), length(f2)));
}