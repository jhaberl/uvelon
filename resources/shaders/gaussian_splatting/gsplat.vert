#version 430
#include "../global_ubo.glsl"
#include "gsplat_data.glsl"

layout (push_constant) uniform PushConstant {
  mat4 model;
} pc;

layout (location = 0) flat out vec2 out_size;
layout (location = 1) flat out vec4 out_color_opacity;
layout (location = 2) flat out mat2 out_inv_cov_2d;

void main() {
  vec4 pre_projection = ubo.view * pc.model * vec4(position, 1.0);
  gl_Position = ubo.projection * pre_projection;
  gl_Position = vec4(gl_Position.xyz / gl_Position.w, 1.0);

  // Culling
  if (any(greaterThan(abs(gl_Position.xyz), vec3(1.3)))/* || gl_Position.z >= 0.002*/) {
    gl_Position = vec4(-100.0, -100.0, -100.0, 1.0);
    return;
  }

  out_color_opacity.a = opacity;
  
  if (out_color_opacity.a < 1 / 255.0) {
    gl_Position = vec4(-100.0, -100.0, -100.0, 1.0);
    return;
  }

  // Cov3d
  vec3 scale_vec = scale;
  mat3 S = mat3(
    scale_vec.x, 0., 0., 
    0., scale_vec.y, 0., 
    0., 0., scale_vec.z);

  vec4 quat = rotation;
  mat3 R = mat3(
    1.0 - 2.0 * (quat.y * quat.y + quat.z * quat.z), 
      2.0 * (quat.x * quat.y - quat.w * quat.z), 
        2.0 * (quat.x * quat.z + quat.w * quat.y),
		2.0 * (quat.x * quat.y + quat.w * quat.z), 
      1.0 - 2.0 * (quat.x * quat.x + quat.z * quat.z), 
        2.0 * (quat.y * quat.z - quat.w * quat.x),
		2.0 * (quat.x * quat.z - quat.w * quat.y), 
      2.0 * (quat.y * quat.z + quat.w * quat.x), 
        1.0 - 2.0 * (quat.x * quat.x + quat.y * quat.y)
  );
  mat3 M = S * R;
  mat3 cov3d = transpose(M) * M;

  float fov = 2.0 * atan(1.0 / ubo.projection[1][1]); // TODO: Pass in instead
  float aspect = ubo.window_size.x / ubo.window_size.y;
  const float tanfovy = tan(fov * 0.5);
  const float tanfovx = tanfovy * aspect;
  float focal_y = ubo.window_size.y / (2.0 * tanfovy);
  float focal_x = ubo.window_size.x / (2.0 * tanfovx);

  const float limx = 1.3 * tanfovx;
  const float limy = 1.3 * tanfovy;
  vec4 t = pre_projection;
  float txtz = t.x / t.z;
  float tytz = t.y / t.z;
  t.x = min(limx, max(-limx, txtz)) * t.z;
  t.y = min(limy, max(-limy, tytz)) * t.z;
  mat3 J = mat3(
    focal_x / t.z, 0.0, -(focal_x * t.x) / (t.z * t.z),
    0.0, focal_y / t.z, -(focal_y * t.y) / (t.z * t.z),
    0.0, 0.0, 0.0
  );

  // Cov2d
  mat3 W = transpose(mat3(ubo.view));
  mat3 T = W * J;
  mat2 cov2d = mat2(transpose(T) * cov3d * T);
  // Low pass filter from the reference implementation
  cov2d[0][0] += 0.3;
  cov2d[1][1] += 0.3;
  
  // Inverse cov2d (symmetric)
  float det = determinant(cov2d);
  float inv_det = 1.0 / det;
  out_inv_cov_2d = mat2(
    cov2d[1][1] * inv_det, -cov2d[0][1] * inv_det, 
    -cov2d[0][1] * inv_det, cov2d[0][0] * inv_det);

  //float radius = ceil(3.0 * sqrt(max(cov2d[0][0], cov2d[1][1])));
  //gl_PointSize = 2.0 * radius;
  //out_size = vec2(gl_PointSize);
  out_size = 2.0 * vec2(3.0 * sqrt(cov2d[0][0]), 3.0 * sqrt(cov2d[1][1]));
  gl_PointSize = max(out_size.x, out_size.y);
#ifndef UVELON_GSPLAT_GEOMETRY
  out_size = vec2(gl_PointSize);
#endif

  // Color from spherical harmonics
  // Adapted from reference implementation (https://github.com/graphdeco-inria/diff-gaussian-rasterization/blob/59f5f77e3ddbac3ed9db93ec2cfe99ed6c5d121d/cuda_rasterizer/forward.cu)
  vec3 dir = normalize(position - ubo.camera_position.xyz);
  vec3 color = SH_C0 * sh(0);
  // Deg > 0
  color = color - SH_C1 * dir.y * sh(1) + SH_C1 * dir.z * sh(2) - SH_C1 * dir.x * sh(3);
  // Deg > 1
  float xx = dir.x * dir.x, yy = dir.y * dir.y, zz = dir.z * dir.z;
  float xy = dir.x * dir.y, yz = dir.y * dir.z, xz = dir.x * dir.z;
  color +=
    SH_C2_0 * xy * sh(4) + 
    SH_C2_1 * yz * sh(5) + 
    SH_C2_2 * (2.0 * zz - xx - yy) * sh(6) + 
    SH_C2_3 * xz * sh(7) + 
    SH_C2_4 * (xx - yy) * sh(8);
  // Deg > 2
  color +=
    SH_C3_0 * dir.y * (3.0 * xx - yy) * sh(9) + 
    SH_C3_1 * xy * dir.z * sh(10) +
    SH_C3_2 * dir.y * (4.0 * zz - xx - yy) * sh(11) + 
    SH_C3_3 * dir.z * (2.0 * zz - 3.0 * xx - 3.0 * yy) * sh(12) + 
    SH_C3_4 * dir.x * (4.0 * zz - xx - yy) * sh(13) + 
    SH_C3_5 * dir.z * (xx - yy) * sh(14) +
    SH_C3_6 * dir.x * (xx - 3.0 * yy) * sh(15);
  
  color += 0.5;
  color = max(color, vec3(0.0)); // Clamp
  out_color_opacity.rgb = color;
}