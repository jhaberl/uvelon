#version 430
#include "../global_ubo.glsl"

layout (points) in;
layout (triangle_strip, max_vertices=4) out;

layout (location = 0) flat in vec2 size[1];
layout (location = 1) flat in vec4 color_opacity[1];
layout (location = 2) flat in mat2 inv_cov_2d[1];

layout (location = 0) flat out vec2 out_size;
layout (location = 1) flat out vec4 out_color_opacity;
layout (location = 2) flat out mat2 out_inv_cov_2d;
layout (location = 4) out vec2 out_quad_coord;

void main() {
  vec2 half_size = size[0] / ubo.window_size;

  // Bottom left
  gl_Position = gl_in[0].gl_Position - vec4(half_size, vec2(0.0));
  out_size = size[0];
  out_color_opacity = color_opacity[0];
  out_inv_cov_2d = inv_cov_2d[0];
  out_quad_coord = vec2(1.0, 1.0);
  EmitVertex();

  // Top left
  gl_Position = gl_in[0].gl_Position + vec4(-half_size.x, half_size.y, vec2(0.0));
  out_size = size[0];
  out_color_opacity = color_opacity[0];
  out_inv_cov_2d = inv_cov_2d[0];
  out_quad_coord = vec2(1.0, 0.0);
  EmitVertex();

  // Bottom right
  gl_Position = gl_in[0].gl_Position + vec4(half_size.x, -half_size.y, vec2(0.0));
  out_size = size[0];
  out_color_opacity = color_opacity[0];
  out_inv_cov_2d = inv_cov_2d[0];
  out_quad_coord = vec2(0.0, 1.0);
  EmitVertex();

  // Top right
  gl_Position = gl_in[0].gl_Position + vec4(half_size, vec2(0.0));
  out_size = size[0];
  out_color_opacity = color_opacity[0];
  out_inv_cov_2d = inv_cov_2d[0];
  out_quad_coord = vec2(0.0, 0.0);
  EmitVertex();
}