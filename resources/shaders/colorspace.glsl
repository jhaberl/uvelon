// We do this conversion ourselves instead of relying on hardware,
// because ImGui doesn't properly support them (https://github.com/ocornut/imgui/issues/6583).
// Additionally, Gaussian Splatting data is in sRGB already.

// https://en.wikipedia.org/wiki/SRGB#Theory_of_the_transformation
// https://gamedev.stackexchange.com/a/148088
// https://stackoverflow.com/questions/61138110/what-is-the-correct-gamma-correction-function

vec4 linear2srgb(vec4 linear) {
  bvec3 is_low = lessThanEqual(linear.rgb, vec3(0.0031308));
  vec3 low = 12.92 * linear.rgb;
  vec3 high = 1.055 * pow(linear.rgb, vec3(1.0 / 2.4)) - 0.055;
  return clamp(vec4(mix(high, low, is_low), linear.a), 0.0, 1.0);
}

vec4 srgb2linear(vec4 srgb) {
  bvec3 is_low = lessThanEqual(srgb.rgb, vec3(0.04045));
  vec3 low = srgb.rgb / 12.92;
  vec3 high = pow((srgb.rgb + 0.055) / 1.055, vec3(2.4));
  return vec4(mix(high, low, is_low), srgb.a);
}

// Tone mapping: https://64.github.io/tonemapping/

float getLuminance(vec4 linear) {
  return dot(linear.rgb, vec3(0.2126, 0.7152, 0.0722));
}

vec4 setLuminance(vec4 linear, float luminance) {
  return vec4(linear.rgb * (luminance / getLuminance(linear)), linear.a);
}

vec4 tone_reinhard(vec4 linear) {
  float pre = getLuminance(linear);
  vec4 tone_mapped = vec4(linear.rgb / (1.0 + linear.rgb), linear.a);
  vec4 luminance_corrected = setLuminance(tone_mapped, pre);
  return luminance_corrected;
}

#define ACES_INPUT transpose(mat3(\
  0.59719, 0.35458, 0.04823,\
  0.07600, 0.90834, 0.01566,\
  0.02840, 0.13383, 0.83777\
))
#define ACES_OUTPUT transpose(mat3(\
   1.60475, -0.53108, -0.07367,\
  -0.10208,  1.10813, -0.00605,\
  -0.00327, -0.07276,  1.07602\
))

vec3 rrtOdtFit(vec3 v) {
  vec3 a = v * (v + 0.0245786) - 0.000090537;
  vec3 b = v * (0.983729 * v + 0.4329510) + 0.238081;
  return a / b;
}

vec4 tone_aces(vec4 linear) {
  vec3 color = ACES_INPUT * linear.rgb;
  color = rrtOdtFit(color);
  color = ACES_OUTPUT * color;
  // Some sort of exposure bias?
  // https://github.com/TheRealMJP/BakingLab/blob/master/BakingLab/ToneMapping.hlsl#L105
  color *= 1.8; 
  return vec4(color, linear.a);
}

vec3 uncharted2_partial(vec3 x) {
  return ((x * (0.15 * x + 0.1 * 0.5) + 0.2 * 0.02) / (x * (0.15 * x + 0.5) + 0.2 * 0.3)) - 0.02 / 0.3;
}

vec4 tone_uncharted2(vec4 linear) {
  float exposure_bias = 2.0;
  linear.rgb *= exposure_bias;

  vec3 curr = uncharted2_partial(linear.rgb);
  vec3 w = vec3(11.2);
  vec3 white_scale = vec3(1.0) / uncharted2_partial(w);
  return vec4(curr * white_scale, linear.a);
}

// TODO: Exposure
// https://github.com/TheRealMJP/BakingLab/blob/master/BakingLab/Exposure.hlsl