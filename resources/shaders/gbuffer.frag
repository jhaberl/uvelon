#version 450
#include "global_ubo.glsl"
#include "colorspace.glsl"

#define MIN_ROUGHNESS 0.01

// https://github.com/KhronosGroup/glTF/blob/main/extensions/2.0/Khronos/KHR_lights_punctual/README.md
// Supported: 
// Directional - Light in -z direction, intensity in lux
// Point - Emit from position, intensity in candela
// Spot - Cone in -z direction (based on position + rotation), intensity in candela
struct Light {
  mat4 model_matrix;
  vec3 color;
  uint type; // &0x7F = type (0 = none, 1 = dir, 2 = point, 3 = spot), no single byte data type in glsl :(
  float intensity;
  float spot_light_angle_scale; // See reference code in glTF readme
  float spot_light_angle_offset; // See reference code in glTF readme
  uint cast_shadows1, cast_shadows2;
  uint padding1, padding2, padding3;
};

layout (location = 0) in vec2 uv;

layout (set = 1, binding = 0) uniform sampler texture_sampler;
layout (set = 1, binding = 1) uniform texture2D tex_base_color;
layout (set = 1, binding = 2) uniform texture2D tex_metal_rough;
layout (set = 1, binding = 3) uniform texture2D tex_normal;
layout (set = 1, binding = 4) uniform texture2D tex_emissive;
layout (set = 1, binding = 5) uniform texture2D tex_depth;
layout (set = 1, binding = 6) uniform LightBuffer {
  Light data[UVELON_MAX_LIGHTS];
} lights;
layout (set = 1, binding = 7) uniform samplerShadow shadow_sampler;
// Dynamically uniform access to only one of the two arrays per index
layout (set = 1, binding = 8) uniform textureCube sm_cube[UVELON_MAX_LIGHTS];
layout (set = 1, binding = 8) uniform texture2D sm_2d[UVELON_MAX_LIGHTS];
layout (set = 1, binding = 9) uniform sampler ibl_sampler;
layout (set = 1, binding = 10) uniform textureCube tex_ibl_diffuse;
layout (set = 1, binding = 11) uniform textureCube tex_ibl_specular; // Pre-filtered convolution
layout (set = 1, binding = 12) uniform texture2D tex_ibl_brdf; // BRDF integration map
layout (set = 1, binding = 13) uniform GBufferData {
  mat4 last_frame_view_proj;
  float shadow_map_limit;
  float shadow_map_far;
} gb_data;
layout (set = 1, binding = 14) uniform texture2D tex_last_frame;
layout (set = 1, binding = 15) uniform texture2D tex_last_frame_blurred;
layout (set = 1, binding = 16) uniform sampler ssr_sampler;

layout (location = 0) out vec4 out_albedo;
layout (location = 1) out vec4 out_albedo_linear;

vec3 getWorldPos(vec2 suv, float depth) {
  vec4 clip = vec4(suv * 2.0 - 1.0, depth, 1.0);
  vec4 world = ubo.inv_view_projection * clip;
  return world.xyz / world.w;
}

vec3 fresnelSchlick(float cos_theta, vec3 r0) {
  float omct = clamp(1.0 - cos_theta, 0.0, 1.0);
  return r0 + (1.0 - r0) * (omct * omct * omct * omct * omct);
}

vec3 fresnelRoughness(float cos_theta, vec3 r0, float roughness) {
  float omct = clamp(1.0 - cos_theta, 0.0, 1.0);
  return r0 + (max(vec3(1.0 - roughness), r0) - r0) * (omct * omct * omct * omct * omct);
}

const vec3 pcf_samples[20] = vec3[] (
  vec3(1, 1,  1), vec3( 1, -1,  1), vec3(-1, -1,  1), vec3(-1, 1,  1), 
  vec3(1, 1, -1), vec3( 1, -1, -1), vec3(-1, -1, -1), vec3(-1, 1, -1),
  vec3(1, 1,  0), vec3( 1, -1,  0), vec3(-1, -1,  0), vec3(-1, 1,  0),
  vec3(1, 0,  1), vec3(-1,  0,  1), vec3( 1,  0, -1), vec3(-1, 0, -1),
  vec3(0, 1,  1), vec3( 0, -1,  1), vec3( 0, -1, -1), vec3( 0, 1, -1)
);

float isLit(uint light_index, vec3 direction, float dist) {
  Light light = lights.data[light_index];
  if ((light.cast_shadows1 | light.cast_shadows2) == 0) return 1.0;

  mat4 projection;
  mat4 view;
  if (light.type == 1) {
    float right = gb_data.shadow_map_limit, left = -gb_data.shadow_map_limit, 
      top = gb_data.shadow_map_limit, bottom = -gb_data.shadow_map_limit, 
      near = gb_data.shadow_map_far, far = 0.0;
    projection = transpose(mat4(
      2.0 / (right - left), 0.0, 0.0, -(right + left) / (right - left),
      0.0, 2.0 / (top - bottom), 0.0, -(top + bottom) / (top - bottom),
      0.0, 0.0, 1.0 / (far - near), -near / (far - near),
      0.0, 0.0, 0.0, 1.0
    ));
    view = inverse(mat4(mat3(light.model_matrix)));
  } else {
    dist -= 0.01 * pow(dist, 1.5); // TODO: Improve bias function
    dist /= gb_data.shadow_map_far;
    dist = 1.0 - dist;
  }

  // PCF
  float pcf_radius = 0.002 * dist; // TODO: Improve radius function
  float num_samples = 20;
  float sum = 0.0;
  for (int i = 0; i < num_samples; i++) {
    if (light.type == 1) {
      // Who cares about performance for directional lights anyway...
      view[3] = vec4(0.0, 0.0, gb_data.shadow_map_limit, 1.0);
      vec4 projected = projection * view * vec4(direction, 1.0);
      vec3 projected3 = (projected.xyz / projected.w) / vec3(2.0, 2.0, 1.0) 
        + vec3(0.5, 0.5, 0.0);
      pcf_radius = 0.0005 * projected3.z;
      projected3.xyz += pcf_samples[i] * pcf_radius;
      projected3.z += 0.01 * pow(projected3.z, 1.5); // TODO: Improve bias function
      sum += texture(sampler2DShadow(sm_2d[light_index], shadow_sampler), 
        projected3.xyz);
    } else {
      sum += texture(samplerCubeShadow(sm_cube[light_index], shadow_sampler), 
        vec4(direction + pcf_samples[i] * pcf_radius, dist));
    }
  }
  return sum / num_samples;
}

const uint ssr_step_pixels = 1;
const uint ssr_max_steps = 200;
const uint ssr_increase_every = 5;
const float ssr_increase_factor = 1.1;
const float ssr_edge_fade_start = 0.1;
const uint ssr_steps_fade_last = 20;

vec4 calculateSSR(vec3 v, float depth, vec3 world_pos, vec3 n, vec2 metal_rough) {
  vec4 result = vec4(0.0);
  if (metal_rough.x < 0.0001) return result;

  vec3 reflected = reflect(-v, n);
  result.a = max(-dot(v, reflected), 0.0);
  if (result.a < 0.01) return result;

  vec3 ray_start = vec3(uv, depth);
  
  vec4 temp_rayend = ubo.projection * ubo.view * vec4(world_pos + reflected, 1.0);
  temp_rayend.xyz /= temp_rayend.w;
  temp_rayend.w = 1.0;
  temp_rayend.xy = (temp_rayend.xy + 1.0) / 2.0;
  vec3 ray_step = normalize(temp_rayend.xyz - ray_start);

  // Step size based on pixel at depth
  vec2 size_temp = vec2(1.0) / ubo.window_size * ssr_step_pixels;
  float step_size = min(size_temp.x, size_temp.y);
  ray_step *= step_size;

  vec3 ray_current = ray_start;
  
  bool hit = false;
  for (uint i = 0; i < ssr_max_steps; i++) {
    float current_depth = texture(sampler2D(tex_depth, texture_sampler), ray_current.xy).r;
    if (ray_current.z < current_depth) {
      if (current_depth - ray_current.z > length(ray_step)) break;
      
      vec4 hit_world = vec4(getWorldPos(ray_current.xy, ray_current.z), 1.0);
      vec4 hit_last_frame = gb_data.last_frame_view_proj * hit_world;
      hit_last_frame.xyz /= hit_last_frame.w;
      hit_last_frame.w = 1.0;
      hit_last_frame.xy = (hit_last_frame.xy + 1.0) / 2.0;
      
      // Sample last frame
      vec4 color_smooth = texture(sampler2D(tex_last_frame, ssr_sampler), hit_last_frame.xy);
      vec4 color_rough = texture(sampler2D(tex_last_frame_blurred, ssr_sampler), hit_last_frame.xy);
      result.rgb = mix(color_smooth.rgb, color_rough.rgb, metal_rough.y);
      result.a *= color_smooth.a;
      
      // Fade out towards screen edges
      float distance_to_edge = 
        min(min(ray_current.x, 1.0 - ray_current.x), min(ray_current.y, 1.0 - ray_current.y));
      result.a *= min(1.0, distance_to_edge / ssr_edge_fade_start);

      // Fade out towards max steps
      float steps_left = float(min(ssr_max_steps - i, ssr_steps_fade_last));
      result.a *= steps_left / ssr_steps_fade_last;

      hit = true;
      break;
    } else if (any(lessThan(ray_current.xyz, vec3(0.0))) || any(greaterThan(ray_current.xy, vec2(1.0)))) {
      break;
    }

    if (i % ssr_increase_every == 0) ray_step *= ssr_increase_factor;

    ray_current += ray_step;
  }
  if (!hit) result.a = 0.0;
  result.a *= metal_rough.x;
  return result;
}

bool haveIBL() {
  return textureSize(sampler2D(tex_ibl_brdf, texture_sampler), 0).x > 1;
}

vec3 calculateIBL(vec3 v, vec3 n, vec3 base_color, vec3 r0, vec2 metal_rough) {
  metal_rough.y = max(MIN_ROUGHNESS, metal_rough.y);

  float cos_theta = max(dot(n, v), 0.0);
  vec3 fresnel_rough = fresnelRoughness(cos_theta, r0, metal_rough.y);
  
  vec3 sample_normal = vec3(n.x, -n.y, n.z);

  // Diffuse
  vec3 diffuse_sample = texture(samplerCube(tex_ibl_diffuse, ibl_sampler), sample_normal).rgb;
  vec3 diffuse = diffuse_sample * base_color;

  // Specular
  vec3 reflected_view = reflect(-v, n) * vec3(1.0, -1.0, 1.0);
  float max_lod = log2(textureSize(samplerCube(tex_ibl_specular, ibl_sampler), 0).x) + 1;
  vec3 specular_sample = textureLod(samplerCube(tex_ibl_specular, ibl_sampler), 
    reflected_view, metal_rough.y * max_lod).rgb;
  vec2 specular_brdf = texture(sampler2D(tex_ibl_brdf, ibl_sampler), 
    vec2(cos_theta, metal_rough.y)).xy;
  vec3 specular = (fresnel_rough * specular_brdf.x + specular_brdf.y) * specular_sample;

  return ((1.0 - fresnel_rough) * (1.0 - metal_rough.x)) * diffuse + specular;
}

void main() {
  vec4 base_color = texture(sampler2D(tex_base_color, texture_sampler), uv);
  vec2 metal_rough = texture(sampler2D(tex_metal_rough, texture_sampler), uv).bg;
  vec4 normal = texture(sampler2D(tex_normal, texture_sampler), uv);
  vec4 emissive = texture(sampler2D(tex_emissive, texture_sampler), uv);
  float depth = texture(sampler2D(tex_depth, texture_sampler), uv).r;

  vec3 world_pos = getWorldPos(uv, depth);
  float surface_present = 1.0 - step(0.0, -depth);

  vec3 v = normalize(ubo.camera_position.xyz / ubo.camera_position.w - world_pos);
  vec3 n = normalize(normal.xyz);

  // r0 for fresnel taken from https://learnopengl.com/PBR/Theory
  vec3 r0 = vec3(0.04);
  r0 = mix(r0, base_color.rgb, metal_rough.x);

  // Cook-Torrance
  vec3 lo = vec3(0.0);
  for (int i = 0; i < UVELON_MAX_LIGHTS; i++) {
    Light light = lights.data[i];
    if (light.type == 0) break; // No more active lights

    vec3 l = vec3(0.0);
    float attenuation = 1.0;
    float shadow_factor = 1.0;
    if (light.type == 1) {
      // Directional
      vec4 temp = mat4(mat3(light.model_matrix)) * vec4(0.0, 0.0, -1.0, 1.0);
      l = temp.xyz / temp.w;
      shadow_factor = isLit(i, world_pos, 0.0);
    } else {
      // Point/spot
      vec3 light_pos = light.model_matrix[3].xyz;
      l = light_pos - world_pos;
      attenuation *= 1.0 / dot(l, l); // Inverse square falloff
      shadow_factor = isLit(i, normalize(l), length(l));
    }
    l = normalize(l);
    if (light.type == 3) {
      // Spot: Angular attenuation
      vec4 temp = mat4(mat3(light.model_matrix)) * vec4(0.0, 0.0, -1.0, 1.0);
      vec3 spotlight_dir = temp.xyz / temp.w;
      // From Khronos reference code
      float cd = dot(spotlight_dir, l);
      float angular_attenuation = 
        clamp(cd * light.spot_light_angle_scale + light.spot_light_angle_offset, 0.0, 1.0);
      angular_attenuation *= angular_attenuation;
      attenuation *= angular_attenuation;
    }

    vec3 h = normalize(l + v);
    float cos_theta = max(dot(n, l), 0.0);

    // Microfacet distribution (roughness) (Beckmann distribution)
    float rough_clamped = max(MIN_ROUGHNESS, metal_rough.y);
    float m_sq = rough_clamped * rough_clamped;
    float n_h = max(dot(n, h), 0.0);
    float n_h_sq = n_h * n_h;
    float n_h_4 = n_h_sq * n_h_sq;
    float microfacet = (1 / (4 * m_sq * n_h_4 + 0.0001)) * exp((n_h_sq - 1) / (m_sq * n_h_sq + 0.0001));
    
    // Fresnel reflectance (Schlick approximation)
    vec3 fresnel = fresnelSchlick(cos_theta, r0);

    // Geometric attenuation (self-shadowing) (Torrance-Sparrow)
    float cos_theta_h = max(dot(h, n), 0.0);
    float cos_theta_o = max(dot(v, n), 0.0);
    float cos_alpha_h = max(dot(v, h), 0.0);
    float geometric = min(1.0, min(
      (2.0 * cos_theta_h * cos_theta_o) / cos_alpha_h, 
      (2.0 * cos_theta_h * cos_theta) / cos_alpha_h));

    vec3 brdf = (microfacet * fresnel * geometric) / (4 * max(dot(n, v), 0.0) * cos_theta + 0.0001);
    vec3 radiance = (light.color * light.intensity) * attenuation * shadow_factor;
    vec3 kd = ((vec3(1.0) - fresnel) * (1.0 - metal_rough.x)); // From learnopengl
    lo += (kd * base_color.rgb / 3.14159265 + brdf) * radiance * cos_theta;
  }

  lo += emissive.xyz;

  vec4 ssr = calculateSSR(v, depth, world_pos, n, metal_rough);
  vec3 non_ssr_indirect = vec3(0.0);
  if (haveIBL())
    non_ssr_indirect = calculateIBL(v, n, base_color.rgb, r0, metal_rough); // Ambient
  else
    non_ssr_indirect = base_color.rgb * 0.03;

  // Image for next SSR pass does not include this SSR pass
  // This avoids the constant warmup artifacts
  out_albedo_linear = clamp(vec4(lo + non_ssr_indirect, base_color.a), 0.0, 1.0);

  lo += mix(non_ssr_indirect, ssr.rgb, ssr.a);

  vec4 linear = vec4(lo, base_color.a);
  // TODO: Exposure
  vec4 tone_mapped = tone_uncharted2(linear);
  vec4 gamma_corrected = linear2srgb(tone_mapped);
  out_albedo = gamma_corrected;
}