#version 430
#extension GL_KHR_shader_subgroup_basic : require
#extension GL_KHR_shader_subgroup_arithmetic : require

layout (set = 0, binding = 0, rgba8) uniform readonly image2D in_image;
layout (set = 0, binding = 1, rgba8) uniform writeonly image2D out_image;

int downscale_factor = 10;
int kernel_size = 20;
int spacing = 5;

layout(local_size_x = 16, local_size_y = 16, local_size_z = 1) in;
void main() {
  ivec2 out_coordinate = ivec2(gl_GlobalInvocationID.xy);
  ivec2 in_center = ivec2(gl_GlobalInvocationID.xy * downscale_factor);
  ivec2 in_image_size = imageSize(in_image);

  if (any(greaterThan(out_coordinate, imageSize(out_image))))
    return;

  vec4 avg = vec4(0.0);
  int count = 0;
  for (int y_offset = -kernel_size; y_offset < kernel_size + 1; y_offset++) {
    for (int x_offset = -kernel_size; x_offset < kernel_size + 1; x_offset++) {
      if ((x_offset + y_offset % spacing) % spacing == 0) continue; // Checkerboard pattern

      ivec2 offset = ivec2(x_offset, y_offset);
      ivec2 in_coordinate = in_center + offset;
      if (all(greaterThan(in_coordinate, ivec2(0))) && 
          all(lessThan(in_coordinate, in_image_size))) {
        avg += imageLoad(in_image, in_coordinate);
        count++;
      }
    }
  }
  avg /= count;
  imageStore(out_image, out_coordinate, avg);
}