#version 450

layout (location = 0) in mat3 tbn;
#ifndef UVELON_NO_UV
layout (location = 3) in vec2 uv;
#endif

layout (set = 1, binding = 0) uniform sampler2D in_base_color;
layout (set = 1, binding = 1) uniform sampler2D in_metal_rough;
layout (set = 1, binding = 2) uniform sampler2D in_normal;
layout (set = 1, binding = 3) uniform sampler2D in_emissive;
layout (set = 1, binding = 4) uniform Factors {
  vec4 base_color;
  vec3 emissive;
  float metallic;
  float roughness;
} factors;

layout (location = 0) out vec4 out_albedo;
layout (location = 1) out vec4 out_metal_rough;
layout (location = 2) out vec4 out_normal;
layout (location = 3) out vec4 out_emissive;

void main() {
  out_albedo = factors.base_color;
#ifndef UVELON_NO_UV
  out_albedo *= texture(in_base_color, uv);
#endif
  
  // Translucent objects are unsupported but we just discard pixels 
  // that are sufficiently translucent to count as transparent
  if (step(0.5, out_albedo.a) == 0.0) discard;
  
  out_metal_rough = vec4(0.0, factors.roughness, factors.metallic, 1.0);
  out_emissive = vec4(factors.emissive, 1.0);
  out_normal = vec4(tbn[2], 1.0);

#ifndef UVELON_NO_UV
  out_metal_rough *= texture(in_metal_rough, uv);
  out_emissive *= texture(in_emissive, uv);

  vec3 normal_sample = texture(in_normal, uv).rgb * 2.0 - 1.0;
  out_normal = vec4(normalize(tbn * normal_sample), 1.0);
#endif
}