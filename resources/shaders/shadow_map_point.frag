#version 450

layout (location = 0) in vec4 in_global_pos;

layout (set = 0, binding = 0) uniform UniformBuffer {
  mat4 light_projection[UVELON_MAX_LIGHTS][6];
  float far;
} ubo;

layout (push_constant) uniform PushConstant {
  mat4 model;
  uint light_index;
} pc;

void main() {
  vec4 temp = ubo.light_projection[pc.light_index][5][3];
  vec3 light_pos = -vec3(temp.x, temp.y, temp.w);
  gl_FragDepth = 1.0 - length(in_global_pos.xyz - light_pos) / ubo.far;
}