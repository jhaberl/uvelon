layout (set = 0, binding = 0) uniform GlobalUniformBuffer {
  mat4 view;
  mat4 projection;
  mat4 inv_view_projection;
  vec4 camera_position;
  vec2 window_size;
} ubo;