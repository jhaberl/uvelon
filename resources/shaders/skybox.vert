#version 450
#include "global_ubo.glsl"

layout (location = 0) out vec3 out_dir;

// https://learnopengl.com/code_viewer.php?code=advanced/cubemaps_skybox_data
const vec3 lut_position[36] = vec3[](
  vec3(-1.0f,  1.0f, -1.0f),
  vec3(-1.0f, -1.0f, -1.0f),
  vec3(1.0f, -1.0f, -1.0f),
  vec3(1.0f, -1.0f, -1.0f),
  vec3(1.0f,  1.0f, -1.0f),
  vec3(-1.0f,  1.0f, -1.0f),
  vec3(-1.0f, -1.0f,  1.0f),
  vec3(-1.0f, -1.0f, -1.0f),
  vec3(-1.0f,  1.0f, -1.0f),
  vec3(-1.0f,  1.0f, -1.0f),
  vec3(-1.0f,  1.0f,  1.0f),
  vec3(-1.0f, -1.0f,  1.0f),
  vec3(1.0f, -1.0f, -1.0f),
  vec3(1.0f, -1.0f,  1.0f),
  vec3(1.0f,  1.0f,  1.0f),
  vec3(1.0f,  1.0f,  1.0f),
  vec3(1.0f,  1.0f, -1.0f),
  vec3(1.0f, -1.0f, -1.0f),
  vec3(-1.0f, -1.0f,  1.0f),
  vec3(-1.0f,  1.0f,  1.0f),
  vec3(1.0f,  1.0f,  1.0f),
  vec3(1.0f,  1.0f,  1.0f),
  vec3(1.0f, -1.0f,  1.0f),
  vec3(-1.0f, -1.0f,  1.0f),
  vec3(-1.0f,  1.0f, -1.0f),
  vec3(1.0f,  1.0f, -1.0f),
  vec3(1.0f,  1.0f,  1.0f),
  vec3(1.0f,  1.0f,  1.0f),
  vec3(-1.0f,  1.0f,  1.0f),
  vec3(-1.0f,  1.0f, -1.0f),
  vec3(-1.0f, -1.0f, -1.0f),
  vec3(-1.0f, -1.0f,  1.0f),
  vec3(1.0f, -1.0f, -1.0f),
  vec3(1.0f, -1.0f, -1.0f),
  vec3(-1.0f, -1.0f,  1.0f),
  vec3(1.0f, -1.0f,  1.0)
);

void main() {
  vec3 position = lut_position[gl_VertexIndex];
  gl_Position = ubo.projection * mat4(mat3(ubo.view)) * vec4(position, 1.0);
  out_dir = vec3(position.x, -position.y, position.z);
}