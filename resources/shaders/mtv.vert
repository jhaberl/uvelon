#version 450
#include "global_ubo.glsl"

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec4 tangent;
#ifndef UVELON_NO_UV
layout (location = 3) in vec2 uv;
#endif

layout (push_constant) uniform PushConstant {
  mat4 model;
} pc;

layout (location = 0) out mat3 outTBN;
#ifndef UVELON_NO_UV
layout (location = 3) out vec2 outUV;
#endif

void main() {
  gl_Position = ubo.projection * ubo.view * pc.model * vec4(position, 1.0);
  
  vec4 t = pc.model * vec4(tangent.xyz, 1.0);
  vec4 b = pc.model * vec4(tangent.w * cross(normal, tangent.xyz), 1.0);
  vec4 n = pc.model * vec4(normal, 1.0);
  outTBN = mat3(t.xyz / t.w, b.xyz / b.w, n.xyz / n.w);

#ifndef UVELON_NO_UV
  outUV = uv;
#endif
}