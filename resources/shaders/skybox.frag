#version 450
#include "global_ubo.glsl"
#include "colorspace.glsl"

layout(location = 0) in vec3 dir;

layout (location = 0) out vec4 out_albedo;

layout (set = 1, binding = 0) uniform samplerCube skybox;

void main() {
  out_albedo = linear2srgb(tone_uncharted2(texture(skybox, dir)));
}