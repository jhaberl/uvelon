#version 450
#extension GL_EXT_multiview : require

layout (location = 0) in vec3 position;

layout (set = 0, binding = 0) uniform UniformBuffer {
  mat4 light_projection[UVELON_MAX_LIGHTS][6];
  float far;
} ubo;

layout (push_constant) uniform PushConstant {
  mat4 model;
  uint light_index;
} pc;

layout (location = 0) out vec4 out_global_pos;

void main() {
  out_global_pos = pc.model * vec4(position, 1.0);
  gl_Position = ubo.light_projection[pc.light_index][gl_ViewIndex] * out_global_pos;
}
