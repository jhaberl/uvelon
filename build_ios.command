export VULKAN_SDK=~/VulkanSDK/1.3.296.0/macOS
BUILD_TYPE=RelWithDebInfo
CONFIGURE_ONLY=true

echo "Using Vulkan SDK at ${VULKAN_SDK}"
echo "Build type: ${BUILD_TYPE}"
echo "Configure only: ${CONFIGURE_ONLY}"

mkdir -p $(dirname "$0")/build_ios
cmake -B $(dirname "$0")/build_ios -DCMAKE_TOOLCHAIN_FILE=$(dirname "$0")/third_party/ios-cmake/ios.toolchain.cmake -DPLATFORM=OS64 -DCMAKE_BUILD_TYPE:STRING=${BUILD_TYPE} -G Xcode -S$(dirname "$0")
if [ "${CONFIGURE_ONLY}" = false ] ; then
  cmake --build $(dirname "$0")/build_ios --config ${BUILD_TYPE} --target UvelonPlayground -j 4
fi